/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class QWHMAS {

    private String QWHCOD;
    private String QWHNAME;
    private String Select;

    public QWHMAS() {
    }

    public QWHMAS(String QWHCOD, String QWHNAME, String Select) {
        this.QWHCOD = QWHCOD;
        this.QWHNAME = QWHNAME;
        this.Select = Select;
    }

    public String getQWHCOD() {
        return QWHCOD;
    }

    public void setQWHCOD(String QWHCOD) {
        this.QWHCOD = QWHCOD;
    }

    public String getQWHNAME() {
        return QWHNAME;
    }

    public void setQWHNAME(String QWHNAME) {
        this.QWHNAME = QWHNAME;
    }

    public String getSelect() {
        return Select;
    }

    public void setSelect(String Select) {
        this.Select = Select;
    }

}
