/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class RCMBPT {

    private String RCTWHSE;
    private String RCTSPNO;
    private String RCTCUNO;
    private String RCTDLDATE;
    private String RCTSCSEQ;
    private String RCTPALNO;
    private String RCTCTID;
    private String RCTCODE;
    private String RCTPACK;
    private String RCTCDT;
    private String RCTCTM;
    private String RCTUSER;

    public RCMBPT() {
    }

    public RCMBPT(String RCTWHSE, String RCTSPNO, String RCTCUNO, String RCTDLDATE, String RCTSCSEQ, String RCTPALNO, String RCTCTID, String RCTCODE, String RCTPACK, String RCTCDT, String RCTCTM, String RCTUSER) {
        this.RCTWHSE = RCTWHSE;
        this.RCTSPNO = RCTSPNO;
        this.RCTCUNO = RCTCUNO;
        this.RCTDLDATE = RCTDLDATE;
        this.RCTSCSEQ = RCTSCSEQ;
        this.RCTPALNO = RCTPALNO;
        this.RCTCTID = RCTCTID;
        this.RCTCODE = RCTCODE;
        this.RCTPACK = RCTPACK;
        this.RCTCDT = RCTCDT;
        this.RCTCTM = RCTCTM;
        this.RCTUSER = RCTUSER;
    }

    public String getRCTWHSE() {
        return RCTWHSE;
    }

    public void setRCTWHSE(String RCTWHSE) {
        this.RCTWHSE = RCTWHSE;
    }

    public String getRCTSPNO() {
        return RCTSPNO;
    }

    public void setRCTSPNO(String RCTSPNO) {
        this.RCTSPNO = RCTSPNO;
    }

    public String getRCTCUNO() {
        return RCTCUNO;
    }

    public void setRCTCUNO(String RCTCUNO) {
        this.RCTCUNO = RCTCUNO;
    }

    public String getRCTDLDATE() {
        return RCTDLDATE;
    }

    public void setRCTDLDATE(String RCTDLDATE) {
        this.RCTDLDATE = RCTDLDATE;
    }

    public String getRCTSCSEQ() {
        return RCTSCSEQ;
    }

    public void setRCTSCSEQ(String RCTSCSEQ) {
        this.RCTSCSEQ = RCTSCSEQ;
    }

    public String getRCTPALNO() {
        return RCTPALNO;
    }

    public void setRCTPALNO(String RCTPALNO) {
        this.RCTPALNO = RCTPALNO;
    }

    public String getRCTCTID() {
        return RCTCTID;
    }

    public void setRCTCTID(String RCTCTID) {
        this.RCTCTID = RCTCTID;
    }

    public String getRCTCODE() {
        return RCTCODE;
    }

    public void setRCTCODE(String RCTCODE) {
        this.RCTCODE = RCTCODE;
    }

    public String getRCTPACK() {
        return RCTPACK;
    }

    public void setRCTPACK(String RCTPACK) {
        this.RCTPACK = RCTPACK;
    }

    public String getRCTCDT() {
        return RCTCDT;
    }

    public void setRCTCDT(String RCTCDT) {
        this.RCTCDT = RCTCDT;
    }

    public String getRCTCTM() {
        return RCTCTM;
    }

    public void setRCTCTM(String RCTCTM) {
        this.RCTCTM = RCTCTM;
    }

    public String getRCTUSER() {
        return RCTUSER;
    }

    public void setRCTUSER(String RCTUSER) {
        this.RCTUSER = RCTUSER;
    }

}
