/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class RCMPOD {

    private String RCDCONO;
    private String RCDPONO;
    private String RCDLINE;
    private String RCDPLANT;
    private String RCDMATCODE;
    private String RCDSTEXT;
    private String RCDUNIT;
    private String RCDPOQTY;
    private String RCDPOPRC;
    private String RCDPPN;
    private String RCDPOAMT;
    private String RCDVT;
    private String RCDST;
    private String RCDDIND;
    private String RCDCONVB;
    private String RCDCONVA;
    private String RCDITCAT;
    private String RCDDLDATE;
    private String RCDLOC;
    private String RCDLNAME;
    private String RCDACCAT;
    private String RCDDLOVER;
    private String RCDDLUNDER;
    private String RCDBASE;
    private String RCDCONVN2;
    private String RCDCONVD2;
    private String RCDMATGRP;
    private String RCDORDNO;
    private String RCDCDT;
    private String RCDCTM;
    private String RCDUSER;

    public RCMPOD() {
    }

    public RCMPOD(String RCDCONO, String RCDPONO, String RCDLINE, String RCDPLANT, String RCDMATCODE, String RCDSTEXT, String RCDUNIT, String RCDPOQTY, String RCDPOPRC, String RCDPPN, String RCDPOAMT, String RCDVT, String RCDST, String RCDDIND, String RCDCONVB, String RCDCONVA, String RCDITCAT, String RCDDLDATE, String RCDLOC, String RCDLNAME, String RCDACCAT, String RCDDLOVER, String RCDDLUNDER, String RCDBASE, String RCDCONVN2, String RCDCONVD2, String RCDMATGRP, String RCDORDNO, String RCDCDT, String RCDCTM, String RCDUSER) {
        this.RCDCONO = RCDCONO;
        this.RCDPONO = RCDPONO;
        this.RCDLINE = RCDLINE;
        this.RCDPLANT = RCDPLANT;
        this.RCDMATCODE = RCDMATCODE;
        this.RCDSTEXT = RCDSTEXT;
        this.RCDUNIT = RCDUNIT;
        this.RCDPOQTY = RCDPOQTY;
        this.RCDPOPRC = RCDPOPRC;
        this.RCDPPN = RCDPPN;
        this.RCDPOAMT = RCDPOAMT;
        this.RCDVT = RCDVT;
        this.RCDST = RCDST;
        this.RCDDIND = RCDDIND;
        this.RCDCONVB = RCDCONVB;
        this.RCDCONVA = RCDCONVA;
        this.RCDITCAT = RCDITCAT;
        this.RCDDLDATE = RCDDLDATE;
        this.RCDLOC = RCDLOC;
        this.RCDLNAME = RCDLNAME;
        this.RCDACCAT = RCDACCAT;
        this.RCDDLOVER = RCDDLOVER;
        this.RCDDLUNDER = RCDDLUNDER;
        this.RCDBASE = RCDBASE;
        this.RCDCONVN2 = RCDCONVN2;
        this.RCDCONVD2 = RCDCONVD2;
        this.RCDMATGRP = RCDMATGRP;
        this.RCDORDNO = RCDORDNO;
        this.RCDCDT = RCDCDT;
        this.RCDCTM = RCDCTM;
        this.RCDUSER = RCDUSER;
    }

    public String getRCDCONO() {
        return RCDCONO;
    }

    public void setRCDCONO(String RCDCONO) {
        this.RCDCONO = RCDCONO;
    }

    public String getRCDPONO() {
        return RCDPONO;
    }

    public void setRCDPONO(String RCDPONO) {
        this.RCDPONO = RCDPONO;
    }

    public String getRCDLINE() {
        return RCDLINE;
    }

    public void setRCDLINE(String RCDLINE) {
        this.RCDLINE = RCDLINE;
    }

    public String getRCDPLANT() {
        return RCDPLANT;
    }

    public void setRCDPLANT(String RCDPLANT) {
        this.RCDPLANT = RCDPLANT;
    }

    public String getRCDMATCODE() {
        return RCDMATCODE;
    }

    public void setRCDMATCODE(String RCDMATCODE) {
        this.RCDMATCODE = RCDMATCODE;
    }

    public String getRCDSTEXT() {
        return RCDSTEXT;
    }

    public void setRCDSTEXT(String RCDSTEXT) {
        this.RCDSTEXT = RCDSTEXT;
    }

    public String getRCDUNIT() {
        return RCDUNIT;
    }

    public void setRCDUNIT(String RCDUNIT) {
        this.RCDUNIT = RCDUNIT;
    }

    public String getRCDPOQTY() {
        return RCDPOQTY;
    }

    public void setRCDPOQTY(String RCDPOQTY) {
        this.RCDPOQTY = RCDPOQTY;
    }

    public String getRCDPOPRC() {
        return RCDPOPRC;
    }

    public void setRCDPOPRC(String RCDPOPRC) {
        this.RCDPOPRC = RCDPOPRC;
    }

    public String getRCDPPN() {
        return RCDPPN;
    }

    public void setRCDPPN(String RCDPPN) {
        this.RCDPPN = RCDPPN;
    }

    public String getRCDPOAMT() {
        return RCDPOAMT;
    }

    public void setRCDPOAMT(String RCDPOAMT) {
        this.RCDPOAMT = RCDPOAMT;
    }

    public String getRCDVT() {
        return RCDVT;
    }

    public void setRCDVT(String RCDVT) {
        this.RCDVT = RCDVT;
    }

    public String getRCDST() {
        return RCDST;
    }

    public void setRCDST(String RCDST) {
        this.RCDST = RCDST;
    }

    public String getRCDDIND() {
        return RCDDIND;
    }

    public void setRCDDIND(String RCDDIND) {
        this.RCDDIND = RCDDIND;
    }

    public String getRCDCONVB() {
        return RCDCONVB;
    }

    public void setRCDCONVB(String RCDCONVB) {
        this.RCDCONVB = RCDCONVB;
    }

    public String getRCDCONVA() {
        return RCDCONVA;
    }

    public void setRCDCONVA(String RCDCONVA) {
        this.RCDCONVA = RCDCONVA;
    }

    public String getRCDITCAT() {
        return RCDITCAT;
    }

    public void setRCDITCAT(String RCDITCAT) {
        this.RCDITCAT = RCDITCAT;
    }

    public String getRCDDLDATE() {
        return RCDDLDATE;
    }

    public void setRCDDLDATE(String RCDDLDATE) {
        this.RCDDLDATE = RCDDLDATE;
    }

    public String getRCDLOC() {
        return RCDLOC;
    }

    public void setRCDLOC(String RCDLOC) {
        this.RCDLOC = RCDLOC;
    }

    public String getRCDLNAME() {
        return RCDLNAME;
    }

    public void setRCDLNAME(String RCDLNAME) {
        this.RCDLNAME = RCDLNAME;
    }

    public String getRCDACCAT() {
        return RCDACCAT;
    }

    public void setRCDACCAT(String RCDACCAT) {
        this.RCDACCAT = RCDACCAT;
    }

    public String getRCDDLOVER() {
        return RCDDLOVER;
    }

    public void setRCDDLOVER(String RCDDLOVER) {
        this.RCDDLOVER = RCDDLOVER;
    }

    public String getRCDDLUNDER() {
        return RCDDLUNDER;
    }

    public void setRCDDLUNDER(String RCDDLUNDER) {
        this.RCDDLUNDER = RCDDLUNDER;
    }

    public String getRCDBASE() {
        return RCDBASE;
    }

    public void setRCDBASE(String RCDBASE) {
        this.RCDBASE = RCDBASE;
    }

    public String getRCDCONVN2() {
        return RCDCONVN2;
    }

    public void setRCDCONVN2(String RCDCONVN2) {
        this.RCDCONVN2 = RCDCONVN2;
    }

    public String getRCDCONVD2() {
        return RCDCONVD2;
    }

    public void setRCDCONVD2(String RCDCONVD2) {
        this.RCDCONVD2 = RCDCONVD2;
    }

    public String getRCDMATGRP() {
        return RCDMATGRP;
    }

    public void setRCDMATGRP(String RCDMATGRP) {
        this.RCDMATGRP = RCDMATGRP;
    }

    public String getRCDORDNO() {
        return RCDORDNO;
    }

    public void setRCDORDNO(String RCDORDNO) {
        this.RCDORDNO = RCDORDNO;
    }

    public String getRCDCDT() {
        return RCDCDT;
    }

    public void setRCDCDT(String RCDCDT) {
        this.RCDCDT = RCDCDT;
    }

    public String getRCDCTM() {
        return RCDCTM;
    }

    public void setRCDCTM(String RCDCTM) {
        this.RCDCTM = RCDCTM;
    }

    public String getRCDUSER() {
        return RCDUSER;
    }

    public void setRCDUSER(String RCDUSER) {
        this.RCDUSER = RCDUSER;
    }

}
