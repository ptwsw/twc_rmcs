/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class RCMBPH {

    private String RCHCONO;
    private String RCHWHSE;
    private String RCHSPNO;
    private String RCHCUNO;
    private String RCHDLDATE;
    private String RCHJBNO;
    private String RCHSNDATE;
    private String RCHSNTIME;
    private String RCHTOTINV;
    private String RCHTOTQTY;
    private String RCHINVAMT;
    private String RCHSTS;
    private String RCHEDT;
    private String RCHETM;
    private String RCHCDT;
    private String RCHCTM;
    private String RCHUSER;

    private String NUMB;
    private String RCHPLANT;
    private String CNTPLANT;
    private String TOTPLANT;
    private String SUMINV;
    private String TOTINV;
    private String RCHTOTPO;
    private String SUMPO;
    private String TOTPO;
    private String SUMQTY;
    private String TOTQTY;
    private String RCHTOTAMT;
    private String SUMAMT;
    private String TOTAMT;
    private String RCHITEM;
    private String SUMITEM;
    private String TOTITEM;

    public RCMBPH() {
    }

    public RCMBPH(String RCHCONO, String RCHWHSE, String RCHSPNO, String RCHCUNO, String RCHDLDATE, String RCHJBNO, String RCHSNDATE, String RCHSNTIME, String RCHTOTINV, String RCHTOTQTY, String RCHINVAMT, String RCHSTS, String RCHEDT, String RCHETM, String RCHCDT, String RCHCTM, String RCHUSER, String NUMB, String RCHPLANT, String CNTPLANT, String TOTPLANT, String SUMINV, String TOTINV, String RCHTOTPO, String SUMPO, String TOTPO, String SUMQTY, String TOTQTY, String RCHTOTAMT, String SUMAMT, String TOTAMT, String RCHITEM, String SUMITEM, String TOTITEM) {
        this.RCHCONO = RCHCONO;
        this.RCHWHSE = RCHWHSE;
        this.RCHSPNO = RCHSPNO;
        this.RCHCUNO = RCHCUNO;
        this.RCHDLDATE = RCHDLDATE;
        this.RCHJBNO = RCHJBNO;
        this.RCHSNDATE = RCHSNDATE;
        this.RCHSNTIME = RCHSNTIME;
        this.RCHTOTINV = RCHTOTINV;
        this.RCHTOTQTY = RCHTOTQTY;
        this.RCHINVAMT = RCHINVAMT;
        this.RCHSTS = RCHSTS;
        this.RCHEDT = RCHEDT;
        this.RCHETM = RCHETM;
        this.RCHCDT = RCHCDT;
        this.RCHCTM = RCHCTM;
        this.RCHUSER = RCHUSER;
        this.NUMB = NUMB;
        this.RCHPLANT = RCHPLANT;
        this.CNTPLANT = CNTPLANT;
        this.TOTPLANT = TOTPLANT;
        this.SUMINV = SUMINV;
        this.TOTINV = TOTINV;
        this.RCHTOTPO = RCHTOTPO;
        this.SUMPO = SUMPO;
        this.TOTPO = TOTPO;
        this.SUMQTY = SUMQTY;
        this.TOTQTY = TOTQTY;
        this.RCHTOTAMT = RCHTOTAMT;
        this.SUMAMT = SUMAMT;
        this.TOTAMT = TOTAMT;
        this.RCHITEM = RCHITEM;
        this.SUMITEM = SUMITEM;
        this.TOTITEM = TOTITEM;
    }

    public String getRCHCONO() {
        return RCHCONO;
    }

    public void setRCHCONO(String RCHCONO) {
        this.RCHCONO = RCHCONO;
    }

    public String getRCHWHSE() {
        return RCHWHSE;
    }

    public void setRCHWHSE(String RCHWHSE) {
        this.RCHWHSE = RCHWHSE;
    }

    public String getRCHSPNO() {
        return RCHSPNO;
    }

    public void setRCHSPNO(String RCHSPNO) {
        this.RCHSPNO = RCHSPNO;
    }

    public String getRCHCUNO() {
        return RCHCUNO;
    }

    public void setRCHCUNO(String RCHCUNO) {
        this.RCHCUNO = RCHCUNO;
    }

    public String getRCHDLDATE() {
        return RCHDLDATE;
    }

    public void setRCHDLDATE(String RCHDLDATE) {
        this.RCHDLDATE = RCHDLDATE;
    }

    public String getRCHJBNO() {
        return RCHJBNO;
    }

    public void setRCHJBNO(String RCHJBNO) {
        this.RCHJBNO = RCHJBNO;
    }

    public String getRCHSNDATE() {
        return RCHSNDATE;
    }

    public void setRCHSNDATE(String RCHSNDATE) {
        this.RCHSNDATE = RCHSNDATE;
    }

    public String getRCHSNTIME() {
        return RCHSNTIME;
    }

    public void setRCHSNTIME(String RCHSNTIME) {
        this.RCHSNTIME = RCHSNTIME;
    }

    public String getRCHTOTINV() {
        return RCHTOTINV;
    }

    public void setRCHTOTINV(String RCHTOTINV) {
        this.RCHTOTINV = RCHTOTINV;
    }

    public String getRCHTOTQTY() {
        return RCHTOTQTY;
    }

    public void setRCHTOTQTY(String RCHTOTQTY) {
        this.RCHTOTQTY = RCHTOTQTY;
    }

    public String getRCHINVAMT() {
        return RCHINVAMT;
    }

    public void setRCHINVAMT(String RCHINVAMT) {
        this.RCHINVAMT = RCHINVAMT;
    }

    public String getRCHSTS() {
        return RCHSTS;
    }

    public void setRCHSTS(String RCHSTS) {
        this.RCHSTS = RCHSTS;
    }

    public String getRCHEDT() {
        return RCHEDT;
    }

    public void setRCHEDT(String RCHEDT) {
        this.RCHEDT = RCHEDT;
    }

    public String getRCHETM() {
        return RCHETM;
    }

    public void setRCHETM(String RCHETM) {
        this.RCHETM = RCHETM;
    }

    public String getRCHCDT() {
        return RCHCDT;
    }

    public void setRCHCDT(String RCHCDT) {
        this.RCHCDT = RCHCDT;
    }

    public String getRCHCTM() {
        return RCHCTM;
    }

    public void setRCHCTM(String RCHCTM) {
        this.RCHCTM = RCHCTM;
    }

    public String getRCHUSER() {
        return RCHUSER;
    }

    public void setRCHUSER(String RCHUSER) {
        this.RCHUSER = RCHUSER;
    }

    public String getNUMB() {
        return NUMB;
    }

    public void setNUMB(String NUMB) {
        this.NUMB = NUMB;
    }

    public String getRCHPLANT() {
        return RCHPLANT;
    }

    public void setRCHPLANT(String RCHPLANT) {
        this.RCHPLANT = RCHPLANT;
    }

    public String getCNTPLANT() {
        return CNTPLANT;
    }

    public void setCNTPLANT(String CNTPLANT) {
        this.CNTPLANT = CNTPLANT;
    }

    public String getTOTPLANT() {
        return TOTPLANT;
    }

    public void setTOTPLANT(String TOTPLANT) {
        this.TOTPLANT = TOTPLANT;
    }

    public String getSUMINV() {
        return SUMINV;
    }

    public void setSUMINV(String SUMINV) {
        this.SUMINV = SUMINV;
    }

    public String getTOTINV() {
        return TOTINV;
    }

    public void setTOTINV(String TOTINV) {
        this.TOTINV = TOTINV;
    }

    public String getRCHTOTPO() {
        return RCHTOTPO;
    }

    public void setRCHTOTPO(String RCHTOTPO) {
        this.RCHTOTPO = RCHTOTPO;
    }

    public String getSUMPO() {
        return SUMPO;
    }

    public void setSUMPO(String SUMPO) {
        this.SUMPO = SUMPO;
    }

    public String getTOTPO() {
        return TOTPO;
    }

    public void setTOTPO(String TOTPO) {
        this.TOTPO = TOTPO;
    }

    public String getSUMQTY() {
        return SUMQTY;
    }

    public void setSUMQTY(String SUMQTY) {
        this.SUMQTY = SUMQTY;
    }

    public String getTOTQTY() {
        return TOTQTY;
    }

    public void setTOTQTY(String TOTQTY) {
        this.TOTQTY = TOTQTY;
    }

    public String getRCHTOTAMT() {
        return RCHTOTAMT;
    }

    public void setRCHTOTAMT(String RCHTOTAMT) {
        this.RCHTOTAMT = RCHTOTAMT;
    }

    public String getSUMAMT() {
        return SUMAMT;
    }

    public void setSUMAMT(String SUMAMT) {
        this.SUMAMT = SUMAMT;
    }

    public String getTOTAMT() {
        return TOTAMT;
    }

    public void setTOTAMT(String TOTAMT) {
        this.TOTAMT = TOTAMT;
    }

    public String getRCHITEM() {
        return RCHITEM;
    }

    public void setRCHITEM(String RCHITEM) {
        this.RCHITEM = RCHITEM;
    }

    public String getSUMITEM() {
        return SUMITEM;
    }

    public void setSUMITEM(String SUMITEM) {
        this.SUMITEM = SUMITEM;
    }

    public String getTOTITEM() {
        return TOTITEM;
    }

    public void setTOTITEM(String TOTITEM) {
        this.TOTITEM = TOTITEM;
    }

}
