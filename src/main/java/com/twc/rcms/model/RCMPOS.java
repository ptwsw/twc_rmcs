/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class RCMPOS {

    private String RCSCONO;
    private String RCSPONO;
    private String RCSLINE;
    private String RCSDOCYEAR;
    private String RCSMATDOC;
    private String RCSPSTRGDATE;
    private String RCSPOQTY;
    private String RCSMATCODE;
    private String RCSPLANT;
    private String RCSDOCDATE;
    private String RCSREFDOCNO;
    private String RCSREFDOCNOLONG;
    private String RCSCDT;
    private String RCSCTM;
    private String RCSUSER;

    public RCMPOS() {
    }

    public RCMPOS(String RCSCONO, String RCSPONO, String RCSLINE, String RCSDOCYEAR, String RCSMATDOC, String RCSPSTRGDATE, String RCSPOQTY, String RCSMATCODE, String RCSPLANT, String RCSDOCDATE, String RCSREFDOCNO, String RCSREFDOCNOLONG, String RCSCDT, String RCSCTM, String RCSUSER) {
        this.RCSCONO = RCSCONO;
        this.RCSPONO = RCSPONO;
        this.RCSLINE = RCSLINE;
        this.RCSDOCYEAR = RCSDOCYEAR;
        this.RCSMATDOC = RCSMATDOC;
        this.RCSPSTRGDATE = RCSPSTRGDATE;
        this.RCSPOQTY = RCSPOQTY;
        this.RCSMATCODE = RCSMATCODE;
        this.RCSPLANT = RCSPLANT;
        this.RCSDOCDATE = RCSDOCDATE;
        this.RCSREFDOCNO = RCSREFDOCNO;
        this.RCSREFDOCNOLONG = RCSREFDOCNOLONG;
        this.RCSCDT = RCSCDT;
        this.RCSCTM = RCSCTM;
        this.RCSUSER = RCSUSER;
    }

    public String getRCSCONO() {
        return RCSCONO;
    }

    public void setRCSCONO(String RCSCONO) {
        this.RCSCONO = RCSCONO;
    }

    public String getRCSPONO() {
        return RCSPONO;
    }

    public void setRCSPONO(String RCSPONO) {
        this.RCSPONO = RCSPONO;
    }

    public String getRCSLINE() {
        return RCSLINE;
    }

    public void setRCSLINE(String RCSLINE) {
        this.RCSLINE = RCSLINE;
    }

    public String getRCSDOCYEAR() {
        return RCSDOCYEAR;
    }

    public void setRCSDOCYEAR(String RCSDOCYEAR) {
        this.RCSDOCYEAR = RCSDOCYEAR;
    }

    public String getRCSMATDOC() {
        return RCSMATDOC;
    }

    public void setRCSMATDOC(String RCSMATDOC) {
        this.RCSMATDOC = RCSMATDOC;
    }

    public String getRCSPSTRGDATE() {
        return RCSPSTRGDATE;
    }

    public void setRCSPSTRGDATE(String RCSPSTRGDATE) {
        this.RCSPSTRGDATE = RCSPSTRGDATE;
    }

    public String getRCSPOQTY() {
        return RCSPOQTY;
    }

    public void setRCSPOQTY(String RCSPOQTY) {
        this.RCSPOQTY = RCSPOQTY;
    }

    public String getRCSMATCODE() {
        return RCSMATCODE;
    }

    public void setRCSMATCODE(String RCSMATCODE) {
        this.RCSMATCODE = RCSMATCODE;
    }

    public String getRCSPLANT() {
        return RCSPLANT;
    }

    public void setRCSPLANT(String RCSPLANT) {
        this.RCSPLANT = RCSPLANT;
    }

    public String getRCSDOCDATE() {
        return RCSDOCDATE;
    }

    public void setRCSDOCDATE(String RCSDOCDATE) {
        this.RCSDOCDATE = RCSDOCDATE;
    }

    public String getRCSREFDOCNO() {
        return RCSREFDOCNO;
    }

    public void setRCSREFDOCNO(String RCSREFDOCNO) {
        this.RCSREFDOCNO = RCSREFDOCNO;
    }

    public String getRCSREFDOCNOLONG() {
        return RCSREFDOCNOLONG;
    }

    public void setRCSREFDOCNOLONG(String RCSREFDOCNOLONG) {
        this.RCSREFDOCNOLONG = RCSREFDOCNOLONG;
    }

    public String getRCSCDT() {
        return RCSCDT;
    }

    public void setRCSCDT(String RCSCDT) {
        this.RCSCDT = RCSCDT;
    }

    public String getRCSCTM() {
        return RCSCTM;
    }

    public void setRCSCTM(String RCSCTM) {
        this.RCSCTM = RCSCTM;
    }

    public String getRCSUSER() {
        return RCSUSER;
    }

    public void setRCSUSER(String RCSUSER) {
        this.RCSUSER = RCSUSER;
    }

}
