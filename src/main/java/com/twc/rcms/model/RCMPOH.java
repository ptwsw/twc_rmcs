/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class RCMPOH {

    private String RCHCONO;
    private String RCHPONO;
    private String RCHPORG;
    private String RCHCRTON;
    private String RCHDOCTYP;
    private String RCHVCODE;
    private String RCHVNAME;
    private String RCHPGRP;
    private String RCHPNAME;
    private String RCHCUR;
    private String RCHPMT;
    private String RCHSTS;
    private String RCHTXLINE;
    private String RCHPRDCD;
    private String RCHDLADDR;
    private String RCHTAXCD;
    private String RCHTOTAMT;
    private String RCHRELGRP;
    private String RCHDOCDATE;
    private String RCHCDT;
    private String RCHCTM;
    private String RCHUSER;

    public RCMPOH() {
    }

    public RCMPOH(String RCHCONO, String RCHPONO, String RCHPORG, String RCHCRTON, String RCHDOCTYP, String RCHVCODE, String RCHVNAME, String RCHPGRP, String RCHPNAME, String RCHCUR, String RCHPMT, String RCHSTS, String RCHTXLINE, String RCHPRDCD, String RCHDLADDR, String RCHTAXCD, String RCHTOTAMT, String RCHRELGRP, String RCHDOCDATE, String RCHCDT, String RCHCTM, String RCHUSER) {
        this.RCHCONO = RCHCONO;
        this.RCHPONO = RCHPONO;
        this.RCHPORG = RCHPORG;
        this.RCHCRTON = RCHCRTON;
        this.RCHDOCTYP = RCHDOCTYP;
        this.RCHVCODE = RCHVCODE;
        this.RCHVNAME = RCHVNAME;
        this.RCHPGRP = RCHPGRP;
        this.RCHPNAME = RCHPNAME;
        this.RCHCUR = RCHCUR;
        this.RCHPMT = RCHPMT;
        this.RCHSTS = RCHSTS;
        this.RCHTXLINE = RCHTXLINE;
        this.RCHPRDCD = RCHPRDCD;
        this.RCHDLADDR = RCHDLADDR;
        this.RCHTAXCD = RCHTAXCD;
        this.RCHTOTAMT = RCHTOTAMT;
        this.RCHRELGRP = RCHRELGRP;
        this.RCHDOCDATE = RCHDOCDATE;
        this.RCHCDT = RCHCDT;
        this.RCHCTM = RCHCTM;
        this.RCHUSER = RCHUSER;
    }

    public String getRCHCONO() {
        return RCHCONO;
    }

    public void setRCHCONO(String RCHCONO) {
        this.RCHCONO = RCHCONO;
    }

    public String getRCHPONO() {
        return RCHPONO;
    }

    public void setRCHPONO(String RCHPONO) {
        this.RCHPONO = RCHPONO;
    }

    public String getRCHPORG() {
        return RCHPORG;
    }

    public void setRCHPORG(String RCHPORG) {
        this.RCHPORG = RCHPORG;
    }

    public String getRCHCRTON() {
        return RCHCRTON;
    }

    public void setRCHCRTON(String RCHCRTON) {
        this.RCHCRTON = RCHCRTON;
    }

    public String getRCHDOCTYP() {
        return RCHDOCTYP;
    }

    public void setRCHDOCTYP(String RCHDOCTYP) {
        this.RCHDOCTYP = RCHDOCTYP;
    }

    public String getRCHVCODE() {
        return RCHVCODE;
    }

    public void setRCHVCODE(String RCHVCODE) {
        this.RCHVCODE = RCHVCODE;
    }

    public String getRCHVNAME() {
        return RCHVNAME;
    }

    public void setRCHVNAME(String RCHVNAME) {
        this.RCHVNAME = RCHVNAME;
    }

    public String getRCHPGRP() {
        return RCHPGRP;
    }

    public void setRCHPGRP(String RCHPGRP) {
        this.RCHPGRP = RCHPGRP;
    }

    public String getRCHPNAME() {
        return RCHPNAME;
    }

    public void setRCHPNAME(String RCHPNAME) {
        this.RCHPNAME = RCHPNAME;
    }

    public String getRCHCUR() {
        return RCHCUR;
    }

    public void setRCHCUR(String RCHCUR) {
        this.RCHCUR = RCHCUR;
    }

    public String getRCHPMT() {
        return RCHPMT;
    }

    public void setRCHPMT(String RCHPMT) {
        this.RCHPMT = RCHPMT;
    }

    public String getRCHSTS() {
        return RCHSTS;
    }

    public void setRCHSTS(String RCHSTS) {
        this.RCHSTS = RCHSTS;
    }

    public String getRCHTXLINE() {
        return RCHTXLINE;
    }

    public void setRCHTXLINE(String RCHTXLINE) {
        this.RCHTXLINE = RCHTXLINE;
    }

    public String getRCHPRDCD() {
        return RCHPRDCD;
    }

    public void setRCHPRDCD(String RCHPRDCD) {
        this.RCHPRDCD = RCHPRDCD;
    }

    public String getRCHDLADDR() {
        return RCHDLADDR;
    }

    public void setRCHDLADDR(String RCHDLADDR) {
        this.RCHDLADDR = RCHDLADDR;
    }

    public String getRCHTAXCD() {
        return RCHTAXCD;
    }

    public void setRCHTAXCD(String RCHTAXCD) {
        this.RCHTAXCD = RCHTAXCD;
    }

    public String getRCHTOTAMT() {
        return RCHTOTAMT;
    }

    public void setRCHTOTAMT(String RCHTOTAMT) {
        this.RCHTOTAMT = RCHTOTAMT;
    }

    public String getRCHRELGRP() {
        return RCHRELGRP;
    }

    public void setRCHRELGRP(String RCHRELGRP) {
        this.RCHRELGRP = RCHRELGRP;
    }

    public String getRCHDOCDATE() {
        return RCHDOCDATE;
    }

    public void setRCHDOCDATE(String RCHDOCDATE) {
        this.RCHDOCDATE = RCHDOCDATE;
    }

    public String getRCHCDT() {
        return RCHCDT;
    }

    public void setRCHCDT(String RCHCDT) {
        this.RCHCDT = RCHCDT;
    }

    public String getRCHCTM() {
        return RCHCTM;
    }

    public void setRCHCTM(String RCHCTM) {
        this.RCHCTM = RCHCTM;
    }

    public String getRCHUSER() {
        return RCHUSER;
    }

    public void setRCHUSER(String RCHUSER) {
        this.RCHUSER = RCHUSER;
    }

}
