/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.model;

/**
 *
 * @author 93176
 */
public class RCMBPD {

    private String RCDCONO;
    private String RCDJBNO;
    private String RCDINVNO;
    private String RCDPONO;
    private String RCDPOLINE;
    private String RCDDLNO;
    private String RCDID;
    private String RCDITNO;
    private String RCDITDS;
    private String RCDCUSMAT;
    private String RCDSUM;
    private String RCDDLDATE;
    private String RCDPKQTY;
    private String RCDINVQTY;
    private String RCDPALNO;
    private String RCDBOXNO;
    private String RCDPKTYPE;
    private String RCDINVPRC;
    private String RCDDUDATE;
    private String RCDMATCD;
    private String RCDMATDES;
    private String RCDAUTHO;
    private String RCDBUM;
    private String RCDMCTRL;
    private String RCDPOPRC;
    private String RCDPOQTY;
    private String RCDRCQTY;
    private String RCDTTQTY;
    private String RCDRATE;
    private String RCDCMXQTY;
    private String RCDCPERC;
    private String RCDSTS;
    private String RCDEDT;
    private String RCDETM;
    private String RCDCDT;
    private String RCDCTM;
    private String RCDUSER;
    private String RCDQRCODE;
    private String CNTPO;
    private String CNTMAT;
    private String PACK;
    private String SUMM;
    private String RCDINVAMT;
    private String RCDPOSTDATE;
    private String RCDPLANT;
    private String RCDTOTAL;
    private String RCDMAX;
    private String RCDQSTS;
    private String RCDPC;
    private String RCDPOFLAG;
    private String RCDRJFLAG;
    private String RCDINVREF;
    private String RCDMATREF;
    private String ISFLAG;
    private String FULLPONO;
    private String RCSPOQTY;
    private String canReceive;
    private String getReceive;
    private String SumcanReceive;
    private String SumgetReceive;

    public RCMBPD() {
    }

    public RCMBPD(String RCDCONO, String RCDJBNO, String RCDINVNO, String RCDPONO, String RCDPOLINE, String RCDDLNO, String RCDID, String RCDITNO, String RCDITDS, String RCDCUSMAT, String RCDSUM, String RCDDLDATE, String RCDPKQTY, String RCDINVQTY, String RCDPALNO, String RCDBOXNO, String RCDPKTYPE, String RCDINVPRC, String RCDDUDATE, String RCDMATCD, String RCDMATDES, String RCDAUTHO, String RCDBUM, String RCDMCTRL, String RCDPOPRC, String RCDPOQTY, String RCDRCQTY, String RCDTTQTY, String RCDRATE, String RCDCMXQTY, String RCDCPERC, String RCDSTS, String RCDEDT, String RCDETM, String RCDCDT, String RCDCTM, String RCDUSER, String RCDQRCODE, String CNTPO, String CNTMAT, String PACK, String SUMM, String RCDINVAMT, String RCDPOSTDATE, String RCDPLANT, String RCDTOTAL, String RCDMAX, String RCDQSTS, String RCDPC, String RCDPOFLAG, String RCDRJFLAG, String RCDINVREF, String RCDMATREF, String ISFLAG, String FULLPONO, String RCSPOQTY, String canReceive, String getReceive, String SumcanReceive, String SumgetReceive) {
        this.RCDCONO = RCDCONO;
        this.RCDJBNO = RCDJBNO;
        this.RCDINVNO = RCDINVNO;
        this.RCDPONO = RCDPONO;
        this.RCDPOLINE = RCDPOLINE;
        this.RCDDLNO = RCDDLNO;
        this.RCDID = RCDID;
        this.RCDITNO = RCDITNO;
        this.RCDITDS = RCDITDS;
        this.RCDCUSMAT = RCDCUSMAT;
        this.RCDSUM = RCDSUM;
        this.RCDDLDATE = RCDDLDATE;
        this.RCDPKQTY = RCDPKQTY;
        this.RCDINVQTY = RCDINVQTY;
        this.RCDPALNO = RCDPALNO;
        this.RCDBOXNO = RCDBOXNO;
        this.RCDPKTYPE = RCDPKTYPE;
        this.RCDINVPRC = RCDINVPRC;
        this.RCDDUDATE = RCDDUDATE;
        this.RCDMATCD = RCDMATCD;
        this.RCDMATDES = RCDMATDES;
        this.RCDAUTHO = RCDAUTHO;
        this.RCDBUM = RCDBUM;
        this.RCDMCTRL = RCDMCTRL;
        this.RCDPOPRC = RCDPOPRC;
        this.RCDPOQTY = RCDPOQTY;
        this.RCDRCQTY = RCDRCQTY;
        this.RCDTTQTY = RCDTTQTY;
        this.RCDRATE = RCDRATE;
        this.RCDCMXQTY = RCDCMXQTY;
        this.RCDCPERC = RCDCPERC;
        this.RCDSTS = RCDSTS;
        this.RCDEDT = RCDEDT;
        this.RCDETM = RCDETM;
        this.RCDCDT = RCDCDT;
        this.RCDCTM = RCDCTM;
        this.RCDUSER = RCDUSER;
        this.RCDQRCODE = RCDQRCODE;
        this.CNTPO = CNTPO;
        this.CNTMAT = CNTMAT;
        this.PACK = PACK;
        this.SUMM = SUMM;
        this.RCDINVAMT = RCDINVAMT;
        this.RCDPOSTDATE = RCDPOSTDATE;
        this.RCDPLANT = RCDPLANT;
        this.RCDTOTAL = RCDTOTAL;
        this.RCDMAX = RCDMAX;
        this.RCDQSTS = RCDQSTS;
        this.RCDPC = RCDPC;
        this.RCDPOFLAG = RCDPOFLAG;
        this.RCDRJFLAG = RCDRJFLAG;
        this.RCDINVREF = RCDINVREF;
        this.RCDMATREF = RCDMATREF;
        this.ISFLAG = ISFLAG;
        this.FULLPONO = FULLPONO;
        this.RCSPOQTY = RCSPOQTY;
        this.canReceive = canReceive;
        this.getReceive = getReceive;
        this.SumcanReceive = SumcanReceive;
        this.SumgetReceive = SumgetReceive;
    }

    public String getRCDCONO() {
        return RCDCONO;
    }

    public void setRCDCONO(String RCDCONO) {
        this.RCDCONO = RCDCONO;
    }

    public String getRCDJBNO() {
        return RCDJBNO;
    }

    public void setRCDJBNO(String RCDJBNO) {
        this.RCDJBNO = RCDJBNO;
    }

    public String getRCDINVNO() {
        return RCDINVNO;
    }

    public void setRCDINVNO(String RCDINVNO) {
        this.RCDINVNO = RCDINVNO;
    }

    public String getRCDPONO() {
        return RCDPONO;
    }

    public void setRCDPONO(String RCDPONO) {
        this.RCDPONO = RCDPONO;
    }

    public String getRCDPOLINE() {
        return RCDPOLINE;
    }

    public void setRCDPOLINE(String RCDPOLINE) {
        this.RCDPOLINE = RCDPOLINE;
    }

    public String getRCDDLNO() {
        return RCDDLNO;
    }

    public void setRCDDLNO(String RCDDLNO) {
        this.RCDDLNO = RCDDLNO;
    }

    public String getRCDID() {
        return RCDID;
    }

    public void setRCDID(String RCDID) {
        this.RCDID = RCDID;
    }

    public String getRCDITNO() {
        return RCDITNO;
    }

    public void setRCDITNO(String RCDITNO) {
        this.RCDITNO = RCDITNO;
    }

    public String getRCDITDS() {
        return RCDITDS;
    }

    public void setRCDITDS(String RCDITDS) {
        this.RCDITDS = RCDITDS;
    }

    public String getRCDCUSMAT() {
        return RCDCUSMAT;
    }

    public void setRCDCUSMAT(String RCDCUSMAT) {
        this.RCDCUSMAT = RCDCUSMAT;
    }

    public String getRCDSUM() {
        return RCDSUM;
    }

    public void setRCDSUM(String RCDSUM) {
        this.RCDSUM = RCDSUM;
    }

    public String getRCDDLDATE() {
        return RCDDLDATE;
    }

    public void setRCDDLDATE(String RCDDLDATE) {
        this.RCDDLDATE = RCDDLDATE;
    }

    public String getRCDPKQTY() {
        return RCDPKQTY;
    }

    public void setRCDPKQTY(String RCDPKQTY) {
        this.RCDPKQTY = RCDPKQTY;
    }

    public String getRCDINVQTY() {
        return RCDINVQTY;
    }

    public void setRCDINVQTY(String RCDINVQTY) {
        this.RCDINVQTY = RCDINVQTY;
    }

    public String getRCDPALNO() {
        return RCDPALNO;
    }

    public void setRCDPALNO(String RCDPALNO) {
        this.RCDPALNO = RCDPALNO;
    }

    public String getRCDBOXNO() {
        return RCDBOXNO;
    }

    public void setRCDBOXNO(String RCDBOXNO) {
        this.RCDBOXNO = RCDBOXNO;
    }

    public String getRCDPKTYPE() {
        return RCDPKTYPE;
    }

    public void setRCDPKTYPE(String RCDPKTYPE) {
        this.RCDPKTYPE = RCDPKTYPE;
    }

    public String getRCDINVPRC() {
        return RCDINVPRC;
    }

    public void setRCDINVPRC(String RCDINVPRC) {
        this.RCDINVPRC = RCDINVPRC;
    }

    public String getRCDDUDATE() {
        return RCDDUDATE;
    }

    public void setRCDDUDATE(String RCDDUDATE) {
        this.RCDDUDATE = RCDDUDATE;
    }

    public String getRCDMATCD() {
        return RCDMATCD;
    }

    public void setRCDMATCD(String RCDMATCD) {
        this.RCDMATCD = RCDMATCD;
    }

    public String getRCDMATDES() {
        return RCDMATDES;
    }

    public void setRCDMATDES(String RCDMATDES) {
        this.RCDMATDES = RCDMATDES;
    }

    public String getRCDAUTHO() {
        return RCDAUTHO;
    }

    public void setRCDAUTHO(String RCDAUTHO) {
        this.RCDAUTHO = RCDAUTHO;
    }

    public String getRCDBUM() {
        return RCDBUM;
    }

    public void setRCDBUM(String RCDBUM) {
        this.RCDBUM = RCDBUM;
    }

    public String getRCDMCTRL() {
        return RCDMCTRL;
    }

    public void setRCDMCTRL(String RCDMCTRL) {
        this.RCDMCTRL = RCDMCTRL;
    }

    public String getRCDPOPRC() {
        return RCDPOPRC;
    }

    public void setRCDPOPRC(String RCDPOPRC) {
        this.RCDPOPRC = RCDPOPRC;
    }

    public String getRCDPOQTY() {
        return RCDPOQTY;
    }

    public void setRCDPOQTY(String RCDPOQTY) {
        this.RCDPOQTY = RCDPOQTY;
    }

    public String getRCDRCQTY() {
        return RCDRCQTY;
    }

    public void setRCDRCQTY(String RCDRCQTY) {
        this.RCDRCQTY = RCDRCQTY;
    }

    public String getRCDTTQTY() {
        return RCDTTQTY;
    }

    public void setRCDTTQTY(String RCDTTQTY) {
        this.RCDTTQTY = RCDTTQTY;
    }

    public String getRCDRATE() {
        return RCDRATE;
    }

    public void setRCDRATE(String RCDRATE) {
        this.RCDRATE = RCDRATE;
    }

    public String getRCDCMXQTY() {
        return RCDCMXQTY;
    }

    public void setRCDCMXQTY(String RCDCMXQTY) {
        this.RCDCMXQTY = RCDCMXQTY;
    }

    public String getRCDCPERC() {
        return RCDCPERC;
    }

    public void setRCDCPERC(String RCDCPERC) {
        this.RCDCPERC = RCDCPERC;
    }

    public String getRCDSTS() {
        return RCDSTS;
    }

    public void setRCDSTS(String RCDSTS) {
        this.RCDSTS = RCDSTS;
    }

    public String getRCDEDT() {
        return RCDEDT;
    }

    public void setRCDEDT(String RCDEDT) {
        this.RCDEDT = RCDEDT;
    }

    public String getRCDETM() {
        return RCDETM;
    }

    public void setRCDETM(String RCDETM) {
        this.RCDETM = RCDETM;
    }

    public String getRCDCDT() {
        return RCDCDT;
    }

    public void setRCDCDT(String RCDCDT) {
        this.RCDCDT = RCDCDT;
    }

    public String getRCDCTM() {
        return RCDCTM;
    }

    public void setRCDCTM(String RCDCTM) {
        this.RCDCTM = RCDCTM;
    }

    public String getRCDUSER() {
        return RCDUSER;
    }

    public void setRCDUSER(String RCDUSER) {
        this.RCDUSER = RCDUSER;
    }

    public String getRCDQRCODE() {
        return RCDQRCODE;
    }

    public void setRCDQRCODE(String RCDQRCODE) {
        this.RCDQRCODE = RCDQRCODE;
    }

    public String getCNTPO() {
        return CNTPO;
    }

    public void setCNTPO(String CNTPO) {
        this.CNTPO = CNTPO;
    }

    public String getCNTMAT() {
        return CNTMAT;
    }

    public void setCNTMAT(String CNTMAT) {
        this.CNTMAT = CNTMAT;
    }

    public String getPACK() {
        return PACK;
    }

    public void setPACK(String PACK) {
        this.PACK = PACK;
    }

    public String getSUMM() {
        return SUMM;
    }

    public void setSUMM(String SUMM) {
        this.SUMM = SUMM;
    }

    public String getRCDINVAMT() {
        return RCDINVAMT;
    }

    public void setRCDINVAMT(String RCDINVAMT) {
        this.RCDINVAMT = RCDINVAMT;
    }

    public String getRCDPOSTDATE() {
        return RCDPOSTDATE;
    }

    public void setRCDPOSTDATE(String RCDPOSTDATE) {
        this.RCDPOSTDATE = RCDPOSTDATE;
    }

    public String getRCDPLANT() {
        return RCDPLANT;
    }

    public void setRCDPLANT(String RCDPLANT) {
        this.RCDPLANT = RCDPLANT;
    }

    public String getRCDTOTAL() {
        return RCDTOTAL;
    }

    public void setRCDTOTAL(String RCDTOTAL) {
        this.RCDTOTAL = RCDTOTAL;
    }

    public String getRCDMAX() {
        return RCDMAX;
    }

    public void setRCDMAX(String RCDMAX) {
        this.RCDMAX = RCDMAX;
    }

    public String getRCDQSTS() {
        return RCDQSTS;
    }

    public void setRCDQSTS(String RCDQSTS) {
        this.RCDQSTS = RCDQSTS;
    }

    public String getRCDPC() {
        return RCDPC;
    }

    public void setRCDPC(String RCDPC) {
        this.RCDPC = RCDPC;
    }

    public String getRCDPOFLAG() {
        return RCDPOFLAG;
    }

    public void setRCDPOFLAG(String RCDPOFLAG) {
        this.RCDPOFLAG = RCDPOFLAG;
    }

    public String getRCDRJFLAG() {
        return RCDRJFLAG;
    }

    public void setRCDRJFLAG(String RCDRJFLAG) {
        this.RCDRJFLAG = RCDRJFLAG;
    }

    public String getRCDINVREF() {
        return RCDINVREF;
    }

    public void setRCDINVREF(String RCDINVREF) {
        this.RCDINVREF = RCDINVREF;
    }

    public String getRCDMATREF() {
        return RCDMATREF;
    }

    public void setRCDMATREF(String RCDMATREF) {
        this.RCDMATREF = RCDMATREF;
    }

    public String getISFLAG() {
        return ISFLAG;
    }

    public void setISFLAG(String ISFLAG) {
        this.ISFLAG = ISFLAG;
    }

    public String getFULLPONO() {
        return FULLPONO;
    }

    public void setFULLPONO(String FULLPONO) {
        this.FULLPONO = FULLPONO;
    }

    public String getRCSPOQTY() {
        return RCSPOQTY;
    }

    public void setRCSPOQTY(String RCSPOQTY) {
        this.RCSPOQTY = RCSPOQTY;
    }

    public String getCanReceive() {
        return canReceive;
    }

    public void setCanReceive(String canReceive) {
        this.canReceive = canReceive;
    }

    public String getGetReceive() {
        return getReceive;
    }

    public void setGetReceive(String getReceive) {
        this.getReceive = getReceive;
    }

    public String getSumcanReceive() {
        return SumcanReceive;
    }

    public void setSumcanReceive(String SumcanReceive) {
        this.SumcanReceive = SumcanReceive;
    }

    public String getSumgetReceive() {
        return SumgetReceive;
    }

    public void setSumgetReceive(String SumgetReceive) {
        this.SumgetReceive = SumgetReceive;
    }

}
