/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.dao;

import com.twc.rcms.model.RCMBPH;
import com.twc.rcms.util.connectiondb;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 93176
 */
public class RCM810Dao {

    private Connection connection;
    connectiondb connect;

    public RCM810Dao() {
        connection = null;
        connect = null;
        connect = new connectiondb();
        connection = connect.getConnection();
    }

    public HashMap<String, String> deleteBillnPO(String wh, String sp, String cu, String dt, String bill, String po) {

        Statement stmtD = null;

        HashMap<String, String> valDelHash = new HashMap<String, String>();

        try {
            stmtD = connection.createStatement();

            if (po.equals("true")) {

                //update RCMBPD Default
                String upDefault = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDDUDATE] = 0 , [RCDPLANT] = '', [RCDMATCD] = '' , [RCDMATDES] = '' , [RCDAUTHO] = '' , [RCDBUM] = '' , [RCDMCTRL] = ''\n"
                        + " ,[RCDPOPRC] = 0 , [RCDPOQTY] = 0 , [RCDTTQTY] = 0 , [RCDRATE] = 0 , [RCDCMXQTY] = 0, [RCDPOFLAG] = NULL \n"
                        + "  WHERE [RCDJBNO] IN (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                        + "  WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND RCHTPDT = '" + dt + "')";
                int updPODRec = stmtD.executeUpdate(upDefault);

                //delete RCMPOH RCMPOD RCMPOS
                String delPOS = "DELETE [RMShipment].[dbo].[RCMPOS] WHERE [RCSPONO] IN \n"
                        + "(SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END) FROM [RMShipment].[dbo].[RCMBPD] \n"
                        + "WHERE [RCDJBNO] IN (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                        + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND RCHTPDT = '" + dt + "'))";
                int delPOSRec = stmtD.executeUpdate(delPOS);

                String delPOD = "DELETE [RMShipment].[dbo].[RCMPOD] WHERE [RCDPONO] IN \n"
                        + "(SELECT DISTINCT [RCHPONO] FROM [RMShipment].[dbo].[RCMPOH] WHERE [RCHPONO]\n"
                        + "IN (SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END) \n"
                        + "  FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] IN (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                        + "  WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND RCHTPDT = '" + dt + "')))";
                int delPODRec = stmtD.executeUpdate(delPOD);

                String delPOH = "DELETE [RMShipment].[dbo].[RCMPOH] WHERE [RCHPONO] IN \n"
                        + "(SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END) FROM [RMShipment].[dbo].[RCMBPD] \n"
                        + "WHERE [RCDJBNO] IN (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                        + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND RCHTPDT = '" + dt + "'))";
                int delPOHRec = stmtD.executeUpdate(delPOH);

                if (updPODRec > 0) {
                    valDelHash.put("UPPOD", "true");
                } else {
                    valDelHash.put("UPPOD", "false");
                }

                if (delPOSRec > 0) {
                    valDelHash.put("POS", "true");
                } else {
                    valDelHash.put("POS", "false");
                }

                if (delPODRec > 0) {
                    valDelHash.put("POD", "true");
                } else {
                    valDelHash.put("POD", "false");
                }

                if (delPOHRec > 0) {
                    valDelHash.put("POH", "true");
                } else {
                    valDelHash.put("POH", "false");
                }

            }

            if (bill.equals("true")) {

                //delete RCMBPT
                String delBPT = "DELETE [RMShipment].[dbo].[RCMBPT] WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cu + "' AND [RCTDLDATE] = '" + dt + "'";
                int delBPTRec = stmtD.executeUpdate(delBPT);

                //delete RCMBPH RCMBPD
                String delBPD = "DELETE [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] IN (SELECT [RCHJBNO]\n"
                        + "  FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND [RCHTPDT] = '" + dt + "')";
                int delBPDRec = stmtD.executeUpdate(delBPD);

                String delBPH = "DELETE [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND [RCHTPDT] = '" + dt + "'";
                int delBPHRec = stmtD.executeUpdate(delBPH);

                if (delBPTRec > 0) {
                    valDelHash.put("BPT", "true");
                } else {
                    valDelHash.put("BPT", "false");
                }

                if (delBPDRec > 0) {
                    valDelHash.put("BPD", "true");
                } else {
                    valDelHash.put("BPD", "false");
                }

                if (delBPHRec > 0) {
                    valDelHash.put("BPH", "true");
                } else {
                    valDelHash.put("BPH", "false");
                }

            }

            stmtD.close();
            connection.close();
            if (connect != null) {
                connect.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RCM810Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return valDelHash;

    }

    public RCMBPH checkBPH(String wh, String sp, String cu, String dt) {

        Statement stmtCK = null;
        ResultSet rs = null;

        RCMBPH ret = new RCMBPH();

        try {
            stmtCK = connection.createStatement();

            String sql = "SELECT TOP 1 RCHSTS FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cu + "' AND [RCHTPDT] = '" + dt + "'";

            rs = stmtCK.executeQuery(sql);

            while (rs.next()) {
                ret.setRCHSTS(rs.getString("RCHSTS"));
            }

            stmtCK.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM810Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

}
