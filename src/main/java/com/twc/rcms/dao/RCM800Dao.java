/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.dao;

import com.twc.rcms.model.RCMBPD;
import com.twc.rcms.model.RCMPOD;
import com.twc.rcms.model.RCMPOH;
import com.twc.rcms.model.RCMPOS;
import com.twc.rcms.util.connectiondb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 93176
 */
public class RCM800Dao {

    private Connection connection;
    connectiondb connect;

    public RCM800Dao() {
        connection = null;
        connect = null;
        connect = new connectiondb();
        connection = connect.getConnection();
    }

    public List<RCMBPD> findRCMPoList(String wh, String sp, String cn, String dt) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT DISTINCT RCDPONO\n"
                + "FROM [RMShipment].[dbo].[RCMBPD] RCD "
                + "WHERE [RCDJBNO] IN (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] RCH WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "') --AND RCDSTS < 2 ";

        try {
//            System.out.println("sql " + sql);

            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDPONO(rs.getString("RCDPONO"));
                productList.add(p);
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public boolean insertRCMPOHnRCMPOD(List<RCMPOH> poHead, List<RCMPOD> poDetail, List<RCMPOS> poHistory, String tpdt) {

        Statement stmt = null;
        Statement stmtS = null;

        boolean result = false;

        StringBuilder sqlH = new StringBuilder();
        StringBuilder sqlD = new StringBuilder();
        StringBuilder sqlS = new StringBuilder();
        StringBuilder sqlUp = new StringBuilder();

        try {

            stmt = connection.createStatement();

            sqlS.append("INSERT INTO RCMPOS (RCSCONO,RCSPONO,RCSLINE,RCSDOCYEAR,RCSMATDOC,RCSPSTRGDATE\n"
                    + ",RCSPOQTY,RCSMATCODE,RCSPLANT,RCSDOCDATE,RCSREFDOCNO,RCSREFDOCNOLONG,RCSCDT,RCSCTM,RCSUSER) VALUES ");

            for (int i = 0; i < poHistory.size(); i++) {
                sqlS.append("('")
                        .append(poHistory.get(i).getRCSCONO()).append("','")
                        .append(poHistory.get(i).getRCSPONO()).append("','")
                        .append(poHistory.get(i).getRCSLINE()).append("','")
                        .append(poHistory.get(i).getRCSDOCYEAR()).append("','")
                        .append(poHistory.get(i).getRCSMATDOC()).append("','")
                        .append(poHistory.get(i).getRCSPSTRGDATE()).append("',")
                        .append(poHistory.get(i).getRCSPOQTY()).append(",'")
                        .append(poHistory.get(i).getRCSMATCODE()).append("','")
                        .append(poHistory.get(i).getRCSPLANT().trim()).append("','")
                        .append(poHistory.get(i).getRCSDOCDATE()).append("','")
                        .append(poHistory.get(i).getRCSREFDOCNO()).append("','")
                        .append(poHistory.get(i).getRCSREFDOCNOLONG()).append("',")
                        .append("FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')").append(",")
                        .append("FORMAT(CURRENT_TIMESTAMP,'HHmmss')").append(",'")
                        .append(poHistory.get(i).getRCSUSER()).append("') ");

                if (i != poHistory.size() - 1) {
                    sqlS.append(",");
                }
            }

            if (poHistory.size() > 0) {
                int record4 = stmt.executeUpdate(sqlS.toString());
            }

            sqlH.append("INSERT INTO RCMPOH (RCHCONO,RCHPONO,RCHPORG,RCHCRTON,RCHDOCTYP,RCHVCODE,RCHVNAME,RCHPGRP,RCHPNAME,RCHCUR,RCHPMT,RCHSTS"
                    + ",RCHTXLINE,RCHPRDCD,RCHDLADDR,RCHTAXCD,RCHTOTAMT,RCHRELGRP,RCHDOCDATE,RCHCDT,RCHCTM,RCHUSER) VALUES ");

            sqlD.append("INSERT INTO RCMPOD (RCDCONO,RCDPONO,RCDLINE,RCDPLANT,RCDMATCODE,RCDSTEXT,RCDUNIT,RCDPOQTY,RCDPOPRC,RCDPPN,RCDPOAMT,RCDVT,RCDST,RCDDIND,RCDCONVB"
                    + ",RCDCONVA,RCDITCAT,RCDDLDATE,RCDLOC,RCDLNAME,RCDACCAT,RCDDLOVER,RCDDLUNDER,RCDBASE,RCDCONVN2,RCDCONVD2,RCDMATGRP,RCDORDNO,RCDCDT,RCDCTM,RCDUSER,RCDTOTAL,RCDMAX,RCDQSTS,RCDPC) VALUES ");

            for (int i = 0; i < poHead.size(); i++) {
                sqlH.append("('")
                        .append(poHead.get(i).getRCHCONO()).append("','")
                        .append(poHead.get(i).getRCHPONO()).append("','")
                        .append(poHead.get(i).getRCHPORG()).append("','")
                        .append(poHead.get(i).getRCHCRTON()).append("','")
                        .append(poHead.get(i).getRCHDOCTYP()).append("','")
                        .append(poHead.get(i).getRCHVCODE()).append("','")
                        .append(poHead.get(i).getRCHVNAME()).append("','")
                        .append(poHead.get(i).getRCHPGRP()).append("','")
                        .append(poHead.get(i).getRCHPNAME()).append("','")
                        .append(poHead.get(i).getRCHCUR()).append("','")
                        .append(poHead.get(i).getRCHPMT()).append("','")
                        .append(poHead.get(i).getRCHSTS()).append("','")
                        .append(poHead.get(i).getRCHTXLINE()).append("','")
                        .append(poHead.get(i).getRCHPRDCD()).append("','")
                        .append(poHead.get(i).getRCHDLADDR()).append("','")
                        .append(poHead.get(i).getRCHTAXCD()).append("',")
                        .append(poHead.get(i).getRCHTOTAMT()).append(",'")
                        .append(poHead.get(i).getRCHRELGRP()).append("','")
                        .append(poHead.get(i).getRCHDOCDATE()).append("',")
                        .append("FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')").append(",")
                        .append("FORMAT(CURRENT_TIMESTAMP,'HHmmss')").append(",'")
                        .append(poHead.get(i).getRCHUSER()).append("') ");

                if (i != poHead.size() - 1) {
                    sqlH.append(",");
                }

                stmtS = connection.createStatement();
//                ResultSet rsPOTWC = stmtS.executeQuery("SELECT DISTINCT RCDPONO,RCDPOLINE FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDPONO LIKE '" + poHead.get(i).getRCHPONO() + "%'");

                ResultSet rsPOTWC = stmtS.executeQuery("SELECT RCDPONO,RCDPOLINE,RCDINVNO,RCDCUSMAT,SUM(RCDINVQTY) RCDINVQTY,ISNULL((SELECT TOP 1 RCSPOQTY FROM [RMShipment].[dbo].[RCMPOS] WHERE RCSPONO = RCDPONO AND RCSLINE = RIGHT(CONCAT('0000',RCDPOLINE),5)),0) RCSPOQTY,RCDRATE FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDPONO LIKE '" + poHead.get(i).getRCHPONO() + "%' AND RCDTPDT = '" + tpdt + "' GROUP BY RCDPONO,RCDPOLINE,RCDINVNO,RCDRATE,RCDCUSMAT");
//                ResultSet rsPOTWC = stmtS.executeQuery("SELECT RCDPONO,RCDPOLINE,RCDINVNO,SUM(RCDINVQTY) RCDINVQTY,ISNULL((SELECT TOP 1 RCSPOQTY FROM [RMShipment].[dbo].[RCMPOS] WHERE RCSPONO = RCDPONO AND RCSLINE = RIGHT(CONCAT('0000',RCDPOLINE),5)),0) RCSPOQTY,RCDRATE FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDPONO LIKE '" + poHead.get(i).getRCHPONO() + "%' AND RCDTPDT = '" + tpdt + "' GROUP BY RCDPONO,RCDPOLINE,RCDINVNO,RCDRATE");

                while (rsPOTWC.next()) {

                    for (int r = 0; r < poDetail.size(); r++) {

                        if (rsPOTWC.getString("RCDPONO").contains(poDetail.get(r).getRCDPONO())) {

                            Double ttqty = 0.0;
                            String poline = "0000" + rsPOTWC.getString("RCDPOLINE");

                            for (int b = 0; b < poHistory.size(); b++) {
                                if (poDetail.get(r).getRCDPONO().equals(poHistory.get(b).getRCSPONO())) {
                                    if (poline.substring(poline.length() - 5).equals(poHistory.get(b).getRCSLINE())) {
                                        ttqty += Double.parseDouble(poHistory.get(b).getRCSPOQTY());
                                    }
                                }
                            }

                            if (poline.substring(poline.length() - 5).equals(poDetail.get(r).getRCDLINE())) {

//                                System.out.println("" + rsPOTWC.getString("RCDCUSMAT") + " " + poDetail.get(r).getRCDMATCODE() + " " + rsPOTWC.getString("RCDCUSMAT").equals(poDetail.get(r).getRCDMATCODE()));
                                if (rsPOTWC.getString("RCDCUSMAT").equals(poDetail.get(r).getRCDMATCODE())) {
//                                    System.out.println("" + poline.substring(poline.length() - 5) + " " + poDetail.get(r).getRCDLINE());

//                                    System.out.println("PO " + rsPOTWC.getString("RCDPONO") + ":" + poDetail.get(r).getRCDPONO() + " Line " + poline + ":" + poDetail.get(r).getRCDLINE() + " MAT " + rsPOTWC.getString("RCDCUSMAT") + ":" + poDetail.get(r).getRCDMATCODE());
                                    //                                Double mxqty = ((Double.parseDouble(poDetail.get(r).getRCDPOQTY()) * Double.parseDouble(poDetail.get(r).getRCDDLOVER())) / 100) + Double.parseDouble(poDetail.get(r).getRCDPOQTY());
                                    String dudate = "0";
                                    if (!poDetail.get(r).getRCDDLDATE().equals("") && !poDetail.get(r).getRCDDLDATE().equals("NULL")) {
                                        String[] splDLDATE = poDetail.get(r).getRCDDLDATE().split("\\."); //04.03.2021
                                        dudate = splDLDATE[2] + splDLDATE[1] + splDLDATE[0];
                                    }

                                    Double POQTYnSAPRCQTY = ttqty + Double.parseDouble(poDetail.get(r).getRCDPOQTY()); // SAP_Received_QTY + PO_QTY

                                    String CalPlantFromPONo = "";

                                    int spaceCount = 0;
                                    for (char c : poDetail.get(r).getRCDPONO().trim().toCharArray()) {
                                        if (c == ' ') {
                                            spaceCount++;
                                        }
                                    }

                                    if (spaceCount == 2) {
                                        CalPlantFromPONo = poDetail.get(r).getRCDPONO().trim().split(" ")[2];
                                    } else if (spaceCount == 1) {
                                        CalPlantFromPONo = "1000";
                                    } else if (spaceCount == 0) {
                                        CalPlantFromPONo = "1000";
                                    }

                                    sqlUp.append("UPDATE RCMBPD SET RCDDUDATE=").append(dudate).append(","
                                    ).append("RCDPLANT='").append(CalPlantFromPONo).append("',"
                                    ).append("RCDMATCD='").append(poDetail.get(r).getRCDMATCODE()).append("',"
                                    ).append("RCDMATDES='").append(poDetail.get(r).getRCDSTEXT()).append("',"
                                    ).append("RCDAUTHO='").append(poHead.get(i).getRCHSTS()).append("',"
                                    ).append("RCDBUM='").append(poDetail.get(r).getRCDBASE()).append("',"
                                    ).append("RCDMCTRL=(").append("SELECT TOP 1 ISNULL([SAPMCTRL],'') AS SAPMCTRL FROM [RMShipment].[dbo].[SAPMAS] WHERE SAPMAT = '").append(poDetail.get(r).getRCDMATCODE()).append("'").append("),"
                                    ).append("RCDPOPRC=").append(poDetail.get(r).getRCDPOPRC()).append(","
                                    ).append("RCDPOQTY=").append(poDetail.get(r).getRCDPOQTY()).append(","
                                    ).append("RCDTTQTY=").append(POQTYnSAPRCQTY).append(","
                                    //                                    ).append("RCDRJFLAG= ").append("'C'").append(","
                                    ).append("RCDRJFLAG= ").append("(CASE WHEN (RCDRJFLAG = 'P' OR RCDRJFLAG = 'R' OR RCDRJFLAG = 'H') THEN RCDRJFLAG ELSE (CASE WHEN (RCDRJFLAG = 'C') AND (RCDCUSMAT = RCDMATCD) AND (RCDMATREF IS NULL) THEN NULL ELSE RCDRJFLAG END) END)").append(","
                                    ).append("RCDRATE=").append(poDetail.get(i).getRCDDLOVER()).append(" ")
                                            //                                ).append("RCDCMXQTY=").append(mxqty)
                                            .append(" WHERE RCDPONO LIKE '").append(poDetail.get(r).getRCDPONO())
                                            .append("%' AND RCDPOLINE = '").append(poDetail.get(r).getRCDLINE()).append("' \n")
                                            .append(" AND RCDCUSMAT = '").append(poDetail.get(r).getRCDMATCODE()).append("' \n");

//                                    System.out.println(sqlUp.toString());
                                    //--------------------------------------------
//                                String sqlSub = "SELECT RCDPONO,RCDPOLINE,RCDINVNO,SUM(RCDINVQTY) RCDINVQTY,ISNULL((SELECT RCSPOQTY FROM [RMShipment].[dbo].[RCMPOS] WHERE RCSPONO = RCDPONO AND RCSLINE = RIGHT(CONCAT('0000',RCDPOLINE),5)),0) RCSPOQTY,RCDRATE FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDPONO LIKE '" + poHead.get(i).getRCHPONO() + "%' AND RCDTPDT = '" + tpdt + "' AND RCDPOLINE = '" + poDetail.get(i).getRCDLINE() + "' GROUP BY RCDPONO,RCDPOLINE,RCDINVNO,RCDRATE";
//                                ResultSet calculate = stmtS.executeQuery(sqlSub);
//
                                    Double sumINVQTY = Double.parseDouble(rsPOTWC.getString("RCDINVQTY"));
                                    Double RCSPOQTY = Double.parseDouble(rsPOTWC.getString("RCSPOQTY"));
                                    Double RCDRATE = Double.parseDouble(rsPOTWC.getString("RCDRATE"));

                                    Double RCDTOTAL = sumINVQTY + RCSPOQTY;
                                    Double RCDMAX = (sumINVQTY * (RCDRATE + 100)) / 100;
                                    Double RCDQSTS = RCDTOTAL - RCDMAX;
                                    Double RCDPC = (RCDQSTS * 100) / sumINVQTY;

                                    String convRCDTOTAL = String.format("%.2f", RCDTOTAL);
                                    String convRCDMAX = String.format("%.2f", RCDMAX);
                                    String convRCDQSTS = String.format("%.2f", RCDQSTS);
                                    String convRCDPC = String.format("%.2f", RCDPC);

//                                    System.out.println("Line " + poDetail.get(r).getRCDLINE() + " POQTY " + RCSPOQTY + " INVQTY " + sumINVQTY + " RCDTOTAL " + convRCDTOTAL
//                                            + " RCDMAX " + convRCDMAX + " RCDQSTS " + convRCDQSTS + " RCDPC " + convRCDPC);
//                                String dd = "('" + poDetail.get(r).getRCDCONO() + "','" + "FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')" + "," + "FORMAT(CURRENT_TIMESTAMP,'HHmmss')" + ",'" + poDetail.get(r).getRCDUSER() + "'," + convRCDTOTAL + "," + convRCDMAX + "," + convRCDQSTS + "," + convRCDPC + " )";
//                               
                                    sqlD.append("('")
                                            .append(poDetail.get(r).getRCDCONO()).append("','")
                                            .append(poDetail.get(r).getRCDPONO()).append("','")
                                            .append(poDetail.get(r).getRCDLINE()).append("','")
                                            .append(poDetail.get(r).getRCDPLANT().trim()).append("','")
                                            .append(poDetail.get(r).getRCDMATCODE()).append("','")
                                            .append(poDetail.get(r).getRCDSTEXT()).append("','")
                                            .append(poDetail.get(r).getRCDUNIT()).append("',")
                                            .append(poDetail.get(r).getRCDPOQTY()).append(",")
                                            .append(poDetail.get(r).getRCDPOPRC()).append(",")
                                            .append(poDetail.get(r).getRCDPPN()).append(",")
                                            .append(poDetail.get(r).getRCDPOAMT()).append(",'")
                                            .append(poDetail.get(r).getRCDVT()).append("','")
                                            .append(poDetail.get(r).getRCDST()).append("','")
                                            .append(poDetail.get(r).getRCDDIND()).append("',")
                                            .append(poDetail.get(r).getRCDCONVB()).append(",")
                                            .append(poDetail.get(r).getRCDCONVA()).append(",'")
                                            .append(poDetail.get(r).getRCDITCAT()).append("','")
                                            .append(poDetail.get(r).getRCDDLDATE()).append("','")
                                            .append(poDetail.get(r).getRCDLOC()).append("','")
                                            .append(poDetail.get(r).getRCDLNAME()).append("','")
                                            .append(poDetail.get(r).getRCDACCAT()).append("',")
                                            .append(poDetail.get(r).getRCDDLOVER()).append(",'")
                                            .append(poDetail.get(r).getRCDDLUNDER()).append("','")
                                            .append(poDetail.get(r).getRCDBASE()).append("',")
                                            .append(poDetail.get(r).getRCDCONVN2()).append(",")
                                            .append(poDetail.get(r).getRCDCONVD2()).append(",'")
                                            .append(poDetail.get(r).getRCDMATGRP()).append("','")
                                            .append(poDetail.get(r).getRCDORDNO()).append("',")
                                            .append("FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd')").append(",")
                                            .append("FORMAT(CURRENT_TIMESTAMP,'HHmmss')").append(",'")
                                            .append(poDetail.get(r).getRCDUSER()).append("',")
                                            .append(convRCDTOTAL).append(",")
                                            .append(convRCDMAX).append(",")
                                            .append(convRCDQSTS).append(",")
                                            .append(convRCDPC).append(" ) ");

                                    if (r != poDetail.size() - 1) {
                                        sqlD.append(",");
                                    }

                                    //--------------------------------------------
                                } else { //CUSMAT != MATCD

                                    String dudate = "0";
                                    if (!poDetail.get(r).getRCDDLDATE().equals("") && !poDetail.get(r).getRCDDLDATE().equals("NULL")) {
                                        String[] splDLDATE = poDetail.get(r).getRCDDLDATE().split("\\."); //04.03.2021
                                        dudate = splDLDATE[2] + splDLDATE[1] + splDLDATE[0];
                                    }

                                    Double POQTYnSAPRCQTY = ttqty + Double.parseDouble(poDetail.get(r).getRCDPOQTY()); // SAP_Received_QTY + PO_QTY

                                    String CalPlantFromPONo = "";

                                    int spaceCount = 0;
                                    for (char c : poDetail.get(r).getRCDPONO().trim().toCharArray()) {
                                        if (c == ' ') {
                                            spaceCount++;
                                        }
                                    }

                                    if (spaceCount == 2) {
                                        CalPlantFromPONo = poDetail.get(r).getRCDPONO().trim().split(" ")[2];
                                    } else if (spaceCount == 1) {
                                        CalPlantFromPONo = "1000";
                                    } else if (spaceCount == 0) {
                                        CalPlantFromPONo = "1000";
                                    }

                                    sqlUp.append("UPDATE RCMBPD SET RCDDUDATE=").append(dudate).append(","
                                    ).append("RCDPLANT=").append("NULL").append(","
                                    ).append("RCDMATCD=").append("NULL").append(","
                                    ).append("RCDMATDES=").append("NULL").append(","
                                    ).append("RCDAUTHO=").append(poHead.get(i).getRCHSTS()).append(","
                                    ).append("RCDBUM=").append("NULL").append(","
                                    ).append("RCDMCTRL=").append("NULL").append(","
                                    ).append("RCDPOPRC=").append("NULL").append(","
                                    ).append("RCDPOQTY=").append("NULL").append(","
                                    ).append("RCDTTQTY=").append("NULL").append(","
                                    ).append("RCDRJFLAG='").append("C").append("',"
                                    //                                    ).append("RCDRJFLAG= ").append("(CASE WHEN (RCDRJFLAG = 'P' OR RCDRJFLAG = 'R' OR RCDRJFLAG = 'H') THEN RCDRJFLAG ELSE (CASE WHEN (RCDRJFLAG = 'C') AND (RCDCUSMAT = RCDMATCD) AND (RCDMATREF IS NULL) THEN NULL ELSE RCDRJFLAG END) END)").append(","
                                    ).append("RCDRATE=").append("NULL").append(" ")
                                            //                                ).append("RCDCMXQTY=").append(mxqty)
                                            .append(" WHERE RCDPONO LIKE '").append(poDetail.get(r).getRCDPONO())
                                            .append("%' AND RCDPOLINE = '").append(poDetail.get(r).getRCDLINE()).append("' \n")
                                            .append(" AND RCDCUSMAT = '").append(rsPOTWC.getString("RCDCUSMAT")).append("' \n");
//                                    sqlUp.append("UPDATE RCMBPD SET RCDDUDATE=").append(dudate).append(","
//                                    ).append("RCDPLANT='").append(CalPlantFromPONo).append("',"
//                                    ).append("RCDMATCD='").append(poDetail.get(r).getRCDMATCODE()).append("',"
//                                    ).append("RCDMATDES='").append(poDetail.get(r).getRCDSTEXT()).append("',"
//                                    ).append("RCDAUTHO='").append(poHead.get(i).getRCHSTS()).append("',"
//                                    ).append("RCDBUM='").append(poDetail.get(r).getRCDBASE()).append("',"
//                                    ).append("RCDMCTRL=(").append("SELECT TOP 1 ISNULL([SAPMCTRL],'') AS SAPMCTRL FROM [RMShipment].[dbo].[SAPMAS] WHERE SAPMAT = '").append(poDetail.get(r).getRCDMATCODE()).append("'").append("),"
//                                    ).append("RCDPOPRC=").append(poDetail.get(r).getRCDPOPRC()).append(","
//                                    ).append("RCDPOQTY=").append(poDetail.get(r).getRCDPOQTY()).append(","
//                                    ).append("RCDTTQTY=").append(POQTYnSAPRCQTY).append(","
//                                    ).append("RCDRJFLAG='").append("C").append("',"
//                                    ).append("RCDRATE=").append(poDetail.get(i).getRCDDLOVER()).append(" ")
//                                            //                                ).append("RCDCMXQTY=").append(mxqty)
//                                            .append(" WHERE RCDPONO LIKE '").append(poDetail.get(r).getRCDPONO())
//                                            .append("%' AND RCDPOLINE = '").append(poDetail.get(r).getRCDLINE()).append("' \n")
//                                            .append(" AND RCDCUSMAT = '").append(poDetail.get(r).getRCDMATCODE()).append("' \n");
                                }
                            }
                        }
                    }
                }
            }

            stmtS.close();

            int record3 = stmt.executeUpdate(sqlUp.toString());
            int record = stmt.executeUpdate(sqlH.toString());
            int record2 = stmt.executeUpdate(removeLastChar(sqlD.toString()));

//            System.out.println("--------------------------------------");
//            System.out.println(sqlUp.toString());
//            System.out.println("--------------------------------------");
            if (record >= 1 && record2 >= 1 && record3 >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmt.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                connect = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public boolean CkPoListDupinPOH(String wh, String sp, String cn, String dt) {
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean ck = false;

        String sql = "SELECT [RCHPONO]\n"
                + "  FROM [RMShipment].[dbo].[RCMPOH] WHERE [RCHPONO] IN (SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END)\n"
                //                + "  FROM [RMShipment].[dbo].[RCMPOH] WHERE [RCHPONO] IN (SELECT DISTINCT LEFT([RCDPONO], charindex(' ', [RCDPONO]) - 1)\n"
                + "  FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] "
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "'))";

        try {
//            System.out.println("sql " + sql);

            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                ck = true;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ck;
    }

    public boolean delPOinPOHnPOD(String wh, String sp, String cn, String dt) {

        boolean ck = false;
        Statement stmtD = null;

        try {

            stmtD = connection.createStatement();

//            String sqlupNULL = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDDUDATE=0"
//                    + ",RCDMATCD=''"
//                    + ",RCDMATDES=''"
//                    + ",RCDAUTHO=''"
//                    + ",RCDBUM=''"
//                    + ",RCDMCTRL=''"
//                    + ",RCDPOPRC=0"
//                    + ",RCDPOQTY=0"
//                    + ",RCDTTQTY=0"
//                    + ",RCDRATE=0"
//                    + ",RCDCMXQTY=0 \n"
//                    + "WHERE [RCDPONO] IN (SELECT DISTINCT [RCDPONO]"
//                    + "  FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "'))";
//            stmtD.executeUpdate(sqlupNULL); // UPDATE RCDPONO To "" Before New Update
            String upDefault = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDDUDATE] = 0 , [RCDPLANT] = '', [RCDMATCD] = '' , [RCDMATDES] = '' , [RCDAUTHO] = '' , [RCDBUM] = '' , [RCDMCTRL] = ''\n"
                    + " ,[RCDPOPRC] = 0 , [RCDPOQTY] = 0 , [RCDTTQTY] = 0 , [RCDRATE] = 0 , [RCDCMXQTY] = 0 ,  [RCDRJFLAG] = (CASE WHEN (RCDRJFLAG = 'P' OR RCDRJFLAG = 'R' OR RCDRJFLAG = 'H') THEN RCDRJFLAG ELSE (CASE WHEN (RCDRJFLAG = 'C') AND (RCDCUSMAT = RCDMATCD) AND (RCDMATREF IS NULL) THEN NULL ELSE RCDRJFLAG END) END)  \n"
                    + "  WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "  WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "')";
            int updPODRec = stmtD.executeUpdate(upDefault);

            String sqldelPOS = "DELETE [RMShipment].[dbo].[RCMPOS] WHERE [RCSPONO] IN \n"
                    + "(SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END) FROM [RMShipment].[dbo].[RCMBPD] \n"
                    + "WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "'))";

            int delS = stmtD.executeUpdate(sqldelPOS);

            String sqldelD = "DELETE [RMShipment].[dbo].[RCMPOD] WHERE [RCDPONO] IN \n"
                    + "(SELECT DISTINCT [RCHPONO] FROM [RMShipment].[dbo].[RCMPOH] WHERE [RCHPONO]\n"
                    + "IN (SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END) \n"
                    + "  FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "  WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "')))";
//            String sqldelD = "DELETE [RMShipment].[dbo].[RCMPOD] WHERE [RCDPONO] IN (SELECT [RCHPONO] FROM [RMShipment].[dbo].[RCMPOH] WHERE ([RCHPONO] + ' ' + [RCHPRDCD])  IN (SELECT DISTINCT [RCDPONO]\n"
//                    + "  FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "')))";

            int delD = stmtD.executeUpdate(sqldelD);

            String sqldelH = "DELETE [RMShipment].[dbo].[RCMPOH] WHERE [RCHPONO] IN \n"
                    + "(SELECT DISTINCT (CASE WHEN charindex(' ', [RCDPONO]) > 0 THEN LEFT([RCDPONO],charindex(' ', [RCDPONO]) - 1) ELSE [RCDPONO] END) FROM [RMShipment].[dbo].[RCMBPD] \n"
                    + "WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "'))";
//            String sqldelH = "DELETE [RMShipment].[dbo].[RCMPOH] WHERE ([RCHPONO] + ' ' + [RCHPRDCD])  IN (SELECT DISTINCT [RCDPONO]\n"
//                    + "  FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = (SELECT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND RCHTPDT = '" + dt + "'))";

            int delH = stmtD.executeUpdate(sqldelH);

            if (updPODRec >= 0 && delD >= 0 && delH >= 0 && delS >= 0) {
                ck = true;
            }

            stmtD.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM800Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ck;
    }

    public String checkNull(String a) {

        if (a == null) {
            a = "NULL";
        } else {
            if (a.equals("")) {
                a = "NULL";
            } else {
                a = "'" + a + "'";
            }
        }

        return a;
    }

    public boolean UpdateFlagPO(String sql) {
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean ck = false;
        Statement stmtD = null;

        try {
            stmtD = connection.createStatement();
            int updPODRec = stmtD.executeUpdate(sql);

            if (updPODRec > 0) {
                ck = true;
            }

            stmtD.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ck;
    }

    public String selStatus2(String wh, String sp, String cn, String dt) {
        ResultSet rs = null;
        PreparedStatement ps = null;
        String ParamStatus = "";

        boolean ck = false;

        String sql = "SELECT TOP 1 RCDSTS FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "')\n"
                + "ORDER BY RCDSTS DESC";

        try {
//            System.out.println("sql " + sql);

            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                ParamStatus = rs.getString("RCDSTS");
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ParamStatus;
    }

    public List<RCMBPH> findRCMHead(String wh, String sp, String cn, String dt) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPH> productList = new ArrayList<RCMBPH>();
        String sql = "SELECT DISTINCT RCHJBNO,RCHPLANT FROM [RMShipment].[dbo].[RCMBPH] \n"
                + "WHERE [RCHWHSE] = ? AND [RCHSPNO] = ? AND [RCHCUNO] = ? AND [RCHTPDT] = ?";

        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, wh);
            ps.setString(2, sp);
            ps.setString(3, cn);
            ps.setString(4, dt);
            rs = ps.executeQuery();

            while (rs.next()) {
                RCMBPH p = new RCMBPH();
                p.setRCHJBNO(rs.getString("RCHJBNO"));
                p.setRCHPLANT(rs.getString("RCHPLANT"));
                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public List<RCMBPD> findPonoByJbNo(String jbno) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPD> productList = new ArrayList<RCMBPD>();
        String sql = "SELECT DISTINCT RCDPONO FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDJBNO = ?";

        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, jbno);
            rs = ps.executeQuery();

            while (rs.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDPONO(rs.getString("RCDPONO"));
                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public boolean UpdatePlantHead(String pono, String jbno) {
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean ck = false;
        Statement stmtD = null;

        String CalPlantFromPONo = "";

        int spaceCount = 0;
        for (char c : pono.trim().toCharArray()) {
            if (c == ' ') {
                spaceCount++;
            }
        }

        if (spaceCount == 2) {
            CalPlantFromPONo = pono.trim().split(" ")[2];
        } else if (spaceCount == 1) {
            CalPlantFromPONo = "1000";
        } else if (spaceCount == 0) {
            CalPlantFromPONo = "1000";
        }

        try {
            String sql = "UPDATE [RMShipment].[dbo].[RCMBPH] SET RCHPLANT = '" + CalPlantFromPONo + "' WHERE RCHJBNO = '" + jbno + "'";

            stmtD = connection.createStatement();
            int updPODRec = stmtD.executeUpdate(sql);

            if (updPODRec > 0) {
                ck = true;
            }

            stmtD.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ck;
    }

    public static String removeLastChar(String s) {
        return (s == null || s.length() == 0)
                ? null
                : (s.substring(0, s.length() - 1));
    }

}
