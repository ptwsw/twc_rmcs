/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.dao;

import com.twc.rcms.model.QWHMAS;
import com.twc.rcms.model.RCMBPD;
import com.twc.rcms.model.RCMBPH;
import com.twc.rcms.model.RCMBPT;
import com.twc.rcms.util.connectiondb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class RCM200Dao {

    private Connection connection;
    connectiondb connect;

    public RCM200Dao() {
        connection = null;
        connect = null;
        connect = new connectiondb();
        connection = connect.getConnection();
    }

//    public List<QWHMAS> findWH() {
//
//        List<QWHMAS> productList = new ArrayList<QWHMAS>();
//        String sql = "SELECT QWHCOD,QWHNAME FROM QWHMAS ORDER BY QWHCOD";
//
//        try {
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ResultSet result = ps.executeQuery();
//            while (result.next()) {
//                QWHMAS p = new QWHMAS();
//                p.setQWHCOD(result.getString("QWHCOD"));
//                p.setQWHNAME(result.getString("QWHNAME"));
//                productList.add(p);
//            }
//            connection.close();
//            connect.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return productList;
//    }
    public List<QWHMAS> findWHbyUID(String uid) {

        List<QWHMAS> UAList = new ArrayList<QWHMAS>();

        String sql = "SELECT QWHCOD, QWHNAME, 'selected hidden' as SEL\n"
                + "FROM QWHMAS \n"
                + "WHERE QWHCOD = (select [MSWHSE] from [MSSUSER] where [USERID] = '" + uid + "')  AND QWHCOD != 'TWC' \n"
                + "/*AND QWHCOD like 'WH%'*/\n"
                + "UNION ALL\n"
                + "SELECT QWHCOD, QWHNAME, ''  \n"
                + "FROM QWHMAS WHERE QWHCOD != 'TWC' /* WHERE QWHCOD like 'WH%'*/";

        try {

            PreparedStatement ps = connection.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QWHMAS p = new QWHMAS();

                p.setQWHCOD(result.getString("QWHCOD"));
                p.setQWHNAME(result.getString("QWHNAME"));
                p.setSelect(result.getString("SEL"));

                UAList.add(p);

            }

            connection.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<QWHMAS> findWHbyCode(String code) {

        List<QWHMAS> productList = new ArrayList<QWHMAS>();
        String sql = "SELECT QWHCOD,QWHNAME FROM QWHMAS WHERE QWHCOD = '" + code + "' ";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                QWHMAS p = new QWHMAS();
                p.setQWHCOD(result.getString("QWHCOD"));
                p.setQWHNAME(result.getString("QWHNAME"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public List<RCMBPH> findTOTbyWhSpDt(String wh, String sp, String cn, String dt) {

        List<RCMBPH> productList = new ArrayList<RCMBPH>();
        String sql = "SELECT SUM(RCHTOTQTY) AS RCHTOTQTY \n"
                + "FROM [RMShipment].[dbo].[RCMBPH]\n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "'";

        try {
//            System.out.println(sql);
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPH p = new RCMBPH();
                p.setRCHTOTQTY(result.getString("RCHTOTQTY"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public List<RCMBPD> findTOTbyWhSpDt210(String wh, String sp, String cn, String dt, String inv, String pono, String mat) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT SUM([RCDINVQTY]) AS RCDINVQTY\n"
                + "FROM [RMShipment].[dbo].[RCMBPD] \n"
                + "WHERE [RCDJBNO] IN (SELECT [RCHJBNO]\n"
                + "FROM [RMShipment].[dbo].[RCMBPH]\n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')\n"
                + "AND [RCDINVNO] = '" + inv + "' AND [RCDPONO] = '" + pono + "' AND [RCDCUSMAT] = '" + mat + "' ";

        try {

            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDINVQTY(result.getString("RCDINVQTY"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public List<RCMBPD> findDLDATEbyQRcode(String qr) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();
        String sql = "SELECT *\n"
                + "FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "WHERE [RCDQRCODE] LIKE '" + qr.trim() + "%'";
//        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDDLDATE(result.getString("RCDTPDT"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public boolean insertRCMBPT(String wh, String sp, String cn, String dt, String seq, String ctid, String ctcode, String totpc, String uid) {

        boolean result = false;
        PreparedStatement ps = null;

        if (dt == null) {
            dt = "NULL";
        } else {
            if (dt.equals("")) {
                dt = "NULL";
            }
        }
        if (totpc == null) {
            totpc = "NULL";
        } else {
            if (totpc.equals("")) {
                totpc = "NULL";
            }
        }

        try {
            String sql = "INSERT INTO RCMBPT (RCTCONO,RCTWHSE, RCTSPNO, RCTCUNO, RCTDLDATE, RCTJBNO, RCTSCSEQ,RCTPALNO, RCTCTID, RCTCODE, RCTPACK, RCTCDT, RCTCTM, RCTUSER) \n"
                    + "VALUES ('TWC'," + checkNull(wh) + "," + checkNull(sp) + "," + checkNull(cn) + "," + dt + "," + "(SELECT TOP 1 [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = " + checkNull(wh) + " AND RCHSPNO = " + checkNull(sp) + " AND RCHCUNO = " + checkNull(cn) + " AND RCHTPDT = '" + dt + "')" + "," + checkNull(seq) + ",NULL," + checkNull(ctid) + "," + checkNull(ctcode) + ","
                    + totpc + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + checkNull(uid) + ")";
//            String sql = "INSERT INTO RCMBPT (RCTCONO,RCTWHSE, RCTSPNO, RCTCUNO, RCTDLDATE, RCTJBNO, RCTSCSEQ,RCTPALNO, RCTCTID, RCTCODE, RCTPACK, RCTCDT, RCTCTM, RCTUSER) \n"
//                    + "VALUES ('TWC'," + checkNull(wh) + "," + checkNull(sp) + "," + checkNull(cn) + "," + dt + "," + "(SELECT TOP 1 [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = " + checkNull(wh) + " AND RCHSPNO = " + checkNull(sp) + " AND RCHCUNO = " + checkNull(cn) + " AND RCHTPDT = '" + dt + "')" + "," + checkNull(seq) + ",(SELECT TOP 1 RCDPALNO FROM RCMBPD WHERE  RCDJBNO IN (SELECT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') AND RCDID = '" + ctid + "' AND RCDCUSMAT = '" + ctcode + "' )," + checkNull(ctid) + "," + checkNull(ctcode) + ","
//                    + totpc + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + checkNull(uid) + ")";

            ps = connection.prepareStatement(sql);

            int record = ps.executeUpdate();
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public boolean insertRCMBPTPal(String wh, String sp, String cn, String dt, String seq, String palno, String ctid, String mcode, String totpc, String uid) {

        boolean result = false;
        PreparedStatement ps = null;

        if (dt == null) {
            dt = "NULL";
        } else {
            if (dt.equals("")) {
                dt = "NULL";
            }
        }
        if (totpc == null) {
            totpc = "NULL";
        } else {
            if (totpc.equals("")) {
                totpc = "NULL";
            }
        }

        try {
            String sql = "INSERT INTO RCMBPT (RCTCONO,RCTWHSE, RCTSPNO, RCTCUNO, RCTDLDATE, RCTJBNO, RCTSCSEQ,RCTPALNO, RCTCTID,RCTCODE, RCTPACK, RCTCDT, RCTCTM, RCTUSER) \n"
                    + "VALUES ('TWC'," + checkNull(wh) + "," + checkNull(sp) + "," + checkNull(cn) + "," + dt + "," + "(SELECT TOP 1 [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = " + checkNull(wh) + " AND RCHSPNO = " + checkNull(sp) + " AND RCHCUNO = " + checkNull(cn) + " AND RCHTPDT = '" + dt + "')" + "," + checkNull(seq) + "," + checkNull(palno) + "," + checkNull(ctid) + "," + checkNull(mcode) + ","
                    + totpc + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + checkNull(uid) + ")";
//            String sql = "INSERT INTO RCMBPT (RCTCONO,RCTWHSE, RCTSPNO, RCTCUNO, RCTDLDATE, RCTJBNO, RCTSCSEQ, RCTCTID, RCTPACK, RCTCDT, RCTCTM, RCTUSER) \n"
//                    + "VALUES ('TWC'," + checkNull(wh) + "," + checkNull(sp) + "," + checkNull(cn) + "," + dt + "," + "(SELECT TOP 1 [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = " + checkNull(wh) + " AND RCHSPNO = " + checkNull(sp) + " AND RCHCUNO = " + checkNull(cn) + " AND RCHTPDT = '" + dt + "')" + "," + checkNull(seq) + "," + checkNull(ctid) + ","
//                    + totpc + ",FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),FORMAT(CURRENT_TIMESTAMP,'HHmmss')," + checkNull(uid) + ")";

            ps = connection.prepareStatement(sql);

            int record = ps.executeUpdate();
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public List<RCMBPT> findRCMBPT(String wh, String sp, String cn, String dt) {

        List<RCMBPT> productList = new ArrayList<RCMBPT>();
        String sql = "SELECT F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE,SUM(F1.RCTPACK) AS RCTPACK\n"
                + "FROM (\n"
                + "	SELECT RCTSCSEQ\n"
                + "	,(CASE WHEN RCTPALNO IS NULL THEN RCTCTID ELSE RCTPALNO END) AS RCTCTID\n"
                + "                ,(CASE WHEN RCTPALNO IS NULL THEN RCTCODE ELSE NULL END) AS RCTCODE\n"
                + "	,RCTPACK FROM [RMShipment].[dbo].[RCMBPT] \n"
                + "	WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "'\n"
                + "	\n"
                + ") F1\n"
                + "\n"
                + "GROUP BY F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE";
//        String sql = "SELECT * "
//                + "FROM [RMShipment].[dbo].[RCMBPT] \n"
//                + "WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "'";

        try {
            Statement selSts = connection.createStatement();

            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                RCMBPT p = new RCMBPT();
                p.setRCTSCSEQ(result.getString("RCTSCSEQ"));
//                p.setRCTPALNO(result.getString("RCTPALNO"));
                p.setRCTCTID(result.getString("RCTCTID"));
                p.setRCTCODE(result.getString("RCTCODE"));
                p.setRCTPACK(result.getString("RCTPACK"));

                String data = result.getString("RCTCTID");

                if (data.length() > 10) { //ID
                    data = "[RCDID] = '" + data.trim() + "' ";
                } else { //PALLET
                    data = "[RCDPALNO] = '" + data.trim() + "'";
                }

                String sqlSTSD = "SELECT TOP 1 RCDSTS FROM [RMShipment].[dbo].[RCMBPD] \n"
                        + "WHERE " + data + " AND\n"
                        + "[RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                        + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "') ORDER BY RCDSTS DESC";

                ResultSet rs = selSts.executeQuery(sqlSTSD);

                while (rs.next()) {
                    p.setRCTUSER(rs.getString("RCDSTS"));
                }

                productList.add(p);
            }

//            System.out.println(productList.size());
//
//            for (int i = 0; i < productList.size(); i++) {
//                System.out.println(productList.get(i).getRCTSCSEQ() + " " + productList.get(i).getRCTCTID() + " " + productList.get(i).getRCTCODE() + " " + productList.get(i).getRCTPACK() + " " + productList.get(i).getRCTUSER() + " " + productList.get(i).getRCTCTM());
//            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public List<RCMBPD> findRCMBPD210(String wh, String sp, String cn, String dt, String inv, String pono, String mat) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();
        String sql = "SELECT ROW_NUMBER ( ) OVER ( PARTITION BY RCDJBNO ORDER BY RCDID ) AS ROWIDX ,* \n"
                + "FROM [RMShipment].[dbo].[RCMBPD] \n"
                + "WHERE [RCDJBNO] IN (SELECT [RCHJBNO]\n"
                + "FROM [RMShipment].[dbo].[RCMBPH]\n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')\n"
                + "AND [RCDINVNO] = '" + inv + "' AND [RCDPONO] = '" + pono + "' AND [RCDCUSMAT] = '" + mat + "' ";

        try {
//            System.out.println(sql);
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                //[RCDID],[RCDBOXNO],[RCDINVQTY]
                p.setRCDDLNO(result.getString("ROWIDX"));
                p.setRCDID(result.getString("RCDID"));
                p.setRCDBOXNO(result.getString("RCDBOXNO"));
                p.setRCDINVQTY(result.getString("RCDINVQTY"));
                p.setRCDRJFLAG(result.getString("RCDRJFLAG"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public List<RCMBPT> findMaxSeq(String wh, String sp, String cn, String dt) {

        List<RCMBPT> productList = new ArrayList<RCMBPT>();
        String sql = "SELECT ISNULL(MAX(RCTSCSEQ),0) + 1 AS RCTSCSEQ,ISNULL(SUM(RCTPACK),0) AS RCTPACK FROM [RMShipment].[dbo].[RCMBPT] \n"
                + "WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "'";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPT p = new RCMBPT();
                p.setRCTSCSEQ(result.getString("RCTSCSEQ"));
                p.setRCTPACK(result.getString("RCTPACK"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public boolean deleteALLROW(String wh, String sp, String cn, String dt) {

        String sql = "DELETE [RMShipment].[dbo].[RCMBPT] \n"
                + "WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "'";

        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(sql);
            int del = ps.executeUpdate();

            if (del >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;

    }

    public boolean deleteOneROW(String wh, String sp, String cn, String dt, String rid) {

        if (rid.length() > 10) { //ID
            rid = "[RCTCTID] = '" + rid.trim() + "' ";
        } else { //PALLET
            rid = "[RCTPALNO] = '" + rid.trim() + "'";
        }

        String sql = "DELETE [RMShipment].[dbo].[RCMBPT] \n"
                + "WHERE " + rid + " AND [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "' ";

        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(sql);
            int del = ps.executeUpdate();

            if (del >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;

    }

    public boolean ckCTIDdup(String wh, String sp, String cn, String dt, String ctid, String ctcode) {

        String sql = "SELECT * FROM [RMShipment].[dbo].[RCMBPT] \n"
                + "WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "' AND RCTCTID = '" + ctid + "' AND RCTCODE = '" + ctcode + "' ";
        boolean ck = false;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ck = true;
            }
            connect.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return ck;
    }

    public boolean ckCTIDdupPal(String wh, String sp, String cn, String dt, String ctid) {

        String sql = "SELECT * FROM [RMShipment].[dbo].[RCMBPT] \n"
                + "WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "' AND RCTCTID = '" + ctid + "' ";
        boolean ck = false;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ck = true;
            }
            connect.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return ck;
    }

    public boolean updateSTSnQTY(String wh, String sp, String cn, String dt, String ctid) {

        boolean result = false;
        Statement stmtU = null;

        try {
            stmtU = connection.createStatement();

//            String sqlSTSH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET [RCHSTS] = 1 "
//                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "'";
//            stmtU.executeUpdate(sqlSTSH);
//            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDSTS] = 1 , RCDRCQTY = RCDINVQTY\n"
            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRCQTY = RCDINVQTY\n"
                    + "WHERE " + ctid + " AND\n"
                    + "[RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')";

            int record = stmtU.executeUpdate(sqlSTSD);

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmtU.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM200Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public boolean updateSTSnQTYConfirm(String wh, String sp, String cn, String dt, String ctid) {

        boolean result = false;
        Statement stmtU = null;

        try {
            stmtU = connection.createStatement();

            String sqlSTSH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET [RCHSTS] = 2 "
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "' AND [RCHSTS] = 1 ";
            stmtU.executeUpdate(sqlSTSH);
            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDSTS] = 2 \n"
                    //            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRCQTY = RCDINVQTY\n"
                    + "WHERE " + ctid + " AND\n"
                    + "[RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "') AND [RCDSTS] = 1 ";

            int record = stmtU.executeUpdate(sqlSTSD);

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmtU.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM200Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public List<RCMBPD> selectRCMBPD(String wh, String sp, String cn, String dt) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT TOP 1 ISNULL(RCDSTS,0) RCDSTS FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')  ORDER BY RCDSTS ASC";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDSTS(result.getString("RCDSTS"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    public List<RCMBPD> findALLidBYPallet(String wh, String sp, String cn, String dt, String ctnid) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();
        String sql = "SELECT RCDID,RCDCUSMAT\n"
                + "FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "WHERE " + ctnid + " AND\n"
                + "[RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')";

        try {
//            System.out.println(sql);
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDID(result.getString("RCDID"));
                p.setRCDCUSMAT(result.getString("RCDCUSMAT"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public boolean updateSTSnQTYafterDEL(String wh, String sp, String cn, String dt) {

        boolean result = false;
        Statement stmtU = null;

        try {
            stmtU = connection.createStatement();

            String sqlSTSH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET [RCHSTS] = 0 "
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "'";

            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDSTS] = 0 , RCDRCQTY = NULL\n"
                    + "WHERE [RCDJBNO] = (SELECT TOP 1 RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')";

            stmtU.executeUpdate(sqlSTSH);
            int record = stmtU.executeUpdate(sqlSTSD);

            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            stmtU.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM200Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public JSONObject updateSTSnQTYafterDELRow(String wh, String sp, String cn, String dt, String rid) {

        boolean result = true;
        Statement stmtU = null;
        Statement stmtS = null;

        JSONObject res = new JSONObject();

        try {
            stmtU = connection.createStatement();
            stmtS = connection.createStatement();

            boolean conti = false;

            String sql3 = "SELECT F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE,SUM(F1.RCTPACK) AS RCTPACK\n"
                    + "FROM (\n"
                    + "	SELECT RCTSCSEQ\n"
                    + "	,(CASE WHEN RCTPALNO IS NULL THEN RCTCTID ELSE RCTPALNO END) AS RCTCTID\n"
                    + "                ,(CASE WHEN RCTPALNO IS NULL THEN RCTCODE ELSE NULL END) AS RCTCODE\n"
                    + "	,RCTPACK FROM [RMShipment].[dbo].[RCMBPT] \n"
                    + "	WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "'\n"
                    + "	\n"
                    + ") F1\n"
                    + "\n"
                    + "GROUP BY F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE";

            ResultSet rs = stmtS.executeQuery(sql3);

//            ResultSet rs = stmtS.executeQuery("SELECT * FROM [RMShipment].[dbo].[RCMBPT] WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "' ORDER BY RIGHT('00000'+RCTSCSEQ,5) ASC");
            int i = 1;
            int summer = 0;
            while (rs.next()) {
                conti = true;

                String idORRai = rs.getString("RCTCTID");

                if (idORRai.length() > 10) { //ID
                    idORRai = "[RCTCTID] = '" + idORRai.trim() + "' ";
                } else { //PALLET
                    idORRai = "[RCTPALNO] = '" + idORRai.trim() + "'";
                }

                String reSEQNo = "UPDATE [RMShipment].[dbo].[RCMBPT] SET RCTSCSEQ = '" + i + "' WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "' AND " + idORRai;
                stmtU.executeUpdate(reSEQNo); //Re-Cal RCTSCSEQ 1,2,3,4 ---> 
                i++;
                summer += Integer.parseInt(rs.getString("RCTPACK"));
            }

            res.put("summer", summer);

//            if (conti == false) {
//                String sqlSTSH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET [RCHSTS] = 0 "
//                        + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "'";
//                stmtU.executeUpdate(sqlSTSH);
//            }
            res.put("conti", conti);

//            String whatisit;
//
//            if (rid.length() > 0) { //xxxxxxxxxxxxxxxxxx
//                whatisit = " AND RCDID = '" + rid + "' ";
//            } else {//xx/xx
//                whatisit = " AND RCDPALNO = '" + rid + "' ";
//            }
//
//            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRCQTY = NULL\n"
//                    + "WHERE [RCDJBNO] = (SELECT TOP 1 RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
//                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "') "
//                    + " " + whatisit + " \n";
//
//            int record = stmtU.executeUpdate(sqlSTSD);
//
//            if (record >= 1) {
//                result = true;
//            } else {
//                result = false;
//            }
            res.put("result", result);

            stmtS.close();
            stmtU.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM200Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    public String checkNull(String a) {

        if (a == null) {
            a = "NULL";
        } else {
            if (a.equals("")) {
                a = "NULL";
            } else {
                a = "'" + a + "'";
            }
        }

        return a;
    }

    public boolean CheckSTS(String wh, String sp, String cn, String dt) {

        boolean result = false;
        Statement stmtS = null;

        try {
            stmtS = connection.createStatement();

            String ckSelectSTS = "SELECT DISTINCT ISNULL(RCDSTS,0) RCDSTS FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDJBNO IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH]\n"
                    + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt + "')";

            ResultSet rs = stmtS.executeQuery(ckSelectSTS);

            while (rs.next()) {
                result = true;
            }

            stmtS.close();
            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM200Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public List<RCMBPD> selectINV(String wh, String sp, String cn, String dt) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT DISTINCT RCDINVNO FROM RCMBPD \n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO]\n"
                + "  FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "')";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDINVNO(result.getString("RCDINVNO"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    public List<RCMBPD> selectPONO(String wh, String sp, String cn, String dt, String inv) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT DISTINCT RCDPONO FROM RCMBPD \n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO]\n"
                + "  FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') AND RCDINVNO = '" + inv + "' ";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDPONO(result.getString("RCDPONO"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    public List<RCMBPD> selectMATC(String wh, String sp, String cn, String dt, String inv, String pono) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT DISTINCT RCDCUSMAT FROM RCMBPD \n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO]\n"
                + "  FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') AND RCDINVNO = '" + inv + "' AND RCDPONO = '" + pono + "' ";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDCUSMAT(result.getString("RCDCUSMAT"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    public List<RCMBPD> selectAllIDbyPALLET(String wh, String sp, String cn, String dt, String palno, String inv, String pono, String mat) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT DISTINCT RCDID FROM RCMBPD \n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO]\n"
                + "  FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') AND RCDINVNO = '" + inv + "' AND RCDPONO = '" + pono + "' AND RCDCUSMAT = '" + mat + "' AND RCDPALNO = '" + palno + "' ";

        try {
//            System.out.println(sql);
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDID(result.getString("RCDID"));
                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

//    public boolean updateRjectByID(String ctid) {
//
//        boolean result = false;
//        Statement stmtU = null;
//
//        try {
//            stmtU = connection.createStatement();
//
//            String sqlSTSD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDRJFLAG] = 'R' \n"
//                    + "WHERE RCDID = '" + ctid + "' ";
//
//            int record = stmtU.executeUpdate(sqlSTSD);
//
//            if (record >= 1) {
//                result = true;
//            } else {
//                result = false;
//            }
//
//            stmtU.close();
//            connection.close();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(RCM200Dao.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return result;
//    }
    public boolean updateRejectBPDbyID(String wh, String sp, String cn, String dt, String inv, String pono, String mat, String ids) {

        boolean result = false;
        PreparedStatement ps = null;

        try {
            String sql = "UPDATE RCMBPD SET RCDRJFLAG = 'R', RCDEDT = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'), RCDETM = FORMAT(CURRENT_TIMESTAMP,'HHmmss') \n"
                    + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') \n"
                    + "AND RCDINVNO = '" + inv + "' AND RCDPONO = '" + pono + "' AND RCDCUSMAT = '" + mat + "' AND RCDID = '" + ids + "' ";

//            System.out.println(sql);
            ps = connection.prepareStatement(sql);

            int record = ps.executeUpdate();
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public boolean updateHoldBPDbyID(String wh, String sp, String cn, String dt, String inv, String pono, String mat) {
//    public boolean updateHoldBPDbyID(String wh, String sp, String cn, String dt, String inv, String pono, String mat, String ids) {

        boolean result = false;
        PreparedStatement ps = null;

        try {
            String sql = "UPDATE RCMBPD SET RCDRJFLAG = 'H', RCDEDT = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'), RCDETM = FORMAT(CURRENT_TIMESTAMP,'HHmmss') \n"
                    + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') \n"
                    + "AND RCDINVNO = '" + inv + "' AND (RCDRJFLAG IS NULL OR (RCDRJFLAG != 'R' AND RCDRJFLAG != 'H')) ";
//                    + "AND RCDINVNO = '" + inv + "' AND RCDPONO = '" + pono + "' AND RCDCUSMAT = '" + mat + "' AND RCDID = '" + ids + "' ";

//            System.out.println(sql);
            ps = connection.prepareStatement(sql);

            int record = ps.executeUpdate();
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public boolean resetRCQty(String wh, String sp, String cn, String dt) {

        boolean result = false;
        PreparedStatement ps = null;

        try {
            String sql = "UPDATE RCMBPD SET RCDRCQTY = NULL \n"
                    + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO] FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') ";

//            System.out.println(sql);
            ps = connection.prepareStatement(sql);

            int record = ps.executeUpdate();
            if (record >= 1) {
                result = true;
            } else {
                result = false;
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
                connect = null;
                ps = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<RCMBPD> getBPDbyPalNo(String wh, String sp, String cn, String dt, String palno) {

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT *  FROM RCMBPD \n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT [RCHJBNO]\n"
                + "  FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = '" + wh + "' AND RCHSPNO = '" + sp + "' AND RCHCUNO = '" + cn + "' AND RCHTPDT = '" + dt + "') \n"
                + " AND RCDPALNO = '" + palno + "'  ORDER BY RCDPALNO,RCDPONO,RCDPOLINE";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDCONO(result.getString("RCDCONO"));
                p.setRCDJBNO(result.getString("RCDJBNO"));
                p.setRCDINVNO(result.getString("RCDINVNO"));
                p.setRCDPONO(result.getString("RCDPONO"));
                p.setRCDPOLINE(result.getString("RCDPOLINE"));
                p.setRCDDLNO(result.getString("RCDDLNO"));
                p.setRCDID(result.getString("RCDID"));
                p.setRCDITNO(result.getString("RCDITNO"));
                p.setRCDITDS(result.getString("RCDITDS"));
                p.setRCDCUSMAT(result.getString("RCDCUSMAT"));
                p.setRCDSUM(result.getString("RCDSUM"));
                p.setRCDDLDATE(result.getString("RCDDLDATE"));
                p.setRCDPKQTY(result.getString("RCDPKQTY"));
                p.setRCDINVQTY(result.getString("RCDINVQTY"));
                p.setRCDPALNO(result.getString("RCDPALNO"));
                p.setRCDBOXNO(result.getString("RCDBOXNO"));
                p.setRCDPKTYPE(result.getString("RCDPKTYPE"));
                p.setRCDINVPRC(result.getString("RCDINVPRC"));
                p.setRCDINVAMT(result.getString("RCDINVAMT"));
                p.setRCDDUDATE(result.getString("RCDDUDATE"));
                p.setRCDPLANT(result.getString("RCDPLANT"));
                p.setRCDMATCD(result.getString("RCDMATCD"));
                p.setRCDMATDES(result.getString("RCDMATDES"));
                p.setRCDAUTHO(result.getString("RCDAUTHO"));
                p.setRCDBUM(result.getString("RCDBUM"));
                p.setRCDMCTRL(result.getString("RCDMCTRL"));
                p.setRCDPOPRC(result.getString("RCDPOPRC"));
                p.setRCDPOQTY(result.getString("RCDPOQTY"));
                p.setRCDRCQTY(result.getString("RCDRCQTY"));
                p.setRCDTTQTY(result.getString("RCDTTQTY"));
                p.setRCDRATE(result.getString("RCDRATE"));
                p.setRCDCMXQTY(result.getString("RCDCMXQTY"));
                p.setRCDCPERC(result.getString("RCDCPERC"));
                p.setRCDSTS(result.getString("RCDSTS"));
                p.setRCDPOSTDATE(result.getString("RCDPOSTDATE"));
                p.setRCDEDT(result.getString("RCDEDT"));
                p.setRCDETM(result.getString("RCDETM"));
                p.setRCDCDT(result.getString("RCDCDT"));
                p.setRCDCTM(result.getString("RCDCTM"));
                p.setRCDUSER(result.getString("RCDUSER"));
                p.setRCDQRCODE(result.getString("RCDQRCODE"));
                p.setRCDPOFLAG(result.getString("RCDPOFLAG"));
                p.setRCDRJFLAG(result.getString("RCDRJFLAG"));
                p.setRCDINVREF(result.getString("RCDINVREF"));
                p.setRCDMATREF(result.getString("RCDMATREF"));

                productList.add(p);
            }
            connection.close();
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    public boolean ckCTIDdupAndPAL(String wh, String sp, String cn, String dt, String ctid, String ctcode, String palno) {

        String sql = "SELECT * FROM [RMShipment].[dbo].[RCMBPT] \n"
                + "WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] = '" + cn + "' AND [RCTDLDATE] = '" + dt + "' AND RCTCTID = '" + ctid + "' AND RCTCODE = '" + ctcode + "' AND RCTPALNO = '" + palno + "' ";
        boolean ck = false;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ck = true;
            }
            connect.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return ck;
    }

}
