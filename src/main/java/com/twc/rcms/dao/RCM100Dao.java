/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.dao;

import com.twc.rcms.model.RCMBPD;
import com.twc.rcms.model.RCMBPH;
import com.twc.rcms.util.connectiondb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class RCM100Dao {

    private Connection connection;
    connectiondb connect;

    public RCM100Dao() {
        connection = null;
        connect = null;
        connect = new connectiondb();
        connection = connect.getConnection();
    }

    public List<RCMBPH> findRCMHead(String wh, String sp, String cn, String dt, String uid) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPH> productList = new ArrayList<RCMBPH>();
        String sql = "SELECT TOP 1 (SELECT SUM(RCHTOTINV) FROM [RMShipment].[dbo].[RCMBPH] WHERE  [RCHWHSE] = RCH.[RCHWHSE] AND [RCHSPNO] = RCH.[RCHSPNO] AND [RCHCUNO] = RCH.[RCHCUNO] AND [RCHTPDT] = RCH.[RCHTPDT]) AS RCHTOTINV \n"
                + ",(SELECT SUM([RCHTOTQTY]) FROM [RMShipment].[dbo].[RCMBPH] WHERE  [RCHWHSE] = RCH.[RCHWHSE] AND [RCHSPNO] = RCH.[RCHSPNO] AND [RCHCUNO] = RCH.[RCHCUNO] AND [RCHTPDT] = RCH.[RCHTPDT]) AS RCHTOTQTY \n"
                + ",(SELECT SUM([RCHTOTPO]) FROM [RMShipment].[dbo].[RCMBPH] WHERE  [RCHWHSE] = RCH.[RCHWHSE] AND [RCHSPNO] = RCH.[RCHSPNO] AND [RCHCUNO] = RCH.[RCHCUNO] AND [RCHTPDT] = RCH.[RCHTPDT]) AS RCHTOTPO \n"
                + ",[RCHSTS],(SELECT TOP 1 USERS FROM MSSUSER WHERE USERID = ?) AS USERS\n"
                + "FROM [RMShipment].[dbo].[RCMBPH] RCH\n"
                + "WHERE [RCHWHSE] = ? AND [RCHSPNO] = ? AND [RCHCUNO] = ? AND [RCHTPDT] = ?";

        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, uid);
            ps.setString(2, wh);
            ps.setString(3, sp);
            ps.setString(4, cn);
            ps.setString(5, dt);
            rs = ps.executeQuery();

            while (rs.next()) {
                RCMBPH p = new RCMBPH();
                p.setRCHTOTINV(rs.getString("RCHTOTINV"));
                p.setRCHTOTQTY(rs.getString("RCHTOTQTY"));
                p.setRCHTOTPO(rs.getString("RCHTOTPO"));
                // p.setRCHUSER(rs.getString("RCHUSER"));
                p.setRCHJBNO(rs.getString("USERS"));
                p.setRCHSTS(rs.getString("RCHSTS"));
                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public List<RCMBPH> findRCMHead110(String wh, String sp, String cn, String dt, String uid) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPH> productList = new ArrayList<RCMBPH>();
        String sql = "SELECT TOP 1 (SELECT SUM(RCHTOTINV) FROM [RMShipment].[dbo].[RCMBPH] WHERE  [RCHWHSE] = RCH.[RCHWHSE] AND [RCHSPNO] = RCH.[RCHSPNO] AND [RCHCUNO] = RCH.[RCHCUNO] AND [RCHTPDT] = RCH.[RCHTPDT]) AS RCHTOTINV \n"
                + ",(SELECT SUM([RCHTOTQTY]) FROM [RMShipment].[dbo].[RCMBPH] WHERE  [RCHWHSE] = RCH.[RCHWHSE] AND [RCHSPNO] = RCH.[RCHSPNO] AND [RCHCUNO] = RCH.[RCHCUNO] AND [RCHTPDT] = RCH.[RCHTPDT]) AS RCHTOTQTY \n"
                + ",(SELECT SUM([RCHTOTPO]) FROM [RMShipment].[dbo].[RCMBPH] WHERE  [RCHWHSE] = RCH.[RCHWHSE] AND [RCHSPNO] = RCH.[RCHSPNO] AND [RCHCUNO] = RCH.[RCHCUNO] AND [RCHTPDT] = RCH.[RCHTPDT]) AS RCHTOTPO \n"
                + ",[RCHSTS],(SELECT TOP 1 USERS FROM MSSUSER WHERE USERID = ?) AS USERS\n"
                + "FROM [RMShipment].[dbo].[RCMBPH] RCH\n"
                + "WHERE [RCHWHSE] = ? AND [RCHSPNO] = ? AND [RCHCUNO] = ? AND [RCHTPDT] = ? AND ([RCHSTS] = 1 OR [RCHSTS] = 2)";

        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, uid);
            ps.setString(2, wh);
            ps.setString(3, sp);
            ps.setString(4, cn);
            ps.setString(5, dt);
            rs = ps.executeQuery();

            while (rs.next()) {
                RCMBPH p = new RCMBPH();
                p.setRCHTOTINV(rs.getString("RCHTOTINV"));
                p.setRCHTOTQTY(rs.getString("RCHTOTQTY"));
                p.setRCHTOTPO(rs.getString("RCHTOTPO"));
                // p.setRCHUSER(rs.getString("RCHUSER"));
                p.setRCHJBNO(rs.getString("USERS"));
                p.setRCHSTS(rs.getString("RCHSTS"));
                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public List<RCMBPD> findRCMDetailBy(String wh, String sp, String cn, String dt) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT F1.[RCDJBNO],F1.NUMB,F1.[RCDINVNO],F1.[RCDPONO],F1.[RCDCUSMAT],F1.[RCDPOLINE]\n"
                + ",(SELECT TOP 1 ISNULL([RCDPOPRC],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS [RCDPOPRC]\n"
                + ",(SELECT TOP 1 ISNULL([RCDINVPRC],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS [RCDINVPRC]\n"
                + ",ISNULL(F1.[RCDINVQTY],0) AS RCDINVQTY\n"
                + ",ISNULL(F1.QTYSTS,0) AS QTYSTS\n"
                + ",ISNULL(F1.RCDRCQTY,0) AS RCDRCQTY\n"
                + ",ISNULL(F1.RCDPOQTY,0) AS RCDPOQTY\n"
                + ",ISNULL(F1.RCDRATE,0) AS RCDRATE\n"
                + ",ISNULL(F1.RCSPOQTY,0) AS RCSPOQTY\n"
                + ",ISNULL(F1.RCDPOQTY + (F1.RCDPOQTY*(F1.RCDRATE/100)),0) AS canReceive,ISNULL(F1.RCSPOQTY + F1.RCDINVQTY,0) AS getReceive\n"
                + ",(SELECT TOP 1 ISNULL([RCDDUDATE],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS DUEDATE\n"
                + ",(SELECT TOP 1 ISNULL([RCDTPDT],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS DLIDATE\n"
                + ",(SELECT TOP 1 ISNULL([RCDSUM],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS UMSUP\n"
                + ",(SELECT TOP 1 ISNULL([RCDBUM],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS UMSTD\n"
                + ",F1.QTYPK\n"
                + ",(SELECT TOP 1 ISNULL([RCDMCTRL],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS MATCT\n"
                + ",(SELECT TOP 1 ISNULL([USERS],'') FROM [RMShipment].[dbo].[MSSUSER] WHERE [USERID] = (SELECT TOP 1 ISNULL([QUSUSERID],'') FROM [RMShipment].[dbo].[QUSMTCTRL] WHERE [QUSMTCTRL] = (SELECT TOP 1 ISNULL([RCDMCTRL],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]))) AS MATCTNAME\n"
                + ",(SELECT TOP 1 COUNT(DISTINCT [RCDPONO]) AS CNTPO FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO]=F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) as CNTPO\n"
                + ",(SELECT TOP 1 COUNT(DISTINCT [RCDCUSMAT]) AS CNTMAT FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO]=F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) as CNTMAT\n"
                + ",(SELECT TOP 1 COUNT([RCDCUSMAT]) AS CNTMAT FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO]=F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) as PACK\n"
                + ",(SELECT TOP 1 [RCDPKTYPE] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDCUSMAT] = F1.[RCDCUSMAT]) AS RCDPKTYPE\n"
                + ",(SELECT TOP 1 SUM([RCDINVAMT]) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDINVAMT\n"
                + ",(SELECT TOP 1 ISNULL([RCDAUTHO],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO] ORDER BY RCDAUTHO DESC ) AS RCDAUTHO\n"
                + ",(SELECT TOP 1 ISNULL([RCDMATCD],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDCUSMAT] = F1.[RCDCUSMAT]) AS RCDMATCD\n"
                // + ",(SELECT TOP 1 ISNULL([RCDMATCD],'') FROM [RMShipment].[dbo].[RCMBPD]
                // WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND
                // [RCDJBNO] = F1.[RCDJBNO]) AS RCDMATCD\n"
                + ",(SELECT TOP 1 [RCDSTS] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND RCDPONO = F1.RCDPONO AND RCDCUSMAT = F1.RCDCUSMAT AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDSTS\n"
                + ",(SELECT TOP 1 ISNULL([RCDPOSTDATE],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDPOSTDATE\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN RCDCUSMAT = [RCDMATCD] THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDMATCD]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDMATCD]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF ASC) AS NOPEor\n"
                + ""
                + ",(SELECT TOP 1 FF2.DDDD FROM (\n"
                + "	SELECT (SELECT TOP 1 CASE WHEN CAST((LEFT([RCDDUDATE],4)+LEFT(RIGHT([RCDDUDATE],4),2)) AS INT) > CAST((LEFT([RCDTPDT],4)+LEFT(RIGHT([RCDTPDT],4),2))AS INT) THEN 'N' ELSE 'Y' END FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS DDDD\n"
                + "	FROM (\n"
                + "		SELECT [RCDINVNO],[RCDCUSMAT],[RCDPOLINE]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY DDDD ASC) AS REDBTN\n"
                + ""
                + ",(SELECT TOP 1 CASE WHEN (FF1.RCQTY > 0 AND FF1.RCDSTS != 0) THEN 'Y' ELSE 'N' END AS RCSS FROM (\n"
                + "	SELECT SUM(DET.[RCDRCQTY]) AS RCQTY,DET.[RCDINVNO],DET.[RCDCUSMAT],DET.[RCDPOLINE],[RCDSTS]\n"
                + "	FROM [RMShipment].[dbo].[RCMBPD] DET\n"
                + "		WHERE DET.[RCDJBNO] = F1.[RCDJBNO]\n"
                + "	GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDSTS]\n"
                + ") FF1 WHERE FF1.RCDINVNO = F1.RCDINVNO ORDER BY RCSS ASC) AS RC \n"
                + ""
                + ",ISNULL((SELECT SUM(ISNULL(RCDINVQTY,0)) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO] AND (RCDRJFLAG != 'R' OR RCDRJFLAG IS NULL)),0) AS SUMINVQTY\n"
                //+ ",(SELECT SUM(ISNULL(RCDINVQTY,0)) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]) AS SUMINVQTY\n"
                + ",(SELECT SUM(ISNULL(RCDRCQTY,0)) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]) AS SUMRCQTY\n"
                + "\n"
                + ",ISNULL((SELECT TOP 1 RCDTOTAL FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDTOTAL\n"
                + ",ISNULL((SELECT TOP 1 RCDMAX FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDMAX\n"
                + ",ISNULL((SELECT TOP 1 RCDQSTS FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDQSTS\n"
                + ",ISNULL((SELECT TOP 1 RCDPC FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDPC\n"
                + ",RCDPOFLAG,[RCDRJFLAG],[RCDINVREF],[RCDMATREF]"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDRJFLAG] = 'R' THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF ASC) AS ISRJ\n"
                + "\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDRJFLAG] = 'C' THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF DESC) AS ISCUS\n"
                + "\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDRJFLAG] = 'P' THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF DESC) AS ISPRI ,F1.RCDPLANT\n"
                + "\n"
                + "\n"
                + "FROM (\n"
                + "	SELECT DENSE_RANK() OVER(ORDER BY [RCDINVNO]) AS NUMB,[RCDINVNO],[RCDPONO],[RCDCUSMAT],[RCDPOLINE]\n"
                + "	,(CASE WHEN [RCDRJFLAG] = 'R' THEN 0 ELSE SUM(ISNULL([RCDINVQTY],0)) END) AS [RCDINVQTY]\n"
                + "     ,ISNULL([RCDTTQTY],0) - ISNULL([RCDCMXQTY],0) AS QTYSTS\n" //THIS CALCULATE
                + "	,SUM(ISNULL(RCDRCQTY,0)) AS RCDRCQTY\n"
                + "	,COUNT(1) AS QTYPK\n"
                + "     ,[RCDJBNO],RCDPOFLAG,[RCDRJFLAG],[RCDINVREF],[RCDMATREF],[RCDPLANT],RCDPOQTY,RCDRATE\n"
                + "     ,ISNULL((SELECT TOP 1 RCSPOQTY FROM [RMShipment].[dbo].[RCMPOS] POD WHERE POD.RCSPONO = (LEFT(RCDPONO, CASE WHEN charindex(' ', RCDPONO) = 0 THEN LEN(RCDPONO) ELSE charindex(' ', RCDPONO) - 1 END)) AND POD.RCSLINE = RIGHT(CONCAT('0000',RCDPOLINE),5) AND POD.RCSMATCODE = RCDCUSMAT),0) AS RCSPOQTY\n"
                + "\n"
                + "	FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "	WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH]\n"
                + "	WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn
                + "' AND [RCHTPDT] = '" + dt + "')\n"
                + "	GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],RCDPOFLAG,[RCDRJFLAG],[RCDINVREF],[RCDMATREF],[RCDPLANT],RCDPOQTY,RCDRATE,[RCDTTQTY],[RCDCMXQTY]\n"
                + ") F1  ORDER BY RCDINVNO,RCDPONO ";

        try {

//             System.out.println(sql);
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDJBNO(rs.getString("RCDJBNO"));
                p.setRCDINVNO(rs.getString("RCDINVNO"));

                if (rs.getString("RCDPONO") != null) {
                    p.setRCDPONO(rs.getString("RCDPONO").split(" ")[0]);
                } else {
                    p.setRCDPONO("");
                }

                p.setRCDPOLINE(rs.getString("RCDPOLINE"));
                p.setRCDDLNO(rs.getString("NUMB"));
                // p.setRCDID(rs.getString("RCDID"));

                p.setRCDITNO(rs.getString("SUMINVQTY")); // SUMINVQTY
                p.setRCDITDS(rs.getString("SUMRCQTY")); // SUMRCQTY

                if (rs.getString("RCDCUSMAT") != null) {
                    p.setRCDCUSMAT(rs.getString("RCDCUSMAT").trim());
                } else {
                    p.setRCDCUSMAT("");
                }

                p.setRCDSUM(rs.getString("UMSUP"));
                p.setRCDDLDATE(rs.getString("DLIDATE"));
                // p.setRCDPKQTY(rs.getString("RCDPKQTY"));
                p.setRCDINVQTY(rs.getString("RCDINVQTY"));
                // p.setRCDPALNO(rs.getString("RCDPALNO"));
                p.setRCDBOXNO(rs.getString("NOPEor"));
                p.setRCDINVPRC(rs.getString("RCDINVPRC"));
                p.setRCDDUDATE(rs.getString("DUEDATE"));

                if (rs.getString("RCDMATCD") != null) {
                    p.setRCDMATCD(rs.getString("RCDMATCD").trim());
                } else {
                    p.setRCDMATCD("");
                }

                // p.setRCDMATDES(rs.getString("RCDMATDES"));
                p.setRCDAUTHO(rs.getString("RCDAUTHO"));
                p.setRCDBUM(rs.getString("UMSTD"));

                if (rs.getString("MATCTNAME") != null) {
                    if (!rs.getString("MATCTNAME").equals("")) {
                        p.setRCDMCTRL(rs.getString("MATCT") + " : " + rs.getString("MATCTNAME").split(" ")[0]);
                    } else {
                        p.setRCDMCTRL(rs.getString("MATCT") + " : " + rs.getString("MATCTNAME"));
                    }
                } else {
                    p.setRCDMCTRL("");
                }

                p.setRCDPOPRC(rs.getString("RCDPOPRC"));
                p.setRCDPOQTY(rs.getString("RCDPOQTY"));
                p.setRCDRCQTY(rs.getString("RCDRCQTY"));
                p.setRCDTTQTY(rs.getString("RC")); // RCSTS
                p.setRCDRATE(rs.getString("RCDRATE"));
                p.setRCDCMXQTY(rs.getString("QTYSTS")); // THIS CALCULATE
                // p.setRCDCPERC(rs.getString("RCDCPERC"));
                p.setRCDSTS(rs.getString("RCDSTS"));
                p.setRCDEDT(rs.getString("QTYPK"));
                // p.setRCDETM(rs.getString("RCDETM"));
                p.setRCDCDT(rs.getString("REDBTN"));
                if (rs.getString("RCDPONO") != null) {
                    p.setRCDCTM(rs.getString("RCDPONO").split(" ")[0]); // PO
                } else {
                    p.setRCDCTM(""); // PO
                }

                int spaceCount = 0;
                for (char c : rs.getString("RCDPONO").trim().toCharArray()) {
                    if (c == ' ') {
                        spaceCount++;
                    }
                }
                if (spaceCount == 2) {
                    p.setFULLPONO(rs.getString("RCDPONO").split(" ")[2].trim());
                } else if (spaceCount == 1) {
                    p.setFULLPONO("1000");
                } else if (spaceCount == 0) {
                    p.setFULLPONO("1000");
                }
                p.setRCDPLANT(rs.getString("RCDPLANT"));
                // p.setRCDUSER(rs.getString("RCDUSER"));
                // p.setRCDQRCODE(rs.getString("RCDQRCODE"));
                p.setCNTPO(rs.getString("CNTPO"));
                p.setCNTMAT(rs.getString("CNTMAT"));
                p.setPACK(rs.getString("PACK"));
                p.setSUMM(rs.getString("RCDINVAMT"));
                p.setRCDPKTYPE(rs.getString("RCDPKTYPE"));
                p.setRCDPOSTDATE(rs.getString("RCDPOSTDATE"));
                p.setRCDTOTAL(rs.getString("RCDTOTAL"));
                p.setRCDMAX(rs.getString("RCDMAX"));
                p.setRCDQSTS(rs.getString("RCDQSTS"));
                p.setRCDPC(rs.getString("RCDPC"));
                p.setRCDPOFLAG(rs.getString("RCDPOFLAG"));
                p.setRCDRJFLAG(rs.getString("RCDRJFLAG"));

                if (rs.getString("ISRJ").equals("Y") || rs.getString("ISCUS").equals("Y")
                        || rs.getString("ISPRI").equals("Y")) {
                    p.setISFLAG("Y");
                } else {
                    p.setISFLAG("N");
                }

                if (rs.getString("RCDINVREF") != null) {
                    p.setRCDINVREF(rs.getString("RCDINVREF").trim());
                } else {
                    p.setRCDINVREF("");
                }

                if (rs.getString("RCDMATREF") != null) {
                    p.setRCDMATREF(rs.getString("RCDMATREF").trim());
                } else {
                    p.setRCDMATREF("");
                }

                p.setRCSPOQTY(rs.getString("RCSPOQTY"));
                p.setCanReceive(rs.getString("canReceive"));
                p.setGetReceive(rs.getString("getReceive"));

                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public List<RCMBPD> findRCMDetailBy110(String wh, String sp, String cn, String dt) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPD> productList = new ArrayList<RCMBPD>();

        String sql = "SELECT F1.[RCDJBNO],F1.NUMB,F1.[RCDINVNO],F1.[RCDPONO],F1.[RCDCUSMAT],F1.[RCDPOLINE]\n"
                + ",(SELECT TOP 1 ISNULL([RCDPOPRC],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS [RCDPOPRC]\n"
                + ",(SELECT TOP 1 ISNULL([RCDINVPRC],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS [RCDINVPRC]\n"
                + ",ISNULL(F1.[RCDINVQTY],0) AS RCDINVQTY\n"
                + ",ISNULL(F1.QTYSTS,0) AS QTYSTS\n"
                + ",ISNULL(F1.RCDRCQTY,0) AS RCDRCQTY\n"
                + ",ISNULL(F1.RCDPOQTY,0) AS RCDPOQTY\n"
                + ",ISNULL(F1.RCDRATE,0) AS RCDRATE\n"
                + ",ISNULL(F1.RCSPOQTY,0) AS RCSPOQTY\n"
                + ",ISNULL(F1.RCDPOQTY + (F1.RCDPOQTY*(F1.RCDRATE/100)),0) AS canReceive,ISNULL(F1.RCSPOQTY + F1.RCDINVQTY,0) AS getReceive\n"
                + ",(SELECT TOP 1 ISNULL([RCDDUDATE],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS DUEDATE\n"
                + ",(SELECT TOP 1 ISNULL([RCDTPDT],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS DLIDATE\n"
                + ",(SELECT TOP 1 ISNULL([RCDSUM],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS UMSUP\n"
                + ",(SELECT TOP 1 ISNULL([RCDBUM],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS UMSTD\n"
                + ",F1.QTYPK\n"
                + ",(SELECT TOP 1 ISNULL([RCDMCTRL],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS MATCT\n"
                + ",(SELECT TOP 1 ISNULL([USERS],'') FROM [RMShipment].[dbo].[MSSUSER] WHERE [USERID] = (SELECT TOP 1 ISNULL([QUSUSERID],'') FROM [RMShipment].[dbo].[QUSMTCTRL] WHERE [QUSMTCTRL] = (SELECT TOP 1 ISNULL([RCDMCTRL],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]))) AS MATCTNAME\n"
                + ",(SELECT TOP 1 COUNT(DISTINCT [RCDPONO]) AS CNTPO FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO]=F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) as CNTPO\n"
                + ",(SELECT TOP 1 COUNT(DISTINCT [RCDCUSMAT]) AS CNTMAT FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO]=F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) as CNTMAT\n"
                + ",(SELECT TOP 1 COUNT([RCDCUSMAT]) AS CNTMAT FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO]=F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) as PACK\n"
                + ",(SELECT TOP 1 [RCDPKTYPE] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDCUSMAT] = F1.[RCDCUSMAT]) AS RCDPKTYPE\n"
                + ",(SELECT TOP 1 SUM([RCDINVAMT]) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDINVAMT\n"
                + ",(SELECT TOP 1 ISNULL([RCDAUTHO],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO] ORDER BY RCDAUTHO DESC ) AS RCDAUTHO\n"
                + ",(SELECT TOP 1 ISNULL([RCDMATCD],'') FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDCUSMAT] = F1.[RCDCUSMAT]) AS RCDMATCD\n"
                // + ",(SELECT TOP 1 ISNULL([RCDMATCD],'') FROM [RMShipment].[dbo].[RCMBPD]
                // WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND
                // [RCDJBNO] = F1.[RCDJBNO]) AS RCDMATCD\n"
                + ",(SELECT TOP 1 [RCDSTS] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND RCDPONO = F1.RCDPONO AND RCDCUSMAT = F1.RCDCUSMAT AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDSTS\n"
                + ",(SELECT TOP 1 ISNULL([RCDPOSTDATE],0) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDPOSTDATE\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN RCDCUSMAT = [RCDMATCD] THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDMATCD]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDMATCD]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF ASC) AS NOPEor\n"
                + ""
                + ",(SELECT TOP 1 FF2.DDDD FROM (\n"
                + "	SELECT (SELECT TOP 1 CASE WHEN CAST((LEFT([RCDDUDATE],4)+LEFT(RIGHT([RCDDUDATE],4),2)) AS INT) > CAST((LEFT([RCDTPDT],4)+LEFT(RIGHT([RCDTPDT],4),2))AS INT) THEN 'N' ELSE 'Y' END FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDCUSMAT] = F1.[RCDCUSMAT] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDINVNO] = F1.[RCDINVNO] AND [RCDJBNO] = F1.[RCDJBNO]) AS DDDD\n"
                + "	FROM (\n"
                + "		SELECT [RCDINVNO],[RCDCUSMAT],[RCDPOLINE]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY DDDD ASC) AS REDBTN\n"
                + ""
                + ",(SELECT TOP 1 CASE WHEN (FF1.RCQTY > 0 AND FF1.RCQTY = FF1.INVQTY AND FF1.RCDSTS != 0) THEN 'Y' ELSE 'N' END AS RCSS FROM (\n" // this
                // new
                // edit
                // + ",(SELECT TOP 1 CASE WHEN (FF1.RCQTY > 0 AND FF1.RCDSTS != 0) THEN 'Y' ELSE
                // 'N' END AS RCSS FROM (\n" //this old if error use this line (save zone)
                + "	SELECT SUM(DET.[RCDRCQTY]) AS RCQTY,SUM(DET.[RCDINVQTY]) AS INVQTY,DET.[RCDINVNO],DET.[RCDCUSMAT],DET.[RCDPOLINE],[RCDSTS]\n"
                + "	FROM [RMShipment].[dbo].[RCMBPD] DET\n"
                + "		WHERE DET.[RCDJBNO] = F1.[RCDJBNO]\n"
                + "	GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDSTS]\n"
                // + ") FF1 WHERE FF1.RCDINVNO = F1.RCDINVNO ORDER BY RCSS DESC) AS RC \n"                          //this old if error use this line (save zone)
                + ") FF1 WHERE FF1.RCDINVNO = F1.RCDINVNO ORDER BY RCSS ASC) AS RC \n" // this new edit
                + ""
                + ",ISNULL((SELECT SUM(ISNULL(RCDINVQTY,0)) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO] AND (RCDRJFLAG != 'R' OR RCDRJFLAG IS NULL)),0) AS SUMINVQTY\n"
                // + ",(SELECT SUM(ISNULL(RCDINVQTY,0)) FROM [RMShipment].[dbo].[RCMBPD] WHERE
                // [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]) AS SUMINVQTY\n"
                + ",(SELECT SUM(ISNULL(RCDRCQTY,0)) FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]) AS SUMRCQTY\n"
                + "\n"
                + ",ISNULL((SELECT TOP 1 RCDTOTAL FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDTOTAL\n"
                + ",ISNULL((SELECT TOP 1 RCDMAX FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDMAX\n"
                + ",ISNULL((SELECT TOP 1 RCDQSTS FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDQSTS\n"
                + ",ISNULL((SELECT TOP 1 RCDPC FROM [RMShipment].[dbo].[RCMPOD] POD WHERE POD.RCDPONO = (LEFT(F1.RCDPONO, CASE WHEN charindex(' ', F1.RCDPONO) = 0 THEN LEN(F1.RCDPONO) ELSE charindex(' ', F1.RCDPONO) - 1 END)) AND POD.RCDLINE = RIGHT(CONCAT('0000',F1.RCDPOLINE),5) AND POD.RCDMATCODE = F1.RCDCUSMAT),0) AS RCDPC\n"
                + ",RCDPOFLAG,[RCDRJFLAG],[RCDINVREF],[RCDMATREF]"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDRJFLAG] = 'R' THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF ASC) AS ISRJ\n"
                + "\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDRJFLAG] = 'C' THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF DESC) AS ISCUS\n"
                + "\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDRJFLAG] = 'P' THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDRJFLAG]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF DESC) AS ISPRI ,F1.[RCDPLANT]\n"
                + ",(SELECT TOP 1 FF2.FFFF FROM (\n"
                + "	SELECT CASE WHEN [RCDINVPRC] = [RCDPOPRC] THEN 'Y' ELSE 'N' END AS  FFFF\n"
                + "	FROM (\n"
                + "		SELECT [RCDJBNO],[RCDINVNO],[RCDINVPRC],[RCDPOPRC]\n"
                + "		FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "		WHERE [RCDJBNO] = F1.[RCDJBNO] AND [RCDINVNO] = F1.[RCDINVNO]\n"
                + "		GROUP BY [RCDJBNO],[RCDINVNO],[RCDINVPRC],[RCDPOPRC]\n"
                + "	) FF1\n"
                + ") FF2 ORDER BY FFFF ASC) AS PRCCompare \n"
                + "\n"
                + "\n"
                + "\n"
                + "FROM (\n"
                + "	SELECT DENSE_RANK() OVER(ORDER BY [RCDINVNO]) AS NUMB,[RCDINVNO],[RCDPONO],[RCDCUSMAT],[RCDPOLINE]\n"
                + "	,(CASE WHEN [RCDRJFLAG] = 'R' THEN 0 ELSE SUM(ISNULL([RCDINVQTY],0)) END) AS [RCDINVQTY]\n"
                + "     ,ISNULL([RCDTTQTY],0) - ISNULL([RCDCMXQTY],0) AS QTYSTS\n" // THIS CALCULATE
                + "	,SUM(ISNULL(RCDRCQTY,0)) AS RCDRCQTY\n"
                + "	,COUNT(1) AS QTYPK\n"
                + "     ,[RCDJBNO],RCDPOFLAG,[RCDRJFLAG],[RCDINVREF],[RCDMATREF],[RCDPLANT],RCDPOQTY,RCDRATE\n"
                + "     ,ISNULL((SELECT TOP 1 RCSPOQTY FROM [RMShipment].[dbo].[RCMPOS] POD WHERE POD.RCSPONO = (LEFT(RCDPONO, CASE WHEN charindex(' ', RCDPONO) = 0 THEN LEN(RCDPONO) ELSE charindex(' ', RCDPONO) - 1 END)) AND POD.RCSLINE = RIGHT(CONCAT('0000',RCDPOLINE),5) AND POD.RCSMATCODE = RCDCUSMAT),0) AS RCSPOQTY\n"
                + "\n"
                + "	FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "	WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH]\n"
                + "	WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn
                + "' AND [RCHTPDT] = '" + dt + "') AND (RCDSTS = 1 OR RCDSTS = 2)\n"
                + "	GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],RCDPOFLAG,[RCDRJFLAG],[RCDINVREF],[RCDMATREF],[RCDPLANT],RCDPOQTY,RCDRATE,[RCDTTQTY],[RCDCMXQTY]\n"
                + ") F1  ORDER BY RCDINVNO,RCDPONO ";

        try {
//             System.out.println(sql);
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDJBNO(rs.getString("RCDJBNO"));
                p.setRCDINVNO(rs.getString("RCDINVNO"));

                if (rs.getString("RCDPONO") != null) {
                    p.setRCDPONO(rs.getString("RCDPONO").split(" ")[0]);
                } else {
                    p.setRCDPONO("");
                }

                p.setRCDPOLINE(rs.getString("RCDPOLINE"));
                p.setRCDDLNO(rs.getString("NUMB"));
                // p.setRCDID(rs.getString("RCDID"));

                p.setRCDITNO(rs.getString("SUMINVQTY")); // SUMINVQTY
                p.setRCDITDS(rs.getString("SUMRCQTY")); // SUMRCQTY

                if (rs.getString("RCDCUSMAT") != null) {
                    p.setRCDCUSMAT(rs.getString("RCDCUSMAT").trim());
                } else {
                    p.setRCDCUSMAT("");
                }

                p.setRCDSUM(rs.getString("UMSUP"));
                p.setRCDDLDATE(rs.getString("DLIDATE"));
                // p.setRCDPKQTY(rs.getString("RCDPKQTY"));
                p.setRCDINVQTY(rs.getString("RCDINVQTY"));
                // p.setRCDPALNO(rs.getString("RCDPALNO"));
                p.setRCDBOXNO(rs.getString("NOPEor"));
                p.setRCDINVPRC(rs.getString("RCDINVPRC"));
                p.setRCDDUDATE(rs.getString("DUEDATE"));

                if (rs.getString("RCDMATCD") != null) {
                    p.setRCDMATCD(rs.getString("RCDMATCD").trim());
                } else {
                    p.setRCDMATCD("");
                }

                // p.setRCDMATDES(rs.getString("RCDMATDES"));
                p.setRCDAUTHO(rs.getString("RCDAUTHO"));
                p.setRCDBUM(rs.getString("UMSTD"));

                if (rs.getString("MATCTNAME") != null) {
                    if (!rs.getString("MATCTNAME").equals("")) {
                        p.setRCDMCTRL(rs.getString("MATCT") + " : " + rs.getString("MATCTNAME").split(" ")[0]);
                    } else {
                        p.setRCDMCTRL(rs.getString("MATCT") + " : " + rs.getString("MATCTNAME"));
                    }
                } else {
                    p.setRCDMCTRL("");
                }

                p.setRCDPOPRC(rs.getString("RCDPOPRC"));
                p.setRCDPOQTY(rs.getString("RCDPOQTY"));
                p.setRCDRCQTY(rs.getString("RCDRCQTY"));
                p.setRCDTTQTY(rs.getString("RC")); // RCSTS
                p.setRCDRATE(rs.getString("RCDRATE"));
                p.setRCDCMXQTY(rs.getString("QTYSTS")); // THIS CALCULATE
                p.setRCDCPERC(rs.getString("PRCCompare")); // price compare
                p.setRCDSTS(rs.getString("RCDSTS"));
                p.setRCDEDT(rs.getString("QTYPK"));
                // p.setRCDETM(rs.getString("RCDETM"));
                p.setRCDCDT(rs.getString("REDBTN"));
                if (rs.getString("RCDPONO") != null) {
                    p.setRCDCTM(rs.getString("RCDPONO").split(" ")[0]); // PO
                } else {
                    p.setRCDCTM(""); // PO
                }

                int spaceCount = 0;
                for (char c : rs.getString("RCDPONO").trim().toCharArray()) {
                    if (c == ' ') {
                        spaceCount++;
                    }
                }

                if (spaceCount == 2) {
                    p.setFULLPONO(rs.getString("RCDPONO").split(" ")[2].trim());
                } else if (spaceCount == 1) {
                    p.setFULLPONO("1000");
                } else if (spaceCount == 0) {
                    p.setFULLPONO("1000");
                }
                p.setRCDPLANT(rs.getString("RCDPLANT"));
                // p.setRCDUSER(rs.getString("RCDUSER"));
                // p.setRCDQRCODE(rs.getString("RCDQRCODE"));
                p.setCNTPO(rs.getString("CNTPO"));
                p.setCNTMAT(rs.getString("CNTMAT"));
                p.setPACK(rs.getString("PACK"));
                p.setSUMM(rs.getString("RCDINVAMT"));
                p.setRCDPKTYPE(rs.getString("RCDPKTYPE"));
                p.setRCDPOSTDATE(rs.getString("RCDPOSTDATE"));
                p.setRCDTOTAL(rs.getString("RCDTOTAL"));
                p.setRCDMAX(rs.getString("RCDMAX"));
                p.setRCDQSTS(rs.getString("RCDQSTS"));
                p.setRCDPC(rs.getString("RCDPC"));
                p.setRCDPOFLAG(rs.getString("RCDPOFLAG"));
                p.setRCDRJFLAG(rs.getString("RCDRJFLAG"));

                // if (rs.getString("ISCUS").equals("Y") || rs.getString("ISPRI").equals("Y")) {
                if (rs.getString("ISRJ").equals("Y") || rs.getString("ISCUS").equals("Y")
                        || rs.getString("ISPRI").equals("Y")) {
                    p.setISFLAG("Y");
                } else {
                    p.setISFLAG("N");
                }

                if (rs.getString("RCDINVREF") != null) {
                    p.setRCDINVREF(rs.getString("RCDINVREF").trim());
                } else {
                    p.setRCDINVREF("");
                }

                if (rs.getString("RCDMATREF") != null) {
                    p.setRCDMATREF(rs.getString("RCDMATREF").trim());
                } else {
                    p.setRCDMATREF("");
                }

                p.setRCSPOQTY(rs.getString("RCSPOQTY"));
                p.setCanReceive(rs.getString("canReceive"));
                p.setGetReceive(rs.getString("getReceive"));

                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public List<RCMBPD> findRCMDetailByINV(String wh, String sp, String cn, String dt, String inv) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPD> productList = new ArrayList<RCMBPD>();
        String sql = "SELECT *\n"
                + "	FROM [RMShipment].[dbo].[RCMBPD]\n"
                + "	WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH]\n"
                + "	WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn
                + "' AND [RCHTPDT] = '" + dt + "')\n"
                + "	AND RCDINVNO = '" + inv
                + "' AND RCDSTS = 1 AND (RCDRJFLAG != 'R' OR RCDRJFLAG IS NULL ) AND RCDRCQTY > 0 ";

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDCONO(rs.getString("RCDCONO"));
                p.setRCDJBNO(rs.getString("RCDJBNO"));
                p.setRCDINVNO(rs.getString("RCDINVNO"));
                p.setRCDPONO(rs.getString("RCDPONO"));
                p.setRCDPOLINE(rs.getString("RCDPOLINE"));
                p.setRCDDLNO(rs.getString("RCDDLNO"));
                p.setRCDID(rs.getString("RCDID"));
                p.setRCDITNO(rs.getString("RCDITNO"));
                p.setRCDITDS(rs.getString("RCDITDS"));
                p.setRCDCUSMAT(rs.getString("RCDCUSMAT"));
                p.setRCDSUM(rs.getString("RCDSUM"));
                p.setRCDDLDATE(rs.getString("RCDTPDT"));
                p.setRCDPKQTY(rs.getString("RCDPKQTY"));
                p.setRCDINVQTY(rs.getString("RCDINVQTY"));
                p.setRCDPALNO(rs.getString("RCDPALNO"));
                p.setRCDBOXNO(rs.getString("RCDBOXNO"));
                p.setRCDINVPRC(rs.getString("RCDINVPRC"));
                p.setRCDDUDATE(rs.getString("RCDDUDATE"));
                p.setRCDMATCD(rs.getString("RCDMATCD"));
                p.setRCDMATDES(rs.getString("RCDMATDES"));
                p.setRCDAUTHO(rs.getString("RCDAUTHO"));
                p.setRCDBUM(rs.getString("RCDBUM"));
                p.setRCDMCTRL(rs.getString("RCDMCTRL"));
                p.setRCDPOPRC(rs.getString("RCDPOPRC"));
                p.setRCDPOQTY(rs.getString("RCDPOQTY"));
                p.setRCDRCQTY(rs.getString("RCDRCQTY"));
                p.setRCDTTQTY(rs.getString("RCDTTQTY"));
                p.setRCDRATE(rs.getString("RCDRATE"));
                p.setRCDCMXQTY(rs.getString("RCDCMXQTY"));
                p.setRCDCPERC(rs.getString("RCDCPERC"));
                p.setRCDSTS(rs.getString("RCDSTS"));
                p.setRCDINVAMT(rs.getString("RCDINVAMT"));
                p.setRCDEDT(rs.getString("RCDEDT"));
                p.setRCDETM(rs.getString("RCDETM"));
                p.setRCDCDT(rs.getString("RCDCDT"));
                p.setRCDCTM(rs.getString("RCDCTM"));
                p.setRCDUSER(rs.getString("RCDUSER"));
                p.setRCDQRCODE(rs.getString("RCDQRCODE"));
                p.setRCDPKTYPE(rs.getString("RCDPKTYPE"));

                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public boolean insertQRMMASbyRCM(List<RCMBPD> ddetail, String uid, String inv, String wh, String sp, String cn,
            String dt) {

        boolean rs = false;

        try {

            StringBuilder sqlA = new StringBuilder();
            sqlA.append("INSERT INTO [RMShipment].[dbo].[QRMMAS] "
                    + "([QRMCOM],[QRMID],[QRMCODE],[QRMVAL],[QRMROLL],[QRMIDTEMP],[QRMDESC],[QRMPO],[QRMTAX],[QRMPACKTYPE]\n"
                    + ",[QRMQTY],[QRMALQTY],[QRMBUN],[QRMAUN],[QRMCVFAC],[QRMCVFQTY],[QRMSTS],[QRMPARENT],[QRMPDDATE],[QRMISDATE]\n"
                    + ",[QRMRCDATE],[QRMTFDATE],[QRMLOTNO],[QRMDYNO],[QRMQIS],[QRMWHSE],[QRMZONE],[QRMPALLET],[QRMRACKNO],[QRMSIDE]\n"
                    + ",[QRMROW],[QRMCOL],[QRMLOC],[QRMPLANT],[QRMSTORAGE],[QRMBARCODE],[QRMSUPCODE],[QRMEDT],[QRMCDT],[QRMUSER]\n"
                    + ",[QRMLCUPDT],[QRMQRCODE]) VALUES ");

            StringBuilder sqlB = new StringBuilder();
            sqlB.append("INSERT INTO [RMShipment].[dbo].[QRMTRA]"
                    + "([QRMCOM],[QRMWHSE],[QRMCODE],[QRMVAL],[QRMPLANT],[QRMSTORAGE],[QRMID],[QRMROLL],[QRMTRDT]\n"
                    + ",[QRMTRTM],[QRMMVT],[QRMTRT],[QRMDOCNO],[QRMSAPNO],[QRMQTY],[QRMBUN],[QRMCDT],[QRMUSER]) VALUES ");

            for (int i = 0; i < ddetail.size(); i++) {

                String rcdid = ddetail.get(i).getRCDID();
                String cusmat = ddetail.get(i).getRCDCUSMAT();
                String desc = ddetail.get(i).getRCDMATDES();
                String po = ddetail.get(i).getRCDPONO();
                String boxno = ddetail.get(i).getRCDBOXNO();

                String invno = ddetail.get(i).getRCDINVNO();
                String packtype = ddetail.get(i).getRCDPKTYPE();

                String invqty = ddetail.get(i).getRCDINVQTY();
                String qr = ddetail.get(i).getRCDQRCODE();

                String bum = ddetail.get(i).getRCDBUM();

                if (invqty == null) {
                    invqty = "NULL";
                } else {
                    if (invqty.equals("")) {
                        invqty = "NULL";
                    }
                }
                if (boxno == null) {
                    boxno = "NULL";
                } else {
                    if (boxno.equals("")) {
                        boxno = "NULL";
                    }
                }

                String plant = "";

                int spaceCount = po.substring(0, po.length() - 1).replaceAll("[^ ]", "").length();

                if (spaceCount > 1) {
                    plant = po.split(" ")[2].trim();
                } else {
                    plant = "1000";
                }

                String pallet = ddetail.get(i).getRCDPALNO();

                // ------------------ sqlA --------------------- QRMMAS
                sqlA.append("('TWC',").append(checkNull(rcdid)).append(",").append(checkNull(cusmat)).append(",'01',")
                        .append(boxno).append(",NULL,").append(checkNull(desc)).append(",").append(checkNull(po))
                        .append(",").append(checkNull(cusmat.substring(0, 2) + invno)).append(",")
                        .append(checkNull(packtype)).append("\n").append(",").append(invqty).append(",").append(invqty)
                        .append(",'").append(bum).append("','").append(bum).append("',1,1,'2',NULL,")
                        .append(checkNull(qr.split("\\|")[11])).append(",NULL\n"
                // ).append(",").append(invqty).append(",").append(invqty).append(",(SELECT TOP
                // 1 SAPBUN FROM SAPMAS WHERE SAPMAT = '").append(cusmat).append("'),(SELECT TOP
                // 1 SAPBUN FROM SAPMAS WHERE SAPMAT =
                // '").append(cusmat).append("'),1,1,'0',NULL,").append(checkNull(qr.split("\\|")[11])).append(",NULL\n"
                ).append(",FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd'),NULL,").append(checkNull(qr.split("\\|")[17]))
                        .append(",NULL,NULL,").append(checkNull(wh)).append(",NULL,").append(checkNull(pallet))
                        .append(",NULL,NULL\n").append(",NULL,NULL,NULL,").append(checkNull(plant))
                        .append(",'RM04',NULL,").append(checkNull(qr.split("\\|")[16]))
                        .append(",CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,").append(checkNull(uid)).append("\n")
                        .append(",NULL,").append(checkNull(qr)).append(")");

                if (i != ddetail.size() - 1) {
                    sqlA.append(",");
                }

                // ------------------ sqlB --------------------- QRMTRA
                sqlB.append("('TWC',").append(checkNull(wh)).append(",").append(checkNull(cusmat)).append(",'01',")
                        .append(checkNull(plant)).append(",'RM04',").append(checkNull(rcdid)).append(",").append(boxno)
                        .append(",FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd')\n")
                        .append(",CAST(CURRENT_TIMESTAMP AS time),'601','MB01',").append(checkNull(po)).append(",")
                        .append(checkNull(cusmat.substring(0, 2) + invno)).append(",").append(invqty).append(",'")
                        .append(bum).append("',CURRENT_TIMESTAMP,").append(checkNull(uid)).append(")\n");

                if (i != ddetail.size() - 1) {
                    sqlB.append(",");
                }

            }

            Statement stmtIns = null;
            stmtIns = connection.createStatement();

            int inssqlA = stmtIns.executeUpdate(sqlA.toString());
            int inssqlB = stmtIns.executeUpdate(sqlB.toString());

            if (inssqlA >= 1 && inssqlB >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            stmtIns.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                connect.close();
            }
        }

        return rs;
    }

    public boolean updateEditPoNo(String job, String inv, String poold, String ponew) {

        boolean rs = false;
        PreparedStatement ps = null;
        String poChge = "";

        try {
            Statement stmtS = connection.createStatement();
            String sqlSelectPONo = "SELECT TOP 1 RCDPONO FROM RCMBPD WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '"
                    + inv + "' AND RCDPONO LIKE '" + poold + "%'";

            ResultSet resset = stmtS.executeQuery(sqlSelectPONo);
            while (resset.next()) {
                String poFull = resset.getString("RCDPONO");
                poChge = poFull.replace(poold, ponew);
            }

            String sql = "UPDATE RCMBPD SET RCDPONO = '" + poChge + "' WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '"
                    + inv + "' AND RCDPONO LIKE '" + poold + "%'";

            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            stmtS.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean updatePriInv(String job, String inv) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRJFLAG = 'P' \n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' ";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean updateRejectInv(String job, String inv) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRJFLAG = 'R' \n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' ";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean updateClearRejectInv(String job, String inv) {

        boolean rs = true;

        String selDetailByJobInv = "SELECT * FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDJBNO = '" + job
                + "' AND RCDINVNO = '" + inv + "' AND RCDRJFLAG = 'R'";

        // System.out.println(selDetailByJobInv);
        try {
            Statement stmtS = connection.createStatement();
            Statement stmtIUD = connection.createStatement();
            ResultSet RsS = stmtS.executeQuery(selDetailByJobInv);

            while (RsS.next()) {

                String delSQL = "DELETE [RMShipment].[dbo].[RCMBPT] WHERE RCTJBNO = '" + job + "'  AND RCTCTID = '"
                        + RsS.getString("RCDID") + "' AND RCTCODE = '" + RsS.getString("RCDCUSMAT") + "' ";
                int del = stmtIUD.executeUpdate(delSQL);

                if (del == 1) {

                    String updBPD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRCQTY = 0 \n"
                            + "WHERE RCDPALNO = '" + RsS.getString("RCDPALNO") + "' AND RCDINVNO = '"
                            + RsS.getString("RCDINVNO") + "'  AND RCDID = '" + RsS.getString("RCDID")
                            + "' AND RCDCUSMAT = '" + RsS.getString("RCDCUSMAT") + "' AND\n"
                            + "[RCDJBNO] = '" + job + "' ";

                    // System.out.println(updBPD);
                    stmtIUD.executeUpdate(updBPD);

                }

            }

            stmtS.close();
            stmtIUD.close();

            Statement stmtSBPT = connection.createStatement();
            Statement stmtUpd = connection.createStatement();

            String bpt = "SELECT TOP 1 * FROM RCMBPT WHERE RCTJBNO = '" + job + "' ORDER BY RCTSCSEQ ";
            ResultSet rsGETBPT = stmtSBPT.executeQuery(bpt);

            String whSel = "";
            String spSel = "";
            String cnSel = "";
            String dlSel = "";

            while (rsGETBPT.next()) {
                whSel = rsGETBPT.getString("RCTWHSE");
                spSel = rsGETBPT.getString("RCTSPNO");
                cnSel = rsGETBPT.getString("RCTCUNO");
                dlSel = rsGETBPT.getString("RCTDLDATE");
            }

            String sql3 = "SELECT F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE,SUM(F1.RCTPACK) AS RCTPACK\n"
                    + "FROM (\n"
                    + "	SELECT RCTSCSEQ\n"
                    + "	,(CASE WHEN RCTPALNO IS NULL THEN RCTCTID ELSE RCTPALNO END) AS RCTCTID\n"
                    + "                ,(CASE WHEN RCTPALNO IS NULL THEN RCTCODE ELSE NULL END) AS RCTCODE\n"
                    + "	,RCTPACK FROM [RMShipment].[dbo].[RCMBPT] \n"
                    + "	WHERE [RCTWHSE] = '" + whSel + "' AND [RCTSPNO] = '" + spSel + "' AND [RCTCUNO] = '" + cnSel
                    + "' AND [RCTDLDATE] = '" + dlSel + "'\n"
                    + "	\n"
                    + ") F1\n"
                    + "\n"
                    + "GROUP BY F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE";

            ResultSet rs3 = stmtSBPT.executeQuery(sql3);

            // ResultSet rs = stmtS.executeQuery("SELECT * FROM [RMShipment].[dbo].[RCMBPT]
            // WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] =
            // '" + cn + "' AND [RCTDLDATE] = '" + dt + "' ORDER BY
            // RIGHT('00000'+RCTSCSEQ,5) ASC");
            int i = 1;
            while (rs3.next()) {

                String idORRai = rs3.getString("RCTCTID");

                if (idORRai.length() > 10) { // ID
                    idORRai = "[RCTCTID] = '" + idORRai.trim() + "' ";
                } else { // PALLET
                    idORRai = "[RCTPALNO] = '" + idORRai.trim() + "'";
                }

                String reSEQNo = "UPDATE [RMShipment].[dbo].[RCMBPT] SET RCTSCSEQ = '" + i + "' WHERE [RCTWHSE] = '"
                        + whSel + "' AND [RCTSPNO] = '" + spSel + "' AND [RCTCUNO] = '" + cnSel
                        + "' AND [RCTDLDATE] = '" + dlSel + "' AND " + idORRai;
                // System.out.println(reSEQNo);
                stmtUpd.executeUpdate(reSEQNo); // Re-Cal RCTSCSEQ 1,2,3,4 --->
                i++;

            }

            stmtUpd.close();
            stmtSBPT.close();

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                connect.close();
            }
        }

        return rs;
    }

    public boolean updateRejectMat(String job, String inv, String po, String poline, String mat) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRJFLAG = 'R' \n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po
                + "%' AND RCDPOLINE = '" + poline + "' AND RCDCUSMAT = '" + mat + "' ";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean updateHoldMat(String job, String inv) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRJFLAG = 'H' \n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv
                + "' AND (RCDRJFLAG IS NULL OR (RCDRJFLAG != 'R' AND RCDRJFLAG != 'H')) ";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean updateClearRejectMat(String job, String inv, String po, String poline, String mat) {

        boolean rs = true;

        String selDetailByJobInv = "SELECT * FROM [RMShipment].[dbo].[RCMBPD] WHERE RCDJBNO = '" + job
                + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po + "%' AND RCDPOLINE = '" + poline
                + "' AND RCDCUSMAT = '" + mat + "' AND RCDRJFLAG = 'R'";

        try {
            Statement stmtS = connection.createStatement();
            Statement stmtIUD = connection.createStatement();
            ResultSet RsS = stmtS.executeQuery(selDetailByJobInv);

            while (RsS.next()) {

                String delSQL = "DELETE [RMShipment].[dbo].[RCMBPT] WHERE RCTJBNO = '" + job + "'  AND RCTCTID = '"
                        + RsS.getString("RCDID") + "' AND RCTCODE = '" + RsS.getString("RCDCUSMAT") + "' ";
                int del = stmtIUD.executeUpdate(delSQL);

                if (del == 1) {

                    String updBPD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDRCQTY = 0 \n"
                            + "WHERE RCDPALNO = '" + RsS.getString("RCDPALNO") + "' AND RCDINVNO = '"
                            + RsS.getString("RCDINVNO") + "'  AND RCDID = '" + RsS.getString("RCDID")
                            + "' AND RCDCUSMAT = '" + RsS.getString("RCDCUSMAT") + "' AND\n"
                            + "[RCDJBNO] = '" + job + "' ";

                    stmtIUD.executeUpdate(updBPD);

                }

            }

            stmtS.close();
            stmtIUD.close();

            Statement stmtSBPT = connection.createStatement();
            Statement stmtUpd = connection.createStatement();

            String bpt = "SELECT TOP 1 * FROM RCMBPT WHERE RCTJBNO = '" + job + "' ORDER BY RCTSCSEQ ";
            ResultSet rsGETBPT = stmtSBPT.executeQuery(bpt);

            String whSel = "";
            String spSel = "";
            String cnSel = "";
            String dlSel = "";

            while (rsGETBPT.next()) {
                whSel = rsGETBPT.getString("RCTWHSE");
                spSel = rsGETBPT.getString("RCTSPNO");
                cnSel = rsGETBPT.getString("RCTCUNO");
                dlSel = rsGETBPT.getString("RCTDLDATE");
            }

            String sql3 = "SELECT F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE,SUM(F1.RCTPACK) AS RCTPACK\n"
                    + "FROM (\n"
                    + "	SELECT RCTSCSEQ\n"
                    + "	,(CASE WHEN RCTPALNO IS NULL THEN RCTCTID ELSE RCTPALNO END) AS RCTCTID\n"
                    + "                ,(CASE WHEN RCTPALNO IS NULL THEN RCTCODE ELSE NULL END) AS RCTCODE\n"
                    + "	,RCTPACK FROM [RMShipment].[dbo].[RCMBPT] \n"
                    + "	WHERE [RCTWHSE] = '" + whSel + "' AND [RCTSPNO] = '" + spSel + "' AND [RCTCUNO] = '" + cnSel
                    + "' AND [RCTDLDATE] = '" + dlSel + "'\n"
                    + "	\n"
                    + ") F1\n"
                    + "\n"
                    + "GROUP BY F1.RCTSCSEQ,F1.RCTCTID,F1.RCTCODE";

            ResultSet rs3 = stmtSBPT.executeQuery(sql3);

            // ResultSet rs = stmtS.executeQuery("SELECT * FROM [RMShipment].[dbo].[RCMBPT]
            // WHERE [RCTWHSE] = '" + wh + "' AND [RCTSPNO] = '" + sp + "' AND [RCTCUNO] =
            // '" + cn + "' AND [RCTDLDATE] = '" + dt + "' ORDER BY
            // RIGHT('00000'+RCTSCSEQ,5) ASC");
            int i = 1;
            while (rs3.next()) {

                String idORRai = rs3.getString("RCTCTID");

                if (idORRai.length() > 10) { // ID
                    idORRai = "[RCTCTID] = '" + idORRai.trim() + "' ";
                } else { // PALLET
                    idORRai = "[RCTPALNO] = '" + idORRai.trim() + "'";
                }

                String reSEQNo = "UPDATE [RMShipment].[dbo].[RCMBPT] SET RCTSCSEQ = '" + i + "' WHERE [RCTWHSE] = '"
                        + whSel + "' AND [RCTSPNO] = '" + spSel + "' AND [RCTCUNO] = '" + cnSel
                        + "' AND [RCTDLDATE] = '" + dlSel + "' AND " + idORRai;
                // System.out.println(reSEQNo);
                stmtUpd.executeUpdate(reSEQNo); // Re-Cal RCTSCSEQ 1,2,3,4 --->
                i++;

            }

            stmtUpd.close();
            stmtSBPT.close();

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                connect.close();
            }
        }

        return rs;
    }

    public boolean updateInv(String job, String invold, String invnew) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDINVNO = '" + invnew + "' , RCDINVREF = '" + invold
                + "'\n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + invold + "' ";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public JSONObject updateCusMat200(String job, String inv, String po, String poline, String cusmatOld,
            String cusmatNew, String wh, String sp, String cs, String dt) {

        JSONObject res = new JSONObject();

        String sqlupdBPT = "UPDATE RCMBPT SET RCTCODE = '" + cusmatNew + "' "
                + "WHERE RCTWHSE = '" + wh + "' AND RCTSPNO = '" + sp + "' AND RCTCUNO = '" + cs + "' AND RCTDLDATE = '"
                + dt + "' "
                + "AND RCTJBNO = '" + job + "' AND RCTCODE = '" + cusmatOld + "' ";

        String sqlupdBPD = "UPDATE RCMBPD SET RCDRJFLAG = NULL "
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po
                + "%' AND RCDPOLINE = '" + poline + "' AND RCDCUSMAT = '" + cusmatNew + "' ";

        try {

            Statement stmtIUD = connection.createStatement();

            int updBPT = stmtIUD.executeUpdate(sqlupdBPT);
            int updBPD = stmtIUD.executeUpdate(sqlupdBPD);

            if (updBPT > 0) {
                res.put("resupdBPT", true);
            } else {
                res.put("resupdBPT", false);
            }

            if (updBPD > 0) {
                res.put("resupdBPD", true);
            } else {
                res.put("resupdBPD", false);
            }

            stmtIUD.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                connect.close();
            }
        }

        return res;
    }

    public JSONObject updateClearYLStar(String job, String inv, String po, String poline, String cusmatOld) {

        JSONObject res = new JSONObject();

        String sqlupdBPD = "UPDATE RCMBPD SET RCDRJFLAG = '' "
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po
                + "%' AND RCDPOLINE = '" + poline + "' AND RCDCUSMAT = '" + cusmatOld
                + "' AND RCDRJFLAG = 'C' AND (RCDMATREF = '' OR RCDMATREF IS NULL) ";

        try {

            Statement stmtIUD = connection.createStatement();

            int updBPD = stmtIUD.executeUpdate(sqlupdBPD);

            if (updBPD > 0) {
                res.put("resupdBPD", true);
            } else {
                res.put("resupdBPD", false);
            }

            stmtIUD.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                connect.close();
            }
        }

        return res;
    }

    public boolean updateCusMat(String job, String inv, String po, String poline, String cusmat) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDCUSMAT = '" + cusmat + "'\n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po
                + "%' AND RCDPOLINE = '" + poline + "'";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean updateSTSTo2(String wh, String sp, String cn, String dt, String inv, String pdate) {

        PreparedStatement ps = null;
        boolean rs = false;

        Statement upD = null;

        String UPH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET RCHSTS = 2,[RCHEDT] = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[RCHETM] = FORMAT(CURRENT_TIMESTAMP,'HHmmss') WHERE [RCHWHSE] = '"
                + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt
                + "' AND [RCHSTS] = 1 ";

        String UPD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDSTS] = 2 ,[RCDPOSTDATE] = " + pdate
                + ",[RCDEDT] = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[RCDETM] = FORMAT(CURRENT_TIMESTAMP,'HHmmss') \n"
                + "WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn
                + "' AND [RCHTPDT] = '" + dt + "') \n"
                + "AND [RCDINVNO] = '" + inv + "' AND [RCDSTS] = 1 ";

        try {
            upD = connection.createStatement();

            int h = upD.executeUpdate(UPH);
            int d = upD.executeUpdate(UPD);

            if (h >= 1 && d >= 1) {
                rs = true;
            }

            upD.close();
            connection.close();

            if (connect != null) {
                connect.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RCM100Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public boolean updateSTSTo2RCM100(String wh, String sp, String cn, String dt, String inv) {

        boolean rs = false;

        Statement upD = null;

        String UPH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET RCHSTS = 1,[RCHEDT] = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[RCHETM] = FORMAT(CURRENT_TIMESTAMP,'HHmmss') WHERE [RCHWHSE] = '"
                + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt
                + "' AND [RCHSTS] = 0 ";

        String UPD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDSTS] = 1,[RCDEDT] = FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd'),[RCDETM] = FORMAT(CURRENT_TIMESTAMP,'HHmmss') \n"
                + "WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] \n"
                + "WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn
                + "' AND [RCHTPDT] = '" + dt + "') \n"
                + "AND [RCDINVNO] = '" + inv + "' AND [RCDSTS] = 0 ";

        try {
            upD = connection.createStatement();

            int h = upD.executeUpdate(UPH);
            int d = upD.executeUpdate(UPD);

            if (h >= 1 || d >= 1) {
                rs = true;
            }

            upD.close();
            connection.close();

            if (connect != null) {
                connect.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RCM100Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public boolean updatePackType(String job, String inv, String po, String poline, String cusmat, String pktype) {

        PreparedStatement ps = null;
        boolean rs = false;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDPKTYPE = '" + pktype + "'\n"
                + "WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po
                + "%' AND RCDPOLINE = '" + poline + "' AND RCDCUSMAT = '" + cusmat + "'";

        try {

            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();
            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rs;
    }

    public boolean deleteQRMMASByInvoice(String invno, String wh, String sp, String cn, String dt) {

        boolean rs = false;

        Statement stmtD = null;

        try {

            stmtD = connection.createStatement();

            String sqlDelQMAS = "DELETE [RMShipment].[dbo].[QRMMAS] WHERE QRMID IN (SELECT RCDID FROM RCMBPD WHERE RCDINVNO = '"
                    + invno + "' \n"
                    + " AND [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '"
                    + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt
                    + "')) ";
            int delqmas = stmtD.executeUpdate(sqlDelQMAS);

            String sqlDelQTRA = "DELETE [RMShipment].[dbo].[QRMTRA] WHERE QRMID IN (SELECT RCDID FROM RCMBPD WHERE RCDINVNO = '"
                    + invno + "' \n"
                    + " AND [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '"
                    + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt
                    + "')) ";
            int delqtra = stmtD.executeUpdate(sqlDelQTRA);

            String sqlUpBPD = "UPDATE [RMShipment].[dbo].[RCMBPD] SET [RCDSTS] = 1,[RCDPOSTDATE] = 0\n"
                    + "WHERE [RCDINVNO] = '" + invno + "' \n"
                    + "AND [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] WHERE [RCHWHSE] = '"
                    + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt
                    + "') AND [RCDSTS] = 2 ";
            int upbpd = stmtD.executeUpdate(sqlUpBPD);

            String sqlUpBPH = "UPDATE [RMShipment].[dbo].[RCMBPH] SET RCHSTS = 1 WHERE [RCHWHSE] = '" + wh
                    + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = '" + dt
                    + "' AND [RCHSTS] = 2 ";
            int upbph = stmtD.executeUpdate(sqlUpBPH);

            if (delqmas >= 1 && delqtra >= 1 && upbpd >= 1 && upbph >= 1) {
                rs = true;
            }

            stmtD.close();
            connection.close();

            if (connect != null) {
                connect.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RCM100Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;

    }

    public boolean updatePOLine(String job, String inv, String po, String oldpoline, String newpoline, String mat) {

        boolean rs = false;
        PreparedStatement ps = null;

        String sql = "UPDATE [RMShipment].[dbo].[RCMBPD] SET RCDPOLINE = '" + newpoline + "'\n"
                + "  WHERE RCDJBNO = '" + job + "' AND RCDINVNO = '" + inv + "' AND RCDPONO LIKE '" + po
                + "%' AND RCDPOLINE = '" + oldpoline + "' AND RCDCUSMAT = '" + mat + "' ";

        try {
            ps = connection.prepareStatement(sql);
            int record = ps.executeUpdate();

            if (record >= 1) {
                rs = true;
            } else {
                rs = false;
            }

            ps.close();
            connection.close();
            connect.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rs;
    }

    public String checkNull(String a) {

        if (a == null) {
            a = "NULL";
        } else {
            if (a.equals("")) {
                a = "NULL";
            } else {
                a = "'" + a + "'";
            }
        }

        return a;
    }

    public List<RCMBPD> findPostDate(String wh, String sp, String cn, String dt) {

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPD> productList = new ArrayList<RCMBPD>();
        String sql = "SELECT DISTINCT RCDPOSTDATE FROM [RMShipment].[dbo].[RCMBPD] \n"
                + "WHERE RCDJBNO IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH] WHERE RCHWHSE = ? AND RCHSPNO = ? AND RCHCUNO = ? AND RCHTPDT = ?)\n"
                + "ORDER BY RCDPOSTDATE";

        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, wh);
            ps.setString(2, sp);
            ps.setString(3, cn);
            ps.setString(4, dt);
            rs = ps.executeQuery();
            while (rs.next()) {
                RCMBPD p = new RCMBPD();
                p.setRCDPOSTDATE(rs.getString("RCDPOSTDATE"));

                productList.add(p);
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

}
