/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.dao;

import com.twc.rcms.model.QWHMAS;
import com.twc.rcms.model.RCMBPH;
import com.twc.rcms.util.connectiondb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 93176
 */
public class RCM900Dao {

    private Connection connection;
    connectiondb connect;

    public RCM900Dao() {
        connection = null;
        connect = null;
        connect = new connectiondb();
        connection = connect.getConnection();
    }

    public List<RCMBPH> findRCM900DetailBy(String wh, String sp, String dt) {

        String selWH = "";
        String selWHH1 = "";

        if (!wh.equals("TWC")) {
            selWH = " H2.RCHWHSE = @WH  AND ";
            selWHH1 = " HED.RCHWHSE = @WH  AND ";
        }

        ResultSet rs = null;
        PreparedStatement ps = null;

        List<RCMBPH> productList = new ArrayList<RCMBPH>();

        String sql = "DECLARE @WH NVARCHAR(3) = '" + wh + "'\n"
                + "DECLARE @SPNO NVARCHAR(6) = '" + sp + "'\n"
                + "DECLARE @TRDT NVARCHAR(8) = '" + dt + "'\n"
                + "\n"
                + "SELECT ROW_NUMBER() OVER(PARTITION BY HED.RCHCDT ORDER BY HED.RCHCDT) AS NUMB,HED.RCHCDT,HED.RCHJBNO,HED.RCHCUNO,HED.RCHPLANT,HED.RCHTOTINV AS CNTINV\n"
                + ",HED.RCHTOTPO AS CNTPO,HED.RCHTOTQTY AS CNTPACK,HED.RCHTOTAMT AS CNTAMT,HED.RCHITEM AS CNTSKU\n"
                + ",(SELECT TOP 1 COUNT(DISTINCT H2.RCHPLANT) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " /*H2.RCHWHSE = HED.RCHWHSE AND*/ H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT AND H2.RCHCDT = HED.RCHCDT) AS TOTPLANT\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTINV) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " /*H2.RCHWHSE = HED.RCHWHSE AND*/ H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT AND H2.RCHCDT = HED.RCHCDT) AS TOTINV\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTPO) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " /*H2.RCHWHSE = HED.RCHWHSE AND*/ H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT AND H2.RCHCDT = HED.RCHCDT) AS TOTPO\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTQTY) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " /*H2.RCHWHSE = HED.RCHWHSE AND*/ H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT AND H2.RCHCDT = HED.RCHCDT) AS TOTPACK\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTAMT) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " /*H2.RCHWHSE = HED.RCHWHSE AND*/ H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT AND H2.RCHCDT = HED.RCHCDT) AS TOTAMT\n"
                + ",(SELECT TOP 1 SUM(H2.RCHITEM) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " /*H2.RCHWHSE = HED.RCHWHSE AND*/ H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT AND H2.RCHCDT = HED.RCHCDT) AS TOTSKU\n"
                + "\n"
                + ",(SELECT TOP 1 COUNT(DISTINCT H2.RCHPLANT) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT) AS GTOTPLANT\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTINV) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT) AS GTOTINV\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTPO) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT) AS GTOTPO\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTQTY) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT) AS GTOTPACK\n"
                + ",(SELECT TOP 1 SUM(H2.RCHTOTAMT) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT) AS GTOTAMT\n"
                + ",(SELECT TOP 1 SUM(H2.RCHITEM) FROM [RMShipment].[dbo].[RCMBPH] H2 WHERE " + selWH + " H2.RCHSPNO = HED.RCHSPNO AND H2.RCHTPDT = HED.RCHTPDT) AS GTOTSKU\n"
                + "\n"
                + "FROM [RMShipment].[dbo].[RCMBPH] HED WHERE " + selWHH1 + " HED.RCHSPNO = @SPNO AND HED.RCHTPDT = @TRDT\n"
                + "ORDER BY HED.RCHCDT,HED.RCHJBNO,HED.RCHCUNO,HED.RCHPLANT";

        try {

//            System.out.println("" + sql);
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                RCMBPH p = new RCMBPH();

                p.setNUMB(rs.getString("NUMB"));
                p.setRCHCDT(rs.getString("RCHCDT"));
                p.setRCHJBNO(rs.getString("RCHJBNO"));
                p.setRCHCUNO(rs.getString("RCHCUNO"));
                p.setRCHPLANT(rs.getString("RCHPLANT").trim());
                p.setRCHTOTINV(rs.getString("CNTINV"));
                p.setRCHTOTPO(rs.getString("CNTPO"));
                p.setRCHTOTQTY(rs.getString("CNTPACK"));
                p.setRCHTOTAMT(rs.getString("CNTAMT"));
                p.setRCHITEM(rs.getString("CNTSKU"));

                p.setCNTPLANT(rs.getString("TOTPLANT"));
                p.setSUMINV(rs.getString("TOTINV"));
                p.setSUMPO(rs.getString("TOTPO"));
                p.setSUMQTY(rs.getString("TOTPACK"));
                p.setSUMAMT(rs.getString("TOTAMT"));
                p.setSUMITEM(rs.getString("TOTSKU"));

                p.setTOTPLANT(rs.getString("GTOTPLANT"));
                p.setTOTINV(rs.getString("GTOTINV"));
                p.setTOTPO(rs.getString("GTOTPO"));
                p.setTOTQTY(rs.getString("GTOTPACK"));
                p.setTOTAMT(rs.getString("GTOTAMT"));
                p.setTOTITEM(rs.getString("GTOTSKU"));

                productList.add(p);
            }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }
    
    public List<QWHMAS> findWHbyUID(String uid) {

        List<QWHMAS> UAList = new ArrayList<QWHMAS>();

        String sql = "SELECT QWHCOD, QWHNAME, 'selected hidden' as SEL\n"
                + "FROM QWHMAS \n"
                + "WHERE QWHCOD = (select [MSWHSE] from [MSSUSER] where [USERID] = '" + uid + "') \n"
                + "UNION ALL\n"
                + "SELECT QWHCOD, QWHNAME, ''  \n"
                + "FROM QWHMAS";

        try {

            PreparedStatement ps = connection.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QWHMAS p = new QWHMAS();

                p.setQWHCOD(result.getString("QWHCOD"));
                p.setQWHNAME(result.getString("QWHNAME"));
                p.setSelect(result.getString("SEL"));

                UAList.add(p);

            }

            connection.close();
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

}
