/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import com.google.gson.Gson;
import com.twc.rcms.dao.RCM900Dao;
import com.twc.rcms.model.QWHMAS;
import com.twc.rcms.model.RCMBPH;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class RCM900Get extends HttpServlet {

    private final Gson gson;

    public RCM900Get() {
        this.gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mode = request.getParameter("mode");
        String json = new String();
        Map<String, Object> m = new HashMap<String, Object>();

        if (mode.equals("getSummaryTable")) {

            String wh = request.getParameter("wh");
            String tdate = request.getParameter("tdate");
            String supli = request.getParameter("supli");

            List<RCMBPH> productList = new ArrayList<RCMBPH>();

            List<RCMBPH> det = new RCM900Dao().findRCM900DetailBy(wh, supli, tdate.replaceAll("-", ""));

            for (int i = 0; i < det.size(); i++) {

                RCMBPH p0 = new RCMBPH();
                RCMBPH p1 = new RCMBPH();

                if (i == 0) {
                    //G.TOTAL
                    p0.setNUMB("G.TOTAL");
                    p0.setRCHCDT("");
                    p1.setRCHJBNO(det.get(i).getRCHJBNO());
                    p1.setRCHCUNO(det.get(i).getRCHCUNO());
                    p0.setRCHPLANT(det.get(i).getTOTPLANT());
                    p0.setRCHTOTINV(det.get(i).getTOTINV());
                    p0.setRCHTOTPO(det.get(i).getTOTPO());
                    p0.setRCHTOTQTY(det.get(i).getTOTQTY());
                    p0.setRCHTOTAMT(det.get(i).getTOTAMT());
                    p0.setRCHITEM(det.get(i).getTOTITEM());
                    productList.add(p0);

                    //TOTAL
                    p1.setNUMB("TOTAL");
                    p1.setRCHCDT(det.get(i).getRCHCDT());
                    p1.setRCHJBNO("");
                    p1.setRCHCUNO("");
                    p1.setRCHPLANT(det.get(i).getCNTPLANT());
                    p1.setRCHTOTINV(det.get(i).getSUMINV());
                    p1.setRCHTOTPO(det.get(i).getSUMPO());
                    p1.setRCHTOTQTY(det.get(i).getSUMQTY());
                    p1.setRCHTOTAMT(det.get(i).getSUMAMT());
                    p1.setRCHITEM(det.get(i).getSUMITEM());
                    productList.add(p1);

                } else {
                    if (!det.get(i).getRCHCDT().equals(det.get(i - 1).getRCHCDT())) {

                        //TOTAL
                        p1.setNUMB("TOTAL");
                        p1.setRCHCDT(det.get(i).getRCHCDT());
                        p1.setRCHJBNO("");
                        p1.setRCHCUNO("");
                        p1.setRCHPLANT(det.get(i).getCNTPLANT());
                        p1.setRCHTOTINV(det.get(i).getSUMINV());
                        p1.setRCHTOTPO(det.get(i).getSUMPO());
                        p1.setRCHTOTQTY(det.get(i).getSUMQTY());
                        p1.setRCHTOTAMT(det.get(i).getSUMAMT());
                        p1.setRCHITEM(det.get(i).getSUMITEM());
                        productList.add(p1);
                    }
                }

                RCMBPH p2 = new RCMBPH();
                if (det.get(i).getNUMB().equals("1")) {

                    //DETAIL
                    p2.setNUMB("");
                    p2.setRCHCDT(det.get(i).getRCHCDT());
                    p2.setRCHJBNO(det.get(i).getRCHJBNO());
                    p2.setRCHCUNO(det.get(i).getRCHCUNO());
                    p2.setRCHPLANT(det.get(i).getRCHPLANT());
                    p2.setRCHTOTINV(det.get(i).getRCHTOTINV());
                    p2.setRCHTOTPO(det.get(i).getRCHTOTPO());
                    p2.setRCHTOTQTY(det.get(i).getRCHTOTQTY());
                    p2.setRCHTOTAMT(det.get(i).getRCHTOTAMT());
                    p2.setRCHITEM(det.get(i).getRCHITEM());
                } else {

                    //DETAIL
                    p2.setNUMB("");
                    p2.setRCHCDT("");
                    p2.setRCHJBNO(det.get(i).getRCHJBNO());
                    p2.setRCHCUNO(det.get(i).getRCHCUNO());
                    p2.setRCHPLANT(det.get(i).getRCHPLANT());
                    p2.setRCHTOTINV(det.get(i).getRCHTOTINV());
                    p2.setRCHTOTPO(det.get(i).getRCHTOTPO());
                    p2.setRCHTOTQTY(det.get(i).getRCHTOTQTY());
                    p2.setRCHTOTAMT(det.get(i).getRCHTOTAMT());
                    p2.setRCHITEM(det.get(i).getRCHITEM());
                }

                productList.add(p2);

            }

            m.put("DList", productList);
            json = this.gson.toJson(m);

        } else if (mode.equals("getWH")) {

            String uid = request.getParameter("uid");

            RCM900Dao dao = new RCM900Dao();

//            List<QWHMAS> ValDao = dao.findWH();
            List<QWHMAS> VVal = dao.findWHbyUID(uid);

            m.put("whList", VVal);
            json = this.gson.toJson(m);

        }

        /**
         * ****************************
         * Return JSON to Client ****************************
         */
        // SET Response header
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        // SET List data to Json
        response.getWriter().write(json);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
