/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 93176
 */
public class RCM200ContScan extends HttpServlet {

    private static final String PAGE_VIEW = "../views/RCM200-Scan.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "RCM200");
        request.setAttribute("PROGRAMDESC", "Receive RM. Count");

        RequestDispatcher view = request.getRequestDispatcher(PAGE_VIEW);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
