/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import com.google.gson.Gson;
import com.twc.rcms.dao.RCM800Dao;
import com.twc.rcms.model.RCMBPD;
import com.twc.rcms.model.RCMPOD;
import com.twc.rcms.model.RCMPOH;
import com.twc.rcms.model.RCMPOS;
import com.twc.rcms.services.BAPIRET2;
import com.twc.rcms.services.TABLEOFBAPIMATRAW;
import com.twc.rcms.services.TABLEOFBAPIMEPOTEXTHEADER;
import com.twc.rcms.services.TABLEOFBAPIRANGESEKGRP;
import com.twc.rcms.services.TABLEOFBAPIREQUESTDOCDATE;
import com.twc.rcms.services.TABLEOFBAPIRET2;
import com.twc.rcms.services.TABLEOFZBAPIEBELNRAM;
import com.twc.rcms.services.TABLEOFZBAPIPURORG;
import com.twc.rcms.services.TABLEOFZBAPIRELGROUP;
import com.twc.rcms.services.TABLEOFZBAPITWCPOHEAD;
import com.twc.rcms.services.TABLEOFZBAPITWCPOHEADERR;
import com.twc.rcms.services.TABLEOFZBAPITWCPOITEM;
import com.twc.rcms.services.TABLEOFZBAPITWCPOMOVEMENT;
import com.twc.rcms.services.TABLEOFZBAPITWCPOSUBCON;
import com.twc.rcms.services.TABLEOFZBAPIVENDRAM;
import com.twc.rcms.services.ZBAPIEBELNRAM;
import com.twc.rcms.services.ZBAPITWCPOMOVEMENT;
import com.twc.rcms.services.ZBAPITWCPOHEAD;
import com.twc.rcms.services.ZBAPITWCPOITEM;
import com.twc.rcms.services.ZBAPITWCRCMSGETLISTPOResponse;
import com.twc.rcms.services.ZWSTWCRCMSGETLISTPO;
import com.twc.rcms.services.ZWSTWCRCMSGETLISTPOService;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;

/**
 *
 * @author 93176
 */
public class RCM800Get extends HttpServlet {

    private final Gson gson;

    public RCM800Get() {
        this.gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mode = request.getParameter("mode");
        String json = new String();
        Map<String, Object> m = new HashMap<String, Object>();

        if (mode.equals("getPOlist")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");

            RCM800Dao dao = new RCM800Dao();

            List<RCMBPD> ValDao = dao.findRCMPoList(wh, sp, cn, dt.replaceAll("-", ""));

            m.put("polist", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("getSAPVal")) {

            String poli = request.getParameter("poli");
            String tpdt = request.getParameter("date");
            String uid = request.getParameter("uid");

            JSONArray jsonArr = new JSONArray(poli);
            List<RCMPOH> listRCMPOH = new ArrayList<RCMPOH>();
            List<RCMPOD> listRCMPOD = new ArrayList<RCMPOD>();
            List<RCMPOS> listRCMPOS = new ArrayList<RCMPOS>();

            boolean ret = false;

            try {

                if (jsonArr.length() > 0) {
                    for (int i = 0; i < jsonArr.length(); i++) {
                        Authenticator.setDefault(new Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {

                                Calendar c = Calendar.getInstance();
                                int month = c.get(Calendar.MONTH) + 1;

                                String m = Integer.toString(month);
                                if (m.length() < 2) {
                                    m = "0" + m;
                                }

                                String pass = "display" + m;

                                return new PasswordAuthentication("tviewwien", pass.toCharArray());
                            }
                        });

                        ZWSTWCRCMSGETLISTPOService service = new ZWSTWCRCMSGETLISTPOService();
                        ZWSTWCRCMSGETLISTPO getlistpo = service.getZWSTWCRCMSGETLISTPOSoapBinding();

                        TABLEOFBAPIREQUESTDOCDATE creatdtselection = new TABLEOFBAPIREQUESTDOCDATE();
                        TABLEOFBAPIMATRAW plantselection = new TABLEOFBAPIMATRAW();
                        TABLEOFZBAPIEBELNRAM ponumberselection = new TABLEOFZBAPIEBELNRAM();
                        TABLEOFZBAPITWCPOHEAD poHEADER = new TABLEOFZBAPITWCPOHEAD();
                        TABLEOFZBAPITWCPOHEADERR poHEADERERR = new TABLEOFZBAPITWCPOHEADERR();
                        TABLEOFZBAPITWCPOMOVEMENT poHISTORY = new TABLEOFZBAPITWCPOMOVEMENT();
                        TABLEOFZBAPITWCPOITEM poITEM = new TABLEOFZBAPITWCPOITEM();
                        TABLEOFZBAPITWCPOSUBCON poSUBCON = new TABLEOFZBAPITWCPOSUBCON();
                        TABLEOFBAPIMEPOTEXTHEADER poTEXTHEADER = new TABLEOFBAPIMEPOTEXTHEADER();
                        TABLEOFBAPIRANGESEKGRP purgrpselection = new TABLEOFBAPIRANGESEKGRP();
                        TABLEOFZBAPIPURORG purorgselection = new TABLEOFZBAPIPURORG();
                        TABLEOFZBAPIRELGROUP relgroupselection = new TABLEOFZBAPIRELGROUP();
                        TABLEOFBAPIRET2 _return = new TABLEOFBAPIRET2();
                        TABLEOFZBAPIVENDRAM vendorselection = new TABLEOFZBAPIVENDRAM();

                        ZBAPIEBELNRAM poSelect = new ZBAPIEBELNRAM();
                        poSelect.setSIGN("I");
                        poSelect.setOPTION("EQ");
                        poSelect.setEBELNLOW(jsonArr.getString(i));
                        poSelect.setEBELNHIGH("");
                        ponumberselection.getItem().add(poSelect);

                        Future<?> getlistpoAsync = getlistpo.zBAPITWCRCMSGETLISTPOAsync(creatdtselection, plantselection, ponumberselection, poHEADER, poHEADERERR, poHISTORY, poITEM, poSUBCON,
                                poTEXTHEADER, purgrpselection, purorgselection, relgroupselection, _return, vendorselection);

                        ZBAPITWCRCMSGETLISTPOResponse getlistpoResponse = null;

                        getlistpoResponse = (ZBAPITWCRCMSGETLISTPOResponse) getlistpoAsync.get();

                        TABLEOFZBAPITWCPOHEAD tablepohead = getlistpoResponse.getPOHEADER();

                        TABLEOFBAPIRET2 tableRet = getlistpoResponse.getRETURN();
                        for (BAPIRET2 rett : tableRet.getItem()) {
                            System.out.println("GETLIST_FIELD : " + rett.getFIELD());
                            System.out.println("ID : " + rett.getID());
                            System.out.println("LOGNO : " + rett.getLOGNO());
                            System.out.println("MESSAGE : " + rett.getMESSAGE());
                            System.out.println("MESSAGEV1 : " + rett.getMESSAGEV1());
                            System.out.println("MESSAGEV2 : " + rett.getMESSAGEV2());
                            System.out.println("MESSAGEV3 : " + rett.getMESSAGEV3());
                            System.out.println("MESSAGEV4 : " + rett.getMESSAGEV4());
                            System.out.println("NUMBER : " + rett.getNUMBER());
                            System.out.println("PARAMETER : " + rett.getPARAMETER());
                            System.out.println("SYSTEM : " + rett.getSYSTEM());
                            System.out.println("TYPE : " + rett.getTYPE());
                        }

                        for (ZBAPITWCPOHEAD getpoheader : tablepohead.getItem()) {

                            RCMPOH poHead = new RCMPOH();
                            poHead.setRCHCONO("TWC");
                            poHead.setRCHPONO(getpoheader.getPONUMBER().trim());
                            poHead.setRCHPORG(getpoheader.getPURCHORG().trim());
                            poHead.setRCHCRTON(getpoheader.getCREATEON().trim());
                            poHead.setRCHDOCTYP(getpoheader.getDOCTYPE().trim());
                            poHead.setRCHVCODE(getpoheader.getVENDOR().trim());
                            poHead.setRCHVNAME(getpoheader.getVENDNAME().trim());
                            poHead.setRCHPGRP(getpoheader.getPURGROUP().trim());
                            poHead.setRCHPNAME(getpoheader.getPURGROUPNAME().trim());
                            poHead.setRCHCUR(getpoheader.getCURRENCY().trim());
                            poHead.setRCHPMT(getpoheader.getPMNTTRMS().trim());

                            if (getpoheader.getRELSTATUS().trim().equals("X")) {
                                poHead.setRCHSTS("L2X");
                            } else if (getpoheader.getRELSTATUS().trim().equals("XX")) {
                                poHead.setRCHSTS("L3X");
                            } else if (getpoheader.getRELSTATUS().trim().equals("XXX")) {
                                poHead.setRCHSTS("L3");
                            } else {
                                poHead.setRCHSTS("L1X");
                            }

                            poHead.setRCHTXLINE(getpoheader.getTEXTLINE().trim());
                            poHead.setRCHPRDCD(getpoheader.getPRODCODE().trim());
                            poHead.setRCHDLADDR(getpoheader.getDLVADDR().trim());
                            poHead.setRCHTAXCD(getpoheader.getTAXCODE().trim());
                            poHead.setRCHTOTAMT(getpoheader.getTOTNETVALUE().toString().trim());
                            poHead.setRCHRELGRP(getpoheader.getRELGROUP().trim());
                            poHead.setRCHDOCDATE(getpoheader.getDOCDATE().trim());
                            poHead.setRCHUSER(uid);

                            listRCMPOH.add(poHead);

                        }

                        TABLEOFZBAPITWCPOMOVEMENT tablepohistory = getlistpoResponse.getPOHISTORY();

                        for (ZBAPITWCPOMOVEMENT getpohistory : tablepohistory.getItem()) {

                            RCMPOS poHistory = new RCMPOS();
                            poHistory.setRCSCONO("TWC");
                            poHistory.setRCSPONO(getpohistory.getPONUMBER().trim());
                            poHistory.setRCSLINE(getpohistory.getPOITEM().trim());
                            poHistory.setRCSDOCYEAR(getpohistory.getDOCYEAR().trim());
                            poHistory.setRCSMATDOC(getpohistory.getMATDOC().trim());
                            poHistory.setRCSPSTRGDATE(getpohistory.getPSTNGDATE().trim());
                            poHistory.setRCSPOQTY(getpohistory.getQUANTITY().toString().trim());
                            poHistory.setRCSMATCODE(getpohistory.getMATERIAL().trim());
                            poHistory.setRCSPLANT(getpohistory.getPLANT().trim());
                            poHistory.setRCSDOCDATE(getpohistory.getDOCDATE().trim());
                            poHistory.setRCSREFDOCNO(getpohistory.getREFDOCNO().trim());
                            poHistory.setRCSREFDOCNOLONG(getpohistory.getREFDOCNOLONG().trim());
                            poHistory.setRCSUSER(uid);

                            listRCMPOS.add(poHistory);
                        }

                        TABLEOFZBAPITWCPOITEM tablepoitem = getlistpoResponse.getPOITEM();

                        for (ZBAPITWCPOITEM getpoitem : tablepoitem.getItem()) {

                            RCMPOD poDetail = new RCMPOD();
                            poDetail.setRCDCONO("TWC");
                            poDetail.setRCDPONO(getpoitem.getPONUMBER().trim());
                            poDetail.setRCDLINE(getpoitem.getPOITEM().trim());
                            poDetail.setRCDPLANT(getpoitem.getPLANT().trim());
                            poDetail.setRCDMATCODE(getpoitem.getMATERIAL().trim());
//                            System.out.println("Mat " + getpoitem.getMATERIAL().trim());
                            poDetail.setRCDSTEXT(getpoitem.getSHORTTEXT().trim());
                            poDetail.setRCDUNIT(getpoitem.getUNIT().trim());
                            poDetail.setRCDPOQTY(getpoitem.getQUANTITY().toString().trim());
                            poDetail.setRCDPOPRC(getpoitem.getNETPRICE().toString().trim());
                            poDetail.setRCDPPN(getpoitem.getPRICEUNIT().toString().trim());
                            poDetail.setRCDPOAMT(getpoitem.getNETVALUE().toString().trim());
                            poDetail.setRCDVT(getpoitem.getVALTYPE().trim());
                            poDetail.setRCDST(getpoitem.getSTOCKTYPE().trim());
                            poDetail.setRCDDIND(getpoitem.getDELETEIND().trim());
                            poDetail.setRCDCONVB(getpoitem.getCONVDEN().toString().trim());
                            poDetail.setRCDCONVA(getpoitem.getCONVNUM().toString().trim());
                            poDetail.setRCDITCAT(getpoitem.getITEMCAT().trim());
                            poDetail.setRCDDLDATE(getpoitem.getDELIVERYDATE().trim());
                            poDetail.setRCDLOC(getpoitem.getSTGELOC().trim());
                            poDetail.setRCDLNAME(getpoitem.getSTGELOCNAME().trim());
                            poDetail.setRCDACCAT(getpoitem.getACCTASSCAT().trim());
                            poDetail.setRCDDLOVER(getpoitem.getOVERDLVTOL().toString().trim());
                            poDetail.setRCDDLUNDER(getpoitem.getUNLIMITEDDLV().trim());
                            poDetail.setRCDBASE(getpoitem.getBASEUNIT().trim());
                            poDetail.setRCDCONVN2(getpoitem.getCONVNUM2().toString().trim());
                            poDetail.setRCDCONVD2(getpoitem.getCONVDEN2().toString().trim());
                            poDetail.setRCDMATGRP(getpoitem.getMATKL().trim());
                            poDetail.setRCDORDNO(getpoitem.getORDERNO().trim());
                            poDetail.setRCDUSER(uid);
                            listRCMPOD.add(poDetail);

                        }

//                        System.out.println("POD in " + jsonArr.getString(i) + " " + listRCMPOD.size());
//                        if (listRCMPOD.size() == 0) {
                        //update Flag
                        String updateFlag = "UPDATE RCMBPD SET RCDPOFLAG = 'Y' WHERE RCDPONO LIKE '" + jsonArr.getString(i) + "%' ";
                        boolean updateF = new RCM800Dao().UpdateFlagPO(updateFlag);
//                        }

                    }

                    System.out.println("POH " + listRCMPOH.size());
                    System.out.println("POD " + listRCMPOD.size());
                    System.out.println("POS " + listRCMPOS.size());

                    boolean ins = new RCM800Dao().insertRCMPOHnRCMPOD(listRCMPOH, listRCMPOD, listRCMPOS, tpdt.replaceAll("-", ""));

                    if (ins == true) {
                        ret = true;
                    }

                    //Find JOBNo By wh sp cn dt in Head
                    List<RCMBPH> HeadList = new RCM800Dao().findRCMHead(wh, sp, cn, tpdt.replaceAll("-", ""));
                    //Loop => All JobBo
                    for (int i = 0; i < HeadList.size(); i++) {
                        if (HeadList.get(i).getRCHPLANT().equals("")) {
                            //Find PONO By RCHJBNO in Detail
                            List<RCMBPD> GetPONO = new RCM800Dao().findPonoByJbNo(HeadList.get(i).getRCHJBNO());
                            //Update Plant When Empty By Check Pono
                            boolean uph = new RCM800Dao().UpdatePlantHead(GetPONO.get(0).getRCDPONO(), HeadList.get(i).getRCHJBNO());
                        }
                    }

                }

            } catch (InterruptedException ex) {
                Logger.getLogger(RCM800Get.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(RCM800Get.class.getName()).log(Level.SEVERE, null, ex);
            }

            m.put("sap", ret);
            json = this.gson.toJson(m);

        } else if (mode.equals("CKDUPpoInPOH")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");

            RCM800Dao dao = new RCM800Dao();

            boolean ck = dao.CkPoListDupinPOH(wh, sp, cn, dt.replaceAll("-", ""));

            m.put("ckpo", ck);
            json = this.gson.toJson(m);

        } else if (mode.equals("delPOinPOHnPOD")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");

            RCM800Dao dao = new RCM800Dao();

            boolean ck = dao.delPOinPOHnPOD(wh, sp, cn, dt.replaceAll("-", ""));

            m.put("delpo", ck);
            json = this.gson.toJson(m);

        } else if (mode.equals("CKStatus2")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");

            RCM800Dao dao = new RCM800Dao();

            String sts = dao.selStatus2(wh, sp, cn, dt.replaceAll("-", ""));

            m.put("sts", sts);
            json = this.gson.toJson(m);

        }

        /**
         * ****************************
         * Return JSON to Client ****************************
         */
        // SET Response header
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        // SET List data to Json
        response.getWriter().write(json);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
