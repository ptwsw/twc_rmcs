/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import com.twc.rcms.util.connectiondb;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author 93176
 */
public class RCM100Print extends HttpServlet {

    private static final String FILE_DEST = "/resources/pdf/";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection connection = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String wh = request.getParameter("wh");
        String sp = request.getParameter("sp");
        String cn = request.getParameter("cn");
        String dt = request.getParameter("dt");
        String pd = request.getParameter("pd");
        String ck = request.getParameter("ck");
        String sel = request.getParameter("sel");

        String[] cksplit = null;

        if (!ck.equals("")) {
            cksplit = ck.split(",");
        }

        String pathPDF = "";

        try {

            if (sel.equals("before")) { //------------ before ------------

                String sql = "SELECT F1.NUMB,ROW_NUMBER() OVER(PARTITION BY F1.[RCDINVNO] ORDER BY F1.[RCDINVNO] ASC) AS NUMBbyINV,F1.[RCDINVNO],F1.[RCDPONO],F1.[RCDPOLINE]\n"
                        + ",(SELECT TOP 1 [RCDPOSTDATE] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDPOSTDATE\n"
                        + ",(SELECT TOP 1 [RCDITNO] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDITNO\n"
                        + ",F1.[RCDCUSMAT]\n"
                        + ",(SELECT TOP 1 [RCDMATDES] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDMATDES\n"
                        + ",F1.RCDRCQTY\n"
                        + ",F1.RCDINVAMT\n"
                        + ",F1.RCDTPDT\n"
                        + ",F1.RCDSUM\n"
                        + ",DENSE_RANK() OVER(ORDER BY F1.NUMB,F1.[RCDINVNO] ) AS INVCNT\n"
                        + "\n"
                        + "FROM (\n"
                        + "	SELECT DENSE_RANK() OVER(ORDER BY SUBSTRING([RCDCUSMAT], 1, 2)) AS NUMB,[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT]\n"
                        + "	,SUM(ISNULL(RCDRCQTY,0)) AS RCDRCQTY,SUM(RCDINVAMT) AS RCDINVAMT, [RCDJBNO], [RCDTPDT], [RCDSUM]\n"
                        + "\n"
                        + "	FROM [RMShipment].[dbo].[RCMBPD]\n"
                        + "	WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH]\n"
                        + "	WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = " + dt.replaceAll("-", "") + " ) \n";

                if (cksplit != null) {
                    if (cksplit.length > 0) {
                        sql += " AND ( ";
                    }

                    for (int i = 0; i < cksplit.length; i++) {

                        if (i == 0) {
                            sql += " RCDINVNO = '" + cksplit[i] + "' ";
                        } else {
                            sql += " OR RCDINVNO = '" + cksplit[i] + "' ";
                        }

                    }

                    if (cksplit.length > 0) {
                        sql += " ) ";
                    }
                }

                sql += " GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDTPDT],[RCDSUM]\n"
                        + ") F1";

                connectiondb mssqlc = new connectiondb();
                connection = mssqlc.getConnection();
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();

                File reportFile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/jasper/RCMS100ReportRCQTYBefore.jasper"));

                if (!reportFile.exists()) {
                    throw new JRRuntimeException("File *.jasper not found. The report design must be compiled first.");
                } else {
                    System.out.println("Found File Jasper");
                }

                byte[] bytes;

                File pdffile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/pdf/RCMS100_BF_RCQTY.pdf"));

                if (pdffile.exists()) { //Delete PDF File Before Create New
                    if (pdffile.delete()) {
                        System.out.println("DELETE PDF File Successful");
                    } else {
                        System.out.println("DELETE PDF File Failed");
                    }
                } else {
                    System.out.println("Not Found PDF Files");
                }

                System.out.println("generate PDF File......");

                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(rs));

                ServletContext servletContext = getServletContext();
                String realPath = servletContext.getRealPath(FILE_DEST);
                String saperate = realPath.contains(":") ? "\\" : "/";
                String path = realPath + saperate + "RCMS100_BF_RCQTY.pdf";

                FileOutputStream out = new FileOutputStream(path);
                out.write(bytes, 0, bytes.length);

                pathPDF = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/resources/pdf/RCMS100_BF_RCQTY.pdf";

            } else {

                String sql = "SELECT F1.NUMB,ROW_NUMBER() OVER(PARTITION BY F1.[RCDINVNO] ORDER BY F1.[RCDINVNO] ASC) AS NUMBbyINV,F1.[RCDINVNO],F1.[RCDPONO],F1.[RCDPOLINE]\n"
                        + ",(SELECT TOP 1 [RCDPOSTDATE] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDPOSTDATE\n"
                        + ",(SELECT TOP 1 [RCDITNO] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDITNO\n"
                        + ",F1.[RCDCUSMAT]\n"
                        + ",(SELECT TOP 1 [RCDMATDES] FROM [RMShipment].[dbo].[RCMBPD] WHERE [RCDINVNO] = F1.[RCDINVNO] AND [RCDPONO] = F1.[RCDPONO] AND [RCDPOLINE] = F1.[RCDPOLINE] AND [RCDJBNO] = F1.[RCDJBNO]) AS RCDMATDES\n"
                        + ",F1.RCDRCQTY\n"
                        + ",F1.RCDINVAMT\n"
                        + ",F1.RCDTPDT\n"
                        + ",F1.RCDSUM\n"
                        + ",DENSE_RANK() OVER(ORDER BY F1.NUMB,F1.[RCDINVNO] ) AS INVCNT\n"
                        + "\n"
                        + "FROM (\n"
                        + "	SELECT DENSE_RANK() OVER(ORDER BY SUBSTRING([RCDCUSMAT], 1, 2)) AS NUMB,[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT]\n"
                        + "	,SUM(ISNULL(RCDRCQTY,0)) AS RCDRCQTY,SUM(RCDINVAMT) AS RCDINVAMT, [RCDJBNO], [RCDTPDT], [RCDSUM]\n"
                        + "\n"
                        + "	FROM [RMShipment].[dbo].[RCMBPD]\n"
                        + "	WHERE [RCDJBNO] IN (SELECT DISTINCT RCHJBNO FROM [RMShipment].[dbo].[RCMBPH]\n"
                        + "	WHERE [RCHWHSE] = '" + wh + "' AND [RCHSPNO] = '" + sp + "' AND [RCHCUNO] = '" + cn + "' AND [RCHTPDT] = " + dt.replaceAll("-", "") + " ) \n";

                if (cksplit != null) {
                    if (cksplit.length > 0) {
                        sql += " AND ( ";
                    }

                    for (int i = 0; i < cksplit.length; i++) {

                        if (i == 0) {
                            sql += " RCDINVNO = '" + cksplit[i] + "' ";
                        } else {
                            sql += " OR RCDINVNO = '" + cksplit[i] + "' ";
                        }

                    }

                    if (cksplit.length > 0) {
                        sql += " ) ";
                    }
                }

                sql += " AND RCDPOSTDATE = '" + pd + "' GROUP BY [RCDJBNO],[RCDINVNO],[RCDPONO],[RCDPOLINE],[RCDCUSMAT],[RCDTPDT],[RCDSUM]\n"
                        + ") F1";

//                System.out.println(sql);
                connectiondb mssqlc = new connectiondb();
                connection = mssqlc.getConnection();
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();

                File reportFile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/jasper/RCMS100ReportRCQTY.jasper"));

                if (!reportFile.exists()) {
                    throw new JRRuntimeException("File *.jasper not found. The report design must be compiled first.");
                } else {
                    System.out.println("Found File Jasper");
                }

                byte[] bytes;

                File pdffile = new File(getServletConfig().getServletContext()
                        .getRealPath("/resources/pdf/RCMS100_RCQTY.pdf"));

                if (pdffile.exists()) { //Delete PDF File Before Create New
                    if (pdffile.delete()) {
                        System.out.println("DELETE PDF File Successful");
                    } else {
                        System.out.println("DELETE PDF File Failed");
                    }
                } else {
                    System.out.println("Not Found PDF Files");
                }

                System.out.println("generate PDF File......");

                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(rs));

                ServletContext servletContext = getServletContext();
                String realPath = servletContext.getRealPath(FILE_DEST);
                String saperate = realPath.contains(":") ? "\\" : "/";
                String path = realPath + saperate + "RCMS100_RCQTY.pdf";

                FileOutputStream out = new FileOutputStream(path);
                out.write(bytes, 0, bytes.length);

                pathPDF = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/resources/pdf/RCMS100_RCQTY.pdf";

            }

            rs.close();
            connection.close();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(RCM100Print.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(RCM100Print.class.getName()).log(Level.SEVERE, null, ex);
        }

//            System.out.println("pathPDF " + pathPDF);
        // String site = new String("http://www.photofuntoos.com");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", pathPDF);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
