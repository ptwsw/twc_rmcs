/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import com.google.gson.Gson;
import com.twc.rcms.dao.RCM100Dao;
import com.twc.rcms.model.RCMBPD;
import com.twc.rcms.model.RCMBPH;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class RCM100Get extends HttpServlet {

    private final Gson gson;

    public RCM100Get() {
        this.gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mode = request.getParameter("mode");
        String json = new String();
        Map<String, Object> m = new HashMap<String, Object>();

        if (mode.equals("getHead")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");
            String uid = request.getParameter("uid");

            RCM100Dao dao = new RCM100Dao();

            List<RCMBPH> ValDao = dao.findRCMHead(wh, sp, cn, dt.replaceAll("-", ""), uid);

            m.put("HList", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("getHead110")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");
            String uid = request.getParameter("uid");

            RCM100Dao dao = new RCM100Dao();

            List<RCMBPH> ValDao = dao.findRCMHead110(wh, sp, cn, dt.replaceAll("-", ""), uid);

            m.put("HList", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("getTable")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");

            RCM100Dao dao = new RCM100Dao();

            List<RCMBPD> ValDao = dao.findRCMDetailBy(wh, sp, cn, dt.replaceAll("-", ""));

            List<RCMBPD> productList = new ArrayList<RCMBPD>();

            double sumCanRe = 0.0;
            double sumRe = 0.0;

            Map<String, List<String>> map = new HashMap<String, List<String>>();
            if (ValDao.size() == 1) {
                List<String> ReNCanRe = new ArrayList<String>();
                sumRe += Double.parseDouble(ValDao.get(0).getGetReceive());
                sumCanRe += Double.parseDouble(ValDao.get(0).getCanReceive());
                ReNCanRe.add(String.valueOf(sumRe));
                ReNCanRe.add(String.valueOf(sumCanRe));
                map.put(ValDao.get(0).getRCDINVNO(), ReNCanRe);
            } else {
                for (int i = 0; i < ValDao.size(); i++) {
                    List<String> ReNCanRe = new ArrayList<String>();

                    if (i == 0) {
                        sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                        sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                    } else if (i == (ValDao.size() - 1)) {
                        if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i - 1).getRCDINVNO(), ReNCanRe);
                            sumRe = 0.0;
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe = 0.0;
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i).getRCDINVNO(), ReNCanRe);
                        } else {
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i).getRCDINVNO(), ReNCanRe);
                        }
                    } else {
                        if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i - 1).getRCDINVNO(), ReNCanRe);
                            sumRe = 0.0;
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe = 0.0;
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                        } else {
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                        }
                    }

                }
            }

            for (int i = 0; i < ValDao.size(); i++) {

                RCMBPD p1 = new RCMBPD();

                if (i == 0) {

                    p1.setRCDDLNO(ValDao.get(i).getRCDDLNO());
                    p1.setRCDINVNO(ValDao.get(i).getRCDINVNO());
                    p1.setRCDJBNO(ValDao.get(i).getRCDJBNO());
                    p1.setRCDPONO(ValDao.get(i).getCNTPO());
                    if (ValDao.get(i).getRCDAUTHO().equals("L3")) { //RCDAUTHO
                        p1.setRCDAUTHO("check"); //L3
                    } else {
                        p1.setRCDAUTHO(ValDao.get(i).getRCDAUTHO()); //L3
                    }
                    p1.setRCDCUSMAT(ValDao.get(i).getCNTMAT());
                    p1.setRCDPOLINE("Head");
                    p1.setRCDPOPRC("");
                    p1.setRCDINVPRC("");
                    p1.setRCDINVQTY(ValDao.get(i).getRCDITNO());
                    p1.setRCDCMXQTY("");
                    p1.setRCDRCQTY(ValDao.get(i).getRCDITDS());
                    p1.setRCDDUDATE("Head");
                    p1.setRCDDLDATE("");
                    p1.setRCDSUM("");
                    p1.setRCDBUM("");
                    p1.setRCDRATE(ValDao.get(i).getRCDRATE());
                    p1.setRCDEDT(ValDao.get(i).getPACK());
                    p1.setRCDPKTYPE("");
                    p1.setRCDMCTRL("");
                    p1.setSUMM(ValDao.get(i).getSUMM());
                    p1.setRCDMATCD("Head");
                    p1.setCNTMAT("");
                    p1.setRCDSTS(ValDao.get(i).getRCDSTS()); // Ststus
                    p1.setRCDPOSTDATE(ValDao.get(i).getRCDPOSTDATE());
                    p1.setRCDCTM(ValDao.get(i).getRCDCTM()); //PO
                    p1.setRCDBOXNO(ValDao.get(i).getRCDBOXNO());
                    p1.setRCDCDT(ValDao.get(i).getRCDCDT()); //REDBTN
                    p1.setRCDPLANT("");
                    p1.setRCDTTQTY(ValDao.get(i).getRCDTTQTY()); //RCSTS
                    p1.setRCDTOTAL(ValDao.get(i).getRCDTOTAL());
                    p1.setRCDMAX(ValDao.get(i).getRCDMAX());
                    p1.setRCDQSTS(ValDao.get(i).getRCDQSTS());
                    p1.setRCDPC(ValDao.get(i).getRCDPC());
                    p1.setRCDPOFLAG("");
                    p1.setRCDRJFLAG(ValDao.get(i).getRCDRJFLAG());
                    p1.setRCDINVREF(ValDao.get(i).getRCDINVREF());
                    p1.setRCDMATREF("");
                    p1.setISFLAG(ValDao.get(i).getISFLAG());
                    p1.setFULLPONO(ValDao.get(i).getFULLPONO());
                    p1.setRCDPOQTY(ValDao.get(i).getRCDPOQTY());
                    p1.setRCSPOQTY(ValDao.get(i).getRCSPOQTY());
                    p1.setGetReceive(map.get(ValDao.get(i).getRCDINVNO()).get(0));
                    p1.setCanReceive(map.get(ValDao.get(i).getRCDINVNO()).get(1));

                    productList.add(p1);

                } else {
                    if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                        p1.setRCDDLNO(ValDao.get(i).getRCDDLNO());
                        p1.setRCDJBNO(ValDao.get(i).getRCDJBNO());
                        p1.setRCDINVNO(ValDao.get(i).getRCDINVNO());
                        p1.setRCDPONO(ValDao.get(i).getCNTPO());
                        if (ValDao.get(i).getRCDAUTHO().equals("L3")) { //RCDAUTHO
                            p1.setRCDAUTHO("check"); //L3
                        } else {
                            p1.setRCDAUTHO(ValDao.get(i).getRCDAUTHO()); //L3
                        }
                        p1.setRCDCUSMAT(ValDao.get(i).getCNTMAT());
                        p1.setRCDPOLINE("Head");
                        p1.setRCDPOPRC("");
                        p1.setRCDINVPRC("");
                        p1.setRCDINVQTY(ValDao.get(i).getRCDITNO());
                        p1.setRCDCMXQTY("");
                        p1.setRCDRCQTY(ValDao.get(i).getRCDITDS());
                        p1.setRCDDUDATE("Head");
                        p1.setRCDDLDATE("");
                        p1.setRCDSUM("");
                        p1.setRCDBUM("");
                        p1.setRCDRATE(ValDao.get(i).getRCDRATE());
                        p1.setRCDEDT(ValDao.get(i).getPACK());
                        p1.setRCDPKTYPE("");
                        p1.setRCDMCTRL("");
                        p1.setSUMM(ValDao.get(i).getSUMM());
                        p1.setRCDMATCD("Head");
                        p1.setCNTMAT("");
                        p1.setRCDSTS(ValDao.get(i).getRCDSTS()); // Ststus
                        p1.setRCDPOSTDATE(ValDao.get(i).getRCDPOSTDATE());
                        p1.setRCDCTM(ValDao.get(i).getRCDCTM()); //PO
                        p1.setRCDBOXNO(ValDao.get(i).getRCDBOXNO());
                        p1.setRCDCDT(ValDao.get(i).getRCDCDT());
                        p1.setRCDPLANT("");
                        p1.setRCDTTQTY(ValDao.get(i).getRCDTTQTY()); //RCSTS
                        p1.setRCDTOTAL(ValDao.get(i).getRCDTOTAL());
                        p1.setRCDMAX(ValDao.get(i).getRCDMAX());
                        p1.setRCDQSTS(ValDao.get(i).getRCDQSTS());
                        p1.setRCDPC(ValDao.get(i).getRCDPC());
                        p1.setRCDPOFLAG("");
                        p1.setRCDRJFLAG(ValDao.get(i).getRCDRJFLAG());
                        p1.setRCDINVREF(ValDao.get(i).getRCDINVREF());
                        p1.setRCDMATREF("");
                        p1.setISFLAG(ValDao.get(i).getISFLAG());
                        p1.setFULLPONO(ValDao.get(i).getFULLPONO());
                        p1.setRCDPOQTY(ValDao.get(i).getRCDPOQTY());
                        p1.setRCSPOQTY(ValDao.get(i).getRCSPOQTY());
                        p1.setGetReceive(map.get(ValDao.get(i).getRCDINVNO()).get(0));
                        p1.setCanReceive(map.get(ValDao.get(i).getRCDINVNO()).get(1));

                        productList.add(p1);
                    }
                }

                RCMBPD p2 = new RCMBPD();
                p2.setRCDJBNO(ValDao.get(i).getRCDJBNO());
                p2.setRCDINVNO(ValDao.get(i).getRCDINVNO() + "|Hide");
//                p2.setRCDPONO(ValDao.get(i).getRCDPONO());

//                System.out.println(ValDao.get(i).getRCDINVNO() + " " + ValDao.get(i).getRCDPONO() + " " + ValDao.get(i).getRCDCUSMAT());
                if (i == 0) {
                    p2.setRCDPONO(ValDao.get(i).getRCDPONO());
                } else {
                    if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                        p2.setRCDPONO(ValDao.get(i).getRCDPONO());
                    } else {
                        if (!ValDao.get(i).getRCDPONO().equals(ValDao.get(i - 1).getRCDPONO())) {
                            p2.setRCDPONO(ValDao.get(i).getRCDPONO());
                        } else {
                            p2.setRCDPONO(ValDao.get(i).getRCDPONO() + "|Hide");
                        }
                    }
                }

                p2.setRCDPLANT(ValDao.get(i).getRCDPLANT());
                p2.setRCDPOLINE(ValDao.get(i).getRCDPOLINE());
                p2.setRCDDLNO("");
                p2.setRCDCUSMAT(ValDao.get(i).getRCDCUSMAT());
                p2.setRCDSUM(ValDao.get(i).getRCDSUM());
                p2.setRCDDLDATE(ValDao.get(i).getRCDDLDATE());
                p2.setRCDINVQTY(ValDao.get(i).getRCDINVQTY());
                p2.setRCDINVPRC(ValDao.get(i).getRCDINVPRC());
                p2.setRCDDUDATE(ValDao.get(i).getRCDDUDATE());
                p2.setRCDBUM(ValDao.get(i).getRCDBUM());
                p2.setRCDMCTRL(ValDao.get(i).getRCDMCTRL());
                p2.setRCDPOPRC(ValDao.get(i).getRCDPOPRC());
                p2.setRCDRCQTY(ValDao.get(i).getRCDRCQTY());
                p2.setRCDRATE(ValDao.get(i).getRCDRATE());
                p2.setRCDEDT(ValDao.get(i).getRCDEDT());
                p2.setRCDPKTYPE(ValDao.get(i).getRCDPKTYPE());
                p2.setRCDCMXQTY(ValDao.get(i).getRCDCMXQTY());
                p2.setSUMM("");
                p2.setRCDAUTHO("NULL"); //L3
                p2.setCNTMAT(ValDao.get(i).getRCDMATCD());
                p2.setRCDSTS(ValDao.get(i).getRCDSTS()); // Ststus
                p2.setRCDPOSTDATE("Nshow");
                p2.setRCDCTM(ValDao.get(i).getRCDCTM()); //PO
                p2.setRCDBOXNO(ValDao.get(i).getRCDBOXNO());
                p2.setRCDCDT(ValDao.get(i).getRCDCDT());
                p2.setRCDTTQTY(ValDao.get(i).getRCDTTQTY()); //RCSTS
                p2.setRCDTOTAL(ValDao.get(i).getRCDTOTAL());
                p2.setRCDMAX(ValDao.get(i).getRCDMAX());
                p2.setRCDQSTS(ValDao.get(i).getRCDQSTS());
                p2.setRCDPC(ValDao.get(i).getRCDPC());
                p2.setRCDPOFLAG(ValDao.get(i).getRCDPOFLAG());
                p2.setRCDRJFLAG(ValDao.get(i).getRCDRJFLAG());
                p2.setRCDINVREF(ValDao.get(i).getRCDINVREF());
                p2.setRCDMATREF(ValDao.get(i).getRCDMATREF());

                if (!ValDao.get(i).getRCDCUSMAT().equals(ValDao.get(i).getRCDMATCD())) { // CUSMAT != MATCD => RED
                    p2.setRCDMATCD("star");
                } else {
                    if (ValDao.get(i).getRCDRJFLAG() != null) {
                        if (ValDao.get(i).getRCDRJFLAG().equals("C")) { //Flag C => YELLOW
                            p2.setRCDMATCD("starY");
                        } else {
                            p2.setRCDMATCD(""); // OTHER
                        }
                    } else {
                        p2.setRCDMATCD(""); //NULL
                    }
                }

                p2.setISFLAG(ValDao.get(i).getISFLAG());
                p2.setFULLPONO(ValDao.get(i).getFULLPONO());
                p2.setRCDPOQTY(ValDao.get(i).getRCDPOQTY());
                p2.setRCSPOQTY(ValDao.get(i).getRCSPOQTY());
                p2.setGetReceive(ValDao.get(i).getGetReceive());
                p2.setCanReceive(ValDao.get(i).getCanReceive());

                productList.add(p2);
            }

            m.put("DList", productList);
            json = this.gson.toJson(m);

        } else if (mode.equals("getTable110")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");

            RCM100Dao dao = new RCM100Dao();

            List<RCMBPD> ValDao = dao.findRCMDetailBy110(wh, sp, cn, dt.replaceAll("-", ""));

            List<RCMBPD> productList = new ArrayList<RCMBPD>();

            double sumCanRe = 0.0;
            double sumRe = 0.0;

            Map<String, List<String>> map = new HashMap<String, List<String>>();
            if (ValDao.size() == 1) {
                List<String> ReNCanRe = new ArrayList<String>();
                sumRe += Double.parseDouble(ValDao.get(0).getGetReceive());
                sumCanRe += Double.parseDouble(ValDao.get(0).getCanReceive());
                ReNCanRe.add(String.valueOf(sumRe));
                ReNCanRe.add(String.valueOf(sumCanRe));
                map.put(ValDao.get(0).getRCDINVNO(), ReNCanRe);
            } else {
                for (int i = 0; i < ValDao.size(); i++) {
                    List<String> ReNCanRe = new ArrayList<String>();

                    if (i == 0) {
                        sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                        sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                    } else if (i == (ValDao.size() - 1)) {
                        if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i - 1).getRCDINVNO(), ReNCanRe);
                            sumRe = 0.0;
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe = 0.0;
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i).getRCDINVNO(), ReNCanRe);
                        } else {
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i).getRCDINVNO(), ReNCanRe);
                        }
                    } else {
                        if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                            ReNCanRe.add(String.valueOf(sumRe));
                            ReNCanRe.add(String.valueOf(sumCanRe));
                            map.put(ValDao.get(i - 1).getRCDINVNO(), ReNCanRe);
                            sumRe = 0.0;
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe = 0.0;
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                        } else {
                            sumRe += Double.parseDouble(ValDao.get(i).getGetReceive());
                            sumCanRe += Double.parseDouble(ValDao.get(i).getCanReceive());
                        }
                    }

                }
            }

            for (int i = 0; i < ValDao.size(); i++) {

                RCMBPD p1 = new RCMBPD();

                if (i == 0) {

                    p1.setRCDDLNO(ValDao.get(i).getRCDDLNO());
                    p1.setRCDINVNO(ValDao.get(i).getRCDINVNO());
                    p1.setRCDJBNO(ValDao.get(i).getRCDJBNO());
                    p1.setRCDPONO(ValDao.get(i).getCNTPO());
                    if (ValDao.get(i).getRCDAUTHO().equals("L3")) { //RCDAUTHO
                        p1.setRCDAUTHO("check"); //L3
                    } else {
                        p1.setRCDAUTHO(ValDao.get(i).getRCDAUTHO()); //L3
                    }
                    p1.setRCDCUSMAT(ValDao.get(i).getCNTMAT());
                    p1.setRCDPOLINE("Head");
                    p1.setRCDPOPRC("");
                    p1.setRCDINVPRC("");
                    p1.setRCDINVQTY(ValDao.get(i).getRCDITNO());
                    p1.setRCDCMXQTY("");
                    p1.setRCDRCQTY(ValDao.get(i).getRCDITDS());
                    p1.setRCDDUDATE("Head");
                    p1.setRCDDLDATE("");
                    p1.setRCDSUM("");
                    p1.setRCDBUM("");
                    p1.setRCDRATE(ValDao.get(i).getRCDRATE());
                    p1.setRCDEDT(ValDao.get(i).getPACK());
                    p1.setRCDPKTYPE("");
                    p1.setRCDMCTRL("");
                    p1.setSUMM(ValDao.get(i).getSUMM());
                    p1.setRCDMATCD("Head");
                    p1.setCNTMAT("");
                    p1.setRCDSTS(ValDao.get(i).getRCDSTS()); // Ststus
                    p1.setRCDPOSTDATE(ValDao.get(i).getRCDPOSTDATE());
                    p1.setRCDCTM(ValDao.get(i).getRCDCTM()); //PO
                    p1.setRCDBOXNO(ValDao.get(i).getRCDBOXNO());
                    p1.setRCDCDT(ValDao.get(i).getRCDCDT()); //REDBTN
                    p1.setRCDPLANT("");
                    p1.setRCDTTQTY(ValDao.get(i).getRCDTTQTY()); //RCSTS
                    p1.setRCDTOTAL(ValDao.get(i).getRCDTOTAL());
                    p1.setRCDMAX(ValDao.get(i).getRCDMAX());
                    p1.setRCDQSTS(ValDao.get(i).getRCDQSTS());
                    p1.setRCDPC(ValDao.get(i).getRCDPC());
                    p1.setRCDPOFLAG("");
                    p1.setRCDRJFLAG(ValDao.get(i).getRCDRJFLAG());
                    p1.setRCDINVREF(ValDao.get(i).getRCDINVREF());
                    p1.setRCDMATREF("");
                    p1.setISFLAG(ValDao.get(i).getISFLAG());
                    p1.setFULLPONO(ValDao.get(i).getFULLPONO());
                    p1.setRCDCPERC(ValDao.get(i).getRCDCPERC());
                    p1.setRCDPOQTY(ValDao.get(i).getRCDPOQTY());
                    p1.setRCSPOQTY(ValDao.get(i).getRCSPOQTY());
                    p1.setGetReceive(map.get(ValDao.get(i).getRCDINVNO()).get(0));
                    p1.setCanReceive(map.get(ValDao.get(i).getRCDINVNO()).get(1));

                    productList.add(p1);

                } else {
                    if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                        p1.setRCDDLNO(ValDao.get(i).getRCDDLNO());
                        p1.setRCDJBNO(ValDao.get(i).getRCDJBNO());
                        p1.setRCDINVNO(ValDao.get(i).getRCDINVNO());
                        p1.setRCDPONO(ValDao.get(i).getCNTPO());
                        if (ValDao.get(i).getRCDAUTHO().equals("L3")) { //RCDAUTHO
                            p1.setRCDAUTHO("check"); //L3
                        } else {
                            p1.setRCDAUTHO(ValDao.get(i).getRCDAUTHO()); //L3
                        }
                        p1.setRCDCUSMAT(ValDao.get(i).getCNTMAT());
                        p1.setRCDPOLINE("Head");
                        p1.setRCDPOPRC("");
                        p1.setRCDINVPRC("");
                        p1.setRCDINVQTY(ValDao.get(i).getRCDITNO());
                        p1.setRCDCMXQTY("");
                        p1.setRCDRCQTY(ValDao.get(i).getRCDITDS());
                        p1.setRCDDUDATE("Head");
                        p1.setRCDDLDATE("");
                        p1.setRCDSUM("");
                        p1.setRCDBUM("");
                        p1.setRCDRATE(ValDao.get(i).getRCDRATE());
                        p1.setRCDEDT(ValDao.get(i).getPACK());
                        p1.setRCDPKTYPE("");
                        p1.setRCDMCTRL("");
                        p1.setSUMM(ValDao.get(i).getSUMM());
                        p1.setRCDMATCD("Head");
                        p1.setCNTMAT("");
                        p1.setRCDSTS(ValDao.get(i).getRCDSTS()); // Ststus
                        p1.setRCDPOSTDATE(ValDao.get(i).getRCDPOSTDATE());
                        p1.setRCDCTM(ValDao.get(i).getRCDCTM()); //PO
                        p1.setRCDBOXNO(ValDao.get(i).getRCDBOXNO());
                        p1.setRCDCDT(ValDao.get(i).getRCDCDT());
                        p1.setRCDPLANT("");
                        p1.setRCDTTQTY(ValDao.get(i).getRCDTTQTY()); //RCSTS
                        p1.setRCDTOTAL(ValDao.get(i).getRCDTOTAL());
                        p1.setRCDMAX(ValDao.get(i).getRCDMAX());
                        p1.setRCDQSTS(ValDao.get(i).getRCDQSTS());
                        p1.setRCDPC(ValDao.get(i).getRCDPC());
                        p1.setRCDPOFLAG("");
                        p1.setRCDRJFLAG(ValDao.get(i).getRCDRJFLAG());
                        p1.setRCDINVREF(ValDao.get(i).getRCDINVREF());
                        p1.setRCDMATREF("");
                        p1.setISFLAG(ValDao.get(i).getISFLAG());
                        p1.setFULLPONO(ValDao.get(i).getFULLPONO());
                        p1.setRCDCPERC(ValDao.get(i).getRCDCPERC());
                        p1.setRCDPOQTY(ValDao.get(i).getRCDPOQTY());
                        p1.setRCSPOQTY(ValDao.get(i).getRCSPOQTY());
                        p1.setGetReceive(map.get(ValDao.get(i).getRCDINVNO()).get(0));
                        p1.setCanReceive(map.get(ValDao.get(i).getRCDINVNO()).get(1));

                        productList.add(p1);
                    }
                }

                RCMBPD p2 = new RCMBPD();
                p2.setRCDJBNO(ValDao.get(i).getRCDJBNO());
                p2.setRCDINVNO(ValDao.get(i).getRCDINVNO() + "|Hide");
//                p2.setRCDPONO(ValDao.get(i).getRCDPONO());

//                System.out.println(ValDao.get(i).getRCDINVNO() + " " + ValDao.get(i).getRCDPONO() + " " + ValDao.get(i).getRCDCUSMAT());
                if (i == 0) {
                    p2.setRCDPONO(ValDao.get(i).getRCDPONO());
                } else {
                    if (!ValDao.get(i).getRCDINVNO().equals(ValDao.get(i - 1).getRCDINVNO())) {
                        p2.setRCDPONO(ValDao.get(i).getRCDPONO());
                    } else {
                        if (!ValDao.get(i).getRCDPONO().equals(ValDao.get(i - 1).getRCDPONO())) {
                            p2.setRCDPONO(ValDao.get(i).getRCDPONO());
                        } else {
                            p2.setRCDPONO(ValDao.get(i).getRCDPONO() + "|Hide");
                        }
                    }
                }

                p2.setRCDPLANT(ValDao.get(i).getRCDPLANT());
                p2.setRCDPOLINE(ValDao.get(i).getRCDPOLINE());
                p2.setRCDDLNO("");
                p2.setRCDCUSMAT(ValDao.get(i).getRCDCUSMAT());
                p2.setRCDSUM(ValDao.get(i).getRCDSUM());
                p2.setRCDDLDATE(ValDao.get(i).getRCDDLDATE());
                p2.setRCDINVQTY(ValDao.get(i).getRCDINVQTY());
                p2.setRCDINVPRC(ValDao.get(i).getRCDINVPRC());
                p2.setRCDDUDATE(ValDao.get(i).getRCDDUDATE());
                p2.setRCDBUM(ValDao.get(i).getRCDBUM());
                p2.setRCDMCTRL(ValDao.get(i).getRCDMCTRL());
                p2.setRCDPOPRC(ValDao.get(i).getRCDPOPRC());
                p2.setRCDRCQTY(ValDao.get(i).getRCDRCQTY());
                p2.setRCDRATE(ValDao.get(i).getRCDRATE());
                p2.setRCDEDT(ValDao.get(i).getRCDEDT());
                p2.setRCDPKTYPE(ValDao.get(i).getRCDPKTYPE());
                p2.setRCDCMXQTY(ValDao.get(i).getRCDCMXQTY());
                p2.setSUMM("");
                p2.setRCDAUTHO("NULL"); //L3
                p2.setCNTMAT(ValDao.get(i).getRCDMATCD());
                p2.setRCDSTS(ValDao.get(i).getRCDSTS()); // Ststus
                p2.setRCDPOSTDATE("Nshow");
                p2.setRCDCTM(ValDao.get(i).getRCDCTM()); //PO
                p2.setRCDBOXNO(ValDao.get(i).getRCDBOXNO());
                p2.setRCDCDT(ValDao.get(i).getRCDCDT());
                p2.setRCDTTQTY(ValDao.get(i).getRCDTTQTY()); //RCSTS
                p2.setRCDTOTAL(ValDao.get(i).getRCDTOTAL());
                p2.setRCDMAX(ValDao.get(i).getRCDMAX());
                p2.setRCDQSTS(ValDao.get(i).getRCDQSTS());
                p2.setRCDPC(ValDao.get(i).getRCDPC());
                p2.setRCDPOFLAG(ValDao.get(i).getRCDPOFLAG());
                p2.setRCDRJFLAG(ValDao.get(i).getRCDRJFLAG());
                p2.setRCDINVREF(ValDao.get(i).getRCDINVREF());
                p2.setRCDMATREF(ValDao.get(i).getRCDMATREF());

                if (!ValDao.get(i).getRCDCUSMAT().equals(ValDao.get(i).getRCDMATCD())) { // CUSMAT != MATCD => RED
                    p2.setRCDMATCD("star");
                } else {
                    if (ValDao.get(i).getRCDRJFLAG() != null) {
                        if (ValDao.get(i).getRCDRJFLAG().equals("C")) { //Flag C => YELLOW
                            p2.setRCDMATCD("starY");
                        } else {
                            p2.setRCDMATCD(""); // OTHER
                        }
                    } else {
                        p2.setRCDMATCD(""); //NULL
                    }
                }

                p2.setISFLAG(ValDao.get(i).getISFLAG());
                p2.setFULLPONO(ValDao.get(i).getFULLPONO());
                p2.setRCDCPERC(ValDao.get(i).getRCDCPERC());
                p2.setRCDPOQTY(ValDao.get(i).getRCDPOQTY());
                p2.setRCSPOQTY(ValDao.get(i).getRCSPOQTY());
                p2.setGetReceive(ValDao.get(i).getGetReceive());
                p2.setCanReceive(ValDao.get(i).getCanReceive());

                productList.add(p2);
            }

            m.put("DList", productList);
            json = this.gson.toJson(m);

        } else if (mode.equals("insertRCMDetailToQRMMAS")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");
            String inv = request.getParameter("inv");
            String postingDate = request.getParameter("postdate");

            String uid = request.getParameter("uid");

            boolean stsout = true;

            List<RCMBPD> ddetail = new RCM100Dao().findRCMDetailByINV(wh, sp, cn, dt.replaceAll("-", ""), inv); // STS = 1

            boolean updSTS2 = new RCM100Dao().updateSTSTo2(wh, sp, cn, dt.replaceAll("-", ""), inv, postingDate.replaceAll("-", ""));

            if (cn.equals("211809")) {
                if (wh.equals("WH1") || wh.equals("WH2") || wh.equals("WH3")) {
//
                    if (ddetail.get(0).getRCDSTS().equals("1")) { //Insert QRMMAS QRMTRA When RCDSTS = 1

                        boolean instQMAS = new RCM100Dao().insertQRMMASbyRCM(ddetail, uid, inv, wh, sp, cn, dt); //Merge IN this one
                        if (instQMAS == false) {
                            stsout = false;
                        } else {
                        }
//                        boolean instQTRA = new RCM100Dao().insertQRMTRAbyRCM(ddetail, uid, inv, wh, sp, cn, dt);
//                        if (instQTRA == false) {
//                            stsout = false;
//                        }
                    }
                }
            }

//            if (ddetail.get(0).getRCDSTS().equals("1")) { // Update Status RCMBPD 1 To 2 When RCDSTS = 1
//                boolean updSTS2 = new RCM100Dao().updateSTSTo2(wh, sp, cn, dt.replaceAll("-", ""), inv, postingDate.replaceAll("-", ""));
//            }
            m.put("insertMASTRA", stsout);
            json = this.gson.toJson(m);

        } else if (mode.equals("UpdateSTSRCM100")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt");
            String inv = request.getParameter("inv");

//            String uid = request.getParameter("uid");
//            List<RCMBPD> ddetail = new RCM100Dao().findRCMDetailByINV(wh, sp, cn, dt.replaceAll("-", ""), inv);
            boolean updSTS2 = new RCM100Dao().updateSTSTo2RCM100(wh, sp, cn, dt.replaceAll("-", ""), inv);

            m.put("ChangeSTS2", updSTS2);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateEditPoNo")) {
            String job = request.getParameter("job");
            String inv = request.getParameter("inv");
            String poOld = request.getParameter("poold");
            String poNew = request.getParameter("ponew");

            boolean upd = new RCM100Dao().updateEditPoNo(job, inv, poOld, poNew);

            m.put("updPoNo", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updatePriInv")) {

            String job = request.getParameter("job");
            String inv = request.getParameter("inv");

            boolean upd = new RCM100Dao().updatePriInv(job, inv);

            m.put("updInv", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateRejectInv")) {
            String job = request.getParameter("job");
            String inv = request.getParameter("inv");

            boolean upd = new RCM100Dao().updateRejectInv(job, inv);

            m.put("updInv", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateClearRejectInv")) {
            String job = request.getParameter("job");
            String inv = request.getParameter("inv");

            boolean upd = new RCM100Dao().updateClearRejectInv(job, inv);

            m.put("updInv", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateRejectMat")) {
            String job = request.getParameter("job");
            String invno = request.getParameter("invno");
            String po = request.getParameter("po");
            String poline = request.getParameter("poline");
            String mat = request.getParameter("mat");

            boolean upd = new RCM100Dao().updateRejectMat(job, invno, po, poline, mat);
            boolean updHold = new RCM100Dao().updateHoldMat(job, invno); //Update Hold Flag

            m.put("updMat", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateClearRejectMat")) {
            String job = request.getParameter("job");
            String invno = request.getParameter("invno");
            String po = request.getParameter("po");
            String poline = request.getParameter("poline");
            String mat = request.getParameter("mat");

            boolean upd = new RCM100Dao().updateClearRejectMat(job, invno, po, poline, mat);
//            boolean updHold = new RCM100Dao().updateHoldMat(job, invno); //Update Hold Flag

            m.put("updMat", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateInv")) {

            String job = request.getParameter("job");
            String invold = request.getParameter("invold");
            String invnew = request.getParameter("invnew");

            boolean upd = new RCM100Dao().updateInv(job, invold, invnew);

            m.put("updInv", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateMat200")) {

            String job = request.getParameter("job");
            String inv = request.getParameter("inv");
            String po = request.getParameter("po");
            String poline = request.getParameter("poline");
            String cusmatOld = request.getParameter("cusmatOld");
            String cusmatNew = request.getParameter("cusmatNew");

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cs = request.getParameter("cs");
            String dt = request.getParameter("dt");

            JSONObject upd = new RCM100Dao().updateCusMat200(job, inv, po, poline, cusmatOld, cusmatNew, wh, sp, cs, dt.replaceAll("-", ""));

            m.put("updCusMat", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("updateMat")) {

            String job = request.getParameter("job");
            String inv = request.getParameter("inv");
            String po = request.getParameter("po");
            String poline = request.getParameter("poline");
            String cusmat = request.getParameter("cusmat");

            boolean upd = new RCM100Dao().updateCusMat(job, inv, po, poline, cusmat);

            m.put("updCusMat", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("editPKType")) {
            String job = request.getParameter("job");
            String inv = request.getParameter("inv");
            String po = request.getParameter("po");
            String poline = request.getParameter("poline");
            String cusmat = request.getParameter("cusmat");
            String packtype = request.getParameter("packtype");

            boolean upd = new RCM100Dao().updatePackType(job, inv, po, poline, cusmat, packtype);

            m.put("updPackType", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("cancelInvoice")) {

            String inv = request.getParameter("inv");
            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt").replaceAll("-", "");

            boolean stsCanCel = true;

            if (!inv.equals("")) {
                //deleteQRMMAS  deleteQRMTRA updateSTSRCMBPD in This One
                boolean delMAS = new RCM100Dao().deleteQRMMASByInvoice(inv, wh, sp, cn, dt);
                if (delMAS == false) {
                    stsCanCel = false;
                }
                //deleteQRMTRA
//                boolean delTRA = new RCM100Dao().deleteQRMTRAByInvoice(inv, wh, sp, cn, dt);
//                if (delTRA == false) {
//                    stsCanCel = false;
//                }
                //updateSTS RCMBPD
//                boolean upSTS = new RCM100Dao().updateSTSTo1ByINV(inv, wh, sp, cn, dt);
//                if (upSTS == false) {
//                    stsCanCel = false;
//                }
            }

            m.put("delByInv", stsCanCel);
            json = this.gson.toJson(m);

        } else if (mode.equals("updatePOLine")) {

            String job = request.getParameter("job");
            String inv = request.getParameter("inv");
            String po = request.getParameter("po");
            String mat = request.getParameter("mat");
            String oldpoline = request.getParameter("poline");
            String newpoline = request.getParameter("newpoline");

            boolean rs = new RCM100Dao().updatePOLine(job, inv, po, oldpoline, newpoline, mat);

            m.put("updPOLine", rs);
            json = this.gson.toJson(m);

        } else if (mode.equals("clearYellowWhenNotFoundRef")) {

            String job = request.getParameter("job");
            String inv = request.getParameter("inv");
            String po = request.getParameter("po");
            String poline = request.getParameter("poline");
            String mat = request.getParameter("mat");

            JSONObject upd = new RCM100Dao().updateClearYLStar(job, inv, po, poline, mat);

            m.put("updCusMat", upd);
            json = this.gson.toJson(m);

        } else if (mode.equals("getPostDate")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cn = request.getParameter("cn");
            String dt = request.getParameter("dt").replaceAll("-", "");

            List<RCMBPD> resObj = new RCM100Dao().findPostDate(wh, sp, cn, dt);

            m.put("resData", resObj);
            json = this.gson.toJson(m);
        }

        /**
         * ****************************
         * Return JSON to Client ****************************
         */
        // SET Response header
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        // SET List data to Json
        response.getWriter().write(json);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
