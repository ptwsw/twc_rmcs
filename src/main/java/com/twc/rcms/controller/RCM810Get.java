/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.twc.rcms.dao.RCM810Dao;
import com.twc.rcms.model.RCMBPH;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;

/**
 *
 * @author 93176
 */
public class RCM810Get extends HttpServlet {

    private final Gson gson;

    public RCM810Get() {
        this.gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mode = request.getParameter("mode");
        String json = new String();
        Map<String, Object> m = new HashMap<String, Object>();

        if (mode.equals("deleteBillPO")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cu = request.getParameter("cu");
            String dt = request.getParameter("dt");
            String bill = request.getParameter("bill");
            String po = request.getParameter("po");

            RCM810Dao dao = new RCM810Dao();

            HashMap<String, String> res = dao.deleteBillnPO(wh, sp, cu, dt.replaceAll("-", ""), bill, po);
//            boolean res = dao.deleteBillnPO(wh, sp, cu, dt.replaceAll("-", ""), bill, po);

            m.put("delVal", res);
            json = this.gson.toJson(m);

        } else if (mode.equals("ckBPH")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String cu = request.getParameter("cu");
            String dt = request.getParameter("dt");

            //select from RCMBPH
            RCM810Dao dao = new RCM810Dao();
            RCMBPH res = dao.checkBPH(wh, sp, cu, dt.replaceAll("-", ""));

            m.put("ckVal", res.getRCHSTS());
            json = this.gson.toJson(m);

        } else if (mode.equals("getdata")) {

            String jbno = request.getParameter("jbno");
//            System.out.println("jbno " + jbno);
//            System.out.println("this Test Get Data P New");

            //http://10.11.9.142:8080/wms-service/rest/user/auth/93176/?p=a93176
            String charset = "UTF-8";
            String url = "http://10.11.18.239:8080/PMS/APP/GETDatapackline?jobno=2022050003";

            URLConnection connection = new URL(url).openConnection();
            connection.setRequestProperty("Accept-Charset", charset);
            InputStream res = connection.getInputStream();

//            try ( Scanner scanner = new Scanner(res)) {
//                String responseBody = scanner.useDelimiter("\\A").next();
//                JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
//                JsonArray JArray = new JsonParser().parse(responseBody).getAsJsonArray();
//
//                System.out.println(JArray);
//
////                for (int i = 0; i < JArray.size(); i++) {
////                    
////                }
////                System.out.println(jsonObject);
////                for (int i = 0; i < jsonObject.size(); i++) {
////                    System.out.println(jsonObject.get("pono"));
////                    JsonObject ary = new JsonObject();
////                    ary.add("pono", jsonObject.get("pono"));
////                    ary.add("jobno", jsonObject.get("jobno"));
////                    ary.add("invno", jsonObject.get("invno"));
////                    ary.add("delyno", jsonObject.get("delyno"));
////                    ary.add("itno", jsonObject.get("itno"));
////
////                    objJ.add(ary);
////                    ary.put(i, jsonObject.get("jobno"));
////                    ary.put(i, jsonObject.get("invno"));
////                    ary.put(i, jsonObject.get("delyno"));
////                    ary.put(i, jsonObject.get("itno"));
////            }
////                System.out.println(objJ);
//                m.put("daTa", false);
//
////                System.out.println(m);
//                json = this.gson.toJson(m);
//
//            }

        }

        /**
         * ****************************
         * Return JSON to Client ****************************
         */
        // SET Response header
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        // SET List data to Json
        response.getWriter().write(json);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
