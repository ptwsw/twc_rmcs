/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.rcms.controller;

import com.google.gson.Gson;
import com.twc.rcms.dao.RCM200Dao;
import com.twc.rcms.model.QWHMAS;
import com.twc.rcms.model.RCMBPD;
import com.twc.rcms.model.RCMBPH;
import com.twc.rcms.model.RCMBPT;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 93176
 */
public class RCM200Get extends HttpServlet {

    private final Gson gson;

    public RCM200Get() {
        this.gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mode = request.getParameter("mode");
        String json = new String();
        Map<String, Object> m = new HashMap<String, Object>();

        if (mode.equals("getWH")) {

            String uid = request.getParameter("uid");

            RCM200Dao dao = new RCM200Dao();

//            List<QWHMAS> ValDao = dao.findWH();
            List<QWHMAS> VVal = dao.findWHbyUID(uid);

            m.put("whList", VVal);
            json = this.gson.toJson(m);

        } else if (mode.equals("getWHbyCode")) {

            String code = request.getParameter("code");

            RCM200Dao dao = new RCM200Dao();

            List<QWHMAS> ValDao = dao.findWHbyCode(code);

            m.put("whList", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("getTOTbyWhSpDt")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String dt = request.getParameter("dt");
            String cn = request.getParameter("cn");

            RCM200Dao dao = new RCM200Dao();

            List<RCMBPH> ValDao = dao.findTOTbyWhSpDt(wh, sp, cn, dt);

            m.put("totalLS", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("getTOTbyWhSpDt210")) {

            String wh = request.getParameter("wh");
            String sp = request.getParameter("sp");
            String dt = request.getParameter("dt");
            String cn = request.getParameter("cn");

            String inv = request.getParameter("inv");
            String pono = request.getParameter("pono");
            String mat = request.getParameter("mat");

            RCM200Dao dao = new RCM200Dao();

            List<RCMBPD> ValDao = dao.findTOTbyWhSpDt210(wh, sp, cn, dt, inv, pono, mat);

            m.put("totalLS", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("getDLDATEbyQRCode")) {

            String qr = request.getParameter("qr");

            RCM200Dao dao = new RCM200Dao();

            List<RCMBPD> ValDao = dao.findDLDATEbyQRcode(qr);

            m.put("datebyQR", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("insertRCMBPT")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String dts = request.getParameter("dt");
            String cns = request.getParameter("cn");
            String seq = request.getParameter("seq");
            String ctnid = request.getParameter("ctnid");
            String ctcode = request.getParameter("ctcode");
            String totpc = request.getParameter("totpc");
            String uid = request.getParameter("uid");

            boolean cc = new RCM200Dao().insertRCMBPT(whs, sps, cns, dts, seq, ctnid, ctcode, totpc, uid);
            m.put("ccInsert", cc);
            json = this.gson.toJson(m);

        } else if (mode.equals("insertRCMBPTPal")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String dts = request.getParameter("dt");
            String cns = request.getParameter("cn");
            String seq = request.getParameter("seq");
            String palno = request.getParameter("palno");
            String ctnid = request.getParameter("ctnid");
            String mcode = request.getParameter("mcode");
            String totpc = request.getParameter("totpc");
            String uid = request.getParameter("uid");

            boolean cc = new RCM200Dao().insertRCMBPTPal(whs, sps, cns, dts, seq, palno, ctnid, mcode, totpc, uid);
            m.put("ccInsert", cc);
            json = this.gson.toJson(m);

        } else if (mode.equals("findRCMBPT")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            List<RCMBPT> ValDao = new RCM200Dao().findRCMBPT(whs, sps, cns, dts);

            m.put("RCMBPTList", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("findRCMBPD210")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            String inv = request.getParameter("inv");
            String pono = request.getParameter("pono");
            String mat = request.getParameter("mat");

            List<RCMBPD> ValDao = new RCM200Dao().findRCMBPD210(whs, sps, cns, dts, inv, pono, mat);

            m.put("RCMBPDList", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("findMaxSeq")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            List<RCMBPT> ValDao = new RCM200Dao().findMaxSeq(whs, sps, cns, dts);

            m.put("RCMBPTList", ValDao);
            json = this.gson.toJson(m);

        } else if (mode.equals("delALLROW")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            boolean ret = new RCM200Dao().deleteALLROW(whs, sps, cns, dts);
//            boolean retdel = new RCM200Dao().updateSTSnQTYafterDEL(whs, sps, cns, dts);

            m.put("delSTS", ret);
            json = this.gson.toJson(m);

        } else if (mode.equals("210Reject")) {

            String whs = request.getParameter("whs");
            String sps = request.getParameter("sps");
            String cns = request.getParameter("cns");
            String dts = request.getParameter("dts");

            String inv = request.getParameter("inv");
            String pono = request.getParameter("pon");
            String mat = request.getParameter("mtc");

            String idAr = request.getParameter("idAr");

            JSONArray jsonArr = new JSONArray(idAr);

            boolean rs = true;

            if (jsonArr.length() > 0) {
                for (int i = 0; i < jsonArr.length(); i++) {
                    boolean Upd = new RCM200Dao().updateRejectBPDbyID(whs, sps, cns, dts, inv, pono, mat, jsonArr.getString(i));

                    if (Upd == false) {
                        rs = false;
                    }
                }
            }

            m.put("updRej", rs);
            json = this.gson.toJson(m);

        } else if (mode.equals("210Hold")) {

            String whs = request.getParameter("whs");
            String sps = request.getParameter("sps");
            String cns = request.getParameter("cns");
            String dts = request.getParameter("dts");

            String inv = request.getParameter("inv");
            String pono = request.getParameter("pon");
            String mat = request.getParameter("mtc");

//            String idAr = request.getParameter("idAr");
//            JSONArray jsonArr = new JSONArray(idAr);
            boolean rs = true;

//            if (jsonArr.length() > 0) {
//                for (int i = 0; i < jsonArr.length(); i++) {
            boolean Upd = new RCM200Dao().updateHoldBPDbyID(whs, sps, cns, dts, inv, pono, mat);
//                boolean Upd = new RCM200Dao().updateHoldBPDbyID(whs, sps, cns, dts, inv, pono, mat, jsonArr.getString(0));

            if (Upd == false) {
                rs = false;
            }
//                }
//            }

            m.put("updHold", rs);
            json = this.gson.toJson(m);

        } else if (mode.equals("getAllIDinPALLET")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            String palno = request.getParameter("palno");
            String inv = request.getParameter("inv");
            String pono = request.getParameter("po");
            String mat = request.getParameter("mt");

            List<RCMBPD> list = new RCM200Dao().selectAllIDbyPALLET(whs, sps, cns, dts, palno, inv, pono, mat);

            m.put("IDList", list);
            json = this.gson.toJson(m);

        } else if (mode.equals("ckCARTONdup")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String ctnid = request.getParameter("ctnid");
            String ctcode = request.getParameter("ctcode");

            boolean ck = new RCM200Dao().ckCTIDdup(whs, sps, cns, dts, ctnid, ctcode);

            m.put("ckRES", ck);
            json = this.gson.toJson(m);

        } else if (mode.equals("ckCARTONdupPal")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String ctnid = request.getParameter("ctnid");

            boolean ck = new RCM200Dao().ckCTIDdupPal(whs, sps, cns, dts, ctnid);

            m.put("ckRES", ck);
            json = this.gson.toJson(m);

        } else if (mode.equals("findRCMBPTbyCARTON")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String ctnid = request.getParameter("ctnid");
            String ctcode = request.getParameter("ctcode");

            boolean ck12 = true;

            if (ctnid.length() > 10) {
                //update box
                ctnid = "[RCDID] = '" + ctnid.trim() + "' AND [RCDCUSMAT] = '" + ctcode + "' ";
                boolean up1 = new RCM200Dao().updateSTSnQTY(whs, sps, cns, dts, ctnid);

                if (up1 == false) {
                    ck12 = false;
                }

            } else {
                //update 1 pallet
                ctnid = "[RCDPALNO] = '" + ctnid.trim() + "'";
                List<RCMBPD> palletList = new RCM200Dao().findALLidBYPallet(whs, sps, cns, dts, ctnid);

                for (int i = 0; i < palletList.size(); i++) {
//                    System.out.println("" + palletList.get(i).getRCDID());
                    String getctnid = "[RCDID] = '" + palletList.get(i).getRCDID().trim() + "' AND [RCDCUSMAT] = '" + palletList.get(i).getRCDCUSMAT().trim() + "' ";
                    boolean up2 = new RCM200Dao().updateSTSnQTY(whs, sps, cns, dts, getctnid);

                    if (up2 == false) {
                        ck12 = false;
                    }

                }

            }

            m.put("upRES", ck12);
            json = this.gson.toJson(m);

        } else if (mode.equals("findRCMBPTbyCARTONUpdateSTS")) {

//            String whs = request.getParameter("wh");
//            String sps = request.getParameter("sp");
//            String cns = request.getParameter("cn");
//            String dts = request.getParameter("dt");
//            String ctnid = request.getParameter("ctnid");
//            String ctcode = request.getParameter("ctcode");
//
//            boolean ck12 = true;
//
//            if (ctnid.length() > 10) {
//                //update box
//                ctnid = "[RCDID] = '" + ctnid.trim() + "' AND [RCDCUSMAT] = '" + ctcode + "' ";
//                boolean up1 = new RCM200Dao().updateSTSnQTYConfirm(whs, sps, cns, dts, ctnid);
//
//                System.out.println("up1 " + up1);
//                if (up1 == false) {
//                    ck12 = false;
//                }
//
//            } else {
//                //update 1 pallet
//                ctnid = "[RCDPALNO] = '" + ctnid.trim() + "'";
//                List<RCMBPD> palletList = new RCM200Dao().findALLidBYPallet(whs, sps, cns, dts, ctnid);
//
//                for (int i = 0; i < palletList.size(); i++) {
////                    System.out.println("" + palletList.get(i).getRCDID());
//                    String getctnid = "[RCDID] = '" + palletList.get(i).getRCDID().trim() + "' AND [RCDCUSMAT] = '" + palletList.get(i).getRCDCUSMAT().trim() + "' ";
//                    boolean up2 = new RCM200Dao().updateSTSnQTYConfirm(whs, sps, cns, dts, getctnid);
//
//                    if (up2 == false) {
//                        ck12 = false;
//                    }
//
//                    System.out.println("up2 " + up2);
//                }
//
//            }
//
//            m.put("upRES", ck12);
//            json = this.gson.toJson(m);
        } else if (mode.equals("ckSTSDetailForConfirmBtn")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            boolean ckSTS = false;

            List<RCMBPD> ck = new RCM200Dao().selectRCMBPD(whs, sps, cns, dts);

            if (!ck.isEmpty()) {
                for (int i = 0; i < ck.size(); i++) {
                    if (Integer.parseInt(ck.get(i).getRCDSTS()) <= 1) {
                        ckSTS = true;
                    }
                }
            }

            m.put("ckSTS", ckSTS);
            json = this.gson.toJson(m);

        } else if (mode.equals("ckSTSafter")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            boolean ckSTS = new RCM200Dao().CheckSTS(whs, sps, cns, dts);

            m.put("ckSTS", ckSTS);
            json = this.gson.toJson(m);

        } else if (mode.equals("ckSTSDetailForBin")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            boolean ckSTS = false;

            List<RCMBPD> ck = new RCM200Dao().selectRCMBPD(whs, sps, cns, dts);

            if (ck.size() > 0) {
                for (int i = 0; i < ck.size(); i++) {
                    if (Integer.parseInt(ck.get(i).getRCDSTS()) > 1) {
                        ckSTS = true;
                    }
                }
            }

            m.put("ckSTS", ckSTS);
            json = this.gson.toJson(m);

        } else if (mode.equals("deloneROW")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String rid = request.getParameter("rid");

            boolean ret = new RCM200Dao().deleteOneROW(whs, sps, cns, dts, rid);
            JSONObject retdel = new RCM200Dao().updateSTSnQTYafterDELRow(whs, sps, cns, dts, rid);

            m.put("delSTS", ret);
            m.put("retdel", retdel);
            json = this.gson.toJson(m);

        } else if (mode.equals("getAllINV")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            List<RCMBPD> inv = new RCM200Dao().selectINV(whs, sps, cns, dts);

            m.put("invList", inv);
            json = this.gson.toJson(m);

        } else if (mode.equals("getAllPONO")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String inv = request.getParameter("inv");

            List<RCMBPD> pono = new RCM200Dao().selectPONO(whs, sps, cns, dts, inv);

            m.put("ponoList", pono);
            json = this.gson.toJson(m);

        } else if (mode.equals("getAllMATC")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String inv = request.getParameter("inv");
            String po = request.getParameter("po");

            List<RCMBPD> mat = new RCM200Dao().selectMATC(whs, sps, cns, dts, inv, po);

            m.put("matList", mat);
            json = this.gson.toJson(m);

        } else if (mode.equals("setResetAllRCQty")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");

            boolean upRES = new RCM200Dao().resetRCQty(whs, sps, cns, dts);

            m.put("upRES", upRES);
            json = this.gson.toJson(m);

        } else if (mode.equals("getAllBPDbyPALNO")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String palno = request.getParameter("palno");

            List<RCMBPD> listbpd = new RCM200Dao().getBPDbyPalNo(whs, sps, cns, dts, palno);

            m.put("bpdList", listbpd);
            json = this.gson.toJson(m);

        } else if (mode.equals("ckCARTONdupAndPAL")) {

            String whs = request.getParameter("wh");
            String sps = request.getParameter("sp");
            String cns = request.getParameter("cn");
            String dts = request.getParameter("dt");
            String palno = request.getParameter("palno");
            String ctnid = request.getParameter("iditem");
            String ctcode = request.getParameter("matcode");

            boolean ck = new RCM200Dao().ckCTIDdupAndPAL(whs, sps, cns, dts, ctnid, ctcode, palno);

            m.put("ckRES", ck);
            json = this.gson.toJson(m);

        }

        /**
         * ****************************
         * Return JSON to Client ****************************
         */
        // SET Response header
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        // SET List data to Json
        response.getWriter().write(json);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
