/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var uu = sessionStorage.getItem('uid');

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWH", uid: uu},
        async: false
    }).done(function (result) {

        for (var i = 0; i < result.whList.length; i++) {
            $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" ' + result.whList[i].Select + ' >' + result.whList[i].QWHNAME + '</option>');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $('#ckBill').on('change', function () { //ckBill checked -> ckPO
        $('#ckPO').prop('checked', this.checked);
    });

    $('#okbtn').on('click', function () {

        var permis = true;

        var wh = $('#WHlist').val();
        var sp = $('#SUPlist').val();
        var cu = $('#CUSlist').val();
        var dt = $('#datesend').val();
        var bill = $('#ckBill').prop("checked");
        var po = $('#ckPO').prop("checked");

        if (po === false) {
            if (bill === false) {
                alertify.error("Please Check Billing Data");
                permis = false;
            }
        }

        if (dt === "") {
            alertify.error("Please Choose Date");
            permis = false;
        }

        if (permis) {

            $.ajax({
                url: "/TWC_RCMS/RCM810Get",
                data: {mode: "ckBPH", wh: wh, sp: sp, cu: cu, dt: dt},
                async: false
            }).done(function (result) {

                if (result.ckVal !== undefined) {
                    if (result.ckVal < 2) {
                        $('#modalConfDel').modal("show");
                    } else {
                        $('#modalCantDel').modal("show");
                    }
                } else {
                    alertify.error("Not Found Billing And PO");
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });

        }

    });

});

function confdelete() {

    var wh = $('#WHlist').val();
    var sp = $('#SUPlist').val();
    var cu = $('#CUSlist').val();
    var dt = $('#datesend').val();
    var bill = $('#ckBill').prop("checked");
    var po = $('#ckPO').prop("checked");

    $.ajax({
        url: "/TWC_RCMS/RCM810Get",
        data: {mode: "deleteBillPO", wh: wh, sp: sp, cu: cu, dt: dt, bill: bill, po: po},
        async: false
    }).done(function (result) {

        if (result.delVal.POH !== undefined && result.delVal.POD !== undefined && result.delVal.UPPOD !== undefined && result.delVal.BPH !== undefined && result.delVal.BPD !== undefined && result.delVal.BPT !== undefined) {
            if (result.delVal.POH === 'true' && result.delVal.POD === 'true' && result.delVal.BPH === 'true' && result.delVal.BPD === 'true' && result.delVal.BPT === 'true') {
                alertify.success("Delete Billing And PO Success");
            } else if (result.delVal.POH === 'false' && result.delVal.POD === 'false' && result.delVal.BPH === 'false' && result.delVal.BPD === 'false' && result.delVal.BPT === 'false') {
                alertify.error("Delete Billing And PO Failed");
            } else {
                if (result.delVal.POH === 'true' && result.delVal.POD === 'true') {
                    alertify.success("Delete PO Success");
                } else {
                    if (result.delVal.POH === 'false' && result.delVal.POD === 'false') {
                        alertify.error("Delete PO Failed");
                    } else {
                        if (result.delVal.POH === 'false') {
                            alertify.error("Delete PO (Header) Failed");
                        }

                        if (result.delVal.POD === 'false') {
                            alertify.error("Delete PO (Detail) Failed");
                        }

                        if (result.delVal.POS === 'false') {
                            alertify.error("Delete PO (History) Failed");
                        }
                    }
                }

                if (result.delVal.UPPOD === 'false') {
                    alertify.error("Update Billing (Detail) To 0 Failed");
                }

                if (result.delVal.BPH === 'true' && result.delVal.BPD === 'true') { // && result.delVal.BPT === 'true'
                    alertify.success("Delete Billing Success");
                } else {
                    if (result.delVal.BPH === 'false' && result.delVal.BPD === 'false') { //|| result.delVal.BPT === 'false'
                        alertify.error("Delete Billing Failed");
                    } else {
                        if (result.delVal.BPH === 'false') {
                            alertify.error("Delete Billing (Header) Failed");
                        }
                        if (result.delVal.BPD === 'false') {
                            alertify.error("Delete Billing (Detail) Failed");
                        }
                        if (result.delVal.BPT === 'false') {
                            alertify.error("Delete Billing (Count) Failed");
                        }
                    }
                }
            }
        } else if (result.delVal.POH !== undefined && result.delVal.POD !== undefined && result.delVal.UPPOD !== undefined && result.delVal.BPH === undefined && result.delVal.BPD === undefined && result.delVal.BPT === undefined) {
            if (result.delVal.POH === 'true' && result.delVal.POD === 'true') {
                alertify.success("Delete PO Success");
            } else {
                if (result.delVal.POH === 'false' && result.delVal.POD === 'false') {
                    alertify.error("Delete PO Failed");
                } else {
                    if (result.delVal.POH === 'false') {
                        alertify.error("Delete PO (Header) Failed");
                    }

                    if (result.delVal.POD === 'false') {
                        alertify.error("Delete PO (Detail) Failed");
                    }

                    if (result.delVal.POS === 'false') {
                        alertify.error("Delete PO (History) Failed");
                    }
                }
            }

            if (result.delVal.UPPOD === 'false') {
                alertify.error("Update Billing (Detail) To 0 Failed");
            }
        } else if (result.delVal.POH === undefined && result.delVal.POD === undefined && result.delVal.UPPOD === undefined && result.delVal.BPH !== undefined && result.delVal.BPD !== undefined && result.delVal.BPT !== undefined) {
            if (result.delVal.BPH === 'true' && result.delVal.BPD === 'true') { //&& result.delVal.BPT === 'true'
                alertify.success("Delete Billing Success");
            } else {
                if (result.delVal.BPH === 'false' && result.delVal.BPD === 'false') { // result.delVal.BPT === 'false'
                    alertify.error("Delete Billing Failed");
                } else {
                    if (result.delVal.BPH === 'false') {
                        alertify.error("Delete Billing (Header) Failed");
                    }
                    if (result.delVal.BPD === 'false') {
                        alertify.error("Delete Billing (Detail) Failed");
                    }
                    if (result.delVal.BPT === 'false') {
                        alertify.error("Delete Billing (Count) Failed");
                    }
                }
            }
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}
