/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

//                sessionStorage.setItem("uid","90309");

    $('#user').val(sessionStorage.getItem('uid'));

    var today = new Date();
    $('#datesend').val(today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2));

    var uu = sessionStorage.getItem('uid');

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWH", uid: uu},
        async: false
    }).done(function (result) {

        for (var i = 0; i < result.whList.length; i++) {
            $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" ' + result.whList[i].Select + ' >' + result.whList[i].QWHNAME + '</option>');
//                        $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" >' + result.whList[i].QWHCOD + ' : ' + result.whList[i].QWHNAME + '</option>');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    let wh = $('#WHlist').val();
    let sp = $('#SUPlist').val();
    let dt = $('#datesend').val().replace(/-/g, "");
    let cn = $('#CUSlist').val();

    let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn;
    $('#goto').attr('href', link);

    $('#WHlist').change(function () {
        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn;
        $('#goto').attr('href', link);
    });

    $('#SUPlist').change(function () {
        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn;
        $('#goto').attr('href', link);
    });

    $('#datesend').change(function () {
        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn;
        $('#goto').attr('href', link);
    });

    $('#CUSlist').change(function () {
        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn;
        $('#goto').attr('href', link);
    });

});
