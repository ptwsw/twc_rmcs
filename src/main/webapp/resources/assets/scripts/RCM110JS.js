/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#printfunc').prop('hidden', true);
    $('#cancbtn').prop('hidden', true);

    $('body').tooltip({selector: '[data-toggle="tooltip"]'}); //show tooltip in datatable
//                $('[data-toggle="tooltip"]').tooltip();

    var table = $('#rcm100Table').DataTable({
        "columnDefs": [
            {"width": "1%", "targets": 0},
            {"width": "1%", "targets": 1},
            {"width": "1%", "targets": 2},
            {"width": "1%", "targets": 3},
            {"width": "1%", "targets": 4},
            {"width": "1%", "targets": 5},
            {"width": "1%", "targets": 6, className: "text-center"},
            {"width": "1%", "targets": 7},
            {"width": "1%", "targets": 8, className: "text-center"},
            {"width": "1%", "targets": 9, className: "text-right"},
            {"width": "1%", "targets": 10, className: "text-right"},
            {"width": "1%", "targets": 11, className: "text-right"},
            {"width": "1%", "targets": 12, className: "text-center"},
            {"width": "1%", "targets": 13},
            {"width": "1%", "targets": 14},
            {"width": "1%", "targets": 15},
            {"width": "2%", "targets": 16},
            {"width": "3%", "targets": 17},
            {"width": "5%", "targets": 18}
        ],
        "bPaginate": false,
        "ordering": false
    });

//                sessionStorage.setItem('uid', '90309');

    $('#user').val(sessionStorage.getItem('uid'));

    var today = new Date();
    $('#datesend').val(today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2));

    var uu = sessionStorage.getItem('uid');

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWH", uid: uu}, //get Warehouse For Select
        async: false
    }).done(function (result) {

        for (var i = 0; i < result.whList.length; i++) {
            $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" ' + result.whList[i].Select + ' >' + result.whList[i].QWHNAME + '</option>');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $('#enterbtn').click(function () {

        $("#exp").css("display", "block");
        $("#com").css("display", "none");

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val();
        let cn = $('#CUSlist').val();

        //----------GetHead-----------
        $.ajax({
            url: "/TWC_RCMS/RCM100Get",
            data: {mode: "getHead110", wh: wh, sp: sp, cn: cn, dt: dt, uid: uu},
            async: false
        }).done(function (result) {

            if (result.HList.length > 0) {

                $('#HeadSTS').val(result.HList[0].RCHSTS);

                $('#WHlist').prop('disabled', true);
                $('#SUPlist').prop('disabled', true);
                $('#datesend').prop('disabled', true);
                $('#CUSlist').prop('disabled', true);

                $('#showttINV').val(result.HList[0].RCHTOTINV);
                $('#showttPO').val(result.HList[0].RCHTOTPO);
                $('#showttPACK').val(result.HList[0].RCHTOTQTY);


//                $('#useriden').val(uu);

                if (result.HList[0].RCHJBNO !== undefined) {
                    $('#useriden').val(uu + " : " + result.HList[0].RCHJBNO.split(" ")[0]);
                } else {
                    $('#useriden').val(uu);
                }

                //----------GetTable-----------

                if ($.fn.DataTable.isDataTable('#rcm100Table')) { //before RE-Create New Datatable
                    $('#rcm100Table').DataTable().destroy();
                }

                $('#rcm100Table tbody').empty();

                $('#rcm100Table').dataTable({
                    ajax: {
                        url: "/TWC_RCMS/RCM100Get",
                        data: {mode: "getTable110", wh: wh, sp: sp, cn: cn, dt: dt},
                        dataSrc: 'DList'
                    },
                    "aoColumns": [
                        {"data": "RCDDLNO"},
                        {'mRender': function (data, type, full) {

                                if (full.RCDINVNO.includes("Hide")) { //Invoice Head Hide
                                    return '';
                                } else { //Invoice in Line Show
                                    let textRJ = "";

                                    if (full.RCDINVREF !== "null" && full.RCDINVREF !== "") {
                                        textRJ = "INV REF : " + full.RCDINVREF;
                                    }

                                    if (full.RCDRJFLAG === "R") {
                                        textRJ = "Rejected";
                                    } else if (full.RCDRJFLAG === "P") {
                                        textRJ = "Price Invalid";
                                    }

                                    if (full.RCDSTS >= 2) {
                                        return '<label title data-original-title="' + textRJ + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" id="inv' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '" >' + full.RCDINVNO + '</label> ';
                                    } else {
                                        return '<label title data-original-title="' + textRJ + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" class="invOpenMenu" id="inv' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '" onclick="listmenuInv(\'' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '\')" >' + full.RCDINVNO + '</label> <div id="myDropdown' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '" class="dropdown-content"><a href="javascript:openmodalRJectInv(\'' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '\')">Reject</a><a href="javascript:openmodalCLRJectInv(\'' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '\')">Clear Reject</a><a href="javascript:openmodalPriInv(\'' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '\')">Price Invalid</a><a style="background-color:red; color:white;" href="javascript:closeMenuList()" >Close</a></div>';
                                    }
                                }
                            }
                        },
                        {'mRender': function (data, type, full) {

                                if (parseInt(full.RCDSTS) >= 2) { //RCDSTS = 2 Show PrintBtn and CancelBtn
                                    $('#printfunc').prop('hidden', false);
                                    $('#cancbtn').prop('hidden', false);
                                }

                                //this

                                if (full.RCDPONO.includes("Hide")) { // PO Line Head Hide
                                    return '<label hidden>' + full.RCDPONO.toString().replaceAll("|Hide", "") + '</label>';
                                } else { //PO Line in Line Show

                                    if (full.RCDSTS >= 2) {
                                        return '<label id="po' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '|' + full.RCDCTM + '" >' + full.RCDPONO + '</label>';
                                    } else {
                                        if (full.RCDMATCD === "Head") {
                                            return '<label id="po' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '|' + full.RCDCTM + '" >' + full.RCDPONO + '</label>';
                                        } else {
                                            if (full.RCDPOFLAG === "Y") { //Download
                                                return '<label id="po' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '|' + full.RCDCTM + '" onclick="openmodalEditPO(\'' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '|' + full.RCDCTM + '\')" >' + full.RCDPONO + '</label>';
                                            } else { //Not Download
                                                return '<label id="po' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '|' + full.RCDCTM + '" onclick="openmodalEditPO(\'' + full.RCDJBNO + '|' + full.RCDINVNO.toString().replaceAll("|Hide", "") + '|' + full.RCDCTM + '\')" >' + full.RCDPONO + '</label>';
                                            }
                                        }
                                    }

                                }
                            }
                        },
                        {'mRender': function (data, type, full) {
                                if (full.RCDAUTHO === "check") { // L3 Check
                                    return '<i class="fa fa-check" aria-hidden="true" style="font-size:20px;"></i>';
                                } else if (full.RCDAUTHO !== "NULL") {

                                    if (full.RCDAUTHO === "") {
                                        return '<i class="fa fa-times" aria-hidden="true" style="font-size:20px;" title data-original-title="Not Found" aria-hidden="true" data-toggle="tooltip" data-placement="top" ></i>';
                                    } else {
                                        return '<i class="fa fa-times" aria-hidden="true" style="font-size:20px;" title data-original-title="' + full.RCDAUTHO.toString().replace("X", "") + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" ></i>';
                                    }

                                } else {
                                    return '';
                                }
                            }
                        }, //L3
                        {'mRender': function (data, type, full) {

                                var textshow = "";

                                if (full.RCDMATREF !== "") {
                                    textshow = "MAT REF : " + full.RCDMATREF;
                                }

                                if (full.RCDMATCD === "star") { // Show Red Star on Line

                                    if (full.RCDSTS >= 2) {
                                        return '<label title data-original-title="' + textshow + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" onclick="">' + full.RCDCUSMAT + '</label> ';
                                    } else {
                                        return '<label title data-original-title="' + textshow + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" onclick="openMenuMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')">' + full.RCDCUSMAT + '</label> <div id="myDropdown' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '" class="dropdown-content"><a href="javascript:openmodalRJectMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')">Reject</a><a href="javascript:openmodalCLRJectMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')">Clear Reject</a><a style="background-color:red; color:white;" href="javascript:closeMenuList()" >Close</a></div> ';
                                    }

                                } else if (full.RCDMATCD === "Head") { //return input on Head
                                    return '<label>' + full.RCDCUSMAT + '</label>';

//                                    return '<input name="hStar" id="hStar' + full.RCDINVNO.replace("|Hide", "") + '" type="text" hidden><i class="fa fa-star" id="starHead' + full.RCDINVNO.replace("|Hide", "") + '" onclick="console.log("reject mat");" style="font-size:20px; color: red; display:none;" aria-hidden="true"></i>';
                                } else {

                                    if (full.RCDSTS >= 2) {
                                        return '<label title data-original-title="' + textshow + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" onclick="">' + full.RCDCUSMAT + '</label> ';
                                    } else {
                                        return '<label title data-original-title="' + textshow + '" aria-hidden="true" data-toggle="tooltip" data-placement="top" onclick="openMenuMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')">' + full.RCDCUSMAT + '</label> <div id="myDropdown' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '" class="dropdown-content"><a href="javascript:openmodalRJectMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')">Reject</a><a href="javascript:openmodalCLRJectMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')">Clear Reject</a><a style="background-color:red; color:white;" href="javascript:closeMenuList()" >Close</a></div> ';
                                    }
                                }
                            }
                        },
                        {'mRender': function (data, type, full) {

                                if (full.RCDMATCD === "star") { // Show Red Star on Line

                                    $('#hStar' + full.RCDINVNO.replace("|Hide", "") + '').val("HSTAR");
                                    $('#starHead' + full.RCDINVNO.replace("|Hide", "")).css('color', 'red');
                                    $('#starHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');

                                    return '<i class="fa fa-star" onclick="" style="font-size:20px; color: red;" aria-hidden="true"></i>';
//                                    return '<i class="fa fa-star" onclick="modalEditMat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.CNTMAT + '\')" style="font-size:20px; color: red;" aria-hidden="true"></i>';
                                } else if (full.RCDMATCD === "starY") { //return input
                                    $('#hStar' + full.RCDINVNO.replace("|Hide", "") + '').val("HSTAR");

                                    if ($('#starHead' + full.RCDINVNO.replace("|Hide", "")).css('color') !== "rgb(255, 0, 0)") { //is Not Red
                                        $('#starHead' + full.RCDINVNO.replace("|Hide", "")).css('color', 'yellow');
                                        $('#starHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
                                    }

                                    if (full.RCDSTS >= 2) {
                                        return '<i class="fa fa-star" onclick="" style="font-size:20px; color: yellow;" aria-hidden="true"></i>';
                                    } else {
                                        return '<i class="fa fa-star" onclick="updScan200Mat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.RCDMATREF + '\')" style="font-size:20px; color: yellow;" aria-hidden="true"></i>';
                                    }

//                                    return '<i class="fa fa-star" onclick="updScan200Mat(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.RCDMATREF + '\')" style="font-size:20px; color: yellow;" aria-hidden="true"></i>';
                                } else if (full.RCDMATCD === "Head") { //return input
                                    return '<input name="hStar" id="hStar' + full.RCDINVNO.replace("|Hide", "") + '" type="text" hidden><i class="fa fa-star" id="starHead' + full.RCDINVNO.replace("|Hide", "") + '" onclick="" style="font-size:20px; display:none;" aria-hidden="true"></i>';
                                } else {
                                    return '';
                                }
                            }
                        },
                        {'mRender': function (data, type, full) {

                                if (full.RCDPOSTDATE === "Nshow") { //Not Show Posting Date Select
                                    return '';
                                } else {
                                    var postingdate = full.RCDPOSTDATE;

                                    if (postingdate === null || postingdate === '0') { // PostingDate Not Have In DB

                                        var today = new Date();
                                        let yyyy = today.getFullYear();
                                        let mm = ('0' + (today.getMonth() + 1)).slice(-2);
                                        let dd = ('0' + today.getDate()).slice(-2);

                                        return '<input type=date id="pdate' + full.RCDINVNO + '" value="' + yyyy + "-" + mm + "-" + dd + '" class="form-control" style="width:169px;" >';
                                    } else {

                                        let yyyy = postingdate.substring(0, 4);
                                        let mm = postingdate.substring(4, 6);
                                        let dd = postingdate.substring(6, 8);

                                        if (parseInt(full.RCDSTS) >= 2) { //RCDSTS = 2 disabled Posting Date 
                                            return '<input type=date id="pdate' + full.RCDINVNO + '" value="' + yyyy + "-" + mm + "-" + dd + '" class="form-control" style="width:169px;" disabled>';
                                        } else {
                                            return '<input type=date id="pdate' + full.RCDINVNO + '" value="' + yyyy + "-" + mm + "-" + dd + '" class="form-control" style="width:169px;" >';
                                        }

                                    }
                                }

                            }
                        },
                        {'mRender': function (data, type, full) {

                                var CUSlist = $('#CUSlist').val();

                                if (CUSlist === "211809") { //วาโก้
                                    if (full.RCDPOLINE === "Head") { //Same Line Invoice return input
                                        return '<input name="hpline" id="hpline' + full.RCDINVNO.replace("|Hide", "") + '" type="text" hidden> <i class="fa fa-circle" id="polineCHead' + full.RCDINVNO.replace("|Hide", "") + '" aria-hidden="true" style="font-size:20px; color: red; display:none;"></i> <input id="sts' + full.RCDINVNO.replace("|Hide", "") + '" name="stsckline" value = "' + full.RCDSTS + '" type="text" hidden></input>';
                                    } else {
                                        if (full.RCDBUM === "" || full.RCDBUM === "NULL") { //Not Check L3 Column and MATCTRL Not Found

                                            if (full.RCDSTS >= 2) {
                                                return '<label>' + full.RCDPOLINE + '</label>';
                                            } else {
                                                $('#hpline' + full.RCDINVNO.replace("|Hide", "") + '').val("HPOLINE");
                                                $('#polineCHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
                                                return '<label style="color:red; cursor: pointer;" onclick="modalEditPOline(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.CNTMAT + '\')" >' + full.RCDPOLINE + '</label>';
                                            }

                                        } else {
                                            return '<label>' + full.RCDPOLINE + '</label>';
                                        }
                                    }
                                } else { // อื่นๆ
                                    if (full.RCDPOLINE === "Head") { //Same Line Invoice return input
                                        return '<input name="hpline" id="hpline' + full.RCDINVNO.replace("|Hide", "") + '" type="text" hidden> <i class="fa fa-circle" id="polineCHead' + full.RCDINVNO.replace("|Hide", "") + '" aria-hidden="true" style="font-size:20px; color: red; display:none;"></i> <input id="sts' + full.RCDINVNO.replace("|Hide", "") + '" name="stsckline" value = "' + full.RCDSTS + '" type="text" hidden></input>';
                                    } else {

                                        if (full.RCDBUM === "" || full.RCDBUM === "NULL") { //Not Check L3 Column and MATCTRL Not Found

                                            if (full.RCDSTS >= 2) {
                                                return '<label>' + full.RCDPOLINE + '</label>';
                                            } else {
                                                $('#hpline' + full.RCDINVNO.replace("|Hide", "") + '').val("HPOLINE");
                                                $('#polineCHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
                                                return '<label style="color:red; cursor: pointer;" onclick="modalEditPOline(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.CNTMAT + '\')" >' + full.RCDPOLINE + '</label>';
                                            }

                                        } else {
                                            return '<label>' + full.RCDPOLINE + '</label>';
                                        }

////                                        if (full.RCDAUTHO === "NULL" && full.RCDMCTRL === "") { //Not Check L3 Column and MATCTRL Not Found
////                                            $('#hpline' + full.RCDINVNO.replace("|Hide", "") + '').val("HPOLINE");
////                                            $('#polineCHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
//////                                        return '<label style="color:red; cursor: pointer;" onclick="" >' + full.RCDPOLINE + '</label>';
////                                            return '<label style="color:red; cursor: pointer;" onclick="modalEditPOline(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.CNTMAT + '\')" >' + full.RCDPOLINE + '</label>';
////                                        } else {
//                                        return '<label>' + full.RCDPOLINE + '</label>';
////                                        }
                                    }
                                }

//                                if (full.RCDPOLINE === "Head") { //Same Line Invoice return input
//                                    return '<input name="hpline" id="hpline' + full.RCDINVNO.replace("|Hide", "") + '" type="text" hidden> <i class="fa fa-circle" id="polineCHead' + full.RCDINVNO.replace("|Hide", "") + '" aria-hidden="true" style="font-size:20px; color: red; display:none;"></i> <input id="sts' + full.RCDINVNO.replace("|Hide", "") + '" name="stsckline" value = "' + full.RCDSTS + '" type="text" hidden></input>';
//                                } else {
//                                    if (full.RCDAUTHO === "NULL" && full.RCDMCTRL === "") { //Not Check L3 Column and MATCTRL Not Found
//                                        $('#hpline' + full.RCDINVNO.replace("|Hide", "") + '').val("HPOLINE");
//                                        $('#polineCHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
//                                        return '<label style="color:red; cursor: pointer;" >' + full.RCDPOLINE + '</label>';
////                                        return '<label style="color:red; cursor: pointer;" onclick="modalEditPOline(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '|' + full.CNTMAT + '\')" >' + full.RCDPOLINE + '</label>';
//                                    } else {
//                                        return '<label>' + full.RCDPOLINE + '</label>';
//                                    }
//                                }
                            }
                        },
                        {'mRender': function (data, type, full) {

                                if (full.RCDINVNO.includes("Hide")) { //Invoice Head Hide
                                    if (full.RCDPOPRC !== full.RCDINVPRC) { //PO Price Not Equal Inv Price
                                        $('#PriceHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
                                        return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title data-original-title=" PO ' + full.RCDPOPRC + ' / INV ' + full.RCDINVPRC + '" style="font-size:20px; color: red;"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title data-original-title=" PO ' + full.RCDPOPRC + ' / INV ' + full.RCDINVPRC + '" style="font-size:20px; color: white;"></i>';
//                                        return '';
                                    }
                                } else { //Invoice in Line Show
                                    return '<i class="fa fa-circle" id="PriceHead' + full.RCDINVNO.replace("|Hide", "") + '" aria-hidden="true" style="font-size:20px; color: red; display:none;"></i>';
                                }

                            }
                        },
                        {'mRender': function (data, type, full) {
                                return '<label>' + deGits(full.RCDINVQTY) + '</label>';

                            }
                        },
                        {'mRender': function (data, type, full) {

                                if (full.RCDINVNO.includes("Hide")) { //Invoice Head Hide
                                    var poqty = parseFloat(full.RCDPOQTY);
                                    var rate = parseFloat(full.RCDRATE);
                                    var canReceive = parseFloat(full.canReceive);
                                    var hispoqty = parseFloat(full.RCSPOQTY);
                                    var invqty = parseFloat(full.RCDINVQTY);
                                    var Receive = parseFloat(full.getReceive);

                                    if (full.RCDQSTS < 0) {
                                        if (Receive > canReceive) { //red
                                            return '<i class="fa fa-circle" style="font-size:20px; color: red;" onclick="clickexp(\'' + poqty + '|' + rate + '|' + canReceive + '|' + hispoqty + '|' + invqty + '|' + Receive + '\')" ></i>';
                                        } else { //white
                                            return '<i class="fa fa-circle" style="font-size:20px; color: white;" onclick="clickexp(\'' + poqty + '|' + rate + '|' + canReceive + '|' + hispoqty + '|' + invqty + '|' + Receive + '\')"></i>';
                                        }
                                    } else {
                                        if (Receive > canReceive) { //red
                                            return '<i class="fa fa-circle" style="font-size:20px; color: red;" onclick="clickexp(\'' + poqty + '|' + rate + '|' + canReceive + '|' + hispoqty + '|' + invqty + '|' + Receive + '\')"></i>';
                                        } else { //white
                                            return '<i class="fa fa-circle" style="font-size:20px; color: white;" onclick="clickexp(\'' + poqty + '|' + rate + '|' + canReceive + '|' + hispoqty + '|' + invqty + '|' + Receive + '\')"></i>';
                                        }
                                    }
                                } else { //Invoice in Line Show
                                    return '';
                                }

                            }
                        },
//                        {"data": "RCDCMXQTY"},
                        {'mRender': function (data, type, full) {
                                if (full.RCDRCQTY === full.RCDINVQTY) {
                                    return '<label style="color:black;" >' + deGits(full.RCDRCQTY) + '</label>';
                                } else {
                                    return '<label style="color:gold;" >' + deGits(full.RCDRCQTY) + '</label>';
                                }
                            }
                        },
                        {'mRender': function (data, type, full) {

                                if (full.RCDDUDATE !== "Head") {

                                    var du;
                                    var dl;

                                    let yyyyDU;
                                    let mmDU;
                                    let ddDU;

                                    var tic = false;

                                    if (full.RCDDUDATE !== null && full.RCDDUDATE !== undefined) {
                                        du = full.RCDDUDATE;
                                        yyyyDU = du.substring(0, 4);
                                        mmDU = du.substring(4, 6);
                                        ddDU = du.substring(6, 8);

                                        tic = true;
                                    }

                                    let yyyyDL;
                                    let mmDL;
                                    let ddDL;

                                    if (full.RCDDLDATE !== null && full.RCDDLDATE !== undefined) {
                                        dl = full.RCDDLDATE;
                                        yyyyDL = dl.substring(0, 4);
                                        mmDL = dl.substring(4, 6);
                                        ddDL = dl.substring(6, 8);
                                        tic = true;
                                    }

                                    if (tic) {
                                        if (mmDU + "/" + ddDU + "/" + yyyyDU === "//0") {
                                            return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (***) " style="font-size:20px; color: black;"></i>';
                                        } else if (mmDU + "/" + ddDU + "/" + yyyyDU === mmDL + "/" + ddDL + "/" + yyyyDL) {
                                            return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (0) " style="font-size:20px; color: white;"></i>';
                                        } else {

                                            if (yyyyDL < yyyyDU) {

                                                var DUDate = new Date(mmDU + "/" + ddDU + "/" + yyyyDU);
                                                var DLDate = new Date(mmDL + "/" + ddDL + "/" + yyyyDL);

                                                var millisBetween = DUDate.getTime() - DLDate.getTime();
                                                var days = millisBetween / (1000 * 3600 * 24);
                                                $('#DueSTSHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
                                                return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (' + Math.round(Math.abs(days)) + ') " style="font-size:20px; color: red;"></i>';
                                            } else {

                                                if (mmDU !== mmDL) {

                                                    var DUDate = new Date(mmDU + "/" + ddDU + "/" + yyyyDU);
                                                    var DLDate = new Date(mmDL + "/" + ddDL + "/" + yyyyDL);

                                                    var millisBetween = DUDate.getTime() - DLDate.getTime();
                                                    var days = millisBetween / (1000 * 3600 * 24);

                                                    if ((yyyyDU + mmDU) > (yyyyDL + mmDL)) { //RED
                                                        $('#DueSTSHead' + full.RCDINVNO.replace("|Hide", "")).css('display', '');
                                                        return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (' + Math.round(Math.abs(days)) + ') " style="font-size:20px; color: red;"></i>';
                                                    } else {

                                                        if (Math.round(Math.abs(days)) > 60) { //GRAY
                                                            return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (' + Math.round(Math.abs(days)) + ') " style="font-size:20px; color: gray;"></i>';
                                                        } else { //YELLOW
                                                            return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (' + Math.round(Math.abs(days)) + ') " style="font-size:20px; color: yellow;"></i>';
                                                        }

                                                    }
                                                } else {
                                                    var DUDate = new Date(mmDU + "/" + ddDU + "/" + yyyyDU);
                                                    var DLDate = new Date(mmDL + "/" + ddDL + "/" + yyyyDL);

                                                    var millisBetween = DUDate.getTime() - DLDate.getTime();
                                                    var days = millisBetween / (1000 * 3600 * 24);

                                                    if (ddDU > ddDL) { //GREEN
                                                        return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (' + Math.round(Math.abs(days)) + ') " style="font-size:20px; color: green;"></i>';
                                                    } else { //YELLOW
                                                        return '<i class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="PO Dt ' + ddDU + '/' + mmDU + '/' + yyyyDU.toString().substring(2, 4) + ' / ' + 'TP Dt ' + ddDL + '/' + mmDL + '/' + yyyyDL.toString().substring(2, 4) + ' (' + Math.round(Math.abs(days)) + ') " style="font-size:20px; color: yellow;"></i>';
                                                    }
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    return '<i class="fa fa-circle" id="DueSTSHead' + full.RCDINVNO.replace("|Hide", "") + '" aria-hidden="true" style="font-size:20px; color: red; display:none;"></i>';
                                }
                            }

                        },
                        {"data": "RCDSUM"},
                        {"data": "RCDBUM"},
                        {"data": "RCDEDT"},
                        {'mRender': function (data, type, full) {
                                if (full.RCDPKTYPE !== "") { //RCMBPD Have PackType Select For Change
//                                    return '<select id="pktype' + full.RCDJBNO + full.RCDINVNO.replace("|Hide", "") + full.RCDPONO.replace("|Hide", "") + full.RCDPOLINE + full.RCDCUSMAT + '" onchange="editPackageType(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')" class="form-control" style="width:92px;" ><option value="' + full.RCDPKTYPE + '" hidden selected>' + full.RCDPKTYPE + '</option><option value="BOX" >BOX</option><option value="BAG" >BAG</option><option value="ROLL" >ROLL</option></select>';

                                    if (parseInt(full.RCDSTS) >= 2) { // RCDSTS >= 2 Checkbox Checked And Not Change
                                        return '<select id="pktype' + full.RCDJBNO + full.RCDINVNO.replace("|Hide", "") + full.RCDPONO.replace("|Hide", "") + full.RCDPOLINE + full.RCDCUSMAT + '" onchange="editPackageType(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')" class="form-control" style="width:92px;" disabled><option value="' + full.RCDPKTYPE + '" hidden selected>' + full.RCDPKTYPE + '</option><option value="BOX" >BOX</option><option value="BAG" >BAG</option><option value="ROLL" >ROLL</option></select>';
                                    } else { // RCDSTS = 0 and RCDSTS = 1 Checkbox Can Check
                                        return '<select id="pktype' + full.RCDJBNO + full.RCDINVNO.replace("|Hide", "") + full.RCDPONO.replace("|Hide", "") + full.RCDPOLINE + full.RCDCUSMAT + '" onchange="editPackageType(\'' + full.RCDJBNO + '|' + full.RCDINVNO.replace("|Hide", "") + '|' + full.RCDPONO.replace("|Hide", "") + '|' + full.RCDPOLINE + '|' + full.RCDCUSMAT + '\')" class="form-control" style="width:92px;" ><option value="' + full.RCDPKTYPE + '" hidden selected>' + full.RCDPKTYPE + '</option><option value="BOX" >BOX</option><option value="BAG" >BAG</option><option value="ROLL" >ROLL</option></select>';
                                    }
                                } else {
                                    return '';
                                }
                            }
                        },
//                        {"data": "RCDPLANT"},
                        {'mRender': function (data, type, full) {
//                                console.log(full.RCDPLANT);
//                                console.log(full.FULLPONO);

                                //fullpono -> plant from po
                                //rcdplant -> plant download

                                if (full.RCDDUDATE !== "Head") {
                                    if (full.RCDPLANT === full.FULLPONO) {
                                        return '<label>' + full.FULLPONO + '</label>';
                                    } else { //red
                                        return '<label style="color:red;">' + full.FULLPONO + '</label>';
                                    }
                                } else {
                                    return '';
                                }

                            }},
                        {'mRender': function (data, type, full) {
                                var canReceive = parseFloat(full.canReceive);
                                var Receive = parseFloat(full.getReceive);

                                if ($('#CUSlist').val() === '211809') { //only Wacoal
                                    if (full.SUMM !== "") {
                                        if (full.RCDAUTHO === "check") {
                                            if (parseInt(full.RCDSTS) >= 2) { // RCDSTS >= 2 Checkbox Checked And Not Change
                                                return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" checked disabled>';
                                            } else { // RCDSTS = 1 Checkbox Can Check
                                                if (Receive > canReceive) {
                                                    return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" disabled>';
                                                } else {
                                                    if (full.RCDBOXNO === "N" || full.RCDCDT === "N" || full.RCDTTQTY === "N" || full.ISFLAG === "Y") { //CUSMAT NOT SAME   //RED DUE DATE
                                                        return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" disabled>';
                                                    } else { //CUSMAT IS THE SAME
                                                        if (full.RCDCPERC === "N") {
                                                            return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" disabled>';
                                                        } else {
                                                            return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" >';
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            return deGits(full.SUMM);
                                        }
                                    } else {
                                        if (full.RCDMCTRL === "") {
                                            return "xxxx";
                                        } else {
                                            return full.RCDMCTRL;
                                        }
                                    }
                                } else { // Other
                                    if (full.SUMM !== "") {
                                        if (parseInt(full.RCDSTS) >= 2) { // RCDSTS >= 2 Checkbox Checked And Not Change
                                            return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" checked disabled>';
                                        } else { // RCDSTS = 1 Checkbox Can Check
                                            if (Receive > canReceive) {
                                                return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" disabled>';
                                            } else {
                                                if (full.RCDBOXNO === "N" || full.RCDCDT === "N" || full.RCDTTQTY === "N" || full.ISFLAG === "Y") { //CUSMAT NOT SAME   //RED DUE DATE
                                                    return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" disabled>';
                                                } else { //CUSMAT IS THE SAME
                                                    if (full.RCDPOPRC !== full.RCDINVPRC) {
                                                        return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" disabled>';
                                                    } else {
                                                        return deGits(full.SUMM) + '&nbsp; <input class="checkbox" name="ck" id="ck' + full.RCDINVNO + '" type="checkbox" style="width:20px; height:20px;" >';
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (full.RCDMCTRL === "") {
                                            return "xxxx";
                                        } else {
                                            return full.RCDMCTRL;
                                        }
                                    }
                                }

                            }
                        } //SUM
                    ],
                    "columnDefs": [
                        {"width": "1%", "targets": 0},
                        {"width": "1%", "targets": 1},
                        {"width": "1%", "targets": 2},
                        {"width": "1%", "targets": 3},
                        {"width": "1%", "targets": 4},
                        {"width": "1%", "targets": 5},
                        {"width": "1%", "targets": 6, className: "text-center"},
                        {"width": "1%", "targets": 7},
                        {"width": "1%", "targets": 8, className: "text-center"},
                        {"width": "1%", "targets": 9, className: "text-right"},
                        {"width": "1%", "targets": 10, className: "text-right"},
                        {"width": "1%", "targets": 11, className: "text-right"},
                        {"width": "1%", "targets": 12, className: "text-center"},
                        {"width": "1%", "targets": 13},
                        {"width": "1%", "targets": 14},
                        {"width": "1%", "targets": 15},
                        {"width": "2%", "targets": 16},
                        {"width": "3%", "targets": 17},
                        {"width": "5%", "targets": 18}
                    ],
                    "bPaginate": false,
                    "ordering": false,
                    rowId: function (a) {
                        if (a.RCDPOLINE === 'Head') {
                            return 'H-' + a.RCDINVNO.replace("|Hide", "");
                        } else {
                            return 'D-' + a.RCDINVNO.replace("|Hide", "");
                        }

                    },
                    "createdRow": function (row, data, dataIndex) {
                        if (data.RCDPOLINE !== 'Head') {
                            $(row).css("display", "none");

                            if (data.RCDMATREF !== "") {
                                $(row).css("cursor", "pointer");
                                $(row).css("background-color", "rgb(145, 230, 199)");
                                $(row).css("color", "rgb(34, 96, 151)");
                            }

                        } else {
                            if (data.RCDINVREF !== "") {
                                $(row).css("cursor", "pointer");
                                $(row).css("background-color", "rgb(145, 199, 230)");
                                $(row).css("color", "rgb(34, 96, 151)");
                            } else {
                                $(row).css("cursor", "pointer");
                                $(row).css("background-color", "rgb(249, 249, 249)");
                                $(row).css("color", "rgb(34, 96, 151)");
                            }
                        }
                    }

                });

            } else {
                alertify.error('Not Found Data');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });

    });

    $('#confbtn').click(function () {

        var StatusLine = false;

        $('input[name=stsckline]').each(function () {
            if (this.value === "1") {
                StatusLine = true;
            }
        });

        var wh = $('#WHlist').val();
        var sp = $('#SUPlist').val();
        var dt = $('#datesend').val();
        var cn = $('#CUSlist').val();

        var uu = sessionStorage.getItem('uid');

        var alwaySTS = false; // true => Invoice Checkbox Checked .... false => Invoice Checkbox Not Check

        if (StatusLine) {

            $('.checkbox').each(function () {
                var ckSTS = this.checked; //get Class CheckBox Value : true => Check .... false => Not Check

                if (ckSTS) {

                    if (!$('#' + this.id).prop('outerHTML').toString().includes("disabled")) { //Checkbox Not disabled

                        var vall = this.id.toString().replace("ck", "");
                        var postingDate = $('#pdate' + vall).val();

//                    var HeadSTS = $('#HeadSTS').val();

                        var hstar = $('#hStar' + vall).val();

                        var hpoline = $('#hpline' + vall).val();

                        var permis = true; //true => insert QRMMAS ..... false => Not insert QRMMAS

//                    if (HeadSTS !== "1") { //Status RCMBPH
//                        alertify.error("Ststus Not Equal 1");
//                        permis = false;
//                    }

                        if (hpoline !== "") { //PO Line
                            permis = false;
                            alertify.error("Please Check Some P/O Line No Data");
                        }

                        if (postingDate === "") { //Posting Date
                            alertify.error("Please Choose PostingDate");
                            permis = false;
                        }

                        if (hstar !== "") { //Material Code
                            alertify.error("Please Change Material Code");
                            permis = false;
                        }

                        if (permis) {

                            $.ajax({
                                url: "/TWC_RCMS/RCM100Get",
                                data: {mode: "insertRCMDetailToQRMMAS", wh: wh, sp: sp, cn: cn, dt: dt, inv: vall, uid: uu, postdate: postingDate},
                                async: false
                            }).done(function (result) {

                                if (result.insertMASTRA) {
                                    alertify.success('Insert Invoice : ' + vall + ' Success');
                                    $('#printfunc').prop('hidden', false);
                                    $('#cancbtn').prop('hidden', false);
                                } else {
                                    alertify.error('Insert Invoice : ' + vall + ' Failed');
                                }

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });

                        }

                    }

                    alwaySTS = true;

                }

            });

            $('#enterbtn').click();

            if (alwaySTS === false) {
                alertify.error('Please Choose Invoice ');
            }

        } else {
            alertify.error("Status Not Equal 1");
        }

    });

    $('#backbtn').click(function () {
        location.reload();
    });

    $('#ckall').change(function () {
        var ckall = this.checked;
        $('.checkbox').each(function () {

            if (!$('#' + this.id).prop('outerHTML').toString().includes('disabled') && !$('#' + this.id).prop('outerHTML').toString().includes('checked')) { //check only not disabled and not checked 
                this.checked = ckall;
            }

        });
    });

    $('#printfunc').click(function () {

        $('#pdlist').find('option').remove().end().append('<option value=""></option>');

        var wh = $('#WHlist').val();
        var sp = $('#SUPlist').val();
        var cn = $('#CUSlist').val();
        var dt = $('#datesend').val();

        $.ajax({
            url: "/TWC_RCMS/RCM100Get",
            data: {mode: "getPostDate", wh: wh, sp: sp, cn: cn, dt: dt}, //get Warehouse For Select
            async: false
        }).done(function (result) {

            for (var i = 0; i < result.resData.length; i++) {

                let postd = result.resData[i].RCDPOSTDATE;
                let postYY = postd.toString().substring(0, 4);
                let postMM = postd.toString().substring(4, 6);
                let postDD = postd.toString().substring(6, 8);

                $('#pdlist').append('<option value="' + postd + '" >' + postMM + "/" + postDD + "/" + postYY + '</option>');

            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });

        $('#modalSelPostDate').modal("show");

    });

    $('#printfunc2').click(function () {
        var wh = $('#WHlist').val();
        var sp = $('#SUPlist').val();
        var cn = $('#CUSlist').val();
        var dt = $('#datesend').val();

        var table = $('#rcm100Table').DataTable();
        var roww = table.rows().count();

        var ck = [];

//        var checkboxes = $('input:checkbox:checked').length;

        $('.checkbox').each(function () {

            var ckSTS = this.checked;

            if (ckSTS) {
                ck.push(this.id.toString().replace("ck", ""));
            }

        });

//        if (checkboxes > 0) {
        if (roww > 0) {
            window.open('/TWC_RCMS/RCM100/Print?wh=' + wh + '&sp=' + sp + '&cn=' + cn + '&dt=' + dt + '&ck=' + ck + '&sel=before');
        } else {
            alertify.error('Error Not Found Data ' + roww);
        }
//        } else {
//            alertify.error('Please Choose Invoice For Confirm Before Output Report');
//        }

    });

    $('#cancbtn').click(function () {

        $('#modalCanCel').modal("show");

    });


//    $('#WHlist').val("WH2");
//    $('#datesend').val("2022-05-11");

});

function confPrintReport() {

    var wh = $('#WHlist').val();
    var sp = $('#SUPlist').val();
    var cn = $('#CUSlist').val();
    var dt = $('#datesend').val();
    var pd = $('#pdlist').val();

    if (pd === "") {
//        $('#modalSelPostDate').modal("show");
        alertify.error("Please Select Posting Date.");
//        $('#printfunc').click();
    } else {
        var table = $('#rcm100Table').DataTable();
        var roww = table.rows().count();

        var ck = [];

        var checkboxes = $('input:checkbox:checked').length;

        $('.checkbox').each(function () {

            var ckSTS = this.checked;

            if (ckSTS) {
                ck.push(this.id.toString().replace("ck", ""));
            }

        });

        if (checkboxes > 0) {
            if (roww > 0) {
                window.open('/TWC_RCMS/RCM100/Print?wh=' + wh + '&sp=' + sp + '&cn=' + cn + '&dt=' + dt + '&pd=' + pd + '&ck=' + ck + '&sel=after');
            } else {
                alertify.error('Error Not Found Data ' + roww);
            }
        } else {
            alertify.error('Please Choose Invoice For Confirm Before Output Report');
        }
    }

}

function listmenuInv(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];

    document.getElementById("myDropdown" + jobno + "|" + invno).classList.toggle("show");

}

function listmenuMat(allparam) { //

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var pono = allparam.split("|")[2];

    document.getElementById("myDropdown" + jobno + "|" + invno).classList.toggle("show");

}

function openmodalEditInv(allparam) {

//    event.stopPropagation();

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];

//    var po = allparam.split("|")[2];
//    var poline = allparam.split("|")[3];
//    var mat = allparam.split("|")[4];
//    var matcd = allparam.split("|")[5];

//    document.getElementById("myDropdown" + jobno + "|" + invno).classList.toggle("show");


//    $('#modal2Way').modal('show');
    $('#modalEditInv').modal('show');
//
//    $('#modal_po').text(po);
//    $('#modal_poline').text(poline);
//
    $('#modal_invold').val(invno);
//    $('#modal_matcd').val(matcd);
//
//
    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
//    $('#hidePo').val(po);
//    $('#hidePoline').val(poline);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function openmodalRJectInv(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];


//    var po = allparam.split("|")[2];
//    var poline = allparam.split("|")[3];
//    var mat = allparam.split("|")[4];
//    var matcd = allparam.split("|")[5];

//    document.getElementById("myDropdown" + jobno + "|" + invno).classList.toggle("show");


//    $('#modal2Way').modal('show');
    $('#modalRJectInv').modal('show');
//
//    $('#modal_po').text(po);
//    $('#modal_poline').text(poline);
//
    $('#modal_invshowRJ').text(invno);
//    $('#modal_matcd').val(matcd);
//
//
    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
//    $('#hidePo').val(po);
//    $('#hidePoline').val(poline);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function openmodalCLRJectInv(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];


//    var po = allparam.split("|")[2];
//    var poline = allparam.split("|")[3];
//    var mat = allparam.split("|")[4];
//    var matcd = allparam.split("|")[5];

//    document.getElementById("myDropdown" + jobno + "|" + invno).classList.toggle("show");


//    $('#modal2Way').modal('show');
    $('#modalCLRJectInv').modal('show');
//
//    $('#modal_po').text(po);
//    $('#modal_poline').text(poline);
//
    $('#modal_invshowCLRJ').text(invno);
//    $('#modal_matcd').val(matcd);
//
//
    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
//    $('#hidePo').val(po);
//    $('#hidePoline').val(poline);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function openMenuMat(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];
    var poline = allparam.split("|")[3];
    var mat = allparam.split("|")[4];

    document.getElementById("myDropdown" + jobno + "|" + invno + "|" + po + "|" + poline + "|" + mat).classList.toggle("show");

}

function openmodalRJectMat(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];
    var poline = allparam.split("|")[3];
    var mat = allparam.split("|")[4];

    $('#modalRJectMat').modal('show');

    $('#modal_matshowRJ').text(mat);

    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
    $('#hidePo').val(po);
    $('#hidePoline').val(poline);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function openmodalCLRJectMat(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];
    var poline = allparam.split("|")[3];
    var mat = allparam.split("|")[4];

    $('#modalCLRJectMat').modal('show');

    $('#modal_matshowCLRJ').text(mat);

    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
    $('#hidePo').val(po);
    $('#hidePoline').val(poline);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function confRJMat() {

    var job = $('#hideJob').val();
    var invno = $('#hideInv').val();
    var po = $('#hidePo').val();
    var poline = $('#hidePoline').val();
    var mat = $('#modal_matshowRJ').text();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateRejectMat", job: job, invno: invno, po: po, poline: poline, mat: mat},
        async: false
    }).done(function (result) {

        if (result.updMat) {
            alertify.success('Reject Cus Mat Success');
            $('#enterbtn').click();
        } else {
            alertify.error('Reject Cus Mat Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function confCLRJMat() {

    var job = $('#hideJob').val();
    var invno = $('#hideInv').val();
    var po = $('#hidePo').val();
    var poline = $('#hidePoline').val();
    var mat = $('#modal_matshowRJ').text();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateClearRejectMat", job: job, invno: invno, po: po, poline: poline, mat: mat},
        async: false
    }).done(function (result) {

        if (result.updMat) {
            alertify.success('Clear Reject Cus Mat Success');
            $('#enterbtn').click();
        } else {
            alertify.error('Clear Reject Cus Mat Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function openmodalCCusMat(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];


//    var po = allparam.split("|")[2];
//    var poline = allparam.split("|")[3];
//    var mat = allparam.split("|")[4];
//    var matcd = allparam.split("|")[5];

//    document.getElementById("myDropdown" + jobno + "|" + invno).classList.toggle("show");


//    $('#modal2Way').modal('show');
    $('#modalRJectInv').modal('show');
//
//    $('#modal_po').text(po);
//    $('#modal_poline').text(poline);
//
    $('#modal_invshowRJ').text(invno);
//    $('#modal_matcd').val(matcd);
//
//
    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
//    $('#hidePo').val(po);
//    $('#hidePoline').val(poline);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function openmodalPriInv(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];

    $('#modalPriInv').modal('show');

    $('#modal_invshowPRI').text(invno);
    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function closeMenuList() {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }

}

function openmodalEditPO(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];

    $('#modalEditPo').modal('show');

    $('#modal_ponoold').val(po);

//    $('#modal_cusmat').val(mat);
//    $('#modal_matcd').val(matcd);


    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
    $('#hidePo').val(po);

}

function modalEditMat(allparam) {
    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];
    var poline = allparam.split("|")[3];
    var mat = allparam.split("|")[4];
    var matcd = allparam.split("|")[5];

    $('#modalEditMat').modal('show');

    $('#modal_po').text(po);
    $('#modal_poline').text(poline);

    $('#modal_cusmat').val(mat);
    $('#modal_matcd').val(matcd);


    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
    $('#hidePo').val(po);
    $('#hidePoline').val(poline);

}

function modalEditPOline(allparam) {
    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];
    var poline = allparam.split("|")[3];
    var mat = allparam.split("|")[4];
    var matcd = allparam.split("|")[5];

    $('#modalEditPOline').modal('show');

    $('#modal_oldpoline').val(poline);

    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
    $('#hidePo').val(po);
    $('#hidePoline').val(poline);
    $('#hideMat').val(mat);

}

function confEditInv() {

    var job = $('#hideJob').val();
    var invold = $('#hideInv').val();
    var invnew = $('#modal_invnew').val();

    if (invnew === "") {
        //modal Error Please Fill New Invoice
        $('#modalAlertPlsFillNewInv').modal('show');
    } else {
        $.ajax({
            url: "/TWC_RCMS/RCM100Get",
            data: {mode: "updateInv", job: job, invold: invold, invnew: invnew},
            async: false
        }).done(function (result) {

            if (result.updInv) {
                alertify.success('Edit Invoice No Success');
                $('#enterbtn').click();
            } else {
                alertify.error('Edit Invoice No Failed');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });

        $('#modal_invnew').val("");
    }

}

function confPriInv() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updatePriInv", job: job, inv: inv},
        async: false
    }).done(function (result) {

        if (result.updInv) {
            alertify.success('Set Price Invalid Inv. ' + inv + ' Success');
            $('#enterbtn').click();
        } else {
            alertify.error('Set Price Invalid Inv. ' + inv + ' Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function confRJInv() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateRejectInv", job: job, inv: inv},
        async: false
    }).done(function (result) {

        if (result.updInv) {
            alertify.success('Reject Inv. ' + inv + ' Success');
            $('#enterbtn').click();
        } else {
            alertify.error('Reject Inv. ' + inv + ' Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function confCLRJInv() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateClearRejectInv", job: job, inv: inv},
        async: false
    }).done(function (result) {

        if (result.updInv) {
            alertify.success('Clear Reject Inv. ' + inv + ' Success');
            $('#enterbtn').click();
        } else {
            alertify.error('Clear Reject Inv. ' + inv + ' Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function confEditPo() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();
    var poold = $('#hidePo').val();
    var ponew = $('#modal_pononew').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateEditPoNo", job: job, inv: inv, poold: poold, ponew: ponew},
        async: false
    }).done(function (result) {

        if (result.updPoNo) {
            alertify.success('Edit PO No. Success');
            $('#enterbtn').click();
        } else {
            alertify.error('Edit PO No. Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $('#modal_pononew').val("");

}

function confYLStarMat() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();
    var po = $('#hidePo').val();
    var poline = $('#hidePoline').val();
    var cusmatOld = $('#modal_matcoderef').val();
    var cusmatNew = $('#modal_matcodePNew').val();

    var wh = $('#WHlist').val();
    var sp = $('#SUPlist').val();
    var cs = $('#CUSlist').val();
    var dt = $('#datesend').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateMat200", job: job, inv: inv, po: po, poline: poline, cusmatOld: cusmatOld, cusmatNew: cusmatNew, wh: wh, sp: sp, cs: cs, dt: dt},
        async: false
    }).done(function (result) {

        if (result.updCusMat.map.resupdBPD) {
            console.log("Update Flag C To NULL : " + result.updCusMat.map.resupdBPD);
        }
        if (result.updCusMat.map.resupdBPT) {
            console.log("Update Old MatCode To New MatCode : " + result.updCusMat.map.resupdBPT);
            alertify.success('Update New MatCode Success');
        } else {
            alertify.error('Update New MatCode Failed');
        }

        if (result.updCusMat.map.resupdBPD === true || result.updCusMat.map.resupdBPT === true) {
            $('#enterbtn').click();
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function confEditMat() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();
    var po = $('#hidePo').val();
    var poline = $('#hidePoline').val();
    var cusmat = $('#modal_cusmat').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updateMat", job: job, inv: inv, po: po, poline: poline, cusmat: cusmat},
        async: false
    }).done(function (result) {

        if (result.updCusMat) {
            alertify.success('Edit Material Code Success');
            $('#hStar' + inv).val("");
            $('#enterbtn').click();
        } else {
            alertify.error('Edit Material Code Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function confEditPOLine() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();
    var po = $('#hidePo').val();
    var mat = $('#hideMat').val();

    var poline = $('#modal_oldpoline').val(); //old po line
    var newpoline = $('#modal_newpoline').val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "updatePOLine", job: job, inv: inv, po: po, poline: poline, newpoline: newpoline, mat: mat},
        async: false
    }).done(function (result) {

        if (result.updPOLine) {
            alertify.success('Edit PO Line Success');
            $('#hpline' + inv).val("");
            $('#enterbtn').click();
        } else {
            alertify.error('Edit PO Line Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function editPackageType(allparam) {

    var jobno = allparam.split("|")[0];
    var invno = allparam.split("|")[1];
    var po = allparam.split("|")[2];
    var poline = allparam.split("|")[3];
    var mat = allparam.split("|")[4];

    var NEWpacktype = $('#pktype' + allparam.replaceAll("|", "")).val();

    $.ajax({
        url: "/TWC_RCMS/RCM100Get",
        data: {mode: "editPKType", job: jobno, inv: invno, po: po, poline: poline, cusmat: mat, packtype: NEWpacktype},
        async: false
    }).done(function (result) {

        if (result.updPackType) {
            alertify.success('Edit Package Type Success');
        } else {
            alertify.error('Edit Package Type Failed');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function canFunc() {

    var wh = $('#WHlist').val();
    var sp = $('#SUPlist').val();
    var cn = $('#CUSlist').val();
    var dt = $('#datesend').val();

    $('.checkbox').each(function () {

        var ckSTS = this.checked;

        if (ckSTS) {
            if ($('#' + this.id).prop('outerHTML').toString().includes("disabled")) {

                var idinv = this.id.toString().replace("ck", "");

                $.ajax({
                    url: "/TWC_RCMS/RCM100Get",
                    data: {mode: "cancelInvoice", inv: this.id.toString().replace("ck", ""), wh: wh, sp: sp, cn: cn, dt: dt}, //inv จะมีซ้ำกันเมื่อต่าง Jobno ไหม
                    async: false
                }).done(function (result) {

                    if (result.delByInv) {
                        alertify.success('Cancel Receive Invoice : ' + idinv + ' Success');
//                        $('#rcm100Table').DataTable().ajax.reload();
                    } else {
                        alertify.error('Cancel Receive Invoice : ' + idinv + ' Failed');
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            } else {
                alertify.error('Please Check Invoice Status : Unable to Cancel');
            }
        }

    });

//    $('#rcm100Table').DataTable().ajax.reload();
    $('#enterbtn').click();
    $('#printfunc').prop('hidden', true);
    $('#cancbtn').prop('hidden', true);

}

function expand() {
    $("#exp").css("display", "none");
    $("#com").css("display", "block");
    var tr = document.getElementsByTagName('tr');
    for (var i = 0; i < tr.length; i++) {
        if (tr[i].id.toString().split("-")[0] === "H") {
            if (!tr[i].getAttribute('style').includes("rgb(145, 199, 230)") && !tr[i].getAttribute('style').includes("rgb(145, 230, 199)")) {
                tr[i].style.cssText = "cursor: pointer; background-color: #003682; color: white;";
            }
        } else if (tr[i].id.toString().split("-")[0] === "D") {
            tr[i].style.display = "";
        }
    }
}
function compress() {
    $("#exp").css("display", "block");
    $("#com").css("display", "none");
    var tr = document.getElementsByTagName('tr');
    for (var i = 0; i < tr.length; i++) {
        if (tr[i].id.toString().split("-")[0] === "H") {
            if (!tr[i].getAttribute('style').includes("rgb(145, 199, 230)") && !tr[i].getAttribute('style').includes("rgb(145, 230, 199)")) {
                tr[i].style.cssText = "cursor: pointer; background-color: #F9F9F9; color: #226097;";
            }
        } else if (tr[i].id.toString().split("-")[0] === "D") {
            tr[i].style.display = "none";
        }
    }
}

function deGits(num) {
    if (num !== "" && num !== undefined && num !== 'undefined') {
        var n = num.toString().split(".");
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return n.join(".");
    } else {
        return 0;
    }
}

function clickexp(param) {

    var poqty = parseFloat(param.toString().split("|")[0]);
    var rate = parseFloat(param.toString().split("|")[1]);
    var canReceive = parseFloat(param.toString().split("|")[2]);
    var hispoqty = parseFloat(param.toString().split("|")[3]);
    var invqty = parseFloat(param.toString().split("|")[4]);
    var Receive = parseFloat(param.toString().split("|")[5]);

    $('#R21').text(poqty);
    $('#R22').text(rate + "%");
    $('#R3').text(canReceive);
    $('#R51').text(hispoqty);
    $('#R52').text(invqty);
    $('#R6').text(Receive);
    $('#R71').text(Receive);
    $('#R72').text(canReceive);

    if (Receive <= canReceive) {
        $('#R73').text("รับได้");
    } else {
        $('#R73').text("รับไม่ได้");
    }

//    $('#R5').text("(" + hispoqty + "*100" + ")" + "/" + parseFloat(invqty).toFixed(2));
//    if (isNaN(Receive)) {
//        Receive = 0;
//    }
//    $('#R6').text(Receive.toFixed(2));

    $('#modalExplain').modal('show');
}

function updScan200Mat(param) {

    var jobno = param.toString().split("|")[0];
    var invno = param.toString().split("|")[1];
    var pono = param.toString().split("|")[2];
    var poline = param.toString().split("|")[3];
    var cusmat = param.toString().split("|")[4];
    var matref = param.toString().split("|")[5];

    $('#hideJob').val(jobno);
    $('#hideInv').val(invno);
    $('#hidePo').val(pono);
    $('#hidePoline').val(poline);
    $('#hideMat').val(cusmat);

    if (matref !== "") {
        $('#modalYLStar').modal('show');

        $('#modal_matcoderef').val(matref);
        $('#modal_matcodePNew').val(cusmat);

    } else {
        //modal Not Found Mat Ref
        $('#modalAlertNotFMatRef').modal('show');
    }

}

function clearYLStar() {

    var job = $('#hideJob').val();
    var inv = $('#hideInv').val();
    var po = $('#hidePo').val();
    var poline = $('#hidePoline').val();
    var mat = $('#hideMat').val();

    $.ajax({
        url: "/TWC_RCMST/RCM100Get",
        data: {mode: "clearYellowWhenNotFoundRef", job: job, inv: inv, po: po, poline: poline, mat: mat},
        async: false
    }).done(function (result) {

        if (result.updCusMat.map.resupdBPD === true) {
            $('#enterbtn').click();
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}


