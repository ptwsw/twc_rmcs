/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    var table = $('#rcm900Table').DataTable({
        "columnDefs": [
            {"width": "2%", "targets": 0},
            {"width": "3%", "targets": 1},
            {"width": "1%", "targets": 2},
            {"width": "3%", "targets": 3},
            {"width": "5%", "targets": 4},
            {"width": "2%", "targets": 5, className: "text-right"},
            {"width": "2%", "targets": 6, className: "text-right"},
            {"width": "2%", "targets": 7, className: "text-right"},
            {"width": "2%", "targets": 8, className: "text-right"},
            {"width": "5%", "targets": 9, className: "text-right"},
            {"width": "2%", "targets": 10, className: "text-right"}
        ],
        "bPaginate": false,
        "ordering": false
    });

    var today = new Date();
    $('#datesend').val(today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2));
    var uu = sessionStorage.getItem('uid');

    $.ajax({
        url: "/TWC_RCMS/RCM900Get",
        data: {mode: "getWH", uid: uu}, //get Warehouse For Select
        async: false
    }).done(function (result) {

        for (var i = 0; i < result.whList.length; i++) {
            $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" ' + result.whList[i].Select + ' >' + result.whList[i].QWHNAME + '</option>');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

//                $('#WHlist').val("WH2");
//                $('#datesend').val("2021-10-20");

    $('#enterbtn').click(function () {

        $("#exp").css("display", "block");
        $("#com").css("display", "none");

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val();

        $('#WHlist').prop('disabled', true);
        $('#SUPlist').prop('disabled', true);
        $('#datesend').prop('disabled', true);
        $('#CUSlist').prop('disabled', true);

        //----------GetTable-----------

        if ($.fn.DataTable.isDataTable('#rcm900Table')) { //before RE-Create New Datatable
            $('#rcm900Table').DataTable().destroy();
        }

        $('#rcm900Table tbody').empty();

        $('#rcm900Table').dataTable({
            ajax: {
                url: "/TWC_RCMS/RCM900Get",
                data: {mode: "getSummaryTable", wh: wh, supli: sp, tdate: dt},
                dataSrc: 'DList'
            },
            "aoColumns": [
                {"data": "NUMB"},
                {'mRender': function (data, type, full) {
                        var date = "";
                        if (full.RCHCDT !== "") {
                            if (full.RCHCDT === "0") {
                                date = "Not Found Create Date.";
                            } else {
                                var dd = full.RCHCDT.toString().substring(6, 8);
                                var mm = full.RCHCDT.toString().substring(4, 6);
                                var yyyy = full.RCHCDT.toString().substring(0, 4);
                                date = dd + "-" + mm + "-" + yyyy;
                            }
                        }

                        return date;
                    }
                },
                {'mRender': function (data, type, full) {
                        return "";
                    }
                },
                {'mRender': function (data, type, full) {

                        if (full.RCHJBNO !== undefined) {
                            if (full.RCHJBNO !== "") {
                                return full.RCHJBNO;
                            } else {
                                return "";
                            }
                        } else {
                            return "";
                        }

                    }
                },
                {'mRender': function (data, type, full) {

                        if (full.RCHCUNO !== undefined) {
                            if (full.RCHCUNO !== "") {
                                if (full.RCHCUNO === "211809") {
                                    return full.RCHCUNO + " : บมจ. ไทยวาโก้";
                                } else if (full.RCHCUNO === "211813") {
                                    return full.RCHCUNO + " : บจก. วาโก้ลำพูน";
                                } else if (full.RCHCUNO === "211821") {
                                    return full.RCHCUNO + " : บจก. ภัทยาอุตสาหกิจ (PMC2)";
                                } else if (full.RCHCUNO === "211823") {
                                    return full.RCHCUNO + " : บจก. มอร์แกน เดอ ทัว(ประเทศไทย)";
                                } else {
                                    return "";
                                }
                            } else {
                                return "";
                            }
                        } else {
                            return "";
                        }

                    }
                },
                {"data": "RCHPLANT"},
                {'mRender': function (data, type, full) {

                        return deGits(full.RCHTOTINV);
                    }
                },
                {'mRender': function (data, type, full) {

                        return deGits(full.RCHTOTPO);
                    }
                },
                {'mRender': function (data, type, full) {

                        return deGits(full.RCHTOTQTY);
                    }
                },
                {'mRender': function (data, type, full) {

                        return deGits(full.RCHTOTAMT);
                    }
                },
                {'mRender': function (data, type, full) {

                        return deGits(full.RCHITEM);
                    }
                }
            ],
            "columnDefs": [
                {"width": "2%", "targets": 0},
                {"width": "3%", "targets": 1},
                {"width": "1%", "targets": 2},
                {"width": "3%", "targets": 3},
                {"width": "5%", "targets": 4},
                {"width": "2%", "targets": 5, className: "text-right"},
                {"width": "2%", "targets": 6, className: "text-right"},
                {"width": "2%", "targets": 7, className: "text-right"},
                {"width": "2%", "targets": 8, className: "text-right"},
                {"width": "5%", "targets": 9, className: "text-right"},
                {"width": "2%", "targets": 10, className: "text-right"}
            ],
            "bPaginate": false,
            "ordering": false,
            rowId: function (a) {
                if (a.NUMB === "G.TOTAL" || a.NUMB === "TOTAL") {
                    return 'H-' + a.RCHCDT;
                } else {
                    return 'D-' + a.RCHCDT;
                }
            },
            "createdRow": function (row, data, dataIndex) {
                if (data.NUMB !== "G.TOTAL" && data.NUMB !== "TOTAL") {
                    $(row).css("display", "none");
                }
            },
            "stripeClasses": []
//                        "drawCallback": function (settings) {
//                            var api = this.api();
//                            var rows = api.rows({page: 'current'}).nodes();
//                            var last = null;
//                            var colonne = 9;
//                            var totale = new Array();
//                            totale['Totale'] = new Array();
//                            var groupid = -1;
//                            var subtotale = new Array();
//
//                            api.column(1, {page: 'current'}).data().each(function (group, i) {
//
//                                if (last !== group) {
//                                    groupid++;
//                                    $(rows).eq(i).before('<tr class="group"><td>TOTAL</td></tr>');
//                                    last = group;
//                                }
//                                
//                                var cntplnt = 0;
//
//
//                                val = api.row(api.row($(rows).eq(i)).index()).data();      //current order index
//                                $.each(val, function (index2, val2) {
//                                    console.log(index2 + " " + val2);
//                                    
//                                    cntplnt+=1;
//
////                                    if (index2 !== "RCDCDT" && index2 !== "RCDPLANT") {
//
//                                    if (typeof subtotale[groupid] == 'undefined') {
//                                        subtotale[groupid] = new Array();
//                                    }
//                                    if (typeof subtotale[groupid][index2] == 'undefined') {
//                                        subtotale[groupid][index2] = 0;
//                                    }
//                                    if (typeof totale['Totale'][index2] == 'undefined') {
//                                        totale['Totale'][index2] = 0;
//                                    }
//
//                                    var valore = Number(val2.replace('€', "").replace('.00', "").replace(',', "."));
////                                    
//                                    subtotale[groupid][index2] += valore;
//                                    totale['Totale'][index2] += valore;
//
////                                    }
//
//                                });
//
//                            });
//
//                            $('tbody').find('.group').each(function (i, v) {
////                                console.log(i);
//                                var rowCount = $(this).nextUntil('.group').length;
//                                $(this).find('td:first').append($('<span />', {'class': 'rowCount-grid'}));
//                                var subtd = '';
//                                for (var a = 2; a < colonne; a++)
//                                {
//                                    var lastparam = "";
//                                    if (a === 2) {
//                                        lastparam = "RCDCDT";
//                                        subtd += '<td></td>';
//                                    } else if (a === 3) {
//                                        lastparam = "RCDPLANT";
//                                        subtd += '<td>' + rowCount + '</td>';
//                                    }
//
//                                }
//
////                                console.log(subtd);
//                                $(this).append(subtd);
//                            });
//
//                        }

        });

    });

    $('#backbtn').on('click', function () {
        location.reload();
    });

    // Collapse / Expand Click Groups
//                $('.grid tbody').on('click', 'tr.group', function () {
//                    var rowsCollapse = $(this).nextUntil('.group');
//                    $(rowsCollapse).toggleClass('hidden');
//                });

});

function expand() {
    $("#exp").css("display", "none");
    $("#com").css("display", "block");
    var tr = document.getElementsByTagName('tr');
    for (var i = 0; i < tr.length; i++) {
        if (tr[i].id.toString().split("-")[0] === "H") {
            tr[i].style.cssText = "cursor: pointer; background-color: #003682; color: white;";
        } else if (tr[i].id.toString().split("-")[0] === "D") {
            tr[i].style.display = "";
        }
    }
}
function compress() {
    $("#exp").css("display", "block");
    $("#com").css("display", "none");
    var tr = document.getElementsByTagName('tr');
    for (var i = 0; i < tr.length; i++) {
        if (tr[i].id.toString().split("-")[0] === "H") {
            tr[i].style.cssText = "";
//                        tr[i].style.cssText = "cursor: pointer; background-color: #F9F9F9; color: #226097;";
        } else if (tr[i].id.toString().split("-")[0] === "D") {
            tr[i].style.display = "none";
        }
    }
}
function deGits(num) {
    var n = num.toString().split(".");
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return n.join(".");
}