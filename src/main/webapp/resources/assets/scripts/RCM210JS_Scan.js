/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#qrreader').focus();

    var whs = getUrlVars()["wh"];
    var sps = getUrlVars()["sp"];
    var dts = getUrlVars()["dt"];
    var cns = getUrlVars()["cn"];

    var inv = getUrlVars()["inv"];
    var po = getUrlVars()["po"];
    var mt = getUrlVars()["mt"];

    $('#inv').text(inv);
    $('#pono').text(po.toString().replace(/%20/g, " "));
    $('#matc').text(mt);

    getTable(whs, sps, cns, dts, inv, po, mt);

    let yyyy = dts.substring(0, 4);
    let mm = dts.substring(4, 6);
    let dd = dts.substring(6, 8);

    $('#dts').text(dd + "-" + mm + "-" + yyyy);
    $('#saveDateHead').val(yyyy + "-" + mm + "-" + dd);

    $('#sps').text(getNameSupCusFix(sps));
    $('#cns').text(getNameSupCusFix(cns));

    $.ajax({// Warehouse Name
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWHbyCode", code: whs},
        async: false
    }).done(function (result) {

        $('#whs').text(result.whList[0].QWHNAME);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $.ajax({// Total
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getTOTbyWhSpDt210", wh: whs, sp: sps, cn: cns, dt: dts, inv: inv, pono: po.toString().replace(/%20/g, " "), mat: mt},
        async: false
    }).done(function (result) {

        if (result.totalLS[0] === undefined) {
//                        alertify.error('Please Check Total Qty');
            $('#totalcnt').text("0");
        } else {
            $('#totalcnt').text(result.totalLS[0].RCDINVQTY);
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $('#qrreader').change(function () { //QR CODE SCAN
        var qrcode = $('#qrreader').val();

        let dtHead = $('#saveDateHead').val();
//                    let dtHead = $('#dts').text();
        let cnHead = $('#cns').text().split(" : ")[0];

//                    console.log(dtHead);

        var countchar = qrcode.toString().length - 1;

        let len = qrcode.split("|").length - 1; //count character => |

        if (countchar > 40) { //len = 18

            var substrQR = qrcode.toString().substring(0, 3);

            if (substrQR === "GTM") {

                var xhrf = ""; //Get Delivery Date By QRcode
                xhrf = $.ajax({
                    url: "/TWC_RCMS/RCM200Get",
                    type: "GET",
                    data: {mode: "getDLDATEbyQRCode", qr: qrcode},
                    async: false
                }).responseJSON;

                let yyyy = xhrf.datebyQR[0].RCDDLDATE.substring(0, 4);
                let mm = xhrf.datebyQR[0].RCDDLDATE.substring(4, 6);
                let dd = xhrf.datebyQR[0].RCDDLDATE.substring(6, 8);

                let dtScan = yyyy + "-" + mm + "-" + dd;

//                            console.log("dtScan " + dtScan);
//                            console.log("dtHead " + dtHead);

                if (dtScan !== dtHead) {
                    $('#ErrorDeliDNotMatchModal').modal("show");
                    $('#qrreader').val("");
                    $('#qrreader').focus();
                } else {

                    let itid = qrcode.split("|")[0];
                    let ctcode = qrcode.split("|")[3];

                    if ($('#ckb' + itid).prop('outerHTML') !== undefined) {
                        if (!$('#ckb' + itid).prop('outerHTML').toString().includes("disabled")) {
                            $('#ckb' + itid).prop('checked', true);
                        } else {
//                                        alertify.error("ID : " + itid + " Is Rejected");
                            $('#rjTextCk').text("ID : " + itid + " Is Rejected ");
                            $('#ErrorIsRejectedModal').modal('show');
                        }
                    } else {
//                                    alertify.error("Not Found ID : " + itid);
                        $('#nfTextCk').text("Not Found ID : " + itid);
                        $('#ErrorNotFoundForCKModal').modal('show');
                    }

                    $('#qrreader').val("");
                    $('#qrreader').focus();

                }

            } else {
                $('#ErrorFormatNotGTechModal').modal("show");
            }

        } else { //len = 4

            if (len === 4) {
//                            console.log("Pallet Detect.");

                let dtScan = qrcode.split("|")[2];
                let cnScan = qrcode.split("|")[3];

//                            console.log(dtScan);
//                            console.log(dtHead);

                if (dtScan !== dtHead) {
                    $('#ErrorDeliDNotMatchModal').modal("show");
                    $('#qrreader').val("");
                    $('#qrreader').focus();
                } else {

                    if (cnScan !== cnHead) {
                        $('#ErrorCusNotMatchModal').modal("show");
                        $('#qrreader').val("");
                        $('#qrreader').focus();
                    } else {

                        let pallet = qrcode.split("|")[0];

                        $.ajax({// All ID in Pallet
                            url: "/TWC_RCMS/RCM200Get",
                            data: {mode: "getAllIDinPALLET", wh: whs, sp: sps, dt: dts, cn: cns, palno: pallet, inv: inv, po: po.toString().replace(/%20/g, " "), mt: mt},
                            async: false
                        }).done(function (result) {

                            if (result.IDList.length > 0) {
                                var Arr = [];
                                for (var i = 0; i < result.IDList.length; i++) {

                                    if (!$('#ckb' + result.IDList[i].RCDID).prop('outerHTML').toString().includes("disabled")) {
                                        $('#ckb' + result.IDList[i].RCDID).prop('checked', true);
                                    } else {
                                        Arr.push(result.IDList[i].RCDID + "\n");
                                    }
                                }

                                $('#rjTextCk').text("ID : " + Arr.toString().replace(/,/g, "") + "");
                                $('#ErrorIsRejectedModal').modal('show');

                            } else {
                                $('#nfTextCk').text("Not Found Pallet : " + pallet + " in List");
                                $('#ErrorNotFoundForCKModal').modal('show');
//                                            alertify.error("Not Found Match ID. In Pallet : " + pallet);
                            }

                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            // needs to implement if it fails
                        });

                        $('#qrreader').val("");
                        $('#qrreader').focus();

                    }

                }
            } else {
                alertify.error("Please Check QRcode Format. ");
            }

        }

    });

    $('.rmall').click(function () {

        var allowRJModal = false;

        $('input:checkbox:checked').each(function () {
            allowRJModal = true;
        });

        if (allowRJModal) {
            $('#cfReject').modal("show");
        } else {
            $('#ErrorNotCheckModal').modal('show');
        }

    });

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getNameSupCusFix(no) {

    let namemerge = "";

    if (no === "211808") {
        namemerge = no + " : บจก. จี เทค แมททีเรียล";
    } else if (no === "211809") {
        namemerge = no + " : บมจ. ไทยวาโก้";
    } else if (no === "211813") {
        namemerge = no + " : บจก. วาโก้ลำพูน";
    } else if (no === "211821") {
        namemerge = no + " : บจก. ภัทยาอุตสาหกิจ (PMC2)";
    } else if (no === "211823") {
        namemerge = no + " : บจก. มอร์แกน เดอ ทัว(ประเทศไทย)";
    }

    return namemerge;
}

function getTable(whs, sps, cns, dts, inv, pono, matc) {

    console.log("getTable");

    var tt2 = $('#scanTable').DataTable({
        ajax: {
            url: "/TWC_RCMS/RCM200Get",
            data: {mode: "findRCMBPD210", wh: whs, sp: sps, cn: cns, dt: dts, inv: inv, pono: pono.toString().replace(/%20/g, " "), mat: matc},
            dataSrc: 'RCMBPDList'
        },
        "order": [[0, 'desc']],
        "columns": [
            {'mRender': function (data, type, full) {
                    return '<label id="idx' + full.RCDID + '" >' + full.RCDDLNO + '</label>';
                }},
            {"data": "RCDID"},
            {"data": "RCDBOXNO"},
            {"data": "RCDINVQTY"},
            {'mRender': function (data, type, full) {
                    return '<input type="checkbox" style="width:30px; height:30px;" id="ckb' + full.RCDID + '" >';
                }}
//                        {"data": "RCTPACK"},
//                        {'mRender': function (data, type, full) {
//                                return full.RCTPACK + ' ' + '<a style="cursor: pointer; color: red;" class="rmrow" onclick="rmvRow(\'' + full.RCTCTID + '\')" ><i class="glyphicon glyphicon-trash"></i></a>';
////                                return '<a style="cursor: pointer; color: red;" class="rmrow" onclick="rmvRow(\'' + full.RCTCTID + '\')" data-value="1" ><i class="glyphicon glyphicon-trash"></i></a>';
//                            }
//                        }
        ],
        "columnDefs": [
            {"width": "1%", "targets": 0},
            {"width": "5%", "targets": 1},
            {"width": "2%", "targets": 2},
            {"width": "5%", "targets": 3},
            {"width": "5%", "targets": 4, "className": "text-center", "orderable": false}
//                        {"width": "1%", "targets": 3, "orderable": false}
        ],
        "bPaginate": false,
        "drawCallback": function (settings) {

            $.ajax({// Warehouse Name
                url: "/TWC_RCMS/RCM200Get",
                data: {mode: "ckSTSDetailForBin", wh: whs, sp: sps, cn: cns, dt: dts},
                async: false
            }).done(function (result) {

                if (result.ckSTS) {
                    $('.rmall').hide();
//                                $('.rmrow').hide();
                } else {
                    $('.rmall').show();
//                                $('.rmrow').show();
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });

        }
        , "createdRow": function (row, data, dataIndex) {

            if (data.RCDRJFLAG === "R") {
//                            $(row).css("background-color", "rgb(240 160 160 / 56%)");
                $(row).attr("aria-hidden", "true");
                $(row).attr("data-toggle", "tooltip");
                $(row).attr("data-data-placement", "top");
                $(row).attr("title", "Rejected");
                $(row).children('td').children('input:checkbox').prop('disabled', true);

            }

        }

    });

}

function recjectByID() {

    var whs = getUrlVars()["wh"];
    var sps = getUrlVars()["sp"];
    var dts = getUrlVars()["dt"];
    var cns = getUrlVars()["cn"];

    var inv = getUrlVars()["inv"];
    var pon = getUrlVars()["po"];
    var mtc = getUrlVars()["mt"];

    var idAr = [];
//    var idArNot = [];

    $('input:checkbox:checked').each(function () {
        var idGetRJ = $(this).attr('id').toString().replace("ckb", "");
        idAr.push(idGetRJ);
    });

//    $('input:checkbox:not(:checked)').each(function () {
//        var idGetRJNot = $(this).attr('id').toString().replace("ckb", "");
//        if (!$(this).prop('disabled')) {
//            idArNot.push(idGetRJNot);
//        }
//    });

    var myJsonString = JSON.stringify(idAr);
//    var myJsonStringNot = JSON.stringify(idArNot);

    if (myJsonString !== "[]") {
        $.ajax({// Warehouse Name
            url: "/TWC_RCMS/RCM200Get",
            data: {mode: "210Reject", idAr: myJsonString, whs: whs, sps: sps, dts: dts, cns: cns, inv: inv, pon: pon.toString().replace(/%20/g, " "), mtc: mtc},
            async: false
        }).done(function (result) {

            if (result.updRej) {
                alertify.success("Reject Success.");
                $('#scanTable').DataTable().ajax.reload();
            } else {
                $('#ErrorRejectFailedModal').modal("show");
            }

            $.ajax({// Warehouse Name
                url: "/TWC_RCMS/RCM200Get",
                data: {mode: "210Hold", whs: whs, sps: sps, dts: dts, cns: cns, inv: inv, pon: pon.toString().replace(/%20/g, " "), mtc: mtc},
                async: false
            }).done(function (result) {

                if (result.updHold) {
                    console.log("Hold Success.");
//                alertify.success("Reject Success.");
//                $('#scanTable').DataTable().ajax.reload();
                } else {
                    console.log("Hold Failed.");
//                $('#ErrorRejectFailedModal').modal("show");
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });
    } else {
        $('#ErrorNotCheckModal').modal('show');
    }



}
