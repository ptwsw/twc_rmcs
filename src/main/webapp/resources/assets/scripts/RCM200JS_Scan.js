/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var counter = 1;

$(document).ready(function () {

    $('.rmrow').hide();
//    $('#conto').hide();

    $('#qrreader').focus();

//                var t = $('#scanTable').DataTable({
//                    "bPaginate": false,
//                    "columnDefs": [
//                        {"targets": [0, 1, 2], "className": "text-left"}
//                    ],
//                    "order": [[0, 'desc']]
//                });

    var whs = getUrlVars()["wh"];
    var sps = getUrlVars()["sp"];
    var dts = getUrlVars()["dt"];
    var cns = getUrlVars()["cn"];

    getTable(whs, sps, cns, dts);

    let yyyy = dts.substring(0, 4);
    let mm = dts.substring(4, 6);
    let dd = dts.substring(6, 8);

    $('#dts').text(dd + "-" + mm + "-" + yyyy);
    $('#saveDateHead').val(yyyy + "-" + mm + "-" + dd);

    $('#sps').text(getNameSupCusFix(sps));
    $('#cns').text(getNameSupCusFix(cns));

    $.ajax({// Warehouse Name
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWHbyCode", code: whs},
        async: false
    }).done(function (result) {

        $('#whs').text(result.whList[0].QWHNAME);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $.ajax({// Total
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getTOTbyWhSpDt", wh: whs, sp: sps, cn: cns, dt: dts},
        async: false
    }).done(function (result) {

        if (result.totalLS[0] === undefined) {
//                        alertify.error('Please Check Total Qty');
            $('#totalcnt').text("0");
        } else {
            $('#totalcnt').text(result.totalLS[0].RCHTOTQTY);
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    $('#qrreader').change(function () { //QR CODE SCAN
        var qrcode = $('#qrreader').val();

        let dtHead = $('#saveDateHead').val();
//                    let dtHead = $('#dts').text();
        let cnHead = $('#cns').text().split(" : ")[0];

        console.log(dtHead);

// GTM01F21052700021|21|9423102361|1FUR4472BET|C1MR42208001BET|32.00|PAA|32.00|PAA|||2021-08-01|1121007899 1FRM 1000|BOX|9420100249|931112102597|C1MR42208001BET|116109|2021-08-28
// GTM01F21052700021|22|9423102361|1FUR4472BET|C1MR42208001BET|32.00|PAA|32.00|PAA|||2021-08-01|1121007899 1FRM 1000|BOX|9420100249|931112102597|C1MR42208001BET|116109|2021-08-28
// GTM01F21052700023|23|9423102361|1FUR4472BET|C1MR42208001BET|8.00|PAA|8.00|PAA|||2021-08-01|1121007899 1FRM 1000|BOX|9420100249|931112102597|C1MR42208001BET|116109|2021-08-28
// GTM01F21052700024|24|9423102361|1FUR4472BET|C1MR42208001BET|32.00|PAA|32.00|PAA|||2021-08-01|1121007899 1FRM 1000|BOX|9420100249|931112102597|C1MR42208001BET|116109|2021-08-28

// 10/27|5|20-10-2021|211809|2110190001
// 10/10|5|20-10-2021|211809|2110190001
// 10/18|5|20-10-2021|211809|2110190001
// 10/32|5|20-10-2021|211809|2110190001

//GTM01F22052600331|24|9423102361|1FUR3890BEC|C1MR42208001BET|32.00|PAA|32.00|PAA|||2022-05-28|1121007899 1FRM 1000|BOX|9420100249|931112102597|C1MR42208001BET|116109|2022-05-28

        var countchar = qrcode.toString().length - 1;

        let len = qrcode.split("|").length - 1; //count character => |
        console.log(len);

        if (countchar > 40) { //len = 18

            var substrQR = qrcode.toString().substring(0, 3);

            if (substrQR === "GTM") {

                var xhrf = ""; //Get Delivery Date By QRcode
                xhrf = $.ajax({
                    url: "/TWC_RCMS/RCM200Get",
                    type: "GET",
                    data: {mode: "getDLDATEbyQRCode", qr: qrcode},
                    async: false
                }).responseJSON;

                console.log("xhrf " + xhrf.datebyQR.length);

                if (xhrf.datebyQR.length > 0) {
                    let yyyy = xhrf.datebyQR[0].RCDDLDATE.substring(0, 4);
                    let mm = xhrf.datebyQR[0].RCDDLDATE.substring(4, 6);
                    let dd = xhrf.datebyQR[0].RCDDLDATE.substring(6, 8);

                    let dtScan = yyyy + "-" + mm + "-" + dd;

                    console.log("dtScan " + dtScan);
                    console.log("dtHead " + dtHead);

                    if (dtScan !== dtHead) {
                        $('#ErrorDeliDNotMatchModal').modal("show");
                        $('#qrreader').val("");
                        $('#qrreader').focus();
                    } else {

                        let itid = qrcode.split("|")[0];
                        let ctcode = qrcode.split("|")[3];

                        var xhrCK = ""; //CHECK CARTON DUPLICATE
                        xhrCK = $.ajax({
                            url: "/TWC_RCMS/RCM200Get",
                            type: "GET",
                            data: {mode: "ckCARTONdup", wh: whs, sp: sps, dt: dts, cn: cns, ctnid: itid, ctcode: ctcode},
                            async: false
                        }).responseJSON;

                        if (xhrCK.ckRES === true) {

                            $('#ErrorCartonDupModal').modal("show");
                            $('#qrreader').val("");
                            $('#qrreader').focus();
                        } else {

                            let totpackage = "1";
                            let user = $('#user').val();

                            var cntTotal = $('#cntTotal').val();

                            if (cntTotal === "") {
                                cntTotal = 0 + parseInt(totpackage);
                            } else {
                                cntTotal = parseInt(cntTotal) + parseInt(totpackage);
                            }

                            $('#cntTotal').val(cntTotal);
                            $('#qrreader').val("");
                            $('#qrreader').focus();

//                                var t = $('#scanTable').DataTable();
//                                t.row.add([
//                                    counter,
////                                    counter + "<input name='ckbox' class='checkdata' type='checkbox'>",
//                                    pallet,
//                                    totpackage
//                                ]).draw(false);

                            //insert RCMDPT
                            $.ajax({// Total
                                url: "/TWC_RCMS/RCM200Get",
                                data: {mode: "insertRCMBPT", wh: whs, sp: sps, dt: dts, cn: cns, seq: counter, ctnid: itid, ctcode: ctcode, totpc: totpackage, uid: user},
                                async: false
                            }).done(function (result) {

                                console.log(result.ccInsert);

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                // needs to implement if it fails
                            });

                            //                                var setter = setInterval(function () {
                            $('#scanTable').DataTable().ajax.reload();
//                                    clearInterval(setter);
//                                }, 1000);

                            alertify.success('Scan Success');

                            counter++;
                        }
                    }
                } else {
                    $('#qrreader').val("");
                    $('#ErrorNotFoundScanModal').modal("show");
                }

            } else {
                $('#qrreader').val("");
                $('#ErrorFormatNotGTechModal').modal("show");
            }

        } else { //len = 4

            let dtScan = qrcode.split("|")[2];
            let cnScan = qrcode.split("|")[3];

            console.log(dtScan);
            console.log(dtHead);

            if (dtScan !== dtHead) {
                $('#ErrorDeliDNotMatchModal').modal("show");
                $('#qrreader').val("");
                $('#qrreader').focus();
            } else {

                console.log(cnScan);
                console.log(cnHead);

                if (cnScan !== cnHead) {
                    $('#ErrorCusNotMatchModal').modal("show");
                    $('#qrreader').val("");
                    $('#qrreader').focus();
                } else {

                    // Pallet no. | Total Package | วันที่ส่ง | Customer | Job No.
                    // 10/27 | 64 | 22-07-2021 | 211809 | 2105120001

                    let pallet = qrcode.split("|")[0];
//                    let totpackage = qrcode.split("|")[1];
                    let user = $('#user').val();

                    let valIns = true;

                    $.ajax({// Total
                        url: "/TWC_RCMS/RCM200Get",
                        data: {mode: "getAllBPDbyPALNO", wh: whs, sp: sps, dt: dts, cn: cns, palno: pallet},
                        async: false
                    }).done(function (result) {

                        for (var i = 0; i < result.bpdList.length; i++) {

//                            console.log(result.bpdList[i]);

                            //check in bpt before insert

                            var xhrCK = ""; //CHECK CARTON DUPLICATE
                            xhrCK = $.ajax({
                                url: "/TWC_RCMS/RCM200Get",
                                type: "GET",
                                data: {mode: "ckCARTONdupAndPAL", wh: whs, sp: sps, dt: dts, cn: cns, palno: pallet, iditem: result.bpdList[i].RCDID, matcode: result.bpdList[i].RCDCUSMAT},
                                async: false
                            }).responseJSON;

                            if (xhrCK.ckRES === true) {
                                //Duplicate

                            } else {

                                $.ajax({// Total
                                    url: "/TWC_RCMS/RCM200Get",
                                    data: {mode: "insertRCMBPTPal", wh: whs, sp: sps, dt: dts, cn: cns, seq: counter, palno: pallet, ctnid: result.bpdList[i].RCDID, mcode: result.bpdList[i].RCDCUSMAT, totpc: "1", uid: user},
                                    async: false
                                }).done(function (result) {

                                    if (result.ccInsert === false) {
                                        valIns = false;
                                    } else {

                                        var cntTotal = $('#cntTotal').val();

                                        if (cntTotal === "") {
                                            cntTotal = 0 + parseInt("1");
                                        } else {
                                            cntTotal = parseInt(cntTotal) + parseInt("1");
                                        }

                                        $('#cntTotal').val(cntTotal);

                                    }

                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    // needs to implement if it fails
                                });

                            }

//                            console.log(cntTotal);

                        }


                        $('#qrreader').val("");
                        $('#qrreader').focus();

                        if (valIns) {
                            alertify.success("Insert Success. ");
                        } else {
                            alertify.error("Insert Error.");
                        }

                        counter++;

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });

//                    var xhrCK = ""; //CHECK CARTON DUPLICATE
//                    xhrCK = $.ajax({
//                        url: "/TWC_RCMS/RCM200Get",
//                        type: "GET",
//                        data: {mode: "ckCARTONdupPal", wh: whs, sp: sps, dt: dts, cn: cns, ctnid: pallet},
//                        async: false
//                    }).responseJSON;

//                    if (xhrCK.ckRES === true) {
//                        $('#ErrorCartonDupModal').modal("show");
//                        $('#qrreader').val("");
//                        $('#qrreader').focus();
//                    } else {






//                                var t = $('#scanTable').DataTable();
//                                t.row.add([
//                                    counter,
////                                    counter + "<input name='ckbox' class='checkdata' type='checkbox'>",
//                                    pallet,
//                                    totpackage
//                                ]).draw(false);

                    //insert RCMDPT

//                    $.ajax({// Total
//                        url: "/TWC_RCMS/RCM200Get",
//                        data: {mode: "insertRCMBPTPal", wh: whs, sp: sps, dt: dts, cn: cns, seq: counter, ctnid: pallet, totpc: totpackage, uid: user},
//                        async: false
//                    }).done(function (result) {
//
//                        console.log(result.ccInsert);
//
//                    }).fail(function (jqXHR, textStatus, errorThrown) {
//                        // needs to implement if it fails
//                    });

//                                getTable(whs, sps, cns, dts);

//                                var setter = setInterval(function () {
                    $('#scanTable').DataTable().ajax.reload();
//                                    clearInterval(setter);
//                                }, 1000);

//                    alertify.success('Scan Success');


//                    }

                }

            }

        }

    });

    $('.rmall').click(function () {
        $('#cfDEL').modal("show");
//                    $('#exampleModalCenter').modal("show");
    });

    $('#rcto').click(function () {
        var ttcnt = $('#totalcnt').text();
        var cnttt = $('#cntTotal').val();

        if (cnttt === "") {
            cnttt = "0";
        }

        if (ttcnt !== "0") {
//            if (ttcnt === cnttt) {

            var table = $('#scanTable').DataTable();
            var ckUP;

            //reset bpd rcqty
            $.ajax({
                url: "/TWC_RCMS/RCM200Get", //Update BPD (RCQTY)
                data: {mode: "setResetAllRCQty", wh: whs, sp: sps, dt: dts, cn: cns},
                async: false
            }).done(function (result) {

                console.log("reset : " + result.upRES);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });

            table.rows().data().map((row) => {
                console.log(row.RCTSCSEQ + " " + row.RCTCTID + " " + row.RCTCODE + " " + row.RCTPACK);

                $.ajax({
                    url: "/TWC_RCMS/RCM200Get", //Update BPD (RCQTY)
                    data: {mode: "findRCMBPTbyCARTON", wh: whs, sp: sps, dt: dts, cn: cns, ctnid: row.RCTCTID, ctcode: row.RCTCODE},
                    async: false
                }).done(function (result) {

                    console.log(result.upRES);

                    ckUP = result.upRES;

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            });

            if (ckUP) {

//                $.ajax({// Total
//                    url: "/TWC_RCMS/RCM200Get",
//                    data: {mode: "ckSTSafter", wh: whs, sp: sps, dt: dts, cn: cns},
//                    async: false
//                }).done(function (result) {
//                    console.log(result.ckSTS);
//                    $('#rcto').hide();
//                }).fail(function (jqXHR, textStatus, errorThrown) {
//                    // needs to implement if it fails
//                });
//
//                        if (result.ckSTS) {
//                            //alert Failed
//                            $('#ckSTSModal').modal('show');
//                        } else {
                alertify.success('Update Success');
//                    $('#conto').hide();
//                        }
//

            } else {
                $('#ckSTSModalNotAll').modal('show');
            }

//                            $('#scanTable').DataTable

//                            $.ajax({// Total
//                                url: "/TWC_RCMS/RCM200Get",
//                                data: {mode: "findRCMBPTbyCARTON", wh: whs, sp: sps, dt: dts, cn: cns, ctnid: pallet},
//                                async: false
//                            }).done(function (result) {
//
//                                console.log(result.upRES);
//
//                            }).fail(function (jqXHR, textStatus, errorThrown) {
//                                // needs to implement if it fails
//                            });

            //updateSTSnQTY

//            } else {
//                $('#ErrorQTYNotEQModal').modal("show");
////                            alertify.error('ปริมาณที่นับไม่เท่ากับปริมาณที่ส่ง');
//            }
        } else {
            $('#ErrorQTYZeroModal').modal("show");
        }

    });

//    $('#conto').on('click', function () {
//        console.log("change sts");
//
//        var ttcnt = $('#totalcnt').text();
//        var cnttt = $('#cntTotal').val();
//
//        if (cnttt === "") {
//            cnttt = "0";
//        }
//
//        if (ttcnt === cnttt) {
//
//            var table = $('#scanTable').DataTable();
//            var ckUP;
//
//            table.rows().data().map((row) => {
////                console.log(row.RCTSCSEQ + " " + row.RCTCTID + " " + row.RCTCODE + " " + row.RCTPACK);
//
//                $.ajax({
//                    url: "/TWC_RCMS/RCM200Get", //Update BPD (RCQTY)
//                    data: {mode: "findRCMBPTbyCARTONUpdateSTS", wh: whs, sp: sps, dt: dts, cn: cns, ctnid: row.RCTCTID, ctcode: row.RCTCODE},
//                    async: false
//                }).done(function (result) {
//
//                    console.log(result.upRES);
//
//                    ckUP = result.upRES;
//
//                }).fail(function (jqXHR, textStatus, errorThrown) {
//                    // needs to implement if it fails
//                });
//
//            });
//
//        }
//    });

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getNameSupCusFix(no) {

    let namemerge = "";

    if (no === "211808") {
        namemerge = no + " : บจก. จี เทค แมททีเรียล";
    } else if (no === "211809") {
        namemerge = no + " : บมจ. ไทยวาโก้";
    } else if (no === "211813") {
        namemerge = no + " : บจก. วาโก้ลำพูน";
    } else if (no === "211821") {
        namemerge = no + " : บจก. ภัทยาอุตสาหกิจ (PMC2)";
    } else if (no === "211823") {
        namemerge = no + " : บจก. มอร์แกน เดอ ทัว(ประเทศไทย)";
    }

    return namemerge;
}

function getTable(whs, sps, cns, dts) {

    console.log("getTable");

    $.ajax({// Warehouse Name
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "findMaxSeq", wh: whs, sp: sps, cn: cns, dt: dts},
        async: false
    }).done(function (result) {

        $('#cntTotal').val(result.RCMBPTList[0].RCTPACK);

//                    console.log(result.RCMBPTList[0].RCTSCSEQ);

        if (result.RCMBPTList[0].RCTSCSEQ === null) {
            counter = 1;
        } else {
            counter = result.RCMBPTList[0].RCTSCSEQ;
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    var tt2 = $('#scanTable').DataTable({
        ajax: {
            url: "/TWC_RCMS/RCM200Get",
            data: {mode: "findRCMBPT", wh: whs, sp: sps, cn: cns, dt: dts},
            dataSrc: 'RCMBPTList'
        },
        "order": [[0, 'desc']],
        "columns": [
            {"data": "RCTSCSEQ"},
            {"data": "RCTCTID"},
//                        {"data": "RCTPACK"},
            {'mRender': function (data, type, full) {

//                    if (full.RCTCTM === "NShow") {
//                        $('.rmall').hide();
//                        $('#rcto').hide();
//                    }

                    if (full.RCTUSER >= 2) { //Hidden Sts >= 2
                        return full.RCTPACK + ' ' + '<a style="cursor: pointer; color: red;" class="rmrow" id="BinSign' + full.RCTCTID.toString().replace("/", "") + '" onclick="rmvRow(\'' + full.RCTCTID + '\')" hidden><i class="glyphicon glyphicon-trash"></i></a>';
                    } else {
                        return full.RCTPACK + ' ' + '<a style="cursor: pointer; color: red;" class="rmrow" id="BinSign' + full.RCTCTID.toString().replace("/", "") + '" onclick="rmvRow(\'' + full.RCTCTID + '\')" ><i class="glyphicon glyphicon-trash"></i></a>';
                    }
//                                return '<a style="cursor: pointer; color: red;" class="rmrow" onclick="rmvRow(\'' + full.RCTCTID + '\')" data-value="1" ><i class="glyphicon glyphicon-trash"></i></a>';
                }
            }
        ],
        "columnDefs": [
            {"width": "1%", "targets": 0},
            {"width": "1%", "targets": 1},
            {"width": "1%", "targets": 2, "className": "text-center", "orderable": false}
//                        {"width": "1%", "targets": 3, "orderable": false}
        ],
        "bPaginate": false,
        "drawCallback": function (settings) {
//            console.log(settings);

//             $('#BinSignGTW').hide();

//            $.ajax({// Warehouse Name
//                url: "/TWC_RCMS/RCM200Get",
//                data: {mode: "ckSTSDetailForBin", wh: whs, sp: sps, cn: cns, dt: dts},
//                async: false
//            }).done(function (result) {
//
//                if (result.ckSTS) {
//                    $('.rmall').hide();
//                    $('.rmrow').hide();
//                } else {
//                    $('.rmall').show();
//                    $('.rmrow').show();
//                }
//
//            }).fail(function (jqXHR, textStatus, errorThrown) {
//                // needs to implement if it fails
//            });

//            var cntTotal = $('#cntTotal').val();
//            var totalcnt = $('#totalcnt').text();

//            if (cntTotal === totalcnt) {
//                $('#conto').show();
//            } else {
//                $('#conto').hide();
//            }

//            $.ajax({// Warehouse Name
//                url: "/TWC_RCMS/RCM200Get",
//                data: {mode: "ckSTSDetailForConfirmBtn", wh: whs, sp: sps, cn: cns, dt: dts},
//                async: false
//            }).done(function (result) {
//
//                if (result.ckSTS) {
//                    $('#conto').hide();
//                } else {
//                    $('#conto').show();
//                }
//
//            }).fail(function (jqXHR, textStatus, errorThrown) {
//                // needs to implement if it fails
//            });

        }

//        }
    });

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "ckSTSDetailForConfirmBtn", wh: whs, sp: sps, cn: cns, dt: dts},
        async: false
    }).done(function (result) {

        if (result.ckSTS) {
            $('.rmall').show();
            $('#rcto').show();
        } else {
            $('.rmall').hide();
            $('#rcto').hide();
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}

function rmvRow(something) {
    $('#cfDELrow').modal("show");
    $('#rmrowid').text(something);
}

function DELALL() {
    var whs = getUrlVars()["wh"];
    var sps = getUrlVars()["sp"];
    var dts = getUrlVars()["dt"];
    var cns = getUrlVars()["cn"];

    $.ajax({// Warehouse Name
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "delALLROW", wh: whs, sp: sps, cn: cns, dt: dts},
        async: false
    }).done(function (result) {

        if (result.delSTS === false) {
            $('#ErrorNotFoundForDelModal').modal("show");
        } else {
            alertify.success('Delete Success');
            $('#scanTable').DataTable().ajax.reload();
            $('#cntTotal').val("0");
            counter = 1;
//            $('#conto').show();
            $('#qrreader').focus();
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });
}

function DELrow() {
    var whs = getUrlVars()["wh"];
    var sps = getUrlVars()["sp"];
    var dts = getUrlVars()["dt"];
    var cns = getUrlVars()["cn"];

    let rid = $('#rmrowid').text();

    $.ajax({// Warehouse Name
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "deloneROW", wh: whs, sp: sps, cn: cns, dt: dts, rid: rid},
        async: false
    }).done(function (result) {

        if (result.delSTS === false) {
            $('#ErrorNotFoundForDelModal').modal("show");
        } else {
            alertify.success('Delete Success');
            $('#scanTable').DataTable().ajax.reload();
            counter -= 1;

            $('#cntTotal').val(result.retdel.map.summer);
            $('#qrreader').focus();

        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });
}