/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

//                sessionStorage.setItem('uid', '90309');

    var today = new Date();
//                $('#datesend').val('2021-10-20');
    $('#datesend').val(today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2));

    var uu = sessionStorage.getItem('uid');
//                uu = "90309";

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWH", uid: uu},
        async: false
    }).done(function (result) {

        for (var i = 0; i < result.whList.length; i++) {
            $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" ' + result.whList[i].Select + ' >' + result.whList[i].QWHNAME + '</option>');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    //-- Default P/O
    let whli = $('#WHlist').val();
    let spli = $('#SUPlist').val();
    let culi = $('#CUSlist').val();
    let date = $('#datesend').val();

    setDisableOKbtn(whli, spli, culi, date);

    $.ajax({
        url: "/TWC_RCMS/RCM800Get",
        data: {mode: "getPOlist", wh: whli, sp: spli, cn: culi, dt: date},
        async: false
    }).done(function (result) {

        $('#pofromData').empty();

        if (result.polist.length > 0) {
            var MaxLen = result.polist.length;
            for (var i = 0; i < result.polist.length; i++) {
                $("#pofromData").append('<li>' + MaxLen + " : " + result.polist[i].RCDPONO.split(" ")[0] + '</li>');
                MaxLen--;
            }
        } else {
            console.log("Not Found P/O No.");
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });
    //-- Default P/O ---- END

    //--Warehouse Change
    $('#WHlist').on('change', function () {
        let whli = $('#WHlist').val();
        let spli = $('#SUPlist').val();
        let culi = $('#CUSlist').val();
        let date = $('#datesend').val();

        setDisableOKbtn(whli, spli, culi, date);

        $.ajax({
            url: "/TWC_RCMS/RCM800Get",
            data: {mode: "getPOlist", wh: whli, sp: spli, cn: culi, dt: date},
            async: false
        }).done(function (result) {

            $('#pofromData').empty();

            if (result.polist.length > 0) {
                var MaxLen = result.polist.length;
                for (var i = 0; i < result.polist.length; i++) {
                    $("#pofromData").append('<li>' + MaxLen + " : " + result.polist[i].RCDPONO.split(" ")[0] + '</li>');
                    MaxLen--;
                }
            } else {
                $('#modalNotFound').modal('show');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });
    });
    //--Warehouse Change ---- END

    //-- Supplier Change
    $('#SUPlist').on('change', function () {
        let whli = $('#WHlist').val();
        let spli = $('#SUPlist').val();
        let culi = $('#CUSlist').val();
        let date = $('#datesend').val();

        setDisableOKbtn(whli, spli, culi, date);

        $.ajax({
            url: "/TWC_RCMS/RCM800Get",
            data: {mode: "getPOlist", wh: whli, sp: spli, cn: culi, dt: date},
            async: false
        }).done(function (result) {

            $('#pofromData').empty();

            if (result.polist.length > 0) {
                var MaxLen = result.polist.length;
                for (var i = 0; i < result.polist.length; i++) {
                    $("#pofromData").append('<li>' + MaxLen + " : " + result.polist[i].RCDPONO.split(" ")[0] + '</li>');
                    MaxLen--;
                }
            } else {
                $('#modalNotFound').modal('show');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });
    });
    //-- Supplier Change ---- END

    //-- Customer Change
    $('#CUSlist').on('change', function () {
        let whli = $('#WHlist').val();
        let spli = $('#SUPlist').val();
        let culi = $('#CUSlist').val();
        let date = $('#datesend').val();

        setDisableOKbtn(whli, spli, culi, date);

        $.ajax({
            url: "/TWC_RCMS/RCM800Get",
            data: {mode: "getPOlist", wh: whli, sp: spli, cn: culi, dt: date},
            async: false
        }).done(function (result) {

            $('#pofromData').empty();

            if (result.polist.length > 0) {
                var MaxLen = result.polist.length;
                for (var i = 0; i < result.polist.length; i++) {
                    $("#pofromData").append('<li>' + MaxLen + " : " + result.polist[i].RCDPONO.split(" ")[0] + '</li>');
                    MaxLen--;
                }
            } else {
                $('#modalNotFound').modal('show');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });
    });
    //-- Customer Change ---- END

    //-- Date Change
    $('#datesend').on('change', function () {
        let whli = $('#WHlist').val();
        let spli = $('#SUPlist').val();
        let culi = $('#CUSlist').val();
        let date = $('#datesend').val();

        setDisableOKbtn(whli, spli, culi, date);

        $.ajax({
            url: "/TWC_RCMS/RCM800Get",
            data: {mode: "getPOlist", wh: whli, sp: spli, cn: culi, dt: date},
            async: false
        }).done(function (result) {

            $('#pofromData').empty();

            if (result.polist.length > 0) {
                var MaxLen = result.polist.length;
                for (var i = 0; i < result.polist.length; i++) {
                    $("#pofromData").append('<li>' + MaxLen + " : " + result.polist[i].RCDPONO.split(" ")[0] + '</li>');
                    MaxLen--;
                }
            } else {
                $('#modalNotFound').modal('show');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });
    });
    //-- Date Change ---- END

    $('#okbtn').on('click', function () {

        var uid = sessionStorage.getItem('uid');
//                    uid = "90309";

        var listpo = [];

        $('#pofromData li').each(function () {
            let texter = $(this).text();
            listpo.push(texter.split(" : ")[1]);
        });

        if (listpo.length > 0) {

            //check Duplicate
            let whli = $('#WHlist').val();
            let spli = $('#SUPlist').val();
            let culi = $('#CUSlist').val();
            let date = $('#datesend').val();

            $.ajax({
                url: "/TWC_RCMS/RCM800Get",
                data: {mode: "CKDUPpoInPOH", wh: whli, sp: spli, cn: culi, dt: date},
                async: false
            }).done(function (result) {

                if (result.ckpo) { //Have PO in POH Make Sure For Download Again
                    $('#modapPODup').modal('show');
                } else { //Not Have PO in POH Downloading
                    console.log("first");
                    var myJsonString = JSON.stringify(listpo);

                    $.ajax({
                        url: "/TWC_RCMS/RCM800Get",
                        data: {mode: "getSAPVal", poli: myJsonString, uid: uid, date: date},
                        async: false
                    }).done(function (result) {

                        if (result.sap) {
                            $('#modalDwnSuccess').modal('show');
                        } else {
                            $('#modalDwnFailed').modal('show');
                        }

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // needs to implement if it fails
                    });
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });

        } else {
            $('#modalCanNotDwn').modal('show');
        }

    });

});

function dwnAgain() {

    console.log("Again");

    var uid = sessionStorage.getItem('uid');

    let whli = $('#WHlist').val();
    let spli = $('#SUPlist').val();
    let culi = $('#CUSlist').val();
    let date = $('#datesend').val();

    $.ajax({//delete po Dup in POH and POD before insert again
        url: "/TWC_RCMS/RCM800Get",
        data: {mode: "delPOinPOHnPOD", wh: whli, sp: spli, cn: culi, dt: date},
        async: false
    }).done(function (result) {

        if (result.delpo) {
            var listpo = [];

            $('#pofromData li').each(function () {
                let texter = $(this).text();
                listpo.push(texter.split(" : ")[1]);
            });

            if (listpo.length > 0) {

                var myJsonString = JSON.stringify(listpo);

                $.ajax({
                    url: "/TWC_RCMS/RCM800Get",
                    data: {mode: "getSAPVal", poli: myJsonString, uid: uid, date: date},
                    async: false
                }).done(function (result) {

                    if (result.sap) {
                        $('#modalDwnSuccess').modal('show');
                    } else {
                        $('#modalDwnFailed').modal('show');
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // needs to implement if it fails
                });

            } else {
                $('#modalCanNotDwn').modal('show');
            }
        } else {
            console.log("del Failed");
        }

    });
}

function setDisableOKbtn(wh, sp, cn, dt) {

    $.ajax({
        url: "/TWC_RCMS/RCM800Get",
        data: {mode: "CKStatus2", wh: wh, sp: sp, cn: cn, dt: dt},
        async: false
    }).done(function (result) {

        if (result.sts >= 2) {
            $('#okbtn').prop('disabled', true);
            $('#modalAlertSts2').modal('show');
        } else {
            $('#okbtn').prop('disabled', false);
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

}
