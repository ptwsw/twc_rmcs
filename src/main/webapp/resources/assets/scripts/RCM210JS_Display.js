/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

//                sessionStorage.setItem("uid","90309");

    $('#user').val(sessionStorage.getItem('uid'));

    var today = new Date();
    $('#datesend').val(today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2));

    var uu = sessionStorage.getItem('uid');

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getWH", uid: uu},
        async: false
    }).done(function (result) {

        for (var i = 0; i < result.whList.length; i++) {
            $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" ' + result.whList[i].Select + ' >' + result.whList[i].QWHNAME + '</option>');
//                        $('#WHlist').append('<option value="' + result.whList[i].QWHCOD + '" >' + result.whList[i].QWHCOD + ' : ' + result.whList[i].QWHNAME + '</option>');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });

    let wh = $('#WHlist').val();
    let sp = $('#SUPlist').val();
    let dt = $('#datesend').val().replace(/-/g, "");
    let cn = $('#CUSlist').val();
    let inv = $('#invnolist').val();
    let po = $('#ponolist').val();
    let mt = $('#matList').val();

    let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
    $('#goto').attr('href', link);

    $('#WHlist').change(function () {

        getINVList();

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);

    });

    $('#SUPlist').change(function () {

        getINVList();

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);

    });

    $('#CUSlist').change(function () {

        getINVList();

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);

    });

    $('#datesend').change(function () {

        getINVList();

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);

    });

    $('#invnolist').change(function () {
        getPOList();

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);
    });

    $('#ponolist').change(function () {
        getMATList();

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);
    });

    $('#matList').change(function () {

        let wh = $('#WHlist').val();
        let sp = $('#SUPlist').val();
        let dt = $('#datesend').val().replace(/-/g, "");
        let cn = $('#CUSlist').val();
        let inv = $('#invnolist').val();
        let po = $('#ponolist').val();
        let mt = $('#matList').val();

        let link = "Scan?wh=" + wh + "&sp=" + sp + "&dt=" + dt + "&cn=" + cn + "&inv=" + inv + "&po=" + po + "&mt=" + mt;
        $('#goto').attr('href', link);

    });


});


function getINVList() {

    let wh = $('#WHlist').val();
    let sp = $('#SUPlist').val();
    let dt = $('#datesend').val().replace(/-/g, "");
    let cn = $('#CUSlist').val();

    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getAllINV", wh: wh, sp: sp, dt: dt, cn: cn},
        async: false
    }).done(function (result) {

        if (result.invList.length > 0) {

            $('#invnolist').empty();

            for (var i = 0; i < result.invList.length; i++) {
                $('#invnolist').append('<option value="' + result.invList[i].RCDINVNO + '" >' + result.invList[i].RCDINVNO + '</option>');
            }

            getPOList();

        } else {
            alertify.error("Error Not Found Invoice.");
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });
}

function getPOList() {
    let wh = $('#WHlist').val();
    let sp = $('#SUPlist').val();
    let dt = $('#datesend').val().replace(/-/g, "");
    let cn = $('#CUSlist').val();
    let inv = $('#invnolist').val();

    //Get_All_PONO
    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getAllPONO", wh: wh, sp: sp, dt: dt, cn: cn, inv: inv},
        async: false
    }).done(function (result) {

        if (result.ponoList.length > 0) {

            $('#ponolist').empty();

            for (var i = 0; i < result.ponoList.length; i++) {
                $('#ponolist').append('<option value="' + result.ponoList[i].RCDPONO + '" >' + result.ponoList[i].RCDPONO + '</option>');
            }

            getMATList();

        } else {
            alertify.error("Error Not Found Invoice.");
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });
}

function getMATList() {
    let wh = $('#WHlist').val();
    let sp = $('#SUPlist').val();
    let dt = $('#datesend').val().replace(/-/g, "");
    let cn = $('#CUSlist').val();
    let inv = $('#invnolist').val();
    let po = $('#ponolist').val();

    //Get_All_MAT
    $.ajax({
        url: "/TWC_RCMS/RCM200Get",
        data: {mode: "getAllMATC", wh: wh, sp: sp, dt: dt, cn: cn, inv: inv, po: po},
        async: false
    }).done(function (result) {

        if (result.matList.length > 0) {

            $('#matList').empty();

            for (var i = 0; i < result.matList.length; i++) {
                $('#matList').append('<option value="' + result.matList[i].RCDCUSMAT + '" >' + result.matList[i].RCDCUSMAT + '</option>');
            }
        } else {
            alertify.error("Error Not Found Invoice.");
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        // needs to implement if it fails
    });
}
