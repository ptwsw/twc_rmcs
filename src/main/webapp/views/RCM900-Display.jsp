<%-- 
    Document   : RCM900-Display
    Created on : Jan 20, 2022, 2:43:58 PM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RCMS</title>

        <jsp:include page = "../fragments/css.jsp" />

        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THSarabunNew/thsarabunnew.css">

        <jsp:include page = "../fragments/script.jsp" />

        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <link rel="stylesheet" href="../resources/assets/styles/alertify.bootstrap.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.core.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.default.css">

        <script src="../resources/assets/scripts/alertify.js"></script>
        <script src="../resources/assets/scripts/alertify.min.js"></script>

        <script src="../resources/assets/scripts/RCM900JS.js"></script>

        <style>
            .inp[type=submit]{
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                border: none;
                border-radius: 4px;
            }
        </style>

    </head>
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">

                <br>

                <form action="../resources/manual/RCM900.pdf" width="100%" target="blank">
                    <table align="right">
                        <tr>
                            <td width="95%"></td>
                            <td width="5%">
                                <input type="submit" value="Manual" class="inp">
                            </td>
                        </tr>
                    </table>
                </form>

                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">
                    <b class="page-header" style="padding-left:5px;font-size:15px;">
                        <div class="row">
                            <div class="col-xs-5" align="left">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">Warehouse : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-5 col-xs-5 col-md-5 col-lg-5" align="left">
                                    <select class="form-control" name="WHlist" id="WHlist"></select>
                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"></div>
                            </div>

                            <div class="col-xs-2" align="left"></div>

                            <div class="col-xs-3" align="left">
                                <!--<div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="left"></div>-->
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                    <label style="margin-top:5px;">วันที่ส่ง : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-8 col-xs-8 col-md-8 col-lg-8" align="left">
                                    <input class="form-control" type="date" name="datesend" id="datesend" >
                                </div>
                            </div>

                            <div class="col-xs-2" align="left">
                                <!--<div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>-->

                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="center">
                                    <a id="enterbtn" style="height: 100%; width: 100%;">
                                        <input type="button" class="btn btn-info" style="width:100px;" value="Enter">
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>

                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="center">
                                    <a id="backbtn" style="height: 100%; width: 100%;">
                                        <input type="button" class="btn btn-danger" style="width:100px;" value="Back">
                                    </a>
                                </div>

                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-5" align="right">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">Supplier no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <select class="form-control" name="SUPlist" id="SUPlist">
                                        <option value="211808">211808 : บจก. จี เทค แมททีเรียล</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"></div>
                            </div>
                            <div class="col-xs-1" align="right"></div>
                            <div class="col-xs-2" align="right"></div>
                            <div class="col-xs-4" align="center"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-1" align="right" style="width:1%;"></div>
                            <div class="col-sm-10" align="right" style="width:98%;">

                                <table id="rcm900Table" class="display" style="width: 100%; border: 1px solid rgb(204, 204, 204); border-radius: 4px;">
                                    <thead>
                                        <tr> 
                                            <th></th>
                                            <th>วันที่รับข้อมูล</th>
                                            <th>
                                                <a style="width: 70%; padding: auto 5px auto 5px;" class="btn btn-primary" id="exp" name="exp" onclick="expand()">
                                                    <i class="fa fa-expand" style="font-size:12px;"></i> Expand All
                                                </a>
                                                <a style=" width: 70%; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress()">
                                                    <i class="fa fa-compress" style="font-size:12px;"></i> Collapse All
                                                </a>
                                            </th>
                                            <th>เลขที่</th>
                                            <th>CusNo</th>
                                            <th>Plant</th>
                                            <th>Invoice</th>
                                            <th>P/O</th>
                                            <th>Package</th>
                                            <th>Amount</th>
                                            <th>SKU</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>
                            <div class="col-sm-1" align="right" style="width:1%;"></div>

                        </div>

                    </b>
                </div>
            </div>

        </div>
    </body>
</html>
