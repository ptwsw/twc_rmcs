<%-- 
    Document   : XMS710-Display
    Created on : Dec 23, 2021, 3:09:56 PM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RCMS</title>

        <jsp:include page = "../fragments/css.jsp" />

        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THSarabunNew/thsarabunnew.css">

        <jsp:include page = "../fragments/script.jsp" />

        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <link rel="stylesheet" href="../resources/assets/styles/alertify.bootstrap.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.core.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.default.css">

        <script src="../resources/assets/scripts/alertify.js"></script>
        <script src="../resources/assets/scripts/alertify.min.js"></script>

        <style>
            .tooltip-inner{
                white-space:nowrap;
                max-width:none;
            }
            input[type="date"]::-webkit-calendar-picker-indicator {
                width: 15px;
                padding: 0px;
                margin: 0px;
            }
        </style>

        <!--<script src="../resources/assets/scripts/RCM100JS.js" async></script>-->

        <script>

            $(document).ready(function () {
                $('body').tooltip({selector: '[data-toggle="tooltip"]'}); //show tooltip in datatable
            });

            function calDiffDay() {
                var duedate = $('#duedate').val();
                var tradate = $('#tradate').val();
                var permis = true;

                if (duedate === "") {
                    alertify.error("Please Choose Due Date");
                    permis = false;
                }

                if (tradate === "") {
                    alertify.error("Please Choose Transport Date");
                    permis = false;
                }

                if (permis) {

                    var DUDate = new Date(duedate);
                    var TPDate = new Date(tradate);

                    var millisBetween = DUDate.getTime() - TPDate.getTime();
                    var days = millisBetween / (1000 * 3600 * 24);

                    $('#diffDay').val(Math.round(Math.abs(days)));

                    alertify.success("Calculate Success");
                }

            }
        </script>

    </head>
    <body>
        <div id="wrapper">
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">
                    <b class="page-header" style="padding-left:5px;font-size:15px;">
                        <div class="row">
                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">Due Date</div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">Transport Date</div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left"></div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <input type="date" name="duedate" id="duedate" class="form-control">
                                </div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <input type="date" name="tradate" id="tradate" class="form-control">
                                </div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">=</div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <input type="text" name="diffDay" id="diffDay" class="form-control" readonly>
                                </div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right">
                                    <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title data-original-title="Diff Of (DueDate To January 1, 1970 And TransDate To January 1, 1970)" style="color:white;" ></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>
                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left"></div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>

                            <div class="col-xs-6" align="left">
                                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" align="center">
                                    <input type="button" name="btncal" id="btncal" style="width:200px;" value="Calculate" onclick="calDiffDay()" class="btn btn-success">
                                </div>
                            </div>

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left"></div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>
                        </div>

                    </b>
                    <br>
                </div>
            </div>
        </div>
    </body>
</html>
