<%-- 
    Document   : RCM210-Display
    Created on : Jun 13, 2022, 2:49:11 PM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <meta charset="UTF-8">
        <title>RCMS</title>

        <jsp:include page = "../fragments/css.jsp" />

        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THSarabunNew/thsarabunnew.css">

        <jsp:include page = "../fragments/script.jsp" />

        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <link rel="stylesheet" href="../resources/assets/styles/alertify.bootstrap.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.core.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.default.css">

        <script src="../resources/assets/scripts/alertify.js"></script>
        <script src="../resources/assets/scripts/alertify.min.js"></script>

        <script src="../resources/assets/scripts/RCM210JS_Display.js"></script>

        <style>
            #WHlist,#CUSlist,#SUPlist,#datesend,#invnolist,#ponolist,#matList{
                border: 2px solid black;
                border-radius: 5px;
                height: 100%;
                font-size: 30px;
                color: black;
            }
            .inp[type=submit]{
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                border: none;
                border-radius: 4px;
            }
        </style>

    </head>

    <body>

        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <!-- ------------ ############################### -------------- -->

            <div class="container-fluid">

                <br>

                <form action="../resources/manual/RCM210.pdf" width="100%" target="blank">
                    <table align="right">
                        <tr>
                            <td width="95%"></td>
                            <td width="5%">
                                <input type="submit" value="Manual" class="inp">
                            </td>
                        </tr>
                    </table>
                </form>

                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">

                    <input name="user" id="user" type="text" hidden>

                    <b class="page-header" style="padding-left:5px;font-size:35px;">
                        <div class="row">

                            <div class="col-xs-12" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">Warehouse : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <select name="WHlist" id="WHlist"></select>
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right"></div>
                            </div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-12" align="right">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">Supplier no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <select name="SUPlist" id="SUPlist">
                                        <option value="211808">211808 : บจก. จี เทค แมททีเรียล</option>
                                    </select>
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-12" align="right">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">Customer no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <select name="CUSlist" id="CUSlist" style="width:100%;">
                                        <option value="211809">211809 : บมจ. ไทยวาโก้</option>
                                        <option value="211813">211813 : บจก. วาโก้ลำพูน</option>
                                        <option value="211821">211821 : บจก. ภัทยาอุตสาหกิจ (PMC2)</option>
                                        <option value="211823">211823 : บจก. มอร์แกน เดอ ทัว(ประเทศไทย)</option>
                                    </select>
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-12" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">วันที่ส่ง : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                    <input type="date" name="datesend" id="datesend" >
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left"></div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left"> 
                                    <a id="goto" style="height: 100%; width: 100%;">
                                        <input type="button" value="Enter">
                                    </a>
                                </div>
                            </div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-12" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">Invoice No. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <select style="width: 80%;" name="invnolist" id="invnolist" ></select>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"></div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left"></div>
                            </div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-12" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">PO No. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <select style="width: 80%;" name="ponolist" id="ponolist" ></select>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"></div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left"></div>
                            </div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-12" align="left">
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                    <label style="margin-top:5px;">Mat Code. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <select style="width: 80%;" name="matList" id="matList" ></select>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"></div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left"></div>
                            </div>

                        </div>

                    </b>

                    <br>

                </div>
            </div>
        </div>

    </body>
</html>
