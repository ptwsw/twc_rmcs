<%-- 
    Document   : RCM210-Scan
    Created on : Jun 14, 2022, 4:15:33 PM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RCMS</title>

        <jsp:include page = "../fragments/css.jsp" />

        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THSarabunNew/thsarabunnew.css">

        <jsp:include page = "../fragments/script.jsp" />

        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <link rel="stylesheet" href="../resources/assets/styles/alertify.bootstrap.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.core.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.default.css">

        <script src="../resources/assets/scripts/alertify.js"></script>
        <script src="../resources/assets/scripts/alertify.min.js"></script>

        <script src="../resources/assets/scripts/RCM210JS_Scan.js"></script>

        <style>

            .tooltip-inner{
                white-space:nowrap;
                max-width:none;
            }

            #whs,#sps,#dts,#cns,#inv,#pono,#matc{
                border: 0;
                box-shadow: none;
                font-size: 30px;
                color: black;
            }

            #cntTotal{
                font-size: 30px;
                height: 2em;
                width: 80%;
            }

            hr{
                width:90%;
                background-color: #226097;
                border: 1px solid #226097;
            }

            #totalcnt{
                font-size: 40px;
            }

            .modal {
                width: 100%;
                margin: 0 auto; 
            }

            .modal-content {
                /*width: 650px;*/
                /*height: 160px;*/
                position: absolute;
                left: 50%;
                top: 50%; 
                margin-left: -220px;        
                margin-top:150px;
            }

        </style>

    </head>
    <body onload="$('#user').val(sessionStorage.getItem('uid'))">

        <div id="wrapper">

            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">

                <!-- ------------ ############################### -------------- -->

                <b class="page-header" style="padding-left:5px;font-size:35px;">

                    <input id="user" name="user" hidden>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">Warehouse : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                <label id="whs" name="whs"></label>
                            </div>
                            <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right"></div>
                        </div>

                    </div>

                    <div class="row" style="height:10px;"></div>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">Supplier no. : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                <label id="sps" name="sps"></label>
                            </div>
                            <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right"></div>
                        </div>
                    </div>

                    <div class="row" style="height:10px;"></div>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">Customer no. : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                <label id="cns" name="cns"></label>
                            </div>
                            <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right"></div>
                        </div>

                    </div>

                    <div class="row" style="height:10px;"></div>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">วันที่ส่ง : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                <label id="dts" name="dts"></label>
                                <input id="saveDateHead" hidden>
                            </div>
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="right"></div>
                            <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                        </div>

                    </div>

                    <div class="row" style="height:10px;"></div>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">Invoice No. : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                <label id="inv" name="inv"></label>
                            </div>
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="right"></div>
                            <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                        </div>

                    </div>

                    <div class="row" style="height:10px;"></div>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">PO No. : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                <label id="pono" name="pono"></label>
                            </div>
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="right"></div>
                            <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                        </div>

                    </div>

                    <div class="row" style="height:10px;"></div>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right">
                                <label style="margin-top:5px;">Mat Code. : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                <label id="matc" name="matc"></label>
                            </div>
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="right">
                                Total : &nbsp; <label id="totalcnt" name="totalcnt"></label>
                            </div>
                            <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                        </div>

                    </div>

                    <hr>

                    <div class="row">

                        <div class="col-xs-12" align="right">
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="right">
                                <label style="margin-top:5px;">QR Code : &nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                <input id="qrreader" name="qrreader" style="width:100%;">
                            </div>
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="right">
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2"></div>
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4">
                                    <!--<a id="conto" ><input style="width:100%; height: 2em; font-size: 30px;" type="button" value="Confirm" class="btn btn-success"></a>-->
                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4"><a id="backto" href="Display"><input style="width:100%; height: 2em; font-size: 30px;" type="button" value="Back" class="btn btn-info"></a></div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-1" align="right" style="width:5%;"></div>
                        <div class="col-sm-10" align="right" style="width:90%;">

                            <table id="scanTable" class="display" style="width: 100%; border: 1px solid rgb(204, 204, 204); border-radius: 4px;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>ID</th>
                                        <th>BOX</th>
                                        <td>QTY</td>
                                        <td><a style="cursor: pointer; color: red;" class="rmall" ><i class="glyphicon glyphicon-trash"></i></a></td>
                                        <!--<th><a style="cursor: pointer; color: red;" class="rmall" ><i class="glyphicon glyphicon-trash"></i></a></th>-->
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                        <div class="col-sm-1" align="right" style="width:5%;"></div>
                    </div>

                    <div class="modal fade" id="cfReject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content" style="width:530px; height: 155px;">
                                <div class="modal-header">

                                    <div class="row">
                                        <h4 class="modal-title" style="font-size:25px;" align="center">Do you Reject ?</h4>
                                    </div>

                                    <div class="row" style="height:18px;"></div>

                                    <div class="row">
                                        <div class="col-sm-1" align="right" style="width:30%;"></div>
                                        <div class="col-sm-1" align="right" style="width:20%;">
                                            <button style="width:90px;" name="Cancel" type="button" class="form-control btn btn-danger" onclick="$('#qrreader').focus();" data-dismiss="modal">
                                                <font color = "white">Cancel</font>
                                            </button>
                                        </div>
                                        <div class="col-sm-1" align="right" style="width:20%;">
                                            <button style="width:90px;" name="ok" type="button" class="form-control btn btn-success" onclick="recjectByID()" data-dismiss="modal">
                                                <font color = "white">OK</font>
                                            </button>
                                        </div>
                                        <div class="col-sm-1" align="right" style="width:30%;"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <center>
                        <div id="ErrorCusNotMatchModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content" style="width:420px; height: 145px;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Error Customer Number Not Match.</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                    <!-- Modal -->
                    <center>
                        <div id="ErrorDeliDNotMatchModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content" style="width:420px; height: 145px;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Error Delivery Date in QRCode Not Match Date in Header.</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                    <!-- Modal -->
                    <center>
                        <div id="ErrorFormatNotGTechModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content" style="width:420px; height: 145px;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Error Not Found Format (Length > 40 and Not G-Tech.).</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center>
                        <div id="ErrorRejectFailedModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content" style="width:420px; height: 145px;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Error Reject Failed.</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="ErrorNotFoundForCKModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 25px;">Error Not Found Data.</h4>
                                        </div>

                                        <div class="modal-body">
                                            <div class="row" style="width:400px;">
                                                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" align="center"><label style="font-size: 18px;" id="nfTextCk"></label></div>
                                            </div>

                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px; width: 150px;" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="ErrorIsRejectedModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 25px;">Already Rejected.</h4>
                                        </div>

                                        <div class="modal-body">
                                            <div class="row" style="width:400px;">
                                                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" align="center"><label style="font-size: 18px;" id="rjTextCk"></label></div>
                                            </div>

                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px; width: 150px;" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="ErrorNotCheckModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="width:400px;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 25px;">Not Found Checked ID.</h4>
                                        </div>

                                        <!--                                        <div class="modal-body">
                                                                                    <div class="row" style="width:400px;">
                                                                                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" align="center"><label style="font-size: 18px;" id="rjTextCk"></label></div>
                                                                                    </div>
                                        
                                                                                </div>-->

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px; width: 150px;" data-dismiss="modal" class="btn btn-success" onclick="$('#qrreader').focus();" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                </b>

            </div>

        </div>

    </body>
</html>
