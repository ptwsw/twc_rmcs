<%-- 
    Document   : RCM110-Display
    Created on : Jun 10, 2022, 3:48:56 PM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RCMS</title>

        <jsp:include page = "../fragments/css.jsp" />

        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THSarabunNew/thsarabunnew.css">

        <jsp:include page = "../fragments/script.jsp" />

        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <link rel="stylesheet" href="../resources/assets/styles/alertify.bootstrap.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.core.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.default.css">

        <script src="../resources/assets/scripts/alertify.js"></script>
        <script src="../resources/assets/scripts/alertify.min.js"></script>

        <style>
            .tooltip-inner{
                white-space:nowrap;
                max-width:none;
            }
            input[type="date"]::-webkit-calendar-picker-indicator {
                width: 15px;
                padding: 0px;
                margin: 0px;
            }
            .inp[type=submit]{
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                border: none;
                border-radius: 4px;
            }

            .dropdown {
                float: left;
                overflow: hidden;
            }

            .dropdown {
                cursor: pointer;
                font-size: 16px;
                border: none;
                outline: none;
                color: white;
                padding: 14px 16px;
                background-color: inherit;
                font-family: inherit;
                margin: 0;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }

            .dropdown-content a {
                float: none;
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
                text-align: left;
            }

            .dropdown-content a:hover {
                background-color: #ddd;
            }

            .show {
                display: block;
            }
        </style>

        <script src="../resources/assets/scripts/RCM110JS.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <!-- ------------ ############################### -------------- -->

            <div class="container-fluid">

                <br>

                <form action="../resources/manual/RCM110.pdf" width="100%" target="blank">
                    <table align="right">
                        <tr>
                            <td width="95%"></td>
                            <td width="5%">
                                <input type="submit" value="Manual" class="inp">
                            </td>
                        </tr>
                    </table>
                </form>

                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">

                    <input name="user" id="user" type="text" hidden>
                    <input name="HeadSTS" id="HeadSTS" type="text" hidden>

                    <b class="page-header" style="padding-left:5px;font-size:15px;">
                        <div class="row">

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-5 col-xs-5 col-md-5 col-lg-5" align="left">
                                    <label style="margin-top:5px;">Warehouse : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <select class="form-control" name="WHlist" id="WHlist"></select>
                                </div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>

                            <div class="col-xs-3" align="right">
                                <div class="col-sm-5 col-xs-5 col-md-5 col-lg-5" align="left">
                                    <label style="margin-top:5px;">Supplier no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-7 col-xs-7 col-md-7 col-lg-7" align="left">
                                    <select class="form-control" name="SUPlist" id="SUPlist">
                                        <option value="211808">211808 : บจก. จี เทค แมททีเรียล</option>
                                    </select>
                                </div>
                                <!--<div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>-->
                            </div>

                            <div class="col-xs-6" align="left">

                                <div class="col-xs-6" align="left">
                                    <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left">
                                        <label style="margin-top:5px;">วันที่ส่ง : &nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-sm-7 col-xs-7 col-md-7 col-lg-7" align="left">
                                        <input class="form-control" type="date" name="datesend" id="datesend" >
                                    </div>
                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="left"></div>
                                </div>
                                <div class="col-xs-6" align="left">
                                    <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                        <a id="enterbtn" style="height: 100%; width: 100%;">
                                            <input type="button" class="btn btn-info" style="width:100px;" value="Enter">
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                        <a id="confbtn" style="height: 100%; width: 100%;">
                                            <input type="button" class="btn btn-success" style="width:100px;" value="Confirm">
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                        <a id="backbtn" style="height: 100%; width: 100%;">
                                            <input type="button" class="btn btn-danger" style="width:100px;" value="Back">
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"> 

                                    </div>
                                </div>
                                <!--<div class="col-xs-1" align="left"></div>-->

                            </div>

                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">

                            <div class="col-xs-4" align="left">
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left">
                                    <label style="margin-top:5px;">Tot Inv : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left">
                                    <input name="showttINV" id="showttINV" class="form-control" style="width:45px; background-color: white; padding: 1px 1px 1px 1px; text-align: center;" readonly>
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left">
                                    <label style="margin-top:5px;">Tot PO : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left">
                                    <input name="showttPO" id="showttPO" class="form-control" style="width:45px; background-color: white; padding: 1px 1px 1px 1px; text-align: center;" readonly>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">Tot Pack : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="left">
                                    <input name="showttPACK" id="showttPACK" class="form-control" style="width:48px; background-color: white; padding: 1px 1px 1px 1px; text-align: center;" readonly>
                                </div>
                            </div>

                            <div class="col-xs-3" align="left">
                                <div class="col-sm-5 col-xs-5 col-md-5 col-lg-5" align="left">
                                    <label style="margin-top:5px;">Customer no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-7 col-xs-7 col-md-7 col-lg-7" align="left">
                                    <select name="CUSlist" id="CUSlist" class="form-control">
                                        <option value="211809">211809 : บมจ. ไทยวาโก้</option>
                                        <option value="211813">211813 : บจก. วาโก้ลำพูน</option>
                                        <option value="211821">211821 : บจก. ภัทยาอุตสาหกิจ (PMC2)</option>
                                        <option value="211823">211823 : บจก. มอร์แกน เดอ ทัว(ประเทศไทย)</option>
                                    </select>
                                </div>
                                <!--<div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>-->
                            </div>

                            <div class="col-xs-5" align="left">
                                <div class="col-xs-6" align="left">
                                    <div class="col-sm-5 col-xs-5 col-md-5 col-lg-5" align="left">
                                        <label style="margin-top:5px;">User : &nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-sm-7 col-xs-7 col-md-7 col-lg-7" align="left">
                                        <input class="form-control" name="useriden" id="useriden" style="background-color: white;" readonly>
                                    </div>
                                    <!--<div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="left"></div>-->
                                </div>
                                <div class="col-xs-6" align="left">
                                    <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="left"></div>
                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left">
                                        <a style="cursor: pointer; font-size: 30px;" id="printfunc2" target="blank" class="printPaper">
                                            <i class="fa fa-print" style="color:red;"></i>
                                        </a>
                                    </div>

                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="right">
                                        <a style="cursor: pointer; font-size: 30px;" id="printfunc" target="blank" class="printPaper">
                                            <i class="fa fa-print"></i>
                                        </a>
                                    </div>

                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left">
                                        <a id="cancbtn" style="height: 100%; width: 100%;" data-toggle="tooltip" title="ยกเลิกการรับสินค้า">
                                            <input type="button" class="btn btn-danger" style="width:100px;" value="Cancel">
                                        </a>
                                    </div>

                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="left"></div>
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-1" align="right" style="width:1%;"></div>
                            <div class="col-sm-10" align="right" style="width:98%;">

                                <table id="rcm100Table" class="display" style="width: 100%; border: 1px solid rgb(204, 204, 204); border-radius: 4px;">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Invoice no.</th>
                                            <th>P/O Customer</th>
                                            <th>L3</th>
                                            <th>Material Code</th>
                                            <th>
                                                <a style="width: 120px;" class="btn btn-primary" id="exp" name="exp" onclick="expand()">
                                                    <i class="fa fa-expand" style="font-size:12px;"></i> Expand All
                                                </a>
                                                <a style=" width: 120px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress()">
                                                    <i class="fa fa-compress" style="font-size:12px;"></i> Collapse All
                                                </a>
                                            </th>
                                            <th>Posting<br>Date</th>
                                            <th>P/O<br>Line</th>
                                            <th>Price<br>Status</th>
                                            <th>Invoice<br>Quantity</th>
                                            <th>QTY.<br>Status</th>
                                            <th style="color:black;">Received<br>Quantity</th>
                                            <th>Due dt<br>Status</th>
                                            <th>U/M<br>SUP.</th>
                                            <th>U/M<br>STD.</th>
                                            <th>QTY.<br>PACK</th>
                                            <th>PACK<br>TYPE</th>
                                            <th>Plant</th>
                                            <th>Total<br>Amount &nbsp; <input name="ckall" id="ckall" value="ckall" type="checkbox" style="width:20px; height:20px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>
                            <div class="col-sm-1" align="right" style="width:1%;"></div>

                            <input id="hideJob" hidden>
                            <input id="hideInv" hidden>
                            <input id="hidePo" hidden>
                            <input id="hidePoline" hidden>
                            <input id="hideMat" hidden>

                        </div>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modal2Way" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Edit Invoice No. Or Reject This Invoice ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-4" align="center" ><button id="ChangeInv" style="background-color: yellow; height: 55px; font-size: 25px;" class="form-control">Change</button></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-4" align="center" ><button id="RejectInv" style="background-color: red; height: 55px; font-size: 25px;" class="form-control">Reject</button></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalEditInv" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Edit Invoice No. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <!--PO : <label id="modal_po"></label> | Line : <label id="modal_poline"></label>-->

                                                <!--<input id="hideJob" hidden>-->
                                                <!--<input id="hideInv" hidden>-->

                                                <div class="row">
                                                    <div class="col-sm-1" align="right" ></div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">Invoice(OLD) : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_invold" class="form-control">
                                                    </div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">Invoice(NEW) : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_invnew" class="form-control">
                                                    </div>
                                                    <div class="col-sm-1" align="right" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confEditInv()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalEditPo" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Edit PO No. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <!--PO : <label id="modal_po"></label> | Line : <label id="modal_poline"></label>-->

                                                <!--<input id="hideJob" hidden>-->
                                                <!--<input id="hideInv" hidden>-->

                                                <div class="row">
                                                    <div class="col-sm-1" align="right" ></div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">PO No. (OLD) : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_ponoold" class="form-control">
                                                    </div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">PO No. (NEW) : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_pononew" class="form-control">
                                                    </div>
                                                    <div class="col-sm-1" align="right" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confEditPo()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalRJectMat" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do You Reject This Material Code. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-3" align="center" ><label style="font-size:25px;">Mat Code : </label></div>
                                                    <div class="col-sm-2" align="center" >
                                                        <label id="modal_matshowRJ" style="font-size:25px;" ></label>
                                                    </div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confRJMat()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalCLRJectMat" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do You Clear Reject This Material Code. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-3" align="center" ><label style="font-size:25px;">Mat Code : </label></div>
                                                    <div class="col-sm-2" align="center" >
                                                        <label id="modal_matshowCLRJ" style="font-size:25px;" ></label>
                                                    </div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confCLRJMat()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalRJectInv" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do You Reject This Invoice No. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ><label style="font-size:25px;">Invoice : </label></div>
                                                    <div class="col-sm-2" align="center" >
                                                        <label id="modal_invshowRJ" style="font-size:25px;" ></label>
                                                    </div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confRJInv()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalCLRJectInv" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do You Clear Reject This Invoice No. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ><label style="font-size:25px;">Invoice : </label></div>
                                                    <div class="col-sm-2" align="center" >
                                                        <label id="modal_invshowCLRJ" style="font-size:25px;" ></label>
                                                    </div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confCLRJInv()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalYLStar" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content" style="width:1000px;">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do You Update This New MatCode To RCM200 ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-2" align="center" ><label style="font-size:16px;">Mat Code (NEW) : </label></div>
                                                    <div class="col-sm-3" align="center" >
                                                        <input id="modal_matcodePNew" class="form-control">
                                                    </div>
                                                    <div class="col-sm-1" align="center" ><label style="font-size: 20px;">=></label></div>
                                                    <div class="col-sm-2" align="center" ><label style="font-size:16px;">Mat Code (OLD) : </label></div>
                                                    <div class="col-sm-3" align="center" >
                                                        <input id="modal_matcoderef" class="form-control"> 
                                                    </div>
                                                    <div class="col-sm-1" align="right" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confYLStarMat()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalPriInv" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do You Set Price Invalid This Invoice No. ?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-1" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ><label style="font-size:25px;">Invoice : </label></div>
                                                    <div class="col-sm-2" align="center" >
                                                        <label id="modal_invshowPRI" style="font-size:25px;" ></label>
                                                    </div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-2" align="center" ></div>
                                                    <div class="col-sm-1" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confPriInv()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalEditMat" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Edit Material Code?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <!--PO : <label id="modal_po"></label> | Line : <label id="modal_poline"></label>-->

                                                <!--<input id="hideJob" hidden>-->
                                                <!--<input id="hideInv" hidden>-->
                                                <!--<input id="hidePo" hidden>-->
                                                <!--<input id="hidePoline" hidden>-->

                                                <div class="row">
                                                    <div class="col-sm-1" align="right" ></div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">Cus Mat : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_cusmat" class="form-control">
                                                    </div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">Mat : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_matcd" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-sm-1" align="right" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confEditMat()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalEditPOline" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Edit PO Line?</h4>
                                            </div>

                                            <div class="modal-body">
                                                <!--PO : <label id="modal_po"></label> | Line : <label id="modal_poline"></label>-->

                                                <!--<input id="hideJob" hidden>-->
                                                <!--<input id="hideInv" hidden>-->
                                                <!--<input id="hidePo" hidden>-->
                                                <!--<input id="hidePoline" hidden>-->

                                                <div class="row">
                                                    <div class="col-sm-1" align="right" ></div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">NEW PO Line : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_newpoline" class="form-control">
                                                    </div>
                                                    <div class="col-sm-2" align="right" ><label style="font-size:16px;">OLD PO Line : </label></div>
                                                    <div class="col-sm-3" align="right" >
                                                        <input id="modal_oldpoline" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-sm-1" align="right" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confEditPOLine()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalCanCel" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Do you cancel this invoice ?</h4>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="canFunc()" value="Confirm">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalExplain" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content" >
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;"></h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="left"><label id="R1" style="font-weight: normal;" >ปริมาณที่สั่งซื้อทั้งหมด(ตามPO.) + OVER %</label></div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="left">
                                                        <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"><label id="R21" style="font-weight: normal;">100</label></div>
                                                        <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"><label style="font-weight: normal;">+</label></div>
                                                        <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"><label id="R22" style="font-weight: normal;">5%</label></div>
                                                    </div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="center"><label style="font-weight: normal;">ปริมาณที่รับได้ = <label id="R3" style="font-weight: bold; color: blue;">105</label> ST</label></div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="left"><label id="R4" style="font-weight: normal;">Check : ปริมาณที่เคยรับ(Partial Ship) + ปริมาณที่ส่งครั้งนี้</label></div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="left">
                                                        <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"><label id="R51" style="font-weight: normal;">70</label></div>
                                                        <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"><label style="font-weight: normal;">+</label></div>
                                                        <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4" align="right"><label id="R52" style="font-weight: normal;">37</label></div>
                                                    </div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="center"><label style="font-weight: normal;">ปริมาณรวมทั้งหมดที่GT.ส่ง ณ ปัจจุบัน = <label id="R6" style="font-weight: bold; color: red;">107</label> ST</label></div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2" align="center"></div>
                                                    <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="center"><label style="font-weight: normal;">Check :   <label id="R71" style="font-weight: bold; color: red;">107</label> ST ≤ <label id="R72" style="font-weight: bold; color: blue;">105</label> ST (<label id="R73" style="font-weight: bold;">รับไม่ได้</label>)</label></div>
                                                    <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="center"></div>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="text-align : center;">
                                                <input type="button" style="font-size: 18px; width: 150px;" data-dismiss="modal" class="btn btn-success" value="OK">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalAlertPlsFillNewInv" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Error Please Fill New Invoice.</h4>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="OK">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalAlertNotFMatRef" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Error Not Found Material Code In Reference Field.</h4>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="OK" onclick="clearYLStar()">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                        <!-- Modal -->
                        <center style="font-size: 23px;">
                            <div id="modalSelPostDate" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" style="font-size: 30px;">Posting Date</h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-4" align="center" ></div>
                                                    <div class="col-sm-4" align="center" >
                                                        <select id="pdlist" style="font-size:20px;" class="form-control">
                                                            <option selected></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4" align="center" ></div>
                                                </div>
                                            </div>

                                            <div class="modal-footer" style="text-align : center">
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Close">
                                                &nbsp;
                                                <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="confPrintReport()" value="Print">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>

                    </b>

                </div>
            </div>
        </div>
    </body>
</html>
