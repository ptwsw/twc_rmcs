<%-- 
    Document   : RCM800-Dwn
    Created on : Nov 30, 2021, 10:42:31 AM
    Author     : 93176
--%>

<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RCMS</title>

        <jsp:include page = "../fragments/css.jsp" />

        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">
        <link rel="stylesheet" href="../resources/font/THSarabunNew/thsarabunnew.css">

        <jsp:include page = "../fragments/script.jsp" />

        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>

        <link rel="stylesheet" href="../resources/assets/styles/alertify.bootstrap.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.core.css">
        <link rel="stylesheet" href="../resources/assets/styles/alertify.default.css">

        <script src="../resources/assets/scripts/alertify.js"></script>
        <script src="../resources/assets/scripts/alertify.min.js"></script>

        <script src="../resources/assets/scripts/RCM800JS.js"></script>

        <style>
            .inp[type=submit]{
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                border: none;
                border-radius: 4px;
            }

            li {
                display: list-item;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">

                <br>

                <form action="../resources/manual/RCM800.pdf" width="100%" target="blank">
                    <table align="right">
                        <tr>
                            <td width="95%"></td>
                            <td width="5%">
                                <input type="submit" value="Manual" class="inp">
                            </td>
                        </tr>
                    </table>
                </form>

                <div class="col-lg-12" style="width: 100%; border:2px solid #ccc; margin-top: 5px; border-radius: 4px;">

                    <b class="page-header" style="padding-left:5px;font-size:15px;">
                        <div class="row">
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">Warehouse : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <select class="form-control" name="WHlist" id="WHlist"></select>
                                </div>
                                <div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">Supplier no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="left">
                                    <select class="form-control" name="SUPlist" id="SUPlist">
                                        <option value="211808">211808 : บจก. จี เทค แมททีเรียล</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">Customer no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-9 col-xs-9 col-md-9 col-lg-9" align="left">
                                    <select name="CUSlist" id="CUSlist" class="form-control">
                                        <option value="211809">211809 : บมจ. ไทยวาโก้</option>
                                        <option value="211813">211813 : บจก. วาโก้ลำพูน</option>
                                        <option value="211821">211821 : บจก. ภัทยาอุตสาหกิจ (PMC2)</option>
                                        <option value="211823">211823 : บจก. มอร์แกน เดอ ทัว(ประเทศไทย)</option>
                                    </select>
                                </div>
                                <!--<div class="col-sm-1 col-xs-1 col-md-1 col-lg-1" align="right"></div>-->
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row"> 
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">วันที่ส่ง : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <input class="form-control" type="date" name="datesend" id="datesend" >
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"></div>
                            </div>
                            <div class="col-xs-1" align="left"><button id="okbtn" class="form-control">OK</button></div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                        <div class="row">
                            <div class="col-xs-1" align="left"></div>
                            <div class="col-xs-5" align="left">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left">
                                    <label style="margin-top:5px;">P/O no. : &nbsp;&nbsp;</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" align="left">
                                    <ul class="form-control" style="height: 250px; list-style-type:none;" name="pofromData" id="pofromData"></ul>
                                </div>
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3" align="left"></div>
                            </div>
                        </div>

                        <div class="row" style="height:10px;"></div>

                    </b>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modapPODup" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">You have ever downloaded. Did you download it again?</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-danger" value="Cancel">
                                            &nbsp;
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" onclick="dwnAgain()" value="Confirm">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalDwnFailed" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Download Failed.</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalDwnSuccess" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Download Success.</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalNotFound" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Not Found P/O No.</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                    <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalCanNotDwn" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Can Not Download Please Check P/O no. Again</h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                    
                     <!-- Modal -->
                    <center style="font-size: 23px;">
                        <div id="modalAlertSts2" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style="font-size: 30px;">Ststus >= 2 , Can Not Download. </h4>
                                        </div>

                                        <div class="modal-footer" style="text-align : center">
                                            <input type="button" style="font-size: 18px;" data-dismiss="modal" class="btn btn-success" value="OK">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>

                </div>
            </div>

        </div>
    </body>
</html>
