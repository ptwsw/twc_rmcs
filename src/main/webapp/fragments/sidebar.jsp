<div id="sidebar-wrapper">  
    <div class="navbar-default sidebar" role="navigation" style="margin: 0">
        <ul class="nav navbar-default" id="side-menu">       
            <li>
                <h4 class="head-menu">
                    <table  style="width:100%;">
                        <tr>
                            <td align="center">Warehouse Management</td>
                        </tr>
                    </table>
                </h4>
            </li>
            <li>
                <a href="#"><i class="fa fa-institution fa-fw"></i> Stock RM <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/WMS/WMS100/RE"><i class="fa fa-cubes fa-fw"></i> RM Receipt</a></li>
                    <li><a href="/WMS2/WMS100/M"><i class="fa fa-cubes fa-fw"></i> MOLD Receipt</a></li>
                    <li><a href="/WMS/WMS001/DP"><i class="fa fa-sort-numeric-asc fa-fw"></i> Display on hand</a></li>
                    <li><a href="/WMS/WMS001Q/EQTY"><i class="fa fa-sort-numeric-asc fa-fw"></i> Change QTY</a></li>
                    <li><a href="/WMS/WMS007/SPRM"><i class="fa fa-cut fa-fw"></i> Separate RM</a></li>
                    <li><a href="/WMS2/WMS110/MOVE"><i class="fa fa-share-square-o fa-fw"></i> Location Movement</a></li>
                    <li><a href="/WMS/WMS600/PRINT"><i class="fa fa-print fa-fw"></i> QR Code Print</a></li>
                    <li><a href="/WMS2/WMS930/DP"><i class="fa fa-bar-chart fa-fw"></i> Summary Stock Checking</a></li>
                    <li><a href="/WMS2/WMS920/DP"><i class="fa fa-files-o fa-fw"></i> Onhand Comparison (SAP:WMS)</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-cogs fa-fw"></i> Configuration <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/TABLE/WMS002/display"><i class="fa fa-cog fa-fw"></i> Warehouse Master</a></li>
                    <li><a href="/TABLE/WMS003/display"><i class="fa fa-cog fa-fw"></i> Warehouse Structure</a></li>
                    <li><a href="/TABLE/WMS006/display"><i class="fa fa-cog fa-fw"></i> RM Reason</a></li>
                    <li><a href="/TABLE/WMS165/display"><i class="fa fa-cog fa-fw"></i> Number Series</a></li>
                    <li><a href="/TABLE2/WMS004/display"><i class="fa fa-cog fa-fw"></i> Material Control Master</a></li>
                    <li><a href="/TABLE2/WMS010/display"><i class="fa fa-cog fa-fw"></i> Destination Master</a></li>
                    <li><a href="/TABLE2/WMS009/login"><i class="fa fa-cog fa-fw"></i> User Authorization</a></li>
                    <li><a href="/TABLE2/WMS011/display"><i class="fa fa-cog fa-fw"></i> Program Master</a></li>
                    <li><a href="/TABLE2/WMS070/display"><i class="fa fa-cog fa-fw"></i> Material History Transaction</a></li>
                    <li><a href="/TABLE2/WMS008/display"><i class="fa fa-cog fa-fw"></i> Rack Master</a></li>
                </ul>
            </li><!--START RCMS-->
            <li id="RCM" >
                <h4 class="head-menu">
                    <table  style="width:100%;">
                        <tr>
                            <td align="center">Receive Management System </td>
                        </tr>
                    </table>
                </h4>
            </li>
            <!--RCMS :: Main Process-->
            <li id="RCM_R1">
                <a href="#"><i class="fa fa-institution fa-fw"></i> Main Process<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li id="RCM800"><a href="/TWC_RCMS/RCM800/Dwn"><i class="fa fa-cog fa-fw"></i> Download P/O Data from SAP</a></li>
                    <li id="RCM810"><a href="/TWC_RCMS/RCM810/Display"><i class="fa fa-cog fa-fw"></i> Billing & PO. Deletion</a></li>
                    <li id="RCM100"><a href="/TWC_RCMS/RCM100/Display"><i class="fa fa-cog fa-fw"></i> Billing & PO. Comparison</a></li>
                    <li id="RCM110"><a href="/TWC_RCMS/RCM110/Display"><i class="fa fa-cog fa-fw"></i> Receive RM. Confirm</a></li>
                    <li id="RCM200"><a href="/TWC_RCMS/RCM200/Display"><i class="fa fa-cog fa-fw"></i> Receive RM. Count</a></li>
                    <li id="RCM210"><a href="/TWC_RCMS/RCM210/Display"><i class="fa fa-cog fa-fw"></i> Reject RM. Count</a></li>
                    <li id="RCM900"><a href="/TWC_RCMS/RCM900/Display"><i class="fa fa-cog fa-fw"></i> Summary Supplier Invoice</a></li>
                    <hr class="new1">
                    <li id="RCM800T"><a href="/TWC_RCMST/RCM800/Dwn"><i class="fa fa-cog fa-fw"></i> (T) Download P/O Data from SAP</a></li>
                    <li id="RCM810T"><a href="/TWC_RCMST/RCM810/Display"><i class="fa fa-cog fa-fw"></i> (T) Billing & PO. Deletion</a></li>
                    <li id="RCM100T"><a href="/TWC_RCMST/RCM100/Display"><i class="fa fa-cog fa-fw"></i> (T) Billing & PO. Comparison</a></li>
                    <li id="RCM110T"><a href="/TWC_RCMST/RCM110/Display"><i class="fa fa-cog fa-fw"></i> (T) Receive RM. Confirm</a></li>
                    <li id="RCM200T"><a href="/TWC_RCMST/RCM200/Display"><i class="fa fa-cog fa-fw"></i> (T) Receive RM. Count</a></li>
                    <li id="RCM210T"><a href="/TWC_RCMST/RCM210/Display"><i class="fa fa-cog fa-fw"></i> (T) Reject RM. Count</a></li>
                    <li id="RCM900T"><a href="/TWC_RCMST/RCM900/Display"><i class="fa fa-cog fa-fw"></i> (T) Summary Supplier Invoice</a></li>
                </ul>
            </li>
            
            <li id="PGM" >
                <h4 class="head-menu">
                    <table  style="width:100%;">
                        <tr>
                            <td align="center">Pallet & Group Management System </td>
                        </tr>
                    </table>
                </h4>
            </li>
            <!--RCMS :: Main Process-->
            <li id="PGM_R1">
                <a href="#"><i class="fa fa-institution fa-fw"></i> Main Process<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li id="PGM100"><a href="/TWC_PGMS/PGM100/Display"><i class="fa fa-cog fa-fw"></i> Pallet & Group RM. Create</a></li>
                    <li id="PGM101"><a href="/TWC_PGMS/PGM101/Display"><i class="fa fa-cog fa-fw"></i> Pallet & Group RM. Edit</a></li>
                    <li id="PGM200"><a href="/TWC_PGMS/PGM200/Display"><i class="fa fa-cog fa-fw"></i> Pallet & Group RM. Count</a></li>
                    <li id="PGM300"><a href="/TWC_PGMS/PGM300/Display"><i class="fa fa-cog fa-fw"></i> Summary Stock Checking. Display</a></li>
                </ul>
            </li>

        </ul>    
    </div>
</div>